package com.bean;

public class LpiLpiBean {
	
	private int lpi_cd;  
	private String lpi_id;               
	private String lpi_nm;               
	private int lpi_typ;  
	private String lpi_mndt;            
	private String lpi_addrss;      
	private int lpi_rgn;          
	private int lpi_prvnc;            
	private int lpi_mncplty;        
	private String lpi_tlphn_nmbr;      
	private String lpi_email;           
	private String lpi_fx_nmbr;          
	private String lpi_wbst;              
	private String lpi_usr_nm;           
	private String lpi_usr_psswrd;       
	private String lpi_prmry_email;     
	private String lpi_scndry_email;      
	private String lpi_crdntr_nm;         
	private String lpi_crdntr_pstn;      
	private String lpi_crdntr_tlphn_nmbr; 
	private String lpi_crdntr_dprtmnt;    
	private String lpi_crdntr_mble_nmbr;
	private int lpi_cmp_id;
	
	
	
	
	
	public int getLpi_cmp_id() {
		return lpi_cmp_id;
	}
	public void setLpi_cmp_id(int lpi_cmp_id) {
		this.lpi_cmp_id = lpi_cmp_id;
	}
	public int getLpi_cd() {
		return lpi_cd;
	}
	public void setLpi_cd(int lpi_cd) {
		this.lpi_cd = lpi_cd;
	}
	public String getLpi_id() {
		return lpi_id;
	}
	public void setLpi_id(String lpi_id) {
		this.lpi_id = lpi_id;
	}
	public String getLpi_nm() {
		return lpi_nm;
	}
	public void setLpi_nm(String lpi_nm) {
		this.lpi_nm = lpi_nm;
	}
	public int getLpi_typ() {
		return lpi_typ;
	}
	public void setLpi_typ(int lpi_typ) {
		this.lpi_typ = lpi_typ;
	}
	public String getLpi_mndt() {
		return lpi_mndt;
	}
	public void setLpi_mndt(String lpi_mndt) {
		this.lpi_mndt = lpi_mndt;
	}
	public String getLpi_addrss() {
		return lpi_addrss;
	}
	public void setLpi_addrss(String lpi_addrss) {
		this.lpi_addrss = lpi_addrss;
	}
	public int getLpi_rgn() {
		return lpi_rgn;
	}
	public void setLpi_rgn(int lpi_rgn) {
		this.lpi_rgn = lpi_rgn;
	}
	public int getLpi_prvnc() {
		return lpi_prvnc;
	}
	public void setLpi_prvnc(int lpi_prvnc) {
		this.lpi_prvnc = lpi_prvnc;
	}
	public int getLpi_mncplty() {
		return lpi_mncplty;
	}
	public void setLpi_mncplty(int lpi_mncplty) {
		this.lpi_mncplty = lpi_mncplty;
	}
	public String getLpi_tlphn_nmbr() {
		return lpi_tlphn_nmbr;
	}
	public void setLpi_tlphn_nmbr(String lpi_tlphn_nmbr) {
		this.lpi_tlphn_nmbr = lpi_tlphn_nmbr;
	}
	public String getLpi_email() {
		return lpi_email;
	}
	public void setLpi_email(String lpi_email) {
		this.lpi_email = lpi_email;
	}
	public String getLpi_fx_nmbr() {
		return lpi_fx_nmbr;
	}
	public void setLpi_fx_nmbr(String lpi_fx_nmbr) {
		this.lpi_fx_nmbr = lpi_fx_nmbr;
	}
	public String getLpi_wbst() {
		return lpi_wbst;
	}
	public void setLpi_wbst(String lpi_wbst) {
		this.lpi_wbst = lpi_wbst;
	}
	public String getLpi_usr_nm() {
		return lpi_usr_nm;
	}
	public void setLpi_usr_nm(String lpi_usr_nm) {
		this.lpi_usr_nm = lpi_usr_nm;
	}
	public String getLpi_usr_psswrd() {
		return lpi_usr_psswrd;
	}
	public void setLpi_usr_psswrd(String lpi_usr_psswrd) {
		this.lpi_usr_psswrd = lpi_usr_psswrd;
	}
	public String getLpi_prmry_email() {
		return lpi_prmry_email;
	}
	public void setLpi_prmry_email(String lpi_prmry_email) {
		this.lpi_prmry_email = lpi_prmry_email;
	}
	public String getLpi_scndry_email() {
		return lpi_scndry_email;
	}
	public void setLpi_scndry_email(String lpi_scndry_email) {
		this.lpi_scndry_email = lpi_scndry_email;
	}
	public String getLpi_crdntr_nm() {
		return lpi_crdntr_nm;
	}
	public void setLpi_crdntr_nm(String lpi_crdntr_nm) {
		this.lpi_crdntr_nm = lpi_crdntr_nm;
	}
	public String getLpi_crdntr_pstn() {
		return lpi_crdntr_pstn;
	}
	public void setLpi_crdntr_pstn(String lpi_crdntr_pstn) {
		this.lpi_crdntr_pstn = lpi_crdntr_pstn;
	}
	public String getLpi_crdntr_tlphn_nmbr() {
		return lpi_crdntr_tlphn_nmbr;
	}
	public void setLpi_crdntr_tlphn_nmbr(String lpi_crdntr_tlphn_nmbr) {
		this.lpi_crdntr_tlphn_nmbr = lpi_crdntr_tlphn_nmbr;
	}
	public String getLpi_crdntr_dprtmnt() {
		return lpi_crdntr_dprtmnt;
	}
	public void setLpi_crdntr_dprtmnt(String lpi_crdntr_dprtmnt) {
		this.lpi_crdntr_dprtmnt = lpi_crdntr_dprtmnt;
	}
	public String getLpi_crdntr_mble_nmbr() {
		return lpi_crdntr_mble_nmbr;
	}
	public void setLpi_crdntr_mble_nmbr(String lpi_crdntr_mble_nmbr) {
		this.lpi_crdntr_mble_nmbr = lpi_crdntr_mble_nmbr;
	} 
	
	

}
