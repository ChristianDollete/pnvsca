package com.bean;

public class LpiProjectManagerBean {
	private int pmCd;
	private String pmId;
	private String pmName;
	private String sex;
	private String pmUserName;
	private String pmUserPassword;
	private String pmPosition;
	private String pmEmail;
	private String pmTelNo;
	private String pmMobNo;
	private String pmFaxNo;
	private int pm_cmpny_id;
	private int pm_usr_cd;
	

	public int getPm_usr_cd() {
		return pm_usr_cd;
	}

	public void setPm_usr_cd(int pm_usr_cd) {
		this.pm_usr_cd = pm_usr_cd;
	}

	public int getPmCd() {
		return pmCd;
	}

	public void setPmCd(int pmCd) {
		this.pmCd = pmCd;
	}

	public int getPm_cmpny_id() {
		return pm_cmpny_id;
	}

	public void setPm_cmpny_id(int pm_cmpny_id) {
		this.pm_cmpny_id = pm_cmpny_id;
	}

	public String getPmId() {
		return pmId;
	}

	public void setPmId(String pmId) {
		this.pmId = pmId;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPmUserName() {
		return pmUserName;
	}

	public void setPmUserName(String pmUserName) {
		this.pmUserName = pmUserName;
	}

	public String getPmUserPassword() {
		return pmUserPassword;
	}

	public void setPmUserPassword(String pmUserPassword) {
		this.pmUserPassword = pmUserPassword;
	}

	public String getPmPosition() {
		return pmPosition;
	}

	public void setPmPosition(String pmPosition) {
		this.pmPosition = pmPosition;
	}

	public String getPmEmail() {
		return pmEmail;
	}

	public void setPmEmail(String pmEmail) {
		this.pmEmail = pmEmail;
	}

	public String getPmTelNo() {
		return pmTelNo;
	}

	public void setPmTelNo(String pmTelNo) {
		this.pmTelNo = pmTelNo;
	}

	public String getPmMobNo() {
		return pmMobNo;
	}

	public void setPmMobNo(String pmMobNo) {
		this.pmMobNo = pmMobNo;
	}

	public String getPmFaxNo() {
		return pmFaxNo;
	}

	public void setPmFaxNo(String pmFaxNo) {
		this.pmFaxNo = pmFaxNo;
	}

}
