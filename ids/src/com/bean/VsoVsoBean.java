package com.bean;

public class VsoVsoBean {
	
    private int vso_cd; 
	private String vso_id; 
	private String vso_nm; 
	private int vso_org_typ; 
	private String vso_org_hd_nm;
	private String vso_org_hd_pstn; 
	private int vso_org_rgn;
	private int vso_org_prvnc;
	private int vso_org_mncplty;
	private String vso_org_addrss;
	private String vso_org_tlph_nmbr;
	private String vso_org_fx_nmbr;
	private String vso_org_email;
	private String vso_org_wbst;
	private String vso_sec_rgstrtn_nmbr;
	private String vso_hd_offc_addrss;
	private int vso_cntry;
	private String vso_dt_estblishd;
	private String vso_vsn;
	private String vso_mssn;
	private String vso_goal;
	private String vso_annl_bdgt;
	private String vso_bdgt_scrc;
	private int vso_rcrtment;
	private int vso_enggmnt;
	private int vso_cpblty_bldng;
	private int vso_prmtn;
	private int vso_ntwrkng_of_vlntrs;
	private int vso_othrs1;
	private String vso_othrs_nt1;
	
	private int vso_lvng;
	private int vso_meal;
	private int vso_insrnc;
	private int vso_rcgntn_or_awrd;
	private int vso_trvl_allwnc;
	private int vso_hsng;
	private int vso_trnng_and_ornttn;
	private int vso_othrs2;
	private String vso_othrs_nt2;
	
	
	
	private String vso_usr_nm;
	private String vso_psswrd;
	private String vso_prmry_email;
	private String vso_scndry_email;
	private String vso_crdntr_nm;
	private String vso_crdntr_pstn;
	private String vso_crdntr_dprtmnt;
	private String vso_crdntr_tlphn_nmbr;
	private String vso_crdntr_mbl_nmbr;
	private int vso_cmp_cd;
	
	
	
	
	
	
	public String getVso_othrs_nt1() {
		return vso_othrs_nt1;
	}
	public void setVso_othrs_nt1(String vso_othrs_nt1) {
		this.vso_othrs_nt1 = vso_othrs_nt1;
	}
	public String getVso_othrs_nt2() {
		return vso_othrs_nt2;
	}
	public void setVso_othrs_nt2(String vso_othrs_nt2) {
		this.vso_othrs_nt2 = vso_othrs_nt2;
	}
	public int getVso_cmp_cd() {
		return vso_cmp_cd;
	}
	public void setVso_cmp_cd(int vso_cmp_cd) {
		this.vso_cmp_cd = vso_cmp_cd;
	}
	public int getVso_cd() {
		return vso_cd;
	}
	public void setVso_cd(int vso_cd) {
		this.vso_cd = vso_cd;
	}
	public String getVso_id() {
		return vso_id;
	}
	public void setVso_id(String vso_id) {
		this.vso_id = vso_id;
	}
	public String getVso_nm() {
		return vso_nm;
	}
	public void setVso_nm(String vso_nm) {
		this.vso_nm = vso_nm;
	}
	public int getVso_org_typ() {
		return vso_org_typ;
	}
	public void setVso_org_typ(int vso_org_typ) {
		this.vso_org_typ = vso_org_typ;
	}
	public String getVso_org_hd_nm() {
		return vso_org_hd_nm;
	}
	public void setVso_org_hd_nm(String vso_org_hd_nm) {
		this.vso_org_hd_nm = vso_org_hd_nm;
	}
	public String getVso_org_hd_pstn() {
		return vso_org_hd_pstn;
	}
	public void setVso_org_hd_pstn(String vso_org_hd_pstn) {
		this.vso_org_hd_pstn = vso_org_hd_pstn;
	}
	public int getVso_org_rgn() {
		return vso_org_rgn;
	}
	public void setVso_org_rgn(int vso_org_rgn) {
		this.vso_org_rgn = vso_org_rgn;
	}
	public int getVso_org_prvnc() {
		return vso_org_prvnc;
	}
	public void setVso_org_prvnc(int vso_org_prvnc) {
		this.vso_org_prvnc = vso_org_prvnc;
	}
	public int getVso_org_mncplty() {
		return vso_org_mncplty;
	}
	public void setVso_org_mncplty(int vso_org_mncplty) {
		this.vso_org_mncplty = vso_org_mncplty;
	}
	public String getVso_org_addrss() {
		return vso_org_addrss;
	}
	public void setVso_org_addrss(String vso_org_addrss) {
		this.vso_org_addrss = vso_org_addrss;
	}
	public String getVso_org_tlph_nmbr() {
		return vso_org_tlph_nmbr;
	}
	public void setVso_org_tlph_nmbr(String vso_org_tlph_nmbr) {
		this.vso_org_tlph_nmbr = vso_org_tlph_nmbr;
	}
	public String getVso_org_fx_nmbr() {
		return vso_org_fx_nmbr;
	}
	public void setVso_org_fx_nmbr(String vso_org_fx_nmbr) {
		this.vso_org_fx_nmbr = vso_org_fx_nmbr;
	}
	public String getVso_org_email() {
		return vso_org_email;
	}
	public void setVso_org_email(String vso_org_email) {
		this.vso_org_email = vso_org_email;
	}
	public String getVso_org_wbst() {
		return vso_org_wbst;
	}
	public void setVso_org_wbst(String vso_org_wbst) {
		this.vso_org_wbst = vso_org_wbst;
	}
	public String getVso_sec_rgstrtn_nmbr() {
		return vso_sec_rgstrtn_nmbr;
	}
	public void setVso_sec_rgstrtn_nmbr(String vso_sec_rgstrtn_nmbr) {
		this.vso_sec_rgstrtn_nmbr = vso_sec_rgstrtn_nmbr;
	}
	public String getVso_hd_offc_addrss() {
		return vso_hd_offc_addrss;
	}
	public void setVso_hd_offc_addrss(String vso_hd_offc_addrss) {
		this.vso_hd_offc_addrss = vso_hd_offc_addrss;
	}
	public int getVso_cntry() {
		return vso_cntry;
	}
	public void setVso_cntry(int vso_cntry) {
		this.vso_cntry = vso_cntry;
	}
	public String getVso_dt_estblishd() {
		return vso_dt_estblishd;
	}
	public void setVso_dt_estblishd(String vso_dt_estblishd) {
		this.vso_dt_estblishd = vso_dt_estblishd;
	}
	public String getVso_vsn() {
		return vso_vsn;
	}
	public void setVso_vsn(String vso_vsn) {
		this.vso_vsn = vso_vsn;
	}
	
	public String getVso_mssn() {
		return vso_mssn;
	}
	public void setVso_mssn(String vso_mssn) {
		this.vso_mssn = vso_mssn;
	}
	public String getVso_goal() {
		return vso_goal;
	}
	
	public void setVso_goal(String vso_goal) {
		this.vso_goal = vso_goal;
	}
	public String getVso_annl_bdgt() {
		return vso_annl_bdgt;
	}
	public void setVso_annl_bdgt(String vso_annl_bdgt) {
		this.vso_annl_bdgt = vso_annl_bdgt;
	}
	public String getVso_bdgt_scrc() {
		return vso_bdgt_scrc;
	}
	public void setVso_bdgt_scrc(String vso_bdgt_scrc) {
		this.vso_bdgt_scrc = vso_bdgt_scrc;
	}
	public int getVso_rcrtment() {
		return vso_rcrtment;
	}
	public void setVso_rcrtment(int vso_rcrtment) {
		this.vso_rcrtment = vso_rcrtment;
	}
	public int getVso_enggmnt() {
		return vso_enggmnt;
	}
	public void setVso_enggmnt(int vso_enggmnt) {
		this.vso_enggmnt = vso_enggmnt;
	}
	public int getVso_cpblty_bldng() {
		return vso_cpblty_bldng;
	}
	public void setVso_cpblty_bldng(int vso_cpblty_bldng) {
		this.vso_cpblty_bldng = vso_cpblty_bldng;
	}
	public int getVso_prmtn() {
		return vso_prmtn;
	}
	public void setVso_prmtn(int vso_prmtn) {
		this.vso_prmtn = vso_prmtn;
	}
	public int getVso_ntwrkng_of_vlntrs() {
		return vso_ntwrkng_of_vlntrs;
	}
	public void setVso_ntwrkng_of_vlntrs(int vso_ntwrkng_of_vlntrs) {
		this.vso_ntwrkng_of_vlntrs = vso_ntwrkng_of_vlntrs;
	}
	public int getVso_othrs1() {
		return vso_othrs1;
	}
	public void setVso_othrs1(int vso_othrs1) {
		this.vso_othrs1 = vso_othrs1;
	}
	public int getVso_lvng() {
		return vso_lvng;
	}
	public void setVso_lvng(int vso_lvng) {
		this.vso_lvng = vso_lvng;
	}
	public int getVso_meal() {
		return vso_meal;
	}
	public void setVso_meal(int vso_meal) {
		this.vso_meal = vso_meal;
	}
	public int getVso_insrnc() {
		return vso_insrnc;
	}
	public void setVso_insrnc(int vso_insrnc) {
		this.vso_insrnc = vso_insrnc;
	}
	public int getVso_rcgntn_or_awrd() {
		return vso_rcgntn_or_awrd;
	}
	public void setVso_rcgntn_or_awrd(int vso_rcgntn_or_awrd) {
		this.vso_rcgntn_or_awrd = vso_rcgntn_or_awrd;
	}
	public int getVso_trvl_allwnc() {
		return vso_trvl_allwnc;
	}
	public void setVso_trvl_allwnc(int vso_trvl_allwnc) {
		this.vso_trvl_allwnc = vso_trvl_allwnc;
	}
	public int getVso_hsng() {
		return vso_hsng;
	}
	public void setVso_hsng(int vso_hsng) {
		this.vso_hsng = vso_hsng;
	}
	public int getVso_trnng_and_ornttn() {
		return vso_trnng_and_ornttn;
	}
	public void setVso_trnng_and_ornttn(int vso_trnng_and_ornttn) {
		this.vso_trnng_and_ornttn = vso_trnng_and_ornttn;
	}
	public int getVso_othrs2() {
		return vso_othrs2;
	}
	public void setVso_othrs2(int vso_othrs2) {
		this.vso_othrs2 = vso_othrs2;
	}
	public String getVso_usr_nm() {
		return vso_usr_nm;
	}
	public void setVso_usr_nm(String vso_usr_nm) {
		this.vso_usr_nm = vso_usr_nm;
	}
	public String getVso_psswrd() {
		return vso_psswrd;
	}
	public void setVso_psswrd(String vso_psswrd) {
		this.vso_psswrd = vso_psswrd;
	}
	public String getVso_prmry_email() {
		return vso_prmry_email;
	}
	public void setVso_prmry_email(String vso_prmry_email) {
		this.vso_prmry_email = vso_prmry_email;
	}
	public String getVso_scndry_email() {
		return vso_scndry_email;
	}
	public void setVso_scndry_email(String vso_scndry_email) {
		this.vso_scndry_email = vso_scndry_email;
	}
	public String getVso_crdntr_nm() {
		return vso_crdntr_nm;
	}
	public void setVso_crdntr_nm(String vso_crdntr_nm) {
		this.vso_crdntr_nm = vso_crdntr_nm;
	}
	public String getVso_crdntr_pstn() {
		return vso_crdntr_pstn;
	}
	public void setVso_crdntr_pstn(String vso_crdntr_pstn) {
		this.vso_crdntr_pstn = vso_crdntr_pstn;
	}
	public String getVso_crdntr_dprtmnt() {
		return vso_crdntr_dprtmnt;
	}
	public void setVso_crdntr_dprtmnt(String vso_crdntr_dprtmnt) {
		this.vso_crdntr_dprtmnt = vso_crdntr_dprtmnt;
	}
	public String getVso_crdntr_tlphn_nmbr() {
		return vso_crdntr_tlphn_nmbr;
	}
	public void setVso_crdntr_tlphn_nmbr(String vso_crdntr_tlphn_nmbr) {
		this.vso_crdntr_tlphn_nmbr = vso_crdntr_tlphn_nmbr;
	}
	public String getVso_crdntr_mbl_nmbr() {
		return vso_crdntr_mbl_nmbr;
	}
	public void setVso_crdntr_mbl_nmbr(String vso_crdntr_mbl_nmbr) {
		this.vso_crdntr_mbl_nmbr = vso_crdntr_mbl_nmbr;
	}
	
	
	
	
	
	

}
