package com.bean;

public class AdUserBean {

	private int usr_id;
	private String usr_usr_nm;
	private String usr_usr_psswrd;
	private String usr_usr_dscrptn;
	private String usr_usr_prmry_email;
	private String usr_usr_scndry_email;
	private int ad_usr_clsfctn;
	private int ad_cmpny;
	
	private String usr_email;
	private String usr_email_psswrd;
	
	public int getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(int usr_id) {
		this.usr_id = usr_id;
	}
	public String getUsr_usr_nm() {
		return usr_usr_nm;
	}
	public void setUsr_usr_nm(String usr_usr_nm) {
		this.usr_usr_nm = usr_usr_nm;
	}
	public String getUsr_usr_psswrd() {
		return usr_usr_psswrd;
	}
	public void setUsr_usr_psswrd(String usr_usr_psswrd) {
		this.usr_usr_psswrd = usr_usr_psswrd;
	}
	public String getUsr_usr_dscrptn() {
		return usr_usr_dscrptn;
	}
	public void setUsr_usr_dscrptn(String usr_usr_dscrptn) {
		this.usr_usr_dscrptn = usr_usr_dscrptn;
	}
	public String getUsr_usr_prmry_email() {
		return usr_usr_prmry_email;
	}
	public void setUsr_usr_prmry_email(String usr_usr_prmry_email) {
		this.usr_usr_prmry_email = usr_usr_prmry_email;
	}
	public String getUsr_usr_scndry_email() {
		return usr_usr_scndry_email;
	}
	public void setUsr_usr_scndry_email(String usr_usr_scndry_email) {
		this.usr_usr_scndry_email = usr_usr_scndry_email;
	}
	
	public int getAd_usr_clsfctn() {
		return ad_usr_clsfctn;
	}
	public void setAd_usr_clsfctn(int ad_usr_clsfctn) {
		// if 1 == pnvsca
		// if 2 == lpi profile
		// if 21 == lpi pm
		// if 3 == vso pofile
		// if 31 == vso vlntr
		
		this.ad_usr_clsfctn = ad_usr_clsfctn;
	}
	public int getAd_cmpny() {
		return ad_cmpny;
	}
	public void setAd_cmpny(int ad_cmpny) {
		this.ad_cmpny = ad_cmpny;
	}
	public String getUsr_email() {
		return usr_email;
	}
	public void setUsr_email(String usr_email) {
		this.usr_email = usr_email;
	}
	public String getUsr_email_psswrd() {
		return usr_email_psswrd;
	}
	public void setUsr_email_psswrd(String usr_email_psswrd) {
		this.usr_email_psswrd = usr_email_psswrd;
	}
	
	
	
	

}
