package com.bean;

public class AdAddRegionBean {

	private int rgn_id;
	private String rgn_nm;
	private String rgn_dscrptn;

	public int getRgn_id() {
		return rgn_id;
	}

	public void setRgn_id(int rgn_id) {
		this.rgn_id = rgn_id;
	}

	public String getRgn_nm() {
		return rgn_nm;
	}

	public void setRgn_nm(String rgn_nm) {
		this.rgn_nm = rgn_nm;
	}

	public String getRgn_dscrptn() {
		return rgn_dscrptn;
	}

	public void setRgn_dscrptn(String rgn_dscrptn) {
		this.rgn_dscrptn = rgn_dscrptn;
	}
}
