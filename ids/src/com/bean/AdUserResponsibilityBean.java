package com.bean;

public class AdUserResponsibilityBean {

	private int res_id;
	private int res_usr_id;
	private int res_lpi_prjct_mngr;
	private int res_lpi_prjct;
	private int res_lpi_rqst_vlntr;
	private int res_vso_vlntr;
	
	

	public int getRes_lpi_rqst_vlntr() {
		return res_lpi_rqst_vlntr;
	}

	public void setRes_lpi_rqst_vlntr(int res_lpi_rqst_vlntr) {
		this.res_lpi_rqst_vlntr = res_lpi_rqst_vlntr;
	}

	public int getRes_id() {
		return res_id;
	}

	public void setRes_id(int res_id) {
		this.res_id = res_id;
	}

	public int getRes_usr_id() {
		return res_usr_id;
	}

	public void setRes_usr_id(int res_usr_id) {
		this.res_usr_id = res_usr_id;
	}

	public int getRes_lpi_prjct_mngr() {
		return res_lpi_prjct_mngr;
	}

	public void setRes_lpi_prjct_mngr(int res_lpi_prjct_mngr) {
		this.res_lpi_prjct_mngr = res_lpi_prjct_mngr;
	}

	public int getRes_lpi_prjct() {
		return res_lpi_prjct;
	}

	public void setRes_lpi_prjct(int res_lpi_prjct) {
		this.res_lpi_prjct = res_lpi_prjct;
	}

	public int getRes_vso_vlntr() {
		return res_vso_vlntr;
	}

	public void setRes_vso_vlntr(int res_vso_vlntr) {
		this.res_vso_vlntr = res_vso_vlntr;
	}

}
