package com.bean;

public class AdAddCountryBean {
	
	private int cntry_cd;
	private String cntry_nm;
	private String cntry_dscrptn;
	
	public int getCntry_cd() {
		return cntry_cd;
	}
	public void setCntry_cd(int cntry_cd) {
		this.cntry_cd = cntry_cd;
	}
	public String getCntry_nm() {
		return cntry_nm;
	}
	public void setCntry_nm(String cntry_nm) {
		this.cntry_nm = cntry_nm;
	}
	public String getCntry_dscrptn() {
		return cntry_dscrptn;
	}
	public void setCntry_dscrptn(String cntry_dscrptn) {
		this.cntry_dscrptn = cntry_dscrptn;
	}
	
	
	
}
