package com.bean;

import java.util.ArrayList;

public class LpiRequestVolunteerBean {
	
	private int rv_cd ;
	private String rv_id;
	private String rv_status;
	private int rv_prj_cd;
	private int rv_cmp_cd;
	private int rv_no_vlntr;
	private String rv_drtn_mnths;
	
	
	
	
	
	public String getRv_drtn_mnths() {
		return rv_drtn_mnths;
	}

	public void setRv_drtn_mnths(String rv_drtn_mnths) {
		this.rv_drtn_mnths = rv_drtn_mnths;
	}

	public int getRv_no_vlntr() {
		return rv_no_vlntr;
	}

	public void setRv_no_vlntr(int rv_no_vlntr) {
		this.rv_no_vlntr = rv_no_vlntr;
	}

	private ArrayList rvlList = new ArrayList();

	public int getRv_cd() {
		return rv_cd;
	}

	public void setRv_cd(int rv_cd) {
		this.rv_cd = rv_cd;
	}

	public String getRv_id() {
		return rv_id;
	}

	public void setRv_id(String rv_id) {
		this.rv_id = rv_id;
	}

	public String getRv_status() {
		return rv_status;
	}

	public void setRv_status(String rv_status) {
		this.rv_status = rv_status;
	}

	public int getRv_prj_cd() {
		return rv_prj_cd;
	}

	public void setRv_prj_cd(int rv_prj_cd) {
		this.rv_prj_cd = rv_prj_cd;
	}

	public int getRv_cmp_cd() {
		return rv_cmp_cd;
	}

	public void setRv_cmp_cd(int rv_cmp_cd) {
		this.rv_cmp_cd = rv_cmp_cd;
	}

	public ArrayList getRvlList() {
		return rvlList;
	}

	public void setRvlList(LpiRequestVolunteerLineBean rvl) {
		this.rvlList.add(rvl);
	}
	
	public void clearRvlList(){
		this.rvlList = new ArrayList();
	}
	
	
	
	
	

}
