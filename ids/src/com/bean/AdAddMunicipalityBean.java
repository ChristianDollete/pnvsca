package com.bean;

public class AdAddMunicipalityBean {
	private int mun_id;
	private String mun_nm;
	private String mun_dscrptn;
	private int mun_prvnc_id;

	public int getMun_id() {
		return mun_id;
	}

	public void setMun_id(int mun_id) {
		this.mun_id = mun_id;
	}

	public String getMun_nm() {
		return mun_nm;
	}

	public void setMun_nm(String mun_nm) {
		this.mun_nm = mun_nm;
	}

	public String getMun_dscrptn() {
		return mun_dscrptn;
	}

	public void setMun_dscrptn(String mun_dscrptn) {
		this.mun_dscrptn = mun_dscrptn;
	}

	public int getMun_prvnc_id() {
		return mun_prvnc_id;
	}

	public void setMun_prvnc_id(int mun_prvnc_id) {
		this.mun_prvnc_id = mun_prvnc_id;
	}

}
