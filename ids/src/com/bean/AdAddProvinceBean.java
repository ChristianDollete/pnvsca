package com.bean;

public class AdAddProvinceBean {
	private int prvnc_id;
	private String prvnc_nm;
	private String prvnc_dscrptn;
	private int prvnc_rgn_id;

	public int getPrvnc_id() {
		return prvnc_id;
	}

	public void setPrvnc_id(int prvnc_id) {
		this.prvnc_id = prvnc_id;
	}

	public String getPrvnc_nm() {
		return prvnc_nm;
	}

	public void setPrvnc_nm(String prvnc_nm) {
		this.prvnc_nm = prvnc_nm;
	}

	public String getPrvnc_dscrptn() {
		return prvnc_dscrptn;
	}

	public void setPrvnc_dscrptn(String prvnc_dscrptn) {
		this.prvnc_dscrptn = prvnc_dscrptn;
	}

	public int getPrvnc_rgn_id() {
		return prvnc_rgn_id;
	}

	public void setPrvnc_rgn_id(int prvnc_rgn_id) {
		this.prvnc_rgn_id = prvnc_rgn_id;
	}

}
