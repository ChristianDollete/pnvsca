package com.bean;

import java.util.ArrayList;

public class LpiRequestVolunteerExtensionAssignmentBean {

	int rva_cd;
	String rva_id;
	int rva_lpi_cd;
	int rva_lpi_prjct_cd;
	int rva_vlntr_cd;
	String rva_drtn_vlntr_assgnmnt;
	String rva_prd_extnsn_rqst;
	String rva_rsn_rqst_extnsn;
	int rva_dfrd_due;
	int rva_dfrd_due_rsn;
	int rva_ds_apprvd;
	int rva_ds_apprvd_rsn;
	String rva_apprvd_to;
	String rva_apprvd_prd;
	String rva_dt;
	int rva_cmp_cd;
	
	public int getRva_cd() {
		return rva_cd;
	}
	public void setRva_cd(int rva_cd) {
		this.rva_cd = rva_cd;
	}
	public String getRva_id() {
		return rva_id;
	}
	public void setRva_id(String rva_id) {
		this.rva_id = rva_id;
	}
	public int getRva_lpi_cd() {
		return rva_lpi_cd;
	}
	public void setRva_lpi_cd(int rva_lpi_cd) {
		this.rva_lpi_cd = rva_lpi_cd;
	}
	public int getRva_lpi_prjct_cd() {
		return rva_lpi_prjct_cd;
	}
	public void setRva_lpi_prjct_cd(int rva_lpi_prjct_cd) {
		this.rva_lpi_prjct_cd = rva_lpi_prjct_cd;
	}
	public int getRva_vlntr_cd() {
		return rva_vlntr_cd;
	}
	public void setRva_vlntr_cd(int rva_vlntr_cd) {
		this.rva_vlntr_cd = rva_vlntr_cd;
	}
	public String getRva_drtn_vlntr_assgnmnt() {
		return rva_drtn_vlntr_assgnmnt;
	}
	public void setRva_drtn_vlntr_assgnmnt(String rva_drtn_vlntr_assgnmnt) {
		this.rva_drtn_vlntr_assgnmnt = rva_drtn_vlntr_assgnmnt;
	}
	public String getRva_prd_extnsn_rqst() {
		return rva_prd_extnsn_rqst;
	}
	public void setRva_prd_extnsn_rqst(String rva_prd_extnsn_rqst) {
		this.rva_prd_extnsn_rqst = rva_prd_extnsn_rqst;
	}
	public String getRva_rsn_rqst_extnsn() {
		return rva_rsn_rqst_extnsn;
	}
	public void setRva_rsn_rqst_extnsn(String rva_rsn_rqst_extnsn) {
		this.rva_rsn_rqst_extnsn = rva_rsn_rqst_extnsn;
	}
	public int getRva_dfrd_due() {
		return rva_dfrd_due;
	}
	public void setRva_dfrd_due(int rva_dfrd_due) {
		this.rva_dfrd_due = rva_dfrd_due;
	}
	public int getRva_dfrd_due_rsn() {
		return rva_dfrd_due_rsn;
	}
	public void setRva_dfrd_due_rsn(int rva_dfrd_due_rsn) {
		this.rva_dfrd_due_rsn = rva_dfrd_due_rsn;
	}
	public int getRva_ds_apprvd() {
		return rva_ds_apprvd;
	}
	public void setRva_ds_apprvd(int rva_ds_apprvd) {
		this.rva_ds_apprvd = rva_ds_apprvd;
	}
	public int getRva_ds_apprvd_rsn() {
		return rva_ds_apprvd_rsn;
	}
	public void setRva_ds_apprvd_rsn(int rva_ds_apprvd_rsn) {
		this.rva_ds_apprvd_rsn = rva_ds_apprvd_rsn;
	}
	public String getRva_apprvd_to() {
		return rva_apprvd_to;
	}
	public void setRva_apprvd_to(String rva_apprvd_to) {
		this.rva_apprvd_to = rva_apprvd_to;
	}
	public String getRva_apprvd_prd() {
		return rva_apprvd_prd;
	}
	public void setRva_apprvd_prd(String rva_apprvd_prd) {
		this.rva_apprvd_prd = rva_apprvd_prd;
	}
	public String getRva_dt() {
		return rva_dt;
	}
	public void setRva_dt(String rva_dt) {
		this.rva_dt = rva_dt;
	}
	public int getRva_cmp_cd() {
		return rva_cmp_cd;
	}
	public void setRva_cmp_cd(int rva_cmp_cd) {
		this.rva_cmp_cd = rva_cmp_cd;
	}
	
	
	

}
