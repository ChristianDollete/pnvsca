package com.bean;

public class VsoVolunteerBean {
	
	private int vlntr_cd;
	private String vlntr_id;
	private String vlntr_fname;
	private String vlntr_mname;
	private String vlntr_lname;
	
	private String vlntr_sex;
	private String vlntr_ntnly;
	private String vlntr_cvl_stts;
	private String vlntr_brth_dt;
	
	private String vlntr_vs_exprtn;
	private int vlntr_prjct_cd;
	private String vlntr_prjct_nm;
	private int vlntr_prjct_pm_cd;
	private String vlntr_prjct_pm_nm;
	
	private String vlntr_usr_nm;
	private String vlntr_usr_psswrd;
	private String vlntr_usr_email;
	private String vlntr_usr_scnd_email;
	private int vlntr_cmp_cd;
	private int vlntr_usr_cd;
	
	
	
	
	public int getVlntr_usr_cd() {
		return vlntr_usr_cd;
	}
	public void setVlntr_usr_cd(int vlntr_usr_cd) {
		this.vlntr_usr_cd = vlntr_usr_cd;
	}
	public String getVlntr_sex() {
		return vlntr_sex;
	}
	public void setVlntr_sex(String vlntr_sex) {
		this.vlntr_sex = vlntr_sex;
	}
	public int getVlntr_cd() {
		return vlntr_cd;
	}
	public void setVlntr_cd(int vlntr_cd) {
		this.vlntr_cd = vlntr_cd;
	}
	public String getVlntr_id() {
		return vlntr_id;
	}
	public void setVlntr_id(String vlntr_id) {
		this.vlntr_id = vlntr_id;
	}
	public String getVlntr_fname() {
		return vlntr_fname;
	}
	public void setVlntr_fname(String vlntr_fname) {
		this.vlntr_fname = vlntr_fname;
	}
	public String getVlntr_mname() {
		return vlntr_mname;
	}
	public void setVlntr_mname(String vlntr_mname) {
		this.vlntr_mname = vlntr_mname;
	}
	public String getVlntr_lname() {
		return vlntr_lname;
	}
	public void setVlntr_lname(String vlntr_lname) {
		this.vlntr_lname = vlntr_lname;
	}
	public String getVlntr_ntnly() {
		return vlntr_ntnly;
	}
	public void setVlntr_ntnly(String vlntr_ntnly) {
		this.vlntr_ntnly = vlntr_ntnly;
	}
	public String getVlntr_cvl_stts() {
		return vlntr_cvl_stts;
	}
	public void setVlntr_cvl_stts(String vlntr_cvl_stts) {
		this.vlntr_cvl_stts = vlntr_cvl_stts;
	}
	public String getVlntr_brth_dt() {
		return vlntr_brth_dt;
	}
	public void setVlntr_brth_dt(String vlntr_brth_dt) {
		this.vlntr_brth_dt = vlntr_brth_dt;
	}
	public String getVlntr_vs_exprtn() {
		return vlntr_vs_exprtn;
	}
	public void setVlntr_vs_exprtn(String vlntr_vs_exprtn) {
		this.vlntr_vs_exprtn = vlntr_vs_exprtn;
	}
	public int getVlntr_prjct_cd() {
		return vlntr_prjct_cd;
	}
	public void setVlntr_prjct_cd(int vlntr_prjct_cd) {
		this.vlntr_prjct_cd = vlntr_prjct_cd;
	}
	public String getVlntr_prjct_nm() {
		return vlntr_prjct_nm;
	}
	public void setVlntr_prjct_nm(String vlntr_prjct_nm) {
		this.vlntr_prjct_nm = vlntr_prjct_nm;
	}
	public int getVlntr_prjct_pm_cd() {
		return vlntr_prjct_pm_cd;
	}
	public void setVlntr_prjct_pm_cd(int vlntr_prjct_pm_cd) {
		this.vlntr_prjct_pm_cd = vlntr_prjct_pm_cd;
	}
	public String getVlntr_prjct_pm_nm() {
		return vlntr_prjct_pm_nm;
	}
	public void setVlntr_prjct_pm_nm(String vlntr_prjct_pm_nm) {
		this.vlntr_prjct_pm_nm = vlntr_prjct_pm_nm;
	}
	public String getVlntr_usr_nm() {
		return vlntr_usr_nm;
	}
	public void setVlntr_usr_nm(String vlntr_usr_nm) {
		this.vlntr_usr_nm = vlntr_usr_nm;
	}
	public String getVlntr_usr_psswrd() {
		return vlntr_usr_psswrd;
	}
	public void setVlntr_usr_psswrd(String vlntr_usr_psswrd) {
		this.vlntr_usr_psswrd = vlntr_usr_psswrd;
	}
	public String getVlntr_usr_email() {
		return vlntr_usr_email;
	}
	public void setVlntr_usr_email(String vlntr_usr_email) {
		this.vlntr_usr_email = vlntr_usr_email;
	}
	public String getVlntr_usr_scnd_email() {
		return vlntr_usr_scnd_email;
	}
	public void setVlntr_usr_scnd_email(String vlntr_usr_scnd_email) {
		this.vlntr_usr_scnd_email = vlntr_usr_scnd_email;
	}
	public int getVlntr_cmp_cd() {
		return vlntr_cmp_cd;
	}
	public void setVlntr_cmp_cd(int vlntr_cmp_cd) {
		this.vlntr_cmp_cd = vlntr_cmp_cd;
	}
	
	
	
	

}
