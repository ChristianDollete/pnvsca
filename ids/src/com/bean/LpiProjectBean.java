package com.bean;

public class LpiProjectBean {
	private int prjCd;
	private String prjId;
	private String prjOrgName;
	private String prjTargetBenefits;
	private String prjDurationFrom;
	private String prjDurationTo;
	private String prjBudgetSource;
	private String prjSource;
	private String prjProjectObjective;
	private String prjProjectDesc;
	private int prjRgn;
	private int prjPrvnc;
	private int prjMncplty;
	private String prjAddress;
	private int prjProjectManager;
	
	public int getPrjCd() {
		return prjCd;
	}
	public void setPrjCd(int prjCd) {
		this.prjCd = prjCd;
	}
	private int prjCmpCode;
		
	public int getPrjCmpCode() {
		return prjCmpCode;
	}
	public void setPrjCmpCode(int prjCmpCode) {
		this.prjCmpCode = prjCmpCode;
	}
	public String getPrjId() {
		return prjId;
	}
	public void setPrjId(String prjId) {
		this.prjId = prjId;
	}
	public String getPrjOrgName() {
		return prjOrgName;
	}
	public void setPrjOrgName(String prjOrgName) {
		this.prjOrgName = prjOrgName;
	}
	public String getPrjTargetBenefits() {
		return prjTargetBenefits;
	}
	public void setPrjTargetBenefits(String prjTargetBenefits) {
		this.prjTargetBenefits = prjTargetBenefits;
	}
	public String getPrjDurationFrom() {
		return prjDurationFrom;
	}
	public void setPrjDurationFrom(String prjDurationFrom) {
		this.prjDurationFrom = prjDurationFrom;
	}
	public String getPrjDurationTo() {
		return prjDurationTo;
	}
	public void setPrjDurationTo(String prjDurationTo) {
		this.prjDurationTo = prjDurationTo;
	}
	public String getPrjBudgetSource() {
		return prjBudgetSource;
	}
	public void setPrjBudgetSource(String prjBudgetSource) {
		this.prjBudgetSource = prjBudgetSource;
	}
	
	public String getPrjSource() {
		return prjSource;
	}
	public void setPrjSource(String prjSource) {
		this.prjSource = prjSource;
	}
	public String getPrjProjectObjective() {
		return prjProjectObjective;
	}
	public void setPrjProjectObjective(String prjProjectObjective) {
		this.prjProjectObjective = prjProjectObjective;
	}
	public String getPrjProjectDesc() {
		return prjProjectDesc;
	}
	public void setPrjProjectDesc(String prjProjectDesc) {
		this.prjProjectDesc = prjProjectDesc;
	}
	public int getPrjRgn() {
		return prjRgn;
	}
	public void setPrjRgn(int prjRgn) {
		this.prjRgn = prjRgn;
	}
	public int getPrjPrvnc() {
		return prjPrvnc;
	}
	public void setPrjPrvnc(int prjPrvnc) {
		this.prjPrvnc = prjPrvnc;
	}
	public int getPrjMncplty() {
		return prjMncplty;
	}
	public void setPrjMncplty(int prjMncplty) {
		this.prjMncplty = prjMncplty;
	}
	public String getPrjAddress() {
		return prjAddress;
	}
	public void setPrjAddress(String prjAddress) {
		this.prjAddress = prjAddress;
	}
	public int getPrjProjectManager() {
		return prjProjectManager;
	}
	public void setPrjProjectManager(int prjProjectManager) {
		this.prjProjectManager = prjProjectManager;
	}
	
	
	
}
