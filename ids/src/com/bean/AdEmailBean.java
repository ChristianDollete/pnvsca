package com.bean;

public class AdEmailBean {

	private int emlCd;
	private String emlName;
	private String emlDefaultSubject;
	private String emlDescription;
	private String emlApplication;
	private int emlEnabled;
	private int emlCmpCode;
	
	private String emlEmailAddress;
	private String emlEmailPassword;
	
	
	
	
	public String getEmlEmailAddress() {
		return emlEmailAddress;
	}
	public void setEmlEmailAddress(String emlEmailAddress) {
		this.emlEmailAddress = emlEmailAddress;
	}
	public String getEmlEmailPassword() {
		return emlEmailPassword;
	}
	public void setEmlEmailPassword(String emlEmailPassword) {
		this.emlEmailPassword = emlEmailPassword;
	}
	
	public int getEmlCd() {
		return emlCd;
	}
	public void setEmlCd(int emlCd) {
		this.emlCd = emlCd;
	}
	public String getEmlName() {
		return emlName;
	}
	public void setEmlName(String emlName) {
		this.emlName = emlName;
	}
	public String getEmlDefaultSubject() {
		return emlDefaultSubject;
	}
	public void setEmlDefaultSubject(String emlDefaultSubject) {
		this.emlDefaultSubject = emlDefaultSubject;
	}
	public String getEmlDescription() {
		return emlDescription;
	}
	public void setEmlDescription(String emlDescription) {
		this.emlDescription = emlDescription;
	}
	public String getEmlApplication() {
		return emlApplication;
	}
	public void setEmlApplication(String emlApplication) {
		this.emlApplication = emlApplication;
	}
	public int getEmlEnabled() {
		return emlEnabled;
	}
	public void setEmlEnabled(int emlEnabled) {
		this.emlEnabled = emlEnabled;
	}
	public int getEmlCmpCode() {
		return emlCmpCode;
	}
	public void setEmlCmpCode(int emlCmpCode) {
		this.emlCmpCode = emlCmpCode;
	}
	
	
	
	

}
