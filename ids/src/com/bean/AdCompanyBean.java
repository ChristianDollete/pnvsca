package com.bean;

public class AdCompanyBean {

	private int cmp_id;
	private String cmp_nm;
	private String cmp_dscrptn;
	private int cmp_typ;

	public int getCmp_id() {
		return cmp_id;
	}

	public void setCmp_id(int cmp_id) {
		this.cmp_id = cmp_id;
	}

	public String getCmp_nm() {
		return cmp_nm;
	}

	public void setCmp_nm(String cmp_nm) {
		this.cmp_nm = cmp_nm;
	}

	public String getCmp_dscrptn() {
		return cmp_dscrptn;
	}

	public void setCmp_dscrptn(String cmp_dscrptn) {
		this.cmp_dscrptn = cmp_dscrptn;
	}

	public int getCmp_typ() {
		return cmp_typ;
	}

	public void setCmp_typ(int cmp_typ) {
		this.cmp_typ = cmp_typ;
	}

}
