package com.web.ad.adUserSession;

import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.web.UserSession;
import com.web.ad.adMain.AdMainAction;

public class AdUserSessionAction extends ActionSupport{
	private AdUserSessionForm frm;
	public AdUserSessionForm getFrm() {return frm;}
	public void setFrm(AdUserSessionForm frm) {this.frm = frm;}
	
	
	
	
	
	
	public String execute() {
		System.out.println("AdUserSessionAction execute");
		String rtrn="failed"; 
		try {
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				System.out.println("frm.getBtnName() >> "+ frm.getBtnName());
				System.out.println("frm.getLink() >> "+ frm.getLink());
				
				if(frm.getLink().toString().equalsIgnoreCase("11")){
					System.out.println("AdUserSessionAction Load userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					frm.reset();
					//Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
					Collection sessionList = sessionsMap.values();//session.getValueNames();
					Iterator i = sessionList.iterator();
					while(i.hasNext()){
						UserSession userLog = (UserSession)i.next();//(UserSession)session.getAttribute(sessionList[x].toString());
						AdUserSessionLine userSessionLine = new AdUserSessionLine();
						userSessionLine.setUserName(userLog.getUserName());
						String type = "User has no Classification !";
						if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==1){
							type = "Admin";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==2){
							type = "LPI";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==21){
							type = "Project Manager";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==3){
							type = "VSO";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==31){
							type = "Volunteer";
						}else{
						}
						
						userSessionLine.setUserType(type);
						userSessionLine.setUserOrgName(userLog.getUserCmpName());
						userSessionLine.setUserSessionId(userLog.getSessionId());
						frm.setUserSessions(userSessionLine);
					}
					
					
					
					rtrn="load"; 
				}else if (frm.getLink().toString().equalsIgnoreCase("111")){
					System.out.println("AdUserSessionAction execute Force out >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					System.out.println("SESSION TO LOG-OUT >> "+frm.getUserSessionId());
					//UserSession userLogout = (UserSession)session.getAttribute(frm.getUserSessionId());
					AdMainAction adMain = new AdMainAction();
					String log = adMain.logout(session, frm.getUserSessionId());
					frm.reset();
					
					//Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
					Collection sessionList = sessionsMap.values();//session.getValueNames();
					Iterator i = sessionList.iterator();
					while(i.hasNext()){
						UserSession userLog = (UserSession)i.next();//(UserSession)session.getAttribute(sessionList[x].toString());
						AdUserSessionLine userSessionLine = new AdUserSessionLine();
						userSessionLine.setUserName(userLog.getUserName());
						String type = "User has no Classification !";
						if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==1){
							type = "Admin";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==2){
							type = "LPI";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==21){
							type = "Project Manager";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==3){
							type = "VSO";
						}else if(userLog.getUserProfile()!=0 && userLog.getUserProfile()==31){
							type = "Volunteer";
						}else{
						}
						
						userSessionLine.setUserType(type);
						userSessionLine.setUserOrgName(userLog.getUserCmpName());
						userSessionLine.setUserSessionId(userLog.getSessionId());
						frm.setUserSessions(userSessionLine);
					}
					
					
					
					
					rtrn="load"; 
					
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("AdUserSessionAction execute Save >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					frm.reset();
					frm.setAppMessage("PNVSCA Account has been SAVE/UPDATE.");
					rtrn="load";
				}else{
					System.out.println("AdUserSessionAction execute ELSE >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="load";
					
				}
				
			}else{
				rtrn="login";
			}
			
			
		} catch (Exception e) {
		
			rtrn="failed";
			e.printStackTrace();

		}
		
		
		
		return rtrn;
		
	}
	

}
