package com.web.ad.adUserSession;

import com.web.ApplicationMessage;

public class AdUserSessionLine {
	
	private String userName;
	private String userType;
	private String userOrgName;
	private String userSessionId;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserOrgName() {
		return userOrgName;
	}
	public void setUserOrgName(String userOrgName) {
		this.userOrgName = userOrgName;
	}
	public String getUserSessionId() {
		return userSessionId;
	}
	public void setUserSessionId(String userSessionId) {
		this.userSessionId = userSessionId;
	}
	
	
	
	
	
	
	
	
	
	
}
