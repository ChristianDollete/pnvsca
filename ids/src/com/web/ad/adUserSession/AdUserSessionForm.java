package com.web.ad.adUserSession;

import java.util.ArrayList;

import com.web.ApplicationMessage;
import com.web.lpi.lpiProjectFind.LpiProjectFindEntryLine;

public class AdUserSessionForm extends ApplicationMessage{
	
	private String userSessionId;
	
	private ArrayList UserSessions = new ArrayList();

	public ArrayList getUserSessions() {
		return UserSessions;
	}

	public void setUserSessions(AdUserSessionLine userSession) {
		this.UserSessions.add(userSession);
	}
	
	public void reset(){
		this.UserSessions = new ArrayList();
	}

	public String getUserSessionId() {
		return userSessionId;
	}

	public void setUserSessionId(String userSessionId) {
		this.userSessionId = userSessionId;
	}
	
	
	

	/*
	public ArrayList getPrjList() {
		return prjList;
	}
	public void setPrjList(LpiProjectFindEntryLine prjLine) {
		this.prjList.add(prjLine);
	}*/

}
