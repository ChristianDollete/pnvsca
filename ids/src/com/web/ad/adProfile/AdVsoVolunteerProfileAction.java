package com.web.ad.adProfile;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.LpiRequestVolunteerLineControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.web.UserSession;
import com.web.lpi.lpiProject.LpiProjectEntryForm;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;

public class AdVsoVolunteerProfileAction extends ActionSupport{
	private AdVsoVolunteerProfileForm frm;
	public AdVsoVolunteerProfileForm getFrm() {return frm;}
	public void setFrm(AdVsoVolunteerProfileForm frm) {this.frm = frm;}
	
	private void reload() throws Exception{
		System.out.println("AdVsoVolunteerProfileAction reload");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		
		frm.reset();
		
		//VLNTR DETAILS
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			VsoVolunteerEntryControllerBean vlC = new VsoVolunteerEntryControllerBean();
			VsoVolunteerModDetails details = vlC.getByVlntrUsrCode(user.getUserCode());
			
			frm.setVlntrCd(details.getVlntr_cd());
			frm.setVlntrId(details.getVlntr_id());
			frm.setVlntrFname(details.getVlntr_fname());
			frm.setVlntrMname(details.getVlntr_mname());
			frm.setVlntrLname(details.getVlntr_lname());
			frm.setGender(details.getVlntr_sex());
			frm.setVlntrNtnly(details.getVlntr_ntnly());
			frm.setVlntrCstatus(details.getVlntr_cvl_stts());
			frm.setVlntrDateOfBirth(details.getVlntr_brth_dt());
			frm.setVlntrVisaExpiration(details.getVlntr_vs_exprtn());
			
			frm.setVlntrUserName(details.getVlntr_usr_nm());
			frm.setVlntrUserEmail(details.getVlntr_usr_email());
			frm.setVlntrUserSecondEmail(details.getVlntr_usr_scnd_email());
			frm.setPrjCd(details.getVlntr_prjct_cd());
			
			
			frm.setVlntrPrjct("");
			frm.setVlntrPrjPm("");
			frm.setVlntrPrjDateFrom("");
			frm.setVlntrPrjDateTo("");
			frm.setVlntr_usr_cd(details.getUsr_cd());
			
			try {
				LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
				LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(details.getVlntr_cd());
				int rv_cd = rvlD.getRvl_rv_cd();
				
				LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
				LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd, user.getUserCmpCode());
				LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
				LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
				frm.setVlntrPrjct(prjMD.getPrjOrgName());
				frm.setVlntrPrjDateFrom(prjMD.getPrjDurationFrom());
				frm.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
				
				LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
				LpiProjectManagerModDetails pmMD = pmC.getPmByPmCode(prjMD.getPrjProjectManager());
				frm.setVlntrPrjPm(pmMD.getPmName());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
			
		}
	}
	
	
	public String execute() {
		System.out.println("AdVsoVolunteerProfileAction execute");
		String rtrn="failed"; 
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			System.out.println("btn name >> "+ frm.getBtnName()+"<<");
			
			if((frm.getLink().toString().equalsIgnoreCase("1")) || frm.getBtnName().equalsIgnoreCase("Reset")){
				System.out.println("AdVsoVolunteerProfileAction execute PM Profile | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				this.reload();
				rtrn="onload"; 
			}else if (frm.getBtnName().equalsIgnoreCase("Update")){
				System.out.println("AdVsoVolunteerProfileAction execute Update");
				String errmsg = this.validateFrm(frm);
				if(errmsg.equalsIgnoreCase("")){
					
					LpiProjectManagerModDetails pmD = new LpiProjectManagerModDetails();
					VsoVolunteerEntryControllerBean vlntrC = new VsoVolunteerEntryControllerBean();
					VsoVolunteerModDetails details = new VsoVolunteerModDetails();
					details.setVlntr_cd(frm.getVlntrCd());
					details.setVlntr_cmp_cd(user.getUserCmpCode());
					details.setVlntr_id(frm.getVlntrId());
					details.setVlntr_fname(frm.getVlntrFname());
					details.setVlntr_mname(frm.getVlntrMname());
					details.setVlntr_lname(frm.getVlntrLname());
					details.setVlntr_ntnly(frm.getVlntrNtnlyLst());
					details.setVlntr_cvl_stts(frm.getVlntrCstatusLst());
					System.out.println("date of birth >> "+ frm.getVlntrDateOfBirth());
					details.setVlntr_brth_dt(frm.getVlntrDateOfBirth());
					details.setVlntr_sex(frm.getGenderList());
					
					details.setVlntr_vs_exprtn(frm.getVlntrVisaExpiration());
					details.setVlntr_usr_nm(frm.getVlntrUserName());
					details.setVlntr_usr_psswrd(frm.getVlntrUserPass());
					details.setVlntr_usr_email(frm.getVlntrUserEmail());
					details.setVlntr_usr_scnd_email(frm.getVlntrUserSecondEmail());
					details.setUsr_cd(user.getUserCode());
					
					vlntrC.Update(details);
					this.reload();
					rtrn = "onload";
					frm.setAppMessage("Volunteer was SUCCESSFULLY Updated!");
				}else{
					this.reload();
					frm.setAppMessage(errmsg);
					rtrn="onload"; 
				}
			} else if (frm.getBtnName().equalsIgnoreCase("Close")){
				System.out.println("AdVsoVolunteerProfileAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				rtrn="main";
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			rtrn="failed";
		}
		
		return rtrn;
		
	}
	
	

	public String validateFrm(AdVsoVolunteerProfileForm vfrm){
		System.out.println("AdVsoVolunteerProfileAction execute validateFrm");
		
		String errMsg = "";
		
		if(frm.getVlntrUserPass().trim().toString().length()==0){
			errMsg += "User Password is Required. ";
		}
		
		if(!frm.getVlntrUserPass().equals(frm.getVlntrUserPassConfirm())){
			errMsg += "Password not Match. ";
		}
		
		return errMsg;
	}
}
