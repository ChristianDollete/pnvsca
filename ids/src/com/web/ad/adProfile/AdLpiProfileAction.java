package com.web.ad.adProfile;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.web.UserAutoEmailSession;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;

public class AdLpiProfileAction extends ActionSupport{
	private AdLpiProfileForm frm;
	public AdLpiProfileForm getFrm() {return frm;}
	public void setFrm(AdLpiProfileForm frm) {this.frm = frm;}
	
	private void reload() throws Exception{
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		
		frm.reset();
		frm.populateType();
		frm.clearCountryLists();
		frm.clearRegionLists();
		frm.clearProvinceLists();
		frm.clearMunicipalityLists();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
		
		
		
		
		//LPI DETAILS
		
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			logonCB = new AdLogonControllerBean();
			LpiLpiModDetails details = logonCB.getLpiByLpiCode(user.getUserCmpCode());

			frm.setLpiName(details.getLpi_nm());
			String lpiType = "0";
			try {
				if(details.getLpi_typ() == 1){
					lpiType = "National Government Agency";
				}else if(details.getLpi_typ() == 2){
					lpiType = "Local Government Unit";
				}else if(details.getLpi_typ() == 3){
					lpiType = "Non-Government Organization";
				}else if(details.getLpi_typ() == 4){
					lpiType = "Academe";
				}else if(details.getLpi_typ() == 5){
					lpiType = "Private Sector";
				} 
			} catch (Exception e1) {
			}
			frm.setLpiType(lpiType);
			frm.setLpiMandate(details.getLpi_mndt());

			rgnList = new ArrayList();
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			String rgn_nm = "0";

			frm.clearRegionLists();
			while (rl.hasNext()) {
				AdAddRegionModDetails detailsR = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(detailsR.getRgn_nm());
				if (detailsR.getRgn_id() == details.getLpi_rgn()) {
					rgn_nm = detailsR.getRgn_nm();
				}
			}
			frm.setRegion(rgn_nm);

			AdAddRegionModDetails rgnd = logonCB.getRegionByRgnName(rgn_nm);
			ArrayList prnvcList = logonCB.getAllProvinceByRgnId(rgnd.getRgn_nm());
			Iterator pl = prnvcList.iterator();
			String prnvc_nm = "0";
			frm.clearProvinceLists();
			while (pl.hasNext()) {
				AdAddProvinceModDetails detailsP = (AdAddProvinceModDetails) pl.next();
				if (detailsP.getPrvnc_id() == details.getLpi_prvnc()) {
					prnvc_nm = detailsP.getPrvnc_nm();
				}
				frm.setProvinceLists(detailsP.getPrvnc_nm().toString());
			}
			frm.setProvince(prnvc_nm);

			ArrayList munList = new ArrayList();
			munList = logonCB
					.getAllMunicipalityByPrvncId(prnvc_nm);
			Iterator muni = munList.iterator();
			frm.clearMunicipalityLists();
			String mun_nm = "0";
			while (muni.hasNext()) {
				AdAddMunicipalityModDetails detailsM = (AdAddMunicipalityModDetails) muni.next();
				if (detailsM.getMun_id() == details.getLpi_mncplty()) {
					mun_nm = detailsM.getMun_nm();
				}
				frm.setMunicipalityLists(detailsM.getMun_nm().toString());
			}
			frm.setMunicipality(mun_nm);
			frm.setAddress(details.getLpi_addrss());
			frm.setLpiTelephoneNum(details.getLpi_tlphn_nmbr());
			frm.setLpiEmail(details.getLpi_email());
			frm.setLpiFaxNum(details.getLpi_fx_nmbr());
			frm.setLpiWebsite(details.getLpi_wbst());
			frm.setLpiId(details.getLpi_id());
			
			frm.setLpiUserName(details.getLpi_usr_nm());
			System.out.println("user name >> "+details.getLpi_usr_nm());
			System.out.println("user pass >> "+details.getLpi_usr_psswrd());
			frm.setLpiUserPass(details.getLpi_usr_psswrd());
			frm.setLpiUserPrimaryEmail(details.getLpi_prmry_email());
			frm.setLpiUserSecondaryEmail(details.getLpi_scndry_email());
			
			frm.setLpiCoordinatorName(details.getLpi_crdntr_nm());
			frm.setLpiCoordinatorPosition(details.getLpi_crdntr_pstn());
			frm.setLpiCoordinatorDepartment(details.getLpi_crdntr_dprtmnt());
			frm.setLpiCoordinatorTelNum(details.getLpi_crdntr_tlphn_nmbr());
			frm.setLpiCoordinatorMobileNum(details.getLpi_crdntr_mble_nmbr());
			frm.setLpiCode(details.getLpi_cd());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		
		
		
		
	}
	
	
	public String execute() {
		System.out.println("AdProfileAction execute");
		String rtrn="failed"; 
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			System.out.println("btn name >> "+ frm.getBtnName()+"<<");
			
			if((frm.getLink().toString().equalsIgnoreCase("1")) || frm.getLpiBtnName().equalsIgnoreCase("Reset")){
				System.out.println("LpiProjectEntryAction execute LPI Profile | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				this.reload();
				rtrn="lpi"; 

			}else if (frm.getLpiBtnName().equalsIgnoreCase("Update")){
				System.out.println("LpiProjectEntryAction execute Update");
				String errmsg = this.validateFrm(frm);
				if(errmsg.equalsIgnoreCase("")){
					
					LpiLpiModDetails details = new LpiLpiModDetails();
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					System.out.println("LPI_CODE >> "+frm.getLpiCode());
					details.setLpi_cd(frm.getLpiCode());
					details.setLpi_id(frm.getLpiId());
					details.setLpi_nm(frm.getLpiName());
					int ltyp = 0;
					try {
						if(frm.getLpiTypeList().equalsIgnoreCase("National Government Agency")){
							ltyp = 1;
						}else if(frm.getLpiTypeList().equalsIgnoreCase("Local Government Unit")){
							ltyp = 2;
						}else if(frm.getLpiTypeList().equalsIgnoreCase("Non-Government Organization")){
							ltyp = 3;
						}else if(frm.getLpiTypeList().equalsIgnoreCase("Academe")){
							ltyp = 4;
						}else if(frm.getLpiTypeList().equalsIgnoreCase("Private Sector")){
							ltyp = 5;
						}
					} catch (Exception e1) {
						
					}
					
					
					details.setLpi_typ(ltyp);
					details.setLpi_mndt(frm.getLpiMandate());
					try {
						AdAddRegionModDetails rgnD = logonCB.getRegionByRgnName(frm.getRegionList());
						details.setLpi_rgn(rgnD.getRgn_id());
					} catch (Exception e) {
						details.setLpi_rgn(0);
					}
					try {
						AdAddProvinceModDetails prnvcD = logonCB.getProvinceByPrvcName(frm.getProvinceList());
						details.setLpi_prvnc(prnvcD.getPrvnc_id());
					} catch (Exception e) {
						details.setLpi_prvnc(0);
					}
					try {
						System.out.println("frm.getMunicipalityList() >> "+frm.getMunicipalityList());
						AdAddMunicipalityModDetails munDB = logonCB.getMunicipalityByMunName(frm.getMunicipalityList());
						details.setLpi_mncplty(munDB.getMun_id());
					} catch (Exception e) {
						details.setLpi_mncplty(0);
					}
					
					details.setLpi_addrss(frm.getAddress());
					details.setLpi_tlphn_nmbr(frm.getLpiTelephoneNum());
					details.setLpi_email(frm.getLpiEmail());
					details.setLpi_fx_nmbr(frm.getLpiFaxNum());
					details.setLpi_wbst(frm.getLpiWebsite());
					
					details.setLpi_usr_nm(frm.getLpiUserName());
					details.setLpi_usr_psswrd(frm.getLpiUserPass());
					details.setLpi_prmry_email(frm.getLpiUserPrimaryEmail());
					details.setLpi_scndry_email(frm.getLpiUserSecondaryEmail());
					details.setLpi_crdntr_nm(frm.getLpiCoordinatorName());
					details.setLpi_crdntr_pstn(frm.getLpiCoordinatorPosition());
					details.setLpi_crdntr_dprtmnt(frm.getLpiCoordinatorDepartment());
					details.setLpi_crdntr_tlphn_nmbr(frm.getLpiCoordinatorTelNum());
					details.setLpi_crdntr_mble_nmbr(frm.getLpiCoordinatorMobileNum());
					details.setLpi_cd(frm.getLpiCode());
					details.setLpi_cmp_id(user.getUserCmpCode());
					details.setUsr_cd(user.getUserCode());
					int LpiCode = logonCB.updateLpiDetails(details);
					this.reload();
					rtrn = "lpi";
					//frm.setAppMessage("Sign-up for LPI Account was Succesfully Created, Please see your Email for Confirmation.");
					
					
					
					
					String msg = "LPI Account was Succesfully UPDATED.";
					
					AdEmailControllerBean emailCB = new AdEmailControllerBean();
					AdEmailBean emlBean = emailCB.getEmail(2);
					
					if(emlBean.getEmlEnabled() == 1){
						
						try {
							UserAutoEmailSession email = new UserAutoEmailSession();
							String Body = ""
							+"<p>HI,</p>"
							+ "<p>Good Day!</p>"
							+ "<p>Your Local Partner Institute Account has been Update.</p>"
							+ "<p>&nbsp;</p>"
							+ "<p>&nbsp;</p>"
							+ "<p>Thanks,</p>"
							+ "<p>PNVSCA</p>"
							;
							email.SendEmail(details.getLpi_email(), "LPI Account Notification", Body);
							
							msg+=" Email has sent to your Email Address.";
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						
						
					}
					frm.setAppMessage(msg);
					
				}else{
					this.reload();
					frm.setAppMessage(errmsg);
					rtrn="lpi"; 
				}
			} else if (frm.getLpiBtnName().equalsIgnoreCase("Close")){
				System.out.println("LpiProjectEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				rtrn="main";
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			rtrn="failed";
		}
		
		return rtrn;
		
	}
	
	

	public String validateFrm(AdLpiProfileForm vfrm){
		System.out.println("AdProfileAction execute validateFrm");
		
String errMsg = "";
		
		/*if(frm.getLpiName() == null || frm.getLpiName().trim().length()==0){
			errMsg +="LPI Organization Name is Required. ";
		}*/
		
		if(frm.getLpiTypeList() == null || frm.getLpiTypeList().equals("0")){
			errMsg +="LPI Type is Required. ";
		}
		
		if(frm.getRegionList() == null || frm.getRegionList().equals("0")){
			errMsg +="Region is Required. ";
		}
		
		if(frm.getProvinceList() == null || frm.getProvinceList().equals("0")){
			errMsg +="Province is Required. ";
		}
		
		if(frm.getMunicipalityList() == null || frm.getMunicipalityList().equals("0")){
			errMsg +="Province is Required. ";
		}
		
		if(frm.getAddress() == null || frm.getAddress().trim().length()==0){
			errMsg +="Address is Required. ";
		}
		
		if(frm.getLpiTelephoneNum() == null || frm.getLpiTelephoneNum().trim().length()==0){
			errMsg +="Telephone No. is Required. ";
		}
		
		if(frm.getLpiEmail() == null || frm.getLpiEmail().trim().length()==0){
			errMsg +="LPI Email Address is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getLpiEmail());
			if(yes == false){
				errMsg += "LPI Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getLpiFaxNum() == null || frm.getLpiFaxNum().trim().length()==0){
			errMsg +="Fax No. is Required. ";
		}
		
		if(frm.getLpiFaxNum() == null || frm.getLpiFaxNum().trim().length()==0){
			errMsg +="Fax No. is Required. ";
		}
		
		if(frm.getLpiId() == null || frm.getLpiId().trim().length()==0){
			errMsg +="Lpi ID / UACS Code is Required. ";
		}else{
			AdLogonControllerBean adlCB = new AdLogonControllerBean();
			try {
				LpiLpiModDetails lpiMD =  adlCB.getLpiByLpiUACS(frm.getLpiId().trim());
				
				if(lpiMD.getLpi_cd() !=  frm.getLpiCode()){
					errMsg +="Lpi ID / UACS Code is already Registered. ";
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		
		if(frm.getLpiUserName() == null || frm.getLpiUserName().trim().length()==0){
			errMsg +="UserName is Required. ";
		}
		
		if(frm.getLpiUserPass() == null || frm.getLpiUserPass().trim().length()==0){
			errMsg +="User Password is Required. ";
		}
		
		if(!frm.getLpiUserPass().equals(frm.getLpiUserPassConfirm())){
			errMsg +="User Password is not matched. ";
		}
		
		if(frm.getLpiUserPrimaryEmail() == null || frm.getLpiUserPrimaryEmail().trim().length()==0){
			errMsg +="User Primary Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getLpiUserPrimaryEmail());
			if(yes == false){
				errMsg += "User Primaryl is Invalid Format. ";
			}
			
		}
		
		
		
		
		
		
		return errMsg;
	}
}
