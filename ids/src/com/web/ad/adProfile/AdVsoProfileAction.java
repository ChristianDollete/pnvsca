package com.web.ad.adProfile;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.lpi.lpiProject.LpiProjectEntryForm;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.AdAddCountryModDetails;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.VsoVsoModDetails;

public class AdVsoProfileAction extends ActionSupport{
	

	private AdVsoProfileForm frm;
	public AdVsoProfileForm getFrm() {return frm;}
	public void setFrm(AdVsoProfileForm frm) {this.frm = frm;}
	
	private void reload() throws Exception{
		System.out.println("AdVsoProfileAction reload");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
			
		frm.reset();
		/*frm.populateType();
		frm.clearCountryLists();
		frm.clearRegionLists();
		frm.clearProvinceLists();
		frm.clearMunicipalityLists();
		*/
		
		//VSO DETAILS
		
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			logonCB = new AdLogonControllerBean();
			System.out.println("CMP CODE >> "+ user.getUserCmpCode());
			VsoVsoModDetails details = logonCB.getVsoByVsoCode(user.getUserCmpCode());
			
			frm.setVsoCode(details.getVso_cd());
			frm.setVsoId(details.getVso_id());
			frm.setVsoName(details.getVso_nm());
			frm.populateType();
			String org_typ = "0";
			System.out.println("VSO TYPE >> "+ details.getVso_org_typ());
			if(details.getVso_org_typ() == 1){
				org_typ = "International Government Organization/ Program";
			}else if(details.getVso_org_typ() == 2){
				org_typ = "International Non-Government Organization";
			}else if(details.getVso_org_typ() == 3){
				org_typ = "Local Organization";
			}
			
			
			frm.setVsoType(org_typ);
			frm.setVsoHeadName(details.getVso_org_hd_nm());
			frm.setVsoHeadPosition(details.getVso_org_hd_pstn());
			
			ArrayList rgnList = new ArrayList();
			try {
				rgnList = logonCB.getAllRegion();
				Iterator rl = rgnList.iterator();
				frm.clearRegionLists();
				frm.setRegion("0");
				while (rl.hasNext()) {
					AdAddRegionModDetails rgnD = (AdAddRegionModDetails)rl.next();
					frm.setRegionLists(rgnD.getRgn_nm());
					if(rgnD.getRgn_id() == details.getVso_org_rgn()){
						frm.setRegion(rgnD.getRgn_nm());
					}
				}
			} catch (Exception e) {}
			
			ArrayList prvncList = new ArrayList();
			Iterator i;
			try {
				prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
				i = prvncList.iterator();
				frm.clearProvinceLists();
				frm.setProvince("0");
				while (i.hasNext()) {
					AdAddProvinceModDetails prvncD = (AdAddProvinceModDetails)i.next();
					frm.setProvinceLists(prvncD.getPrvnc_nm().toString());
					if(prvncD.getPrvnc_id() == details.getVso_org_prvnc()){
						frm.setProvince(prvncD.getPrvnc_nm());
					}
				}
			} catch (Exception e) {}
			

			ArrayList munList = new ArrayList();
			try {
				munList = logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
				i = munList.iterator();
				frm.clearMunicipalityLists();
				while (i.hasNext()) {
					AdAddMunicipalityModDetails munD = (AdAddMunicipalityModDetails)i.next();
					frm.setMunicipalityLists(munD.getMun_nm().toString());
					if(munD.getMun_id() == details.getVso_org_mncplty()){
						frm.setMunicipality(munD.getMun_nm());
					}
				}
			} catch (Exception e) {}
			

			frm.setAddress(details.getVso_org_addrss());
			frm.setVsoTelephoneNum(details.getVso_org_tlph_nmbr());
			frm.setVsoFaxNum(details.getVso_org_fx_nmbr());
			frm.setVsoEmail(details.getVso_org_email());
			frm.setVsoWebsite(details.getVso_org_wbst());
			frm.setVsoSecRegNum(details.getVso_sec_rgstrtn_nmbr());
			frm.setVsoHeadOfficeAddress(details.getVso_hd_offc_addrss());
			
			ArrayList cntryList = new ArrayList();
			frm.clearCountryLists();
			try {
				cntryList = logonCB.getAllCountry();
				Iterator cl = cntryList.iterator();
				frm.setCountry("0");
				while (cl.hasNext()) {
					AdAddCountryModDetails cntrD = (AdAddCountryModDetails)cl.next();
					frm.setCountryLists(cntrD.getCntry_nm());
					if(cntrD.getCntry_cd() == details.getVso_cntry()){
						frm.setCountry(cntrD.getCntry_nm());
					}
				}
			} catch (Exception e) {}
			
			frm.setVsoDateEstablished(details.getVso_dt_estblishd());;
			frm.setVsoVsion(details.getVso_vsn());
			frm.setVsoMission(details.getVso_mssn());
			frm.setVsoGoals(details.getVso_goal());
			frm.setVsoAnnualBudget(details.getVso_annl_bdgt());
			
			frm.setVsoBudgetSource(details.getVso_bdgt_scrc());
			
			frm.populateRecruitment();
			String rctmnt = "0";
			if(details.getVso_rcrtment() == 1){
				rctmnt = "Foreign";
			}else if(details.getVso_rcrtment() == 2){
				rctmnt = "Local";
			}
			frm.setVsoRecruitment(rctmnt);
			
			
			frm.setVsoEngagement(common.IntToBoolean(details.getVso_enggmnt()));
			frm.setVsoCapability(common.IntToBoolean(details.getVso_cpblty_bldng()));
			frm.setVsoPromotion(common.IntToBoolean(details.getVso_prmtn()));
			frm.setVsoNetworking(common.IntToBoolean(details.getVso_ntwrkng_of_vlntrs()));
			frm.setVsoOther1(common.IntToBoolean(details.getVso_othrs1()));
			frm.setVsoOtherNote1(details.getVso_othrs_nt1());
			
			frm.setVsoLiving(common.IntToBoolean(details.getVso_lvng()));
			frm.setVsoMeal(common.IntToBoolean(details.getVso_meal()));
			frm.setVsoInsurance(common.IntToBoolean(details.getVso_insrnc()));
			frm.setVsoRecognition(common.IntToBoolean(details.getVso_rcgntn_or_awrd()));
			frm.setVsoTravel(common.IntToBoolean(details.getVso_trvl_allwnc()));
			frm.setVsoHousing(common.IntToBoolean(details.getVso_hsng()));
			frm.setVsoTraining(common.IntToBoolean(details.getVso_trnng_and_ornttn()));
			frm.setVsoOther2(common.IntToBoolean(details.getVso_othrs2()));
			frm.setVsoOtherNote2(details.getVso_othrs_nt2());
			
			frm.setVsoUserName(details.getVso_usr_nm());
			frm.setVsoUserPass(details.getVso_psswrd());
			frm.setVsoUserPrimaryEmail(details.getVso_prmry_email());
			frm.setVsoUserSecondaryEmail(details.getVso_scndry_email());
			frm.setVsoCoordinatorName(details.getVso_crdntr_nm());
			frm.setVsoCoordinatorPosition(details.getVso_crdntr_pstn());
			frm.setVsoCoordinatorDepartment(details.getVso_crdntr_dprtmnt());
			frm.setVsoCoordinatorTelNum(details.getVso_crdntr_tlphn_nmbr());
			frm.setVsoCoordinatorMobileNum(details.getVso_crdntr_mbl_nmbr());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
	}
	
	
	public String execute() {
		System.out.println("AdVsoProfileAction execute");
		String rtrn="failed"; 
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			System.out.println("btn name >> "+ frm.getBtnName()+"<<");
			
			if((frm.getLink().toString().equalsIgnoreCase("1")) || frm.getBtnName().equalsIgnoreCase("Reset")){
				System.out.println("AdVsoProfileAction execute LPI Profile | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				this.reload();
				rtrn="onload"; 

			}else if (frm.getBtnName().equalsIgnoreCase("Update")){

				System.out.println("AdVsoProfileAction execute Update");
				String errMsg = this.validateFrm(frm);
				if (!errMsg.equalsIgnoreCase("")) {
					frm.setAppMessage(errMsg);
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					frm.populateRecruitment();
					frm.setVsoRecruitment(frm.getVsoRecruitmentList());
					frm.populateBudgetSourcr();
					frm.setVsoBudgetSource(frm.getVsoBudgetSourceList());
					
					frm.populateType();
					frm.setVsoType(frm.getVsoTypeList());
					
					ArrayList cntryList = new ArrayList();
					frm.clearCountryLists();
					try {
						cntryList = logonCB.getAllCountry();
						Iterator cl = cntryList.iterator();
						while (cl.hasNext()) {
							AdAddCountryModDetails cntrD = (AdAddCountryModDetails)cl.next();
							frm.setCountryLists(cntrD.getCntry_nm());
						}
					} catch (Exception e) {
					}
					frm.setCountry(frm.getCountryList());

					ArrayList rgnList = new ArrayList();
					try {
						rgnList = logonCB.getAllRegion();
						Iterator rl = rgnList.iterator();
						frm.clearRegionLists();
						while (rl.hasNext()) {
							AdAddRegionModDetails rgnD = (AdAddRegionModDetails)rl.next();
							frm.setRegionLists(rgnD.getRgn_nm());
						}
					} catch (Exception e) {

						e.printStackTrace();
					}
					frm.setRegion(frm.getRegionList());

					ArrayList prvncList = new ArrayList();
					Iterator i;
					try {
						prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
						i = prvncList.iterator();
						frm.clearProvinceLists();
						while (i.hasNext()) {
							AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
							frm.setProvinceLists(details.getPrvnc_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					frm.setProvince(frm.getProvinceList());

					ArrayList munList = new ArrayList();
					try {
						munList = logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
						i = munList.iterator();
						frm.clearMunicipalityLists();
						while (i.hasNext()) {
							AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
							frm.setMunicipalityLists(details.getMun_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					frm.setMunicipality(frm.getMunicipalityList());
					
					
					rtrn = "onload";

				}else{
					System.out.println("AdVsoProfileAction execute Update");
					
					VsoVsoModDetails details = new VsoVsoModDetails();
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					details.setVso_cd(frm.getVsoCode());
					details.setVso_id(frm.getVsoId());
					details.setVso_nm(frm.getVsoName());
					
					int org_type = 0;
					if(frm.getVsoTypeList().equals("International Government Organization/ Program")){
						org_type = 1;
					}else if(frm.getVsoTypeList().equals("International Non-Government Organization")){
						org_type = 2;
					}else if(frm.getVsoTypeList().equals("Local Organization")){
						org_type = 3;
					}
					details.setVso_org_typ(org_type);
					details.setVso_org_hd_nm(frm.getVsoHeadName());
					details.setVso_org_hd_pstn(frm.getVsoHeadPosition());
					
					
					try {
						AdAddRegionModDetails rgnD = logonCB.getRegionByRgnName(frm.getRegionList());
						details.setVso_org_rgn(rgnD.getRgn_id());
					} catch (Exception e) {
						details.setVso_org_rgn(0);
					}
					try {
						AdAddProvinceModDetails prnvcD = logonCB.getProvinceByPrvcName(frm.getProvinceList());
						details.setVso_org_prvnc(prnvcD.getPrvnc_id());
					} catch (Exception e) {
						details.setVso_org_prvnc(0);
					}
					try {
						AdAddMunicipalityModDetails munDB = logonCB.getMunicipalityByMunName(frm.getMunicipalityList());
						details.setVso_org_mncplty(munDB.getMun_id());
					} catch (Exception e) {
						details.setVso_org_mncplty(0);
					}
					details.setVso_org_addrss(frm.getAddress());
					details.setVso_org_tlph_nmbr(frm.getVsoTelephoneNum());
					details.setVso_org_fx_nmbr(frm.getVsoFaxNum());
					details.setVso_org_email(frm.getVsoEmail());
					details.setVso_org_wbst(frm.getVsoWebsite());
					details.setVso_sec_rgstrtn_nmbr(frm.getVsoSecRegNum());
					details.setVso_hd_offc_addrss(frm.getVsoHeadOfficeAddress());
					try {
						AdAddCountryModDetails cntryDB = logonCB.getAllCountryByCntryName(frm.getCountryList());
						details.setVso_cntry(cntryDB.getCntry_cd());
					} catch (Exception e) {
						details.setVso_cntry(0);
					}
					
					details.setVso_dt_estblishd(frm.getVsoDateEstablished());;
					details.setVso_vsn(frm.getVsoVsion());
					details.setVso_mssn(frm.getVsoMission());
					details.setVso_goal(frm.getVsoGoals());
					details.setVso_annl_bdgt(frm.getVsoAnnualBudget());
					details.setVso_bdgt_scrc(frm.getVsoBudgetSourceList());
					
					int rctmnt = 0;
					if(frm.getVsoRecruitmentList().equals("Foreign")){
						rctmnt = 1;
					}else if(frm.getVsoRecruitmentList().equals("Local")){
						rctmnt = 2;
					}
					details.setVso_rcrtment(rctmnt);
					details.setVso_enggmnt(common.booleanToInt(frm.isVsoEngagement()));
					details.setVso_cpblty_bldng(common.booleanToInt(frm.isVsoCapability()));
					details.setVso_prmtn(common.booleanToInt(frm.isVsoPromotion()));
					details.setVso_ntwrkng_of_vlntrs(common.booleanToInt(frm.isVsoNetworking()));
					details.setVso_othrs1(common.booleanToInt(frm.isVsoOther1()));
					details.setVso_othrs_nt1(frm.getVsoOtherNote1());
					
					
					details.setVso_lvng(common.booleanToInt(frm.isVsoLiving()));
					details.setVso_meal(common.booleanToInt(frm.isVsoMeal()));
					details.setVso_insrnc(common.booleanToInt(frm.isVsoInsurance()));
					details.setVso_rcgntn_or_awrd(common.booleanToInt(frm.isVsoRecognition()));
					details.setVso_trvl_allwnc(common.booleanToInt(frm.isVsoTravel()));
					details.setVso_hsng(common.booleanToInt(frm.isVsoHousing()));
					details.setVso_trnng_and_ornttn(common.booleanToInt(frm.isVsoTraining()));
					details.setVso_othrs2(common.booleanToInt(frm.isVsoOther2()));
					details.setVso_othrs_nt2(frm.getVsoOtherNote2());
					
					details.setUsr_cd(user.getUserCode());
					details.setVso_usr_nm(frm.getVsoUserName());
					details.setVso_psswrd(frm.getVsoUserPass());
					details.setVso_prmry_email(frm.getVsoUserPrimaryEmail());
					details.setVso_scndry_email(frm.getVsoUserSecondaryEmail());
					details.setVso_crdntr_nm(frm.getVsoCoordinatorName());
					details.setVso_crdntr_pstn(frm.getVsoCoordinatorPosition());
					details.setVso_crdntr_dprtmnt(frm.getVsoCoordinatorDepartment());
					details.setVso_crdntr_tlphn_nmbr(frm.getVsoCoordinatorTelNum());
					details.setVso_crdntr_mbl_nmbr(frm.getVsoCoordinatorMobileNum());
					details.setVso_cmp_cd(user.getUserCmpCode());
					
					int vsoCode = logonCB.updateVsoDetails(details);
					reload();
					frm.setAppMessage("VSO Account was Succesfully UPDATED");
					
					rtrn = "onload";
				}
				
			} else if (frm.getBtnName().equalsIgnoreCase("Close")){
				System.out.println("LpiProjectEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				rtrn="main";
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			rtrn="failed";
		}
		
		return rtrn;
		
	}
	
	

	public String validateFrm(AdVsoProfileForm vfrm){
		System.out.println("AdVsoProfileAction execute validateFrm");
		
		String errMsg = "";
		
		if(frm.getVsoName() == null || frm.getVsoName().trim().length()==0){
			errMsg = "Organization Name is Required. ";
		}else{
			VsoVsoControllerBean vsoCB = new VsoVsoControllerBean();
			 
			try {
				VsoVsoModDetails vsoMD = vsoCB.getVsoByVsoId(frm.getVsoName());
				if(vsoMD.getVso_cd()>0){
					errMsg += "Organization Names is already Registered. ";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if(frm.getVsoTypeList() == null || frm.getVsoTypeList().equals("0")){
			errMsg += "Type of Organization is Required. ";
		}
		
		if(frm.getVsoHeadName() == null || frm.getVsoHeadName().trim().length() == 0){
			errMsg += "VSO Head Name is Required. ";
		}
		
		if(frm.getVsoHeadPosition() == null || frm.getVsoHeadPosition().trim().length() == 0){
			errMsg += "Head Position is Required. ";
		}
		
		if(frm.getRegionList().equals("0")){
			errMsg += "Region is Required. ";
		}
		
		if(frm.getProvinceList().equals("0")){
			errMsg += "Province is Required. ";
		}
		
		if(frm.getMunicipalityList().equals("0")){
			errMsg += "Municipality is Required. ";
		}
		
		if(frm.getAddress() == null || frm.getAddress().trim().length() == 0){
			errMsg += "Address is Required. ";
		}
		
		if(frm.getVsoTelephoneNum() == null || frm.getVsoTelephoneNum().trim().length() == 0 ){
			errMsg += "Telephone No. is Required. ";
		}
		
		if(frm.getVsoEmail() == null || frm.getVsoEmail().trim().length() == 0){
			errMsg += "VSO Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoEmail());
			if(yes == false){
				errMsg += "VSO Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getVsoFaxNum() == null || frm.getVsoFaxNum().trim().length() == 0){
			errMsg += "Fax No. is Required. ";
		}
		/*
		if(frm.getVsoSecRegNum() == null || frm.getVsoSecRegNum().trim().length() == 0){
			errMsg += "Sec Registration No. is Required. ";
		}
		*/
		if(frm.getVsoDateEstablished()==null || frm.getVsoDateEstablished().trim().length() == 0){
			errMsg += "Date Established is Required. ";
		}
		
		if(frm.getVsoVsion() == null || frm.getVsoVsion().trim().length() == 0){
			errMsg += "VSO Vision is Required. ";
		}
		
		if(frm.getVsoMission() == null || frm.getVsoMission().trim().length() == 0){
			errMsg += "VSO Mission is Required. ";
		}
		
		if(frm.getVsoGoals() == null || frm.getVsoGoals().trim().length() == 0){
			errMsg += "VSO Goal is Required. ";
		}
		
		if(frm.getVsoAnnualBudget() == null || frm.getVsoAnnualBudget().trim().length() == 0){
			errMsg += "Annual Budget Range is Required. ";
		}
		
		
		if(frm.getVsoBudgetSourceList() == null || frm.getVsoBudgetSourceList().equals("0")){
			errMsg += "VSO Budget Souce is Required. ";
		}

		
		if(frm.getVsoUserName() == null || frm.getVsoUserName().trim().length()==0){
			errMsg += "UserName is Required. ";
		}
		
		if(frm.getVsoUserPass() == null || frm.getVsoUserPass().trim().length() == 0){
			errMsg += "User Password is Required. ";
		}else{
			if(!frm.getVsoUserPass().equals(frm.getVsoUserPassConfirm())){
				errMsg += "User Password is not Matched. ";
			}
			
		}
		
		if(frm.getVsoUserPrimaryEmail() == null || frm.getVsoUserPrimaryEmail().trim().length() == 0){
			errMsg += "User Primary Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoUserPrimaryEmail());
			if(yes == false){
				errMsg += "User Primary Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getVsoUserSecondaryEmail() == null || frm.getVsoUserSecondaryEmail().trim().length() == 0){
			errMsg += "User Secondary Email is Requied. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoUserSecondaryEmail());
			if(yes == false){
				errMsg += "User Secondary Email is Invalid Format. ";
			}
			
		}
		
		
		if(frm.getVsoCoordinatorName() == null || frm.getVsoCoordinatorName().trim().length() == 0){
			errMsg += "PNVSCA Coordinator is Required. ";
		}
		
		return errMsg;
	}
            
	
	
	
	
}
