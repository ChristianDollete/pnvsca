package com.web.ad.adProfile;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.lpi.lpiProject.LpiProjectEntryForm;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;

public class AdLpiProjectManagerProfileAction extends ActionSupport{
	private AdLpiProjectManagerProfileForm frm;
	public AdLpiProjectManagerProfileForm getFrm() {return frm;}
	public void setFrm(AdLpiProjectManagerProfileForm frm) {this.frm = frm;}
	
	private void reload() throws Exception{
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		
		frm.reset();
		
		//PM DETAILS
		
		String pmID = frm.getPmId();
		try {
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			
			LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
			LpiProjectManagerModDetails pmD= pmC.getPmByUsrCode(user.getUserCode());
			frm.setPmCd(pmD.getPmCd());
			frm.setPmId(pmD.getPmId());
			frm.setPmName(pmD.getPmName());
			frm.setPmPosition(pmD.getPmPosition());
			frm.setGender(pmD.getSex());
			frm.setPmEmail(pmD.getPmEmail());
			frm.setPmTelNo(pmD.getPmTelNo());
			frm.setPmMobNo(pmD.getPmMobNo());
			frm.setPmFaxNo(pmD.getPmFaxNo());
			frm.setPmUserName(pmD.getPmUserName());
			frm.setPmUserCode(pmD.getPm_usr_cd());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
			
		}
	}
	
	
	public String execute() {
		System.out.println("AdLpiProjectManagerProfileAction execute");
		String rtrn="failed"; 
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			System.out.println("btn name >> "+ frm.getBtnName()+"<<");
			
			if((frm.getLink().toString().equalsIgnoreCase("1")) || frm.getBtnName().equalsIgnoreCase("Reset")){
				System.out.println("AdLpiProjectManagerProfileAction execute PM Profile | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				this.reload();
				rtrn="onload"; 
			}else if (frm.getBtnName().equalsIgnoreCase("Update")){
				System.out.println("AdLpiProjectManagerProfileAction execute Update");
				String errmsg = this.validateFrm(frm);
				if(errmsg.equalsIgnoreCase("")){
					
					LpiProjectManagerModDetails pmD = new LpiProjectManagerModDetails();
					pmD.setPmId(frm.getPmId());
					pmD.setPmName(frm.getPmName());
					pmD.setSex(frm.getGenderList());
					pmD.setPmEmail(frm.getPmEmail());
					pmD.setPmPosition(frm.getPmPosition());
					pmD.setPmTelNo(frm.getPmTelNo());
					pmD.setPmMobNo(frm.getPmMobNo());
					pmD.setPmFaxNo(frm.getPmFaxNo());
					pmD.setPmUserName(frm.getPmUserName());
					pmD.setPmUserPassword(frm.getPmUserPass());
					pmD.setPm_cmpny_id(user.getUserCmpCode());
					pmD.setPm_usr_cd(user.getUserCode());
					LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
					pmC.updatePM(pmD);
					
					this.reload();
					rtrn = "onload";
					frm.setAppMessage("Project Manager Account was Succesfully UPDATED");
				}else{
					this.reload();
					frm.setAppMessage(errmsg);
					rtrn="onload"; 
				}
			} else if (frm.getBtnName().equalsIgnoreCase("Close")){
				System.out.println("AdLpiProjectManagerProfileAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				rtrn="main";
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			rtrn="failed";
		}
		
		return rtrn;
		
	}
	
	

	public String validateFrm(AdLpiProjectManagerProfileForm vfrm){
		System.out.println("AdLpiProjectManagerProfileAction execute validateFrm");
		
		String errMsg = "";
		
		if(frm.getPmName() == null || frm.getPmName().trim().length()==0){
			errMsg+="PM Full Name is Required.  ";
		}
		
		if(frm.getGenderList().equals("0")){
			errMsg+="Gender is Required.  ";
		}
		
		if(frm.getPmEmail() == null || frm.getPmEmail().trim().length() == 0){
			errMsg+="Email is Required.  ";
		}else{
			boolean eml = common.validateEmail(frm.getPmEmail());
			if(eml == false){
				errMsg+="Email is Invalid Format.  ";
			}
		}
		
		
		if(frm.getPmTelNo() == null || frm.getPmTelNo().trim().length()==0){
			errMsg+="Telephone No. is Required.  ";
		}
		
		
		if(frm.getPmMobNo() == null || frm.getPmMobNo().trim().length()==0){
			errMsg+="Mobile No. is Required.  ";
		}
		
		if(frm.getPmFaxNo() == null || frm.getPmFaxNo().trim().length()==0){
			errMsg+="Fax No. is Required.  ";
		}
		
		if(frm.getPmUserName() == null || frm.getPmUserName().trim().length()==0){
			errMsg += "User Name is Required.  ";
		}
		
		if(frm.getPmUserPass() == null || frm.getPmUserPass().trim().length()==0){
			errMsg+="Password is Required.  ";
		}else if(!frm.getPmUserPass().equals(frm.getPmUserConfirmPass())){
			errMsg+="Passwrod not Matched.  ";
		}
		
		return errMsg;
	}
}
