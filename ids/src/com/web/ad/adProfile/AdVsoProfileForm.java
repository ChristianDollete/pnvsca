package com.web.ad.adProfile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.web.ApplicationMessage;

public class AdVsoProfileForm extends ApplicationMessage {

	private int vsoCode;
	
	
	
	
	public int getVsoCode() {
		return vsoCode;
	}
	public void setVsoCode(int vsoCode) {
		this.vsoCode = vsoCode;
	}




	
	private String vsoId;
	private String vsoName;
	//-- type
	private ArrayList vsoTypeLists =  new ArrayList();
	private String vsoTypeList;
	private String vsoType;
	//--
	
	private String vsoHeadName;
	private String vsoHeadPosition;
	private String vsoMandate;
	
	
	private String vsoTelephoneNum;
	private String vsoEmail;
	private String vsoFaxNum;
	private String vsoWebsite;
	
	private String vsoSecRegNum;
	private String vsoHeadOfficeAddress;
	private String vsoDateEstablished;
	private String vsoVsion;
	private String vsoMission;
	private String vsoGoals;
	private String vsoAnnualBudget;
	private String vsoBudgetSource;
	private String vsoBudgetSourceList;
	private ArrayList vsoBudgetSourceLists = new ArrayList();
	
	
	private String vsoRecruitment;
	private String vsoRecruitmentList;
	private ArrayList vsoRecruitmentLists = new ArrayList();
	
	
	private boolean vsoEngagement;
	public boolean vsoCapability;
	private boolean vsoPromotion;
	private boolean vsoNetworking;
	private boolean vsoOther1;
	private String vsoOtherNote1;

	private boolean vsoLiving;
	private boolean vsoMeal;
	private boolean vsoInsurance;
	private boolean vsoRecognition;
	private boolean vsoTravel;
	private boolean vsoHousing;
	private boolean vsoTraining;
	private boolean vsoOther2;
	private String vsoOtherNote2;
	
	
	private String vsoUserName;
	private String vsoUserPass;
	private String vsoUserPassConfirm;
	
	private String vsoUserPrimaryEmail;
	private String vsoUserSecondaryEmail;
	
	private String vsoCoordinatorName;
	private String vsoCoordinatorPosition;
	private String vsoCoordinatorTelNum;
	private String vsoCoordinatorDepartment;
	private String vsoCoordinatorMobileNum;
	
	
	public String getVsoId() {
		return vsoId;
	}
	public void setVsoId(String vsoId) {
		this.vsoId = vsoId;
	}
	public String getVsoName() {
		return vsoName;
	}
	public void setVsoName(String vsoName) {
		this.vsoName = vsoName;
	}
	public ArrayList getVsoTypeLists() {
		return vsoTypeLists;
	}
	public void setVsoTypeLists(ArrayList vsoTypeLists) {
		this.vsoTypeLists = vsoTypeLists;
	}
	public String getVsoTypeList() {
		return vsoTypeList;
	}
	public void setVsoTypeList(String vsoTypeList) {
		this.vsoTypeList = vsoTypeList;
	}
	public String getVsoType() {
		return vsoType;
	}
	public void setVsoType(String vsoType) {
		this.vsoType = vsoType;
	}
	public String getVsoHeadName() {
		return vsoHeadName;
	}
	public void setVsoHeadName(String vsoHeadName) {
		this.vsoHeadName = vsoHeadName;
	}
	public String getVsoHeadPosition() {
		return vsoHeadPosition;
	}
	public void setVsoHeadPosition(String vsoHeadPosition) {
		this.vsoHeadPosition = vsoHeadPosition;
	}
	public String getVsoMandate() {
		return vsoMandate;
	}
	public void setVsoMandate(String vsoMandate) {
		this.vsoMandate = vsoMandate;
	}
	public String getVsoTelephoneNum() {
		return vsoTelephoneNum;
	}
	public void setVsoTelephoneNum(String vsoTelephoneNum) {
		this.vsoTelephoneNum = vsoTelephoneNum;
	}
	public String getVsoEmail() {
		return vsoEmail;
	}
	public void setVsoEmail(String vsoEmail) {
		this.vsoEmail = vsoEmail;
	}
	public String getVsoFaxNum() {
		return vsoFaxNum;
	}
	public void setVsoFaxNum(String vsoFaxNum) {
		this.vsoFaxNum = vsoFaxNum;
	}
	public String getVsoWebsite() {
		return vsoWebsite;
	}
	public void setVsoWebsite(String vsoWebsite) {
		this.vsoWebsite = vsoWebsite;
	}
	public String getVsoSecRegNum() {
		return vsoSecRegNum;
	}
	public void setVsoSecRegNum(String vsoSecRegNum) {
		this.vsoSecRegNum = vsoSecRegNum;
	}
	public String getVsoHeadOfficeAddress() {
		return vsoHeadOfficeAddress;
	}
	public void setVsoHeadOfficeAddress(String vsoHeadOfficeAddress) {
		this.vsoHeadOfficeAddress = vsoHeadOfficeAddress;
	}
	public String getVsoDateEstablished() {
		return vsoDateEstablished;
	}
	public void setVsoDateEstablished(String vsoDateEstablished) {
		this.vsoDateEstablished = vsoDateEstablished;
	}
	public String getVsoVsion() {
		return vsoVsion;
	}
	public void setVsoVsion(String vsoVsion) {
		this.vsoVsion = vsoVsion;
	}
	public String getVsoMission() {
		return vsoMission;
	}
	public void setVsoMission(String vsoMission) {
		this.vsoMission = vsoMission;
	}
	public String getVsoGoals() {
		return vsoGoals;
	}
	public void setVsoGoals(String vsoGoals) {
		this.vsoGoals = vsoGoals;
	}
	public String getVsoAnnualBudget() {
		return vsoAnnualBudget;
	}
	public void setVsoAnnualBudget(String vsoAnnualBudget) {
		this.vsoAnnualBudget = vsoAnnualBudget;
	}
	public String getVsoBudgetSource() {
		return vsoBudgetSource;
	}
	public void setVsoBudgetSource(String vsoBudgetSource) {
		this.vsoBudgetSource = vsoBudgetSource;
	}
	
	
	public String getVsoBudgetSourceList() {
		return vsoBudgetSourceList;
	}
	public void setVsoBudgetSourceList(String vsoBudgetSourceList) {
		this.vsoBudgetSourceList = vsoBudgetSourceList;
	}
	public ArrayList getVsoBudgetSourceLists() {
		return vsoBudgetSourceLists;
	}
	public void setVsoBudgetSourceLists(ArrayList vsoBudgetSourceLists) {
		this.vsoBudgetSourceLists = vsoBudgetSourceLists;
	}
	
	
	public String getVsoRecruitment() {
		return vsoRecruitment;
	}
	public void setVsoRecruitment(String vsoRecruitment) {
		this.vsoRecruitment = vsoRecruitment;
	}
	public String getVsoRecruitmentList() {
		return vsoRecruitmentList;
	}
	public void setVsoRecruitmentList(String vsoRecruitmentList) {
		this.vsoRecruitmentList = vsoRecruitmentList;
	}
	public ArrayList getVsoRecruitmentLists() {
		return vsoRecruitmentLists;
	}
	public void setVsoRecruitmentLists(ArrayList vsoRecruitmentLists) {
		this.vsoRecruitmentLists = vsoRecruitmentLists;
	}
	public boolean isVsoEngagement() {
		return vsoEngagement;
	}
	public void setVsoEngagement(boolean vsoEngagement) {
		this.vsoEngagement = vsoEngagement;
	}
	public boolean isVsoCapability() {
		return vsoCapability;
	}
	public void setVsoCapability(boolean vsoCapability) {
		this.vsoCapability = vsoCapability;
	}
	public boolean isVsoPromotion() {
		return vsoPromotion;
	}
	public void setVsoPromotion(boolean vsoPromotion) {
		this.vsoPromotion = vsoPromotion;
	}
	public boolean isVsoNetworking() {
		return vsoNetworking;
	}
	public void setVsoNetworking(boolean vsoNetworking) {
		this.vsoNetworking = vsoNetworking;
	}
	public boolean isVsoOther1() {
		return vsoOther1;
	}
	public void setVsoOther1(boolean vsoOther1) {
		this.vsoOther1 = vsoOther1;
	}
	
	
	public String getVsoOtherNote1() {
		return vsoOtherNote1;
	}
	public void setVsoOtherNote1(String vsoOtherNote1) {
		this.vsoOtherNote1 = vsoOtherNote1;
	}
	public boolean isVsoLiving() {
		return vsoLiving;
	}
	public void setVsoLiving(boolean vsoLiving) {
		this.vsoLiving = vsoLiving;
	}
	public boolean isVsoMeal() {
		return vsoMeal;
	}
	public void setVsoMeal(boolean vsoMeal) {
		this.vsoMeal = vsoMeal;
	}
	public boolean isVsoInsurance() {
		return vsoInsurance;
	}
	public void setVsoInsurance(boolean vsoInsurance) {
		this.vsoInsurance = vsoInsurance;
	}
	public boolean isVsoRecognition() {
		return vsoRecognition;
	}
	public void setVsoRecognition(boolean vsoRecognition) {
		this.vsoRecognition = vsoRecognition;
	}
	public boolean isVsoTravel() {
		return vsoTravel;
	}
	public void setVsoTravel(boolean vsoTravel) {
		this.vsoTravel = vsoTravel;
	}
	public boolean isVsoHousing() {
		return vsoHousing;
	}
	public void setVsoHousing(boolean vsoHousing) {
		this.vsoHousing = vsoHousing;
	}
	public boolean isVsoTraining() {
		return vsoTraining;
	}
	public void setVsoTraining(boolean vsoTraining) {
		this.vsoTraining = vsoTraining;
	}
	public boolean isVsoOther2() {
		return vsoOther2;
	}
	public void setVsoOther2(boolean vsoOther2) {
		this.vsoOther2 = vsoOther2;
	}
	
	public String getVsoOtherNote2() {
		return vsoOtherNote2;
	}
	public void setVsoOtherNote2(String vsoOtherNote2) {
		this.vsoOtherNote2 = vsoOtherNote2;
	}
	public String getVsoUserName() {
		return vsoUserName;
	}
	public void setVsoUserName(String vsoUserName) {
		this.vsoUserName = vsoUserName;
	}
	public String getVsoUserPass() {
		return vsoUserPass;
	}
	public void setVsoUserPass(String vsoUserPass) {
		this.vsoUserPass = vsoUserPass;
	}
	public String getVsoUserPassConfirm() {
		return vsoUserPassConfirm;
	}
	public void setVsoUserPassConfirm(String vsoUserPassConfirm) {
		this.vsoUserPassConfirm = vsoUserPassConfirm;
	}
	public String getVsoUserPrimaryEmail() {
		return vsoUserPrimaryEmail;
	}
	public void setVsoUserPrimaryEmail(String vsoUserPrimaryEmail) {
		this.vsoUserPrimaryEmail = vsoUserPrimaryEmail;
	}
	public String getVsoUserSecondaryEmail() {
		return vsoUserSecondaryEmail;
	}
	public void setVsoUserSecondaryEmail(String vsoUserSecondaryEmail) {
		this.vsoUserSecondaryEmail = vsoUserSecondaryEmail;
	}
	public String getVsoCoordinatorName() {
		return vsoCoordinatorName;
	}
	public void setVsoCoordinatorName(String vsoCoordinatorName) {
		this.vsoCoordinatorName = vsoCoordinatorName;
	}
	public String getVsoCoordinatorPosition() {
		return vsoCoordinatorPosition;
	}
	public void setVsoCoordinatorPosition(String vsoCoordinatorPosition) {
		this.vsoCoordinatorPosition = vsoCoordinatorPosition;
	}
	public String getVsoCoordinatorTelNum() {
		return vsoCoordinatorTelNum;
	}
	public void setVsoCoordinatorTelNum(String vsoCoordinatorTelNum) {
		this.vsoCoordinatorTelNum = vsoCoordinatorTelNum;
	}
	public String getVsoCoordinatorDepartment() {
		return vsoCoordinatorDepartment;
	}
	public void setVsoCoordinatorDepartment(String vsoCoordinatorDepartment) {
		this.vsoCoordinatorDepartment = vsoCoordinatorDepartment;
	}
	public String getVsoCoordinatorMobileNum() {
		return vsoCoordinatorMobileNum;
	}
	public void setVsoCoordinatorMobileNum(String vsoCoordinatorMobileNum) {
		this.vsoCoordinatorMobileNum = vsoCoordinatorMobileNum;
	}
	
	public void populateRecruitment(){
		vsoRecruitment = "0";
		
		vsoRecruitmentLists = new ArrayList();
		vsoRecruitmentLists.add("Foreign");
		vsoRecruitmentLists.add("Local");
	}
	
	public void populateType(){
		vsoTypeLists = new ArrayList();
		vsoTypeLists.add("International Government Organization/ Program");
		vsoTypeLists.add("International Non-Government Organization");
		vsoTypeLists.add("Local Organization");
		
		vsoType = "0";
	}
	
	public void populateBudgetSourcr() {
		vsoBudgetSourceLists = new ArrayList();
		vsoBudgetSourceLists.add("Government");
		vsoBudgetSourceLists.add("Grants / Donations (private)");
		vsoBudgetSourceLists.add("Self Generating Income");
		vsoBudgetSourceLists.add("Others");
	}
	
	public void reset(){
		
		clearCountryLists();
		clearRegionLists();
		clearProvinceLists();
		clearMunicipalityLists();
		
		  vsoId = "";
		  vsoName= "";
		  vsoRecruitmentList = "";
		  vsoTypeList = "";
		  populateType();
		  populateRecruitment();
		 
		
		  vsoHeadName= "";
		  vsoHeadPosition= "";
		  vsoMandate= "";
		
		
		  vsoTelephoneNum= "";
		  vsoEmail= "";
		  vsoFaxNum= "";
		  vsoWebsite= "";
		
		  vsoSecRegNum= "";
		  vsoHeadOfficeAddress= "";
		  vsoDateEstablished= "";
		  vsoVsion= "";
		  vsoMission= "";
		  vsoGoals= "";
		  vsoAnnualBudget= "";
		  vsoBudgetSource= "";
		  vsoBudgetSourceList="0";
		  populateBudgetSourcr();
		 
		  
		
		
		  vsoEngagement = false;
		  vsoCapability = false;
		  vsoPromotion = false;
		  vsoNetworking = false;
		  vsoOther1 = false;
		  vsoOtherNote1 = "";

		  vsoLiving = false;
		  vsoMeal = false;
		  vsoInsurance = false;
		  vsoRecognition = false;
		  vsoTravel = false;
		  vsoHousing = false;
		  vsoTraining = false;
		  vsoOther2 = false;
		  vsoOtherNote2="";
		
		
		  vsoUserName= "";
		  vsoUserPass= "";
		  vsoUserPassConfirm= "";
		
		  vsoUserPrimaryEmail= "";
		  vsoUserSecondaryEmail= "";
		
		  vsoCoordinatorName= "";
		  vsoCoordinatorPosition= "";
		  vsoCoordinatorTelNum= "";
		  vsoCoordinatorDepartment= "";
		  vsoCoordinatorMobileNum= "";
		
		
		
	}
	

	
}
