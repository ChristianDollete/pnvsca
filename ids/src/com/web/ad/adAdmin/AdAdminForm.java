package com.web.ad.adAdmin;

import com.web.ApplicationMessage;

public class AdAdminForm extends ApplicationMessage{
	
	private String adUserName;
	private String adUserDesc;
	private String adUserPassword;
	private String adUserConfirmPassword;
	
	private String adEmailAddress;
	private String adEmailPassword;
	private String adEmailConfirmPassword;
	
	
	public String getAdUserName() {
		return adUserName;
	}
	public void setAdUserName(String adUserName) {
		this.adUserName = adUserName;
	}
	
	public String getAdUserDesc() {
		return adUserDesc;
	}
	public void setAdUserDesc(String adUserDesc) {
		this.adUserDesc = adUserDesc;
	}
	public String getAdUserPassword() {
		return adUserPassword;
	}
	public void setAdUserPassword(String adUserPassword) {
		this.adUserPassword = adUserPassword;
	}
	public String getAdUserConfirmPassword() {
		return adUserConfirmPassword;
	}
	public void setAdUserConfirmPassword(String adUserConfirmPassword) {
		this.adUserConfirmPassword = adUserConfirmPassword;
	}
	
	
	
	
	public String getAdEmailAddress() {
		return adEmailAddress;
	}
	public void setAdEmailAddress(String adEmailAddress) {
		this.adEmailAddress = adEmailAddress;
	}
	public String getAdEmailPassword() {
		return adEmailPassword;
	}
	public void setAdEmailPassword(String adEmailPassword) {
		this.adEmailPassword = adEmailPassword;
	}
	public String getAdEmailConfirmPassword() {
		return adEmailConfirmPassword;
	}
	public void setAdEmailConfirmPassword(String adEmailConfirmPassword) {
		this.adEmailConfirmPassword = adEmailConfirmPassword;
	}
	public void reset(){
		adUserName=null;
		adUserDesc=null;
		adUserPassword=null;
		adUserConfirmPassword=null;
		adEmailAddress=null;
		adEmailPassword=null;
		adEmailConfirmPassword=null;
	}
	

}
