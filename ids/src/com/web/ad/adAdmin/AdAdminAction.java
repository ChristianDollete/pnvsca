package com.web.ad.adAdmin;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.bean.AdUserBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdUserControllerBean;
import com.web.UserSession;
import com.web.util.AdUserModDetails;

public class AdAdminAction extends ActionSupport{
	private AdAdminForm frm;
	public AdAdminForm getFrm() {return frm;}
	public void setFrm(AdAdminForm frm) {this.frm = frm;}
	
	
	public String execute() {
		System.out.println("AdAdminAction execute");
		String rtrn="failed"; 
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			System.out.println("session ID >> "+ request.getSession().getId());
			UserSession user = (UserSession)session.getAttribute(request.getSession().getId());
			
			if(frm.getLink().toString().equalsIgnoreCase("10")){
				System.out.println("AdAdminAction Load userName >> "+user.getUserName() +" "+user.getUserDescription() +" | session ID >> "+user.getSessionId());
				frm.reset();
				
				AdUserControllerBean usrCB = new AdUserControllerBean();
				AdUserModDetails usrMD = usrCB.getAdmin(1);
				
				frm.setAdUserName(usrMD.getUsr_usr_nm());
				frm.setAdUserDesc(usrMD.getUsr_usr_dscrptn());
				frm.setAdUserPassword(usrMD.getUsr_usr_psswrd());
				frm.setAdUserConfirmPassword(null);
				frm.setAdEmailAddress(usrMD.getUsr_email());
				frm.setAdEmailPassword(usrMD.getUsr_email_psswrd());
				frm.setAdEmailConfirmPassword(null);
				
				rtrn="load"; 
				
			
			}else if (frm.getBtnName().equalsIgnoreCase("Save")){
				System.out.println("AdAdminAction execute Save >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
				
				AdUserModDetails admin = new AdUserModDetails();
				admin.setUsr_usr_nm(frm.getAdUserName());
				admin.setUsr_usr_dscrptn(frm.getAdUserDesc());
				admin.setUsr_usr_psswrd(frm.getAdUserPassword());
				admin.setUsr_id(user.getUserCode());
				admin.setUsr_usr_prmry_email(frm.getAdEmailAddress());
				admin.setUsr_usr_scndry_email("");
				admin.setUsr_email(frm.getAdEmailAddress());
				admin.setUsr_email_psswrd(frm.getAdEmailConfirmPassword());
				admin.setAd_cmpny(1);
				AdUserControllerBean usrCB = new AdUserControllerBean();
				usrCB.update(admin);
				
				AdEmailControllerBean emlCB = new AdEmailControllerBean();
				ArrayList list = emlCB.getAllEmail();
				Iterator i = list.iterator();
				while(i.hasNext()){
					AdEmailBean emlBean = (AdEmailBean)i.next();
					AdEmailControllerBean emlCB2 = new AdEmailControllerBean();
					emlBean.setEmlEmailAddress(admin.getUsr_email());
					emlBean.setEmlEmailPassword(admin.getUsr_email_psswrd());
					emlCB2.update(emlBean);
				}
				//frm.reset();
				frm.setAppMessage("PNVSCA Account has been SAVE/UPDATE.");
				rtrn="load";
			}else{
				
				rtrn="load";
				
			}
			
		} catch (Exception e) {
		
			rtrn="failed";
			e.printStackTrace();

		}
		
		
		
		return rtrn;
		
	}
	

}
