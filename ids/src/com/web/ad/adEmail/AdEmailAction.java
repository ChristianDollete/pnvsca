package com.web.ad.adEmail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.ad.adMain.AdMainAction;

public class AdEmailAction extends ActionSupport{
	private AdEmailForm frm;
	public AdEmailForm getFrm() {return frm;}
	public void setFrm(AdEmailForm frm) {this.frm = frm;}
	
	
	private String reset() throws Exception{
		System.out.println("AdEmailAction reset");
		
		String rtrn = "failed";
		
		frm.reset();
		frm.clearEmailLine();
		
		
		
		try {
			AdEmailControllerBean emlCB = new AdEmailControllerBean();
			ArrayList emlList = emlCB.getAllEmail();
			if(emlList.size()==0){
				frm.setAppMessage("No Emails is set-up");
			}else{
				Iterator i = emlList.iterator();
				while(i.hasNext()){
					AdEmailBean emlBean = (AdEmailBean)i.next();
					AdEmailLine emlLine = new AdEmailLine();
					emlLine.setEmlLineCd(emlBean.getEmlCd());
					emlLine.setEmlLineApplication(emlBean.getEmlApplication());
					emlLine.setEmlLineDefaultSubject(emlBean.getEmlDefaultSubject());
					emlLine.setEmlLineName(emlBean.getEmlName());
					String enble = "No";
					if(emlBean.getEmlEnabled()>0){
						enble="Yes";
					}
					emlLine.setEmlLineEnabled(enble);
					frm.setEmailEmailLine(emlLine);
				}
				
			}
			rtrn = "success";
		} catch (Exception e) {
			System.out.println("AdEmailAction reset Exception");
			e.printStackTrace();
			rtrn = "failed";
			throw new Exception();
		}
		
		
		return rtrn;
	}
	
	
	public String execute() {
		System.out.println("AdEmailAction execute");
		String rtrn="failed"; 
		try {
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				System.out.println("frm.getBtnName() >> "+ frm.getBtnName());
				System.out.println("frm.getLink() >> "+ frm.getLink());
				AdEmailControllerBean emlCB = new AdEmailControllerBean();
				if(frm.getLink().toString().equalsIgnoreCase("12")){
					System.out.println("AdEmailAction Load userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					String resetReturn = this.reset();
					rtrn="load"; 

				}else if (frm.getLink().toString().equalsIgnoreCase("121")){
					System.out.println("AdEmailAction execute Force out >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					int eml_cd = frm.getEmlCd();
					String resetReturn = this.reset();
					
					
					AdEmailBean emlBean = emlCB.getEmail(eml_cd);
					frm.setEmlCd(emlBean.getEmlCd());
					frm.setEmlApplication(emlBean.getEmlApplication());
					frm.setEmlDefaultSubject(emlBean.getEmlDefaultSubject());
					frm.setEmlDescription(emlBean.getEmlDescription());
					frm.setEmlEnabled(common.IntToBoolean(emlBean.getEmlEnabled()));
					frm.setEmlName(emlBean.getEmlName());
					
					rtrn="load"; 
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("AdEmailAction execute Save >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					int eml_cd = frm.getEmlCd();
					
					if(eml_cd>0){
						
						AdEmailBean emlBean = new AdEmailBean();
						emlBean.setEmlCd(eml_cd);
						emlBean.setEmlApplication(frm.getEmlApplication());
						emlBean.setEmlDefaultSubject(frm.getEmlDefaultSubject());
						emlBean.setEmlDescription(frm.getEmlDescription());
						emlBean.setEmlName(frm.getEmlName());
						emlBean.setEmlEnabled(common.booleanToInt(frm.isEmlEnabled()));
						emlCB.update(emlBean);
						
						this.reset();
						frm.setAppMessage("Email Entry has been Updated.");
						
						
					}else{
						this.reset();
						frm.setAppMessage("No Email has been selected!");
					}
					
					rtrn="load";
				}else{
					System.out.println("AdEmailAction execute ELSE >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="load";
					
				}
				
			}else{
				rtrn="login";
			}
			
			
		} catch (Exception e) {
		
			rtrn="failed";
			e.printStackTrace();

		}
		
		
		
		return rtrn;
		
	}
	
	public String validateFrm(AdEmailForm frm){
System.out.println("AdEmailAction execute validateFrm");
		
		String errMsg = "";
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
	}
	

}
