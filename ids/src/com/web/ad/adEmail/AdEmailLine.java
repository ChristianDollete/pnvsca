package com.web.ad.adEmail;

import com.web.ApplicationMessage;

public class AdEmailLine {

	private int emlLineCd;
	private String emlLineName;
	private String emlLineDefaultSubject;
	private String emlLineDescription;
	private String emlLineApplication;
	private String emlLineEnabled;


	public int getEmlLineCd() {
		return emlLineCd;
	}

	public void setEmlLineCd(int emlLineCd) {
		this.emlLineCd = emlLineCd;
	}

	public String getEmlLineName() {
		return emlLineName;
	}

	public void setEmlLineName(String emlLineName) {
		this.emlLineName = emlLineName;
	}

	public String getEmlLineDefaultSubject() {
		return emlLineDefaultSubject;
	}

	public void setEmlLineDefaultSubject(String emlLineDefaultSubject) {
		this.emlLineDefaultSubject = emlLineDefaultSubject;
	}

	public String getEmlLineDescription() {
		return emlLineDescription;
	}

	public void setEmlLineDescription(String emlLineDescription) {
		this.emlLineDescription = emlLineDescription;
	}

	public String getEmlLineApplication() {
		return emlLineApplication;
	}

	public void setEmlLineApplication(String emlLineApplication) {
		this.emlLineApplication = emlLineApplication;
	}

	public String getEmlLineEnabled() {
		return emlLineEnabled;
	}

	public void setEmlLineEnabled(String emlLineEnabled) {
		this.emlLineEnabled = emlLineEnabled;
	}

}
