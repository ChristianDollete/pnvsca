package com.web.ad.adEmail;

import java.util.ArrayList;

import com.web.ApplicationMessage;
import com.web.lpi.lpiProjectFind.LpiProjectFindEntryLine;

public class AdEmailForm extends ApplicationMessage {
	private int emlCd;
	private String emlName;
	private String emlDefaultSubject;
	private String emlDescription;
	private String emlApplication;
	private boolean emlEnabled;

	private ArrayList emailEmailLine = new ArrayList();

	public int getEmlCd() {
		return emlCd;
	}

	public void setEmlCd(int emlCd) {
		this.emlCd = emlCd;
	}

	public String getEmlName() {
		return emlName;
	}

	public void setEmlName(String emlName) {
		this.emlName = emlName;
	}

	public String getEmlDefaultSubject() {
		return emlDefaultSubject;
	}

	public void setEmlDefaultSubject(String emlDefaultSubject) {
		this.emlDefaultSubject = emlDefaultSubject;
	}

	public String getEmlDescription() {
		return emlDescription;
	}

	public void setEmlDescription(String emlDescription) {
		this.emlDescription = emlDescription;
	}

	public String getEmlApplication() {
		return emlApplication;
	}

	public void setEmlApplication(String emlApplication) {
		this.emlApplication = emlApplication;
	}

	public boolean isEmlEnabled() {
		return emlEnabled;
	}

	public void setEmlEnabled(boolean emlEnabled) {
		this.emlEnabled = emlEnabled;
	}

	public void clearEmailLine(){
		this.emailEmailLine = new ArrayList();
	}
	public ArrayList getEmailEmailLine() {
		return emailEmailLine;
	}

	public void setEmailEmailLine(AdEmailLine emailEmailLine) {
		this.emailEmailLine.add(emailEmailLine);
	}
	
	
	public void reset(){
		emlCd=0;
		emlName=null;
		emlDefaultSubject=null;
		emlDescription=null;
		emlApplication=null;
		emlEnabled=false;
	}

}
