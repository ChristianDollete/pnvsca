package com.web.ad.adLogon;

import java.io.File;

import com.web.ApplicationMessage;

public class AdLogonForm extends ApplicationMessage{

	private String cmpShortName;
	private String userName;
	private String userPassword;

	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	

	public String getCmpShortName() {
		return cmpShortName;
	}

	public void setCmpShortName(String cmpShortName) {
		this.cmpShortName = cmpShortName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	
	public void reset(){
		cmpShortName = "";
		userName = "";
		userPassword = "";

		myFile = new File("");
		myFileContentType = "";
		myFileFileName = "";
	}

}
