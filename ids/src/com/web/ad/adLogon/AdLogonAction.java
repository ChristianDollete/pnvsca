package com.web.ad.adLogon;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.AdUserResponsibilityControllerBean;
import com.web.UserAutoEmailSession;
import com.web.UserSession;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.AdUserModDetails;
import com.web.util.AdUserResponsibilityModDetails;
import com.web.vso.vsoSignup.VsoSignupForm;
import com.web.zreport.Annex_A.AnnexAPrintAction;

public class AdLogonAction  extends ActionSupport{
	
	private AdLogonForm frm;
	public AdLogonForm getFrm() {return frm;}
	public void setFrm(AdLogonForm frm) {this.frm = frm;}
	
	private LpiSignupForm lpifrm;
	public LpiSignupForm getLpifrm(){return lpifrm;}
	public void setLpiFrm(LpiSignupForm lpifrm){this.lpifrm = lpifrm;}
	
	private VsoSignupForm vsofrm;
	public VsoSignupForm getVsofrm(){return vsofrm;}
	public void setVsoFrm(VsoSignupForm vsofrm){this.vsofrm = vsofrm;}
	
	
	
	public String execute() {
		System.out.println("AdLogonAction execute");
		
		
		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest)ServletActionContext.getRequest();
		String requestId = request.getSession().getId();
		
		/*
		HttpSession session = (HttpSession)ServletActionContext.getContext();// .getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest)ServletActionContext.getRequest();*/
		
		String rtrn="failed";
		//HttpSession session = (HttpSession)ActionContext.getContext().get(ServletActionContext.getRequest().getSession().getId());
		try {
			
			if(frm.getLink()!=null && frm.getLink().equals("1")){
				System.out.println("LOAD LPI SIGN-UP");
				rtrn="lpiLoad";
			}else if(frm.getLink()!=null && frm.getLink().equals("2")){
				System.out.println("LOAD VSO SIGN-UP");
				rtrn="vsoLoad";
			}else if(frm.getLink()!=null && frm.getLink().equals("3")){
				System.out.println("LOAD LPI SIGN-UP");
				rtrn="lpiLoad";
			}else if(frm.getLink()!=null && frm.getLink().equals("0")){
				System.out.println("LOAD LOG-ON");
				rtrn="onload";
			}else{
				System.out.println("LOAD LOG-IN PROCESS");
				
				
				try {
					AdLogonControllerBean logCBean = new AdLogonControllerBean();
					AdUserModDetails usrDetails = null;
					System.out.println("company >> " + frm.getCmpShortName().toString());
					System.out.println("username >> " + frm.getUserName().toString());
					System.out.println("password >> " + frm.getUserPassword().toString());
					
					
					try {
						usrDetails = logCBean.excecuteLogin(frm.getCmpShortName(), frm.getUserName(), frm.getUserPassword());
						if(usrDetails.getUsr_id()>0){
							
							
							Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");	 
							
							if (sessionsMap == null) {
								sessionsMap = new Hashtable();
								System.out.println("SESSION NEW >> "+ requestId);
							}else{
								if(sessionsMap.containsKey(requestId)){
									sessionsMap.remove(requestId);
									System.out.println("SESSION NOT NULL >> "+ requestId);
								}
							}
							
							System.out.println("session >> "+requestId);
							UserSession userSession = new UserSession();
							userSession.setSessionId(requestId);
							userSession.setUserFName(usrDetails.getUsr_usr_dscrptn());
							userSession.setUserCmpCode(usrDetails.getAd_cmpny());
							userSession.setUserName(usrDetails.getUsr_usr_nm());
							userSession.setUserCmpName(frm.getCmpShortName());
							userSession.setUserDescription(usrDetails.getUsr_usr_dscrptn());
							/*
							userSession.setUserMName("MName");
							userSession.setUserLName("LName");
							userSession.setUserDescription("UserDescription");*/
							userSession.setUserResCode(usrDetails.getUsr_id());
							userSession.setUserCode(usrDetails.getUsr_id());
							
							AdUserResponsibilityControllerBean adResC = new AdUserResponsibilityControllerBean();
							AdUserResponsibilityModDetails adRes = adResC.getUserRes(usrDetails.getUsr_id());
							userSession.setPrjct(adRes.getRes_lpi_prjct());
							userSession.setPrjctMngr(adRes.getRes_lpi_prjct_mngr());
							userSession.setRqstVlntr(adRes.getRes_lpi_rqst_vlntr());
							userSession.setVlntr(adRes.getRes_vso_vlntr());
							
							
							//User Profile
							userSession.setUserProfile(usrDetails.getAd_usr_clsfctn());
							
							if(userSession.getUserProfile()==1){
								int lpiNo = logCBean.getAllLpiSize();
								userSession.setLpiNo(lpiNo);
								
								int vsoNo = logCBean.getAllVsoSize();
								userSession.setVsoNo(vsoNo);
							}
							
							
							
							//ActionContext.getContext().put(requestId, userSession);
							session.setAttribute(session.getId(), userSession);
							
							sessionsMap.put(requestId, userSession);
							
							// Store sessions to servlet context	   	
							System.out.println("AdLogonAction Load userName >> "+userSession.getUserName() +"  | session ID >> "+userSession.getSessionId());
							ServletActionContext.getServletContext().setAttribute("sessionKey", sessionsMap);
							
							
							
							// USER LOG IN NOTIFICATION
							/*
							UserAutoEmailSession email = new UserAutoEmailSession();
							String Body = "Your account has been login";
							email.SendEmail("chan.dollete08@gmail.com", "PNVSCA LOG-IN Notification", Body);
							*/
						
							
						   	
							rtrn = "success";
							
							
							
						}else{
							frm.setAppMessage("Invalid Institute Code , User Name or Password.");
							rtrn="onload";
						}
					
						
						
					} catch (NullPointerException e){
						rtrn="onload";
						frm.setAppMessage("Invalid Institute Code , User Name or Password.");
					} catch (Exception e) {
						rtrn="onload";
						throw new Exception(e);
					}
					
				} catch (Exception e) {
					rtrn="onload";
					e.printStackTrace();
					frm.setAppMessage("Invalid Institute Code , User Name or Password.");
					
				}
				
				
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			rtrn="onload";
		}
		return rtrn;
	}
	
	
	public void test(){
		
		if(frm.getUserName().toString().equalsIgnoreCase(frm.getUserPassword().toString())){
		//	System.out.println("session >> "+session.getId());
		//	UserSession userSession = new UserSession();
		//	userSession.setSessionId(session.getId());
		//	userSession.setUserFName("Fname");
		//	userSession.setUserMName("MName");
		//	userSession.setUserLName("LName");
		//	userSession.setUserDescription("UserDescription");
			
			try {
				String destPath = "C:/opt/";
				System.out.println("Src File name: " + frm.getMyFile());
				System.out.println("Dst File name: " + frm.getMyFileFileName());
				File destFile  = new File(destPath, frm.getMyFileFileName());
				FileUtils.copyFile(frm.getMyFile(), destFile);
				
			} catch (Exception e1) {  
				System.out.println("INVALID FILE EXCEPTION");
				//e1.printStackTrace();
			}
			//this.AutoEmail(/*userEmailAdd, userName, userPass, userEmailAddTo, userEmailSubject, userEmailBody*/);
		//	rtrn = "success";  
		}
		
	}

	private void AutoEmail(/*String userEmailAdd,String userName,String userPass,String userEmailAddTo,String userEmailSubject,String userEmailBody*/){
		 String userEmailAdd = "";
		 String userName = "";
		 String userPass = "";
		 String userEmailAddTo = "dollete.christian@yahoo.com";
		 String userEmailSubject =  "TEST EMAIL 2";
		 String userEmailBody = "TEST EMAIL BODY 2";
		 
		try {
			UserAutoEmailSession email = new UserAutoEmailSession();
			String emailsent = email.UserAutoEmailSession(userEmailAdd, userName,userPass , 
					userEmailAddTo ,userEmailSubject, userEmailBody);
			System.out.println("EMAIL SENT >> "+ emailsent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
