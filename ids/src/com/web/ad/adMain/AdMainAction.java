package com.web.ad.adMain;

import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.web.UserSession;

public class AdMainAction extends ActionSupport{
	private AdMainForm frm;
	public AdMainForm getFrm() {return frm;}
	public void setFrm(AdMainForm frm) {this.frm = frm;}
	
	
	public String execute() {
		System.out.println("AdMainAction execute");
		String rtrn="failed"; 
		try {
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				rtrn = "onLoad";
				if(frm.getLink().toString().equalsIgnoreCase("1")){
					System.out.println("AdMainAction execute LOG-OUT userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					sessionsMap.remove(user.getSessionId());
					rtrn="logout"; 
				}else if(frm.getLink().toString().equalsIgnoreCase("2")){
					System.out.println("AdMainAction execute Home");
					rtrn="home";
				}else{
					rtrn="home";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			rtrn="failed";
			e.printStackTrace();
		}
		
		return rtrn;
		
	}
	
	public String logout(HttpSession session, String sessionId){
		System.out.println("AdMainAction execute logout");
		String rtrn="failed";
		
		try {
			
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			if(sessionsMap.get(sessionId)!=null){
				System.out.println("SESSION ID IS NOT NULL >> "+sessionId);
				sessionsMap.remove(sessionId);
			}
			rtrn="logout";
		} catch (Exception e) {
			rtrn="failed";
			e.printStackTrace();
		} 
		
		
		
		return rtrn;
	}
	

}
