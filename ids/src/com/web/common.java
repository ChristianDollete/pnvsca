package com.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class common {
	
	
	 public String userEmailAdd = "";
	 public String userName = "";
	 public String userPass = "";
	 public String userEmailAddTo = "dollete.christian@yahoo.com";
	 public String userEmailSubject =  "TEST EMAIL 2";
	 public String userEmailBody = "TEST EMAIL BODY 2";
	 

	public static String incrementStringNumber(String stringNumber) {
		String strNumberExtract = stringNumber.replaceFirst(".*\\D(\\d+)\\D*$","$1");
		long num;
		try {
			num = Long.parseLong(strNumberExtract);
		} catch (NumberFormatException ex) {
			return strNumberExtract + "1";
		}
		String strIncrementedNumberExtract = Long.toString(++num);
		int diffLen = strNumberExtract.length()	- strIncrementedNumberExtract.length();
		String zeroPadding = "";
		if (diffLen > 0)
			for (int i = 0; i < diffLen; i++)
				zeroPadding = zeroPadding + "0";
		return stringNumber.replaceFirst("\\d+(\\D*)$", zeroPadding+ strIncrementedNumberExtract + "$1");

	}
	
	public static int booleanToInt(boolean x){
		int y = 0;
		
		if(x==true){
			y=1;
		}
		return y;
		
	}
	
	public static boolean IntToBoolean(int x){
		boolean y = false;
		
		if(x==1){
			y=true;
		}
		return y;
		
	}
	
	public static boolean validateEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
	}
	
	  public static boolean validateDateFormat(String strDate){
		   if(strDate != null  && strDate.length() >= 1){
			   SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			   sdf.setLenient(false);
			   try{
				   Date dt = sdf.parse(strDate.trim());
				   return	(true);
			   }catch(ParseException e){
				   return(false);
			   }
		   }else{
			   return(true);
		   }
	   }

}
