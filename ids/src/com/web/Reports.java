package com.web;

import net.sf.jasperreports.engine.JasperPrint;


public class Reports implements java.io.Serializable{
   
   public Reports() { ; }
   
   private byte[] bytes;
   private String viewType;
   private JasperPrint jasperPrint;

   public byte[] getBytes(){
      return(bytes);
   }

   public void setBytes(byte[] bytes){
      this.bytes = bytes;
   }
   
   public String getViewType() {
      return(viewType);   	     	     	
   }
   
   public void setViewType(String viewType) {
   	  this.viewType = viewType;   	
   }
   
  public JasperPrint getJasperPrint() {
   	  return(jasperPrint);
   }
   
   public void setJasperPrint(JasperPrint jasperPrint){
   	  this.jasperPrint = jasperPrint;   	
   }
   
}
