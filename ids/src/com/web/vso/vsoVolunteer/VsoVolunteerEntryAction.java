package com.web.vso.vsoVolunteer;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.LpiRequestVolunteerLineControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.web.UserAutoEmailSession;
import com.web.UserSession;
import com.web.common;
import com.web.lpi.lpiProjectManager.LpiProjectManagerEntryForm;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;
import com.web.util.VsoVsoModDetails;

public class VsoVolunteerEntryAction extends ActionSupport {
	
	private VsoVolunteerEntryForm frm;
	public VsoVolunteerEntryForm getFrm() {return frm;}
	public void setFrm(VsoVolunteerEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		frm.reset();
	}
	
	public String execute() {
		System.out.println("VsoVolunteerEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				
				if (frm.getLink()!=null && frm.getLink().equals("66")){
					System.out.println("VsoVolunteerEntryAction execute LOAD");
					
					System.out.println("VLNTR CD >> "+frm.getVlntrCd());
					VsoVolunteerEntryControllerBean vlC = new VsoVolunteerEntryControllerBean();
					VsoVolunteerModDetails details = vlC.getByVlntrCd(frm.getVlntrCd());
					this.reload();
					frm.setVlntrCd(details.getVlntr_cd());
					frm.setVlntrId(details.getVlntr_id());
					frm.setVlntrFname(details.getVlntr_fname());
					frm.setVlntrMname(details.getVlntr_mname());
					frm.setVlntrLname(details.getVlntr_lname());
					frm.setGender(details.getVlntr_sex());
					frm.setVlntrNtnly(details.getVlntr_ntnly());
					frm.setVlntrCstatus(details.getVlntr_cvl_stts());
					frm.setVlntrDateOfBirth(details.getVlntr_brth_dt());
					frm.setVlntrVisaExpiration(details.getVlntr_vs_exprtn());
					
					frm.setVlntrUserName(details.getVlntr_usr_nm());
					frm.setVlntrUserEmail(details.getVlntr_usr_email());
					frm.setVlntrUserSecondEmail(details.getVlntr_usr_scnd_email());
					frm.setPrjCd(details.getVlntr_prjct_cd());
					frm.setVlntr_usr_cd(details.getUsr_cd());
					
					
					frm.setVlntrPrjct("");
					frm.setVlntrPrjPm("");
					frm.setVlntrPrjDateFrom("");
					frm.setVlntrPrjDateTo("");
					frm.setVlntr_usr_cd(details.getUsr_cd());
					
					try {
						LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
						LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(details.getVlntr_cd());
						int rv_cd = rvlD.getRvl_rv_cd();
						
						LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
						LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd,0);
						LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
						LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
						frm.setVlntrPrjct(prjMD.getPrjOrgName());
						frm.setVlntrPrjDateFrom(prjMD.getPrjDurationFrom());
						frm.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
						
						LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
						LpiProjectManagerModDetails pmMD = pmC.getPmByPmCode(prjMD.getPrjProjectManager());
						frm.setVlntrPrjPm(pmMD.getPmName());
						
					} catch (Exception e) {
						e.printStackTrace();
					}

					rtrn = "onLoad";
				
				}else if ((frm.getLink() != null && frm.getLink().equals("6")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("VsoVolunteerEntryAction execute LOAD");
					this.reload();
					rtrn = "onLoad";
					
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					
					
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					VsoVsoModDetails vsoMD = logonCB.getVsoByVsoCode(user.getUserCmpCode());
					
					AdEmailControllerBean emailCB = new AdEmailControllerBean();
					AdEmailBean emlBean = emailCB.getEmail(8);
					
					String errmsg = this.validateFrm(frm, user);
					if(errmsg.equalsIgnoreCase("")){
						String msg = "";
						
						VsoVolunteerEntryControllerBean vlntrC = new VsoVolunteerEntryControllerBean();
						VsoVolunteerModDetails details = new VsoVolunteerModDetails();
						
						details.setVlntr_cd(frm.getVlntrCd());
						details.setVlntr_cmp_cd(user.getUserCmpCode());
						details.setVlntr_id(frm.getVlntrId());
						details.setVlntr_fname(frm.getVlntrFname());
						details.setVlntr_mname(frm.getVlntrMname());
						details.setVlntr_lname(frm.getVlntrLname());
						details.setVlntr_ntnly(frm.getVlntrNtnlyLst());
						details.setVlntr_cvl_stts(frm.getVlntrCstatusLst());
						System.out.println("date of birth >> "+ frm.getVlntrDateOfBirth());
						details.setVlntr_brth_dt(frm.getVlntrDateOfBirth());
						details.setVlntr_sex(frm.getGenderList());
						
						details.setVlntr_vs_exprtn(frm.getVlntrVisaExpiration());
						details.setVlntr_usr_nm(frm.getVlntrUserName());
						details.setVlntr_usr_psswrd(frm.getVlntrUserPass());
						details.setVlntr_usr_email(frm.getVlntrUserEmail());
						details.setVlntr_usr_scnd_email(frm.getVlntrUserSecondEmail());
						details.setUsr_cd(user.getUserCode());
						int vlntr_cd_key = frm.getVlntrCd();
						
						
						if(frm.getVlntrCd()>0){
							System.out.println("VsoVolunteerEntryAction execute Updated");
							int vlntr_cd = vlntrC.Update(details);
							this.reload();
							//frm.setAppMessage("Volunteer was SUCCESSFULLY Updated!");
							
							msg+="Volunteer was SUCCESSFULLY Updated!. ";
							
							if(emlBean.getEmlEnabled() == 1){
								
								try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>Your Volunteer Service Organization has been update your Account.</p>"
									//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to log-in and View/Edit your Profile.</p>"
									+ "<p>Institute Code : "+vsoMD.getVso_nm()+"</p>"
									+ "<p>User Name : "+details.getVlntr_usr_nm()+"</p>"
									+ "<p>Password : "+details.getVlntr_usr_psswrd()+"</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(details.getVlntr_usr_email(), "VSO Volunteer Notification", Body);
									
									msg+=" Email has sent to Volunteer Email Address. ";
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								
								
								
								
								/*try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>New Volunteer has been Created.</p>"
									+ "<p>PNVSCA will validate the information for APPROVAL.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to Search and Edit Volunteer Informationt.</p>"
									+ "<p>Volunteer ID : "+details.getVlntr_id()+"</p>"
									+ "<p>Project Nae : "+frm.getVlntrPrjct()+"</p>"
									+ "<p>Project Manager : "+frm.getVlntrPrjPm()+"</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(vsoMD.getVso_prmry_email(), "VSO Volunteer Notification", Body);
									
									msg+=" Email has sent to VSO Email Address. ";
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								*/
								
								
							}
							
						}else{
							System.out.println("VsoVolunteerEntryAction execute Save");
							int vlntr_cd = vlntrC.Create(details);
							this.reload();
							//frm.setAppMessage("Volunteer was SUCCESSFULLY Added!");
							if(details.getVlntr_id() == null || details.getVlntr_id().trim().length()==0){
								msg+="Volunteer ID was Auto Generated.  ";
							}
							msg+="Volunteer was SUCCESSFULLY Added!. ";
							
							
							
							
							if(emlBean.getEmlEnabled() == 1){
								
								VsoVolunteerEntryControllerBean vsoVCB = new VsoVolunteerEntryControllerBean();
								VsoVolunteerModDetails vMD =  vsoVCB.getByVlntrCd(vlntr_cd);
								try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>Your Volunteer Service Organization Account has been created new Volunteer.</p>"
									//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to Edit Volunteer Profile.</p>"
									+ "<p>Volunteer ID : "+vMD.getVlntr_id()+"</p>"
									+ "<p>Volunteer Name : "+details.getVlntr_fname() + " "+details.getVlntr_mname() +" "+details.getVlntr_lname() +"</p>"
									//+ "<p>User Name : "+details.getLpi_usr_nm()+"</p>"
									//+ "<p>Password : "+details.getLpi_usr_psswrd()+"</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(vsoMD.getVso_prmry_email(), "VSO Volunteer Notification", Body);
									
									msg+=" Email has sent to your VSO Email Address.";
								} catch (MessagingException e){
									e.printStackTrace();
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								
								
								
								
								try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>Your Volunteer Service Organization Account has been assigned you as new Volunteer.</p>"
									//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to log-in and Edit your Profile.</p>"

									+ "<p>Institute Code : "+vsoMD.getVso_nm()+"</p>"
									+ "<p>User Name : "+details.getVlntr_usr_nm()+"</p>"
									+ "<p>Password : "+details.getVlntr_usr_psswrd()+"</p>"
									
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(details.getVlntr_usr_email(), "VSO Volunteer Notification", Body);
									
									msg+=" Email has sent to Volunteer Email Address.";
								} catch (MessagingException e){
									e.printStackTrace();
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}
							
						}
						
						
						System.out.println("VLNTR CD >> "+frm.getVlntrCd());
						VsoVolunteerEntryControllerBean vlC = new VsoVolunteerEntryControllerBean();
						VsoVolunteerModDetails details1 = vlC.getByVlntrCd(vlntr_cd_key);
						this.reload();
						frm.setVlntrCd(details1.getVlntr_cd());
						frm.setVlntrId(details1.getVlntr_id());
						frm.setVlntrFname(details1.getVlntr_fname());
						frm.setVlntrMname(details1.getVlntr_mname());
						frm.setVlntrLname(details1.getVlntr_lname());
						frm.setGender(details1.getVlntr_sex());
						frm.setVlntrNtnly(details1.getVlntr_ntnly());
						frm.setVlntrCstatus(details1.getVlntr_cvl_stts());
						frm.setVlntrDateOfBirth(details1.getVlntr_brth_dt());
						frm.setVlntrVisaExpiration(details1.getVlntr_vs_exprtn());
						
						frm.setVlntrUserName(details1.getVlntr_usr_nm());
						frm.setVlntrUserEmail(details1.getVlntr_usr_email());
						frm.setVlntrUserSecondEmail(details1.getVlntr_usr_scnd_email());
						frm.setPrjCd(details1.getVlntr_prjct_cd());
						frm.setVlntr_usr_cd(details1.getUsr_cd());
						
						
						frm.setVlntrPrjct("");
						frm.setVlntrPrjPm("");
						frm.setVlntrPrjDateFrom("");
						frm.setVlntrPrjDateTo("");
						frm.setVlntr_usr_cd(details1.getUsr_cd());
						
						try {
							LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
							LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(details1.getVlntr_cd());
							int rv_cd = rvlD.getRvl_rv_cd();
							
							LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
							LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd,0);
							LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
							LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
							frm.setVlntrPrjct(prjMD.getPrjOrgName());
							frm.setVlntrPrjDateFrom(prjMD.getPrjDurationFrom());
							frm.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
							
							LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
							LpiProjectManagerModDetails pmMD = pmC.getPmByPmCode(prjMD.getPrjProjectManager());
							frm.setVlntrPrjPm(pmMD.getPmName());
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						frm.setAppMessage(msg);;
						rtrn = "onLoad";
						
					}else{
						frm.populateCStatus();
						frm.setVlntrCstatus(frm.getVlntrCstatusLst());
						
						frm.populateNationality();
						frm.setVlntrNtnly(frm.getVlntrNtnlyLst());
						
						frm.clearGenderLists();
						frm.setGender(frm.getGenderList());
						
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("VsoVolunteerEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	public String validateFrm(VsoVolunteerEntryForm vfrm, UserSession user){
		System.out.println("VsoVolunteerEntryAction execute validateFrm");
		
		String errMsg = "";
		
		if(user.getUserProfile()!=3){
			errMsg+="Volunteer Information can not be edited. Please contact Assigned VSO. ";
		}
		
		if((frm.getVlntrId() != null || frm.getVlntrId().trim().length()>0) && frm.getVlntrCd() == 0){
			
			VsoVolunteerEntryControllerBean vlntrCB = new VsoVolunteerEntryControllerBean();
			
			 try {
				VsoVolunteerModDetails vlntrMD =  vlntrCB.getByVlntrId(frm.getVlntrId());
			
				if(vlntrMD.getVlntr_cd() > 0){
					errMsg+= "Volunteer ID is already Registered. ";
				}
			} catch (NullPointerException e){
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(frm.getVlntrFname() == null || frm.getVlntrFname().trim().length()==0){
			errMsg+= "First Name is Required. ";
		}
		
		if(frm.getVlntrLname() == null || frm.getVlntrLname().trim().length()==0){
			errMsg+= "Last Name is Required. ";
		}
		
		if(frm.getGenderList().equals("0")){
			errMsg+= "Gender is Required. ";
		}
		
		if(frm.getVlntrNtnlyLst().equals("0")){
			errMsg+="Nationality is Required.  ";
		}
		
		if(frm.getVlntrCstatusLst().equals("0")){
			errMsg+="Country is Required.  ";
		}
		
		if(frm.getVlntrDateOfBirth() == null || frm.getVlntrDateOfBirth().trim().length()==0){
			errMsg+="Birth Date is Required.  ";
		}else{
			boolean date = common.validateDateFormat(frm.getVlntrDateOfBirth());
			if(date == false){
				errMsg+="Birth Date is Invalid Format..";
			}
		}
		
		if(frm.getVlntrVisaExpiration() == null || frm.getVlntrVisaExpiration().trim().length()==0){
			errMsg+="Visa Expiration is Required.  ";
		}else{
			boolean date = common.validateDateFormat(frm.getVlntrVisaExpiration());
			if(date == false){
				errMsg+="Visa Expiration is Invalid Format..";
			}
		}
		
		if(frm.getVlntrUserName() == null || frm.getVlntrUserName().trim().length()==0){
			errMsg+="User Name is Required.  ";
		}
		
		if(frm.getVlntrUserPass() == null || frm.getVlntrUserPass().trim().length()==0){
			errMsg+="User Password is Required.  ";
		}else if(!frm.getVlntrUserPass().equals(frm.getVlntrUserPassConfirm())) {
			errMsg+="User Password is not Matched.  ";
		}
		
		if(frm.getVlntrUserEmail() == null ||  frm.getVlntrUserEmail().trim().length() == 0){
			errMsg+="Email is Required.  ";
		}else{
			boolean eml = common.validateEmail(frm.getVlntrUserEmail());
			if(eml == false){
				errMsg+="Email is Invalid Format.  ";
			}
		}
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}
	
	

}
