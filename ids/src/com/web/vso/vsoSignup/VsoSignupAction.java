package com.web.vso.vsoSignup;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserAutoEmailSession;
import com.web.common;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.AdAddCountryModDetails;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.VsoVsoModDetails;

public class VsoSignupAction extends ActionSupport{
	private VsoSignupForm frm;
	public VsoSignupForm getFrm() {return frm;}
	public void setFrm(VsoSignupForm frm) {this.frm = frm;}
	
	public String onload = "";
	
	

	
	public void reload(){
		
		

		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		frm.clearRegionLists();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl
						.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");

		ArrayList cntryList = new ArrayList();
		try {
			cntryList = logonCB.getAllCountry();
			Iterator cl = cntryList.iterator();
			while (cl.hasNext()) {
				AdAddCountryModDetails cntrD = (AdAddCountryModDetails) cl
						.next();
				frm.setCountryLists(cntrD.getCntry_nm());
			}
		} catch (Exception e) {
		}
		frm.setCountry("0");
		
	}
	
	
	public String execute() throws Exception {
		System.out.println("VsoSignupAction execute");
		String rtrn="onLoad";
		
		HttpSession session = ServletActionContext.getRequest().getSession();
		if ((frm.getLink() != null && frm.getLink().equals("3")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
			rtrn = "onLoad";
			System.out.println("VsoSignupAction execute Reset");
			frm.reset();
			reload();

		}else{
			if(frm.getBtnName()!= null && frm.getBtnName().equals("LogIn")){

				System.out.println("VsoSignupAction reload Log-in");
				rtrn="login";
			}else if(frm.getBtnName()!= null && frm.getBtnName().equals("Sign-Up")){
				System.out.println("VsoSignupAction execute Sign-up");
				rtrn = "onLoad";
				String errMsg = this.validateFrm(frm);
				if (!errMsg.equalsIgnoreCase("")) {
					System.out.println("VsoSignupAction execute Sign-up reload");
					frm.setAppMessage(errMsg);
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					frm.populateRecruitment();
					frm.setVsoRecruitment(frm.getVsoRecruitmentList());
					
					frm.populateType();
					frm.setVsoType(frm.getVsoTypeList());
					
					frm.populateBudgetSource();
					frm.setVsoBudgetSource(frm.getVsoBudgetSourceList());
					
					ArrayList cntryList = new ArrayList();
					frm.clearCountryLists();
					try {
						cntryList = logonCB.getAllCountry();
						Iterator cl = cntryList.iterator();
						while (cl.hasNext()) {
							AdAddCountryModDetails cntrD = (AdAddCountryModDetails)cl.next();
							frm.setCountryLists(cntrD.getCntry_nm());
						}
					} catch (Exception e) {
					}
					frm.setCountry(frm.getCountryList());

					ArrayList rgnList = new ArrayList();
					try {
						rgnList = logonCB.getAllRegion();
						Iterator rl = rgnList.iterator();
						frm.clearRegionLists();
						while (rl.hasNext()) {
							AdAddRegionModDetails rgnD = (AdAddRegionModDetails)rl.next();
							frm.setRegionLists(rgnD.getRgn_nm());
						}
					} catch (Exception e) {

						e.printStackTrace();
					}
					frm.setRegion(frm.getRegionList());

					ArrayList prvncList = new ArrayList();
					Iterator i;
					try {
						prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
						i = prvncList.iterator();
						provinces = new ArrayList<String>();
						frm.clearProvinceLists();
						while (i.hasNext()) {
							AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
							frm.setProvinceLists(details.getPrvnc_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					frm.setProvince(frm.getProvinceList());

					ArrayList munList = new ArrayList();
					try {
						munList = logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
						i = munList.iterator();
						municipalities = new ArrayList<String>();
						frm.clearMunicipalityLists();
						while (i.hasNext()) {
							AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
							frm.setMunicipalityLists(details.getMun_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					frm.setMunicipality(frm.getMunicipalityList());

				}else{
					System.out.println("VsoSignupAction execute Sign-up to save");
					rtrn = "onLoad";
					VsoVsoModDetails details = new VsoVsoModDetails();
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					details.setVso_id(frm.getVsoId());
					details.setVso_nm(frm.getVsoName());
					
					int org_type = 0;
					
			
					if(frm.getVsoTypeList().equals("International Government Organization/ Program")){
						org_type = 1;
					}else if(frm.getVsoTypeList().equals("International Non-Government Organization")){
						org_type = 2;
					}else if(frm.getVsoTypeList().equals("Local Organization")){
						org_type = 3;
					}
					details.setVso_org_typ(org_type);
					details.setVso_org_hd_nm(frm.getVsoHeadName());
					details.setVso_org_hd_pstn(frm.getVsoHeadPosition());
					
					
					try {
						AdAddRegionModDetails rgnD = logonCB.getRegionByRgnName(frm.getRegionList());
						details.setVso_org_rgn(rgnD.getRgn_id());
					} catch (Exception e) {
						details.setVso_org_rgn(0);
					}
					try {
						AdAddProvinceModDetails prnvcD = logonCB.getProvinceByPrvcName(frm.getProvinceList());
						details.setVso_org_prvnc(prnvcD.getPrvnc_id());
					} catch (Exception e) {
						details.setVso_org_prvnc(0);
					}
					try {
						AdAddMunicipalityModDetails munDB = logonCB.getMunicipalityByMunName(frm.getMunicipalityList());
						details.setVso_org_mncplty(munDB.getMun_id());
					} catch (Exception e) {
						details.setVso_org_mncplty(0);
					}
					details.setVso_org_addrss(frm.getAddress());
					details.setVso_org_tlph_nmbr(frm.getVsoTelephoneNum());
					details.setVso_org_fx_nmbr(frm.getVsoFaxNum());
					details.setVso_org_email(frm.getVsoEmail());
					details.setVso_org_wbst(frm.getVsoWebsite());
					details.setVso_sec_rgstrtn_nmbr(frm.getVsoSecRegNum());
					details.setVso_hd_offc_addrss(frm.getVsoHeadOfficeAddress());
					try {
						AdAddCountryModDetails cntryDB = logonCB.getAllCountryByCntryName(frm.getCountryList());
						details.setVso_cntry(cntryDB.getCntry_cd());
					} catch (Exception e) {
						details.setVso_cntry(0);
					}
					
					details.setVso_dt_estblishd(frm.getVsoDateEstablished());;
					details.setVso_vsn(frm.getVsoVsion());
					details.setVso_mssn(frm.getVsoMission());
					details.setVso_goal(frm.getVsoGoals());
					details.setVso_annl_bdgt(frm.getVsoAnnualBudget());
					details.setVso_bdgt_scrc(frm.getVsoBudgetSourceList());
					
					int rctmnt = 0;
					if(frm.getVsoRecruitmentList().equals("Foreign")){
						rctmnt = 1;
					}else if(frm.getVsoRecruitmentList().equals("Local")){
						rctmnt = 2;
					}
					details.setVso_rcrtment(rctmnt);
					details.setVso_enggmnt(common.booleanToInt(frm.isVsoEngagement()));
					details.setVso_cpblty_bldng(common.booleanToInt(frm.isVsoCapability()));
					details.setVso_prmtn(common.booleanToInt(frm.isVsoPromotion()));
					details.setVso_ntwrkng_of_vlntrs(common.booleanToInt(frm.isVsoNetworking()));
					details.setVso_othrs1(common.booleanToInt(frm.isVsoOther1()));
					details.setVso_othrs_nt1(frm.getVsoOtherNote1());
					
					
					details.setVso_lvng(common.booleanToInt(frm.isVsoLiving()));
					details.setVso_meal(common.booleanToInt(frm.isVsoMeal()));
					details.setVso_insrnc(common.booleanToInt(frm.isVsoInsurance()));
					details.setVso_rcgntn_or_awrd(common.booleanToInt(frm.isVsoRecognition()));
					details.setVso_trvl_allwnc(common.booleanToInt(frm.isVsoTravel()));
					details.setVso_hsng(common.booleanToInt(frm.isVsoHousing()));
					details.setVso_trnng_and_ornttn(common.booleanToInt(frm.isVsoTraining()));
					details.setVso_othrs2(common.booleanToInt(frm.isVsoOther2()));
					details.setVso_othrs_nt2(frm.getVsoOtherNote2());
					
					details.setVso_usr_nm(frm.getVsoUserName());
					details.setVso_psswrd(frm.getVsoUserPass());
					details.setVso_prmry_email(frm.getVsoUserPrimaryEmail());
					details.setVso_scndry_email(frm.getVsoUserSecondaryEmail());
					details.setVso_crdntr_nm(frm.getVsoCoordinatorName());
					details.setVso_crdntr_pstn(frm.getVsoCoordinatorPosition());
					details.setVso_crdntr_dprtmnt(frm.getVsoCoordinatorDepartment());
					details.setVso_crdntr_tlphn_nmbr(frm.getVsoCoordinatorTelNum());
					details.setVso_crdntr_mbl_nmbr(frm.getVsoCoordinatorMobileNum());
					
					
					
				
					
					int vsoCode = logonCB.saveVsoDetails(details);
					rtrn = "onLoad";
					frm.reset();
					reload();
					rtrn = "login";
					//frm.setAppMessage("Sign-up for LPI Account was Succesfully Created, Please see your Email for Confirmation.");
					String msg = "Your VSO Account was successfully created."
							+ "You can now input your ID and password to Login";
					
					
					
					AdEmailControllerBean emailCB = new AdEmailControllerBean();
					AdEmailBean emlBean = emailCB.getEmail(3);
					
					if(emlBean.getEmlEnabled() == 1){
						
						try {
							UserAutoEmailSession email = new UserAutoEmailSession();
							String Body = "<html><head></head><body>"
							+"<p>HI</p>"
							+ "<p>Good Day!</p>"
							+ "<p>Your Volunteer Service Organization Account has been Created.</p>"
							+ "<p>PnVSCA will validate the information for you to use the application.</p>"
							+ "<p>&nbsp;</p>"
							+ "<p>Please use the following information below to log-in and Edit your Profile.</p>"
							+ "<p>Institute Code : "+details.getVso_nm()+"</p>"
							+ "<p>User Name : "+details.getVso_usr_nm()+"</p>"
							+ "<p>Password : "+details.getVso_psswrd()+"</p>"
							+ "<p>&nbsp;</p>"
							+ "<p>Thanks,</p>"
							+ "<p>PNVSCA</p>"
							+ "</body></html>";
							email.SendEmail(details.getVso_prmry_email(), "VSO Sign-up Notification", Body);
							
							msg+=" Email has sent to your Email Address.";
						} catch (Exception e) {
							
							e.printStackTrace();
						}
						
						
					}
					
					
					
					
					
					frm.setAppMessage(msg);
					
				
				}
			}
		}
		
		return rtrn;
	}
	
	
	public String validateFrm(VsoSignupForm frm){
		System.out.println("VsoSignupAction Validate Form");
		String errMsg = "";
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		
		if(frm.getVsoName() == null || frm.getVsoName().trim().length()==0){
			errMsg = "Organization Name is Required. ";
		}else{
			VsoVsoControllerBean vsoCB = new VsoVsoControllerBean();
			 
			try {
				VsoVsoModDetails vsoMD = vsoCB.getVsoByVsoId(frm.getVsoName());
				if(vsoMD.getVso_cd()>0){
					errMsg += "Organization Names is already Registered. ";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if(frm.getVsoTypeList() == null || frm.getVsoTypeList().equals("0")){
			errMsg += "Type of Organization is Required. ";
		}
		
		if(frm.getVsoHeadName() == null || frm.getVsoHeadName().trim().length() == 0){
			errMsg += "VSO Head Name is Required. ";
		}
		
		if(frm.getVsoHeadPosition() == null || frm.getVsoHeadPosition().trim().length() == 0){
			errMsg += "Head Position is Required. ";
		}
		
		if(frm.getRegionList().equals("0")){
			errMsg += "Region is Required. ";
		}
		
		if(frm.getProvinceList().equals("0")){
			errMsg += "Province is Required. ";
		}
		
		if(frm.getMunicipalityList().equals("0")){
			errMsg += "Municipality is Required. ";
		}
		
		if(frm.getAddress() == null || frm.getAddress().trim().length() == 0){
			errMsg += "Address is Required. ";
		}
		
		if(frm.getVsoTelephoneNum() == null || frm.getVsoTelephoneNum().trim().length() == 0 ){
			errMsg += "Telephone No. is Required. ";
		}
		
		if(frm.getVsoEmail() == null || frm.getVsoEmail().trim().length() == 0){
			errMsg += "VSO Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoEmail());
			if(yes == false){
				errMsg += "VSO Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getVsoFaxNum() == null || frm.getVsoFaxNum().trim().length() == 0){
			errMsg += "Fax No. is Required. ";
		}
		
		/*
		if(frm.getVsoSecRegNum() == null || frm.getVsoSecRegNum().trim().length() == 0){
			errMsg += "Sec Registration No. is Required. ";
		}
		*/
		
		if(frm.getVsoDateEstablished()==null || frm.getVsoDateEstablished().trim().length() == 0){
			errMsg += "Date Established is Required. ";
		}
		
		if(frm.getVsoVsion() == null || frm.getVsoVsion().trim().length() == 0){
			errMsg += "VSO Vision is Required. ";
		}
		
		if(frm.getVsoMission() == null || frm.getVsoMission().trim().length() == 0){
			errMsg += "VSO Mission is Required. ";
		}
		
		if(frm.getVsoGoals() == null || frm.getVsoGoals().trim().length() == 0){
			errMsg += "VSO Goal is Required. ";
		}
		
		if(frm.getVsoAnnualBudget() == null || frm.getVsoAnnualBudget().trim().length() == 0){
			errMsg += "Annual Budget Range is Required. ";
		}
		
		if(frm.getVsoBudgetSourceList() == null || frm.getVsoBudgetSourceList().equals("0")){
			errMsg += "VSO Budget Souce is Required. ";
		}

		
		if(frm.getVsoUserName() == null || frm.getVsoUserName().trim().length()==0){
			errMsg += "UserName is Required. ";
		}
		
		if(frm.getVsoUserPass() == null || frm.getVsoUserPass().trim().length() == 0){
			errMsg += "User Password is Required. ";
		}else{
			if(!frm.getVsoUserPass().equals(frm.getVsoUserPassConfirm())){
				errMsg += "User Password is not Matched. ";
			}
			
		}
		
		if(frm.getVsoUserPrimaryEmail() == null || frm.getVsoUserPrimaryEmail().trim().length() == 0){
			errMsg += "User Primary Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoUserPrimaryEmail());
			if(yes == false){
				errMsg += "User Primary Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getVsoUserSecondaryEmail() == null || frm.getVsoUserSecondaryEmail().trim().length() == 0){
			errMsg += "User Secondary Email is Requied. ";
		}else{
			boolean yes = common.validateEmail(frm.getVsoUserSecondaryEmail());
			if(yes == false){
				errMsg += "User Secondary Email is Invalid Format. ";
			}
			
		}
		
		
		if(frm.getVsoCoordinatorName() == null || frm.getVsoCoordinatorName().trim().length() == 0){
			errMsg += "PNVSCA Coordinator is Required. ";
		}
		
		return errMsg;
		
	}
	
	
	
	private ArrayList<String> provinces;
	private ArrayList<String> municipalities;

	public ArrayList<String> getProvinces() {
		return provinces;
	}

	public ArrayList<String> getMunicipalities() {
		return municipalities;
	}

	public String getProvince() throws Exception {
		System.out.println("VsoSignupAction getProvince");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList prvncList =  logonCB.getAllProvinceByRgnId(frm.getRegion());
		Iterator i = prvncList.iterator();
		provinces =  new ArrayList<String>();
		frm.clearProvinceLists();
		while(i.hasNext()){
			AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
			System.out.println("prvnc >> "+ details.getPrvnc_nm());
			frm.setProvinceLists(details.getPrvnc_nm().toString());
			provinces.add(details.getPrvnc_nm().toString());
		}
		return "success";
	}
	
	
	public String getMunicipality() throws Exception {
		System.out.println("VsoSignupAction getMunicipality");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		System.out.println("frm.getProvince() 2 >> "+frm.getProvince());
		ArrayList munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
		Iterator i = munList.iterator();
		municipalities =  new ArrayList<String>();
		frm.clearMunicipalityLists();
		while(i.hasNext()){
			AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
			frm.setMunicipalityLists(details.getMun_nm().toString());
			municipalities.add(details.getMun_nm().toString());
		}
		return "success";
	}
	
	
	
}
