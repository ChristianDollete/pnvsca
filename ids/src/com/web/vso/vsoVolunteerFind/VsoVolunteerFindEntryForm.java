package com.web.vso.vsoVolunteerFind;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class VsoVolunteerFindEntryForm extends ApplicationMessage {

	private int vlntrCd;
	private String vlntrId;
	private String vlntrFname;
	private String vlntrMname;
	private String vlntrLname;

	private String vlntrNtnly;
	private String vlntrNtnlyLst;
	private ArrayList vlntrNtnlyLsts = new ArrayList();

	private String vlntrCstatus;
	private String vlntrCstatusLst;
	private ArrayList vlntrCstatusLsts = new ArrayList();

	private String vlntrDateOfBirth;
	private String vlntrVisaExpiration;

	private String vlntrPrjct;
	private int prjCd;
	private String vlntrPrjPm;
	private int pmCd;
	private String vlntrPrjDateFrom;
	private String vlntrPrjDateTo;

	private String vlntrUserName;
	private String vlntrUserPass;
	private String vlntrUserPassConfirm;
	private String vlntrUserEmail;
	private String vlntrUserSecondEmail;

	private ArrayList vlntrList = new ArrayList();

	private String vlntrPrj;
	private String vlntrPrjLst;
	private ArrayList vlntrPrjLsts = new ArrayList();
	

	public String getVlntrPrj() {
		return vlntrPrj;
	}

	public void setVlntrPrj(String vlntrPrj) {
		this.vlntrPrj = vlntrPrj;
	}

	public String getVlntrPrjLst() {
		return vlntrPrjLst;
	}

	public void setVlntrPrjLst(String vlntrPrjLst) {
		this.vlntrPrjLst = vlntrPrjLst;
	}

	public ArrayList getVlntrPrjLsts() {
		return vlntrPrjLsts;
	}

	public void setVlntrPrjLsts(String prj) {
		this.vlntrPrjLsts.add(prj);
	}
	public void clearVlntrPrjLsts(){
		vlntrPrjLsts = new ArrayList();
	}

	public ArrayList getVlntrList() {
		return vlntrList;
	}

	public void setVlntrList(VsoVolunteerFindEntryLine vlntrLine) {
		this.vlntrList.add(vlntrLine);
	}

	public void clearVlntrList() {
		vlntrList = new ArrayList();
	}

	public int getPrjCd() {
		return prjCd;
	}

	public void setPrjCd(int prjCd) {
		this.prjCd = prjCd;
	}

	public int getPmCd() {
		return pmCd;
	}

	public void setPmCd(int pmCd) {
		this.pmCd = pmCd;
	}

	public int getVlntrCd() {
		return vlntrCd;
	}

	public void setVlntrCd(int vlntrCd) {
		this.vlntrCd = vlntrCd;
	}

	public String getVlntrId() {
		return vlntrId;
	}

	public void setVlntrId(String vlntrId) {
		this.vlntrId = vlntrId;
	}

	public String getVlntrFname() {
		return vlntrFname;
	}

	public void setVlntrFname(String vlntrFname) {
		this.vlntrFname = vlntrFname;
	}

	public String getVlntrMname() {
		return vlntrMname;
	}

	public void setVlntrMname(String vlntrMname) {
		this.vlntrMname = vlntrMname;
	}

	public String getVlntrLname() {
		return vlntrLname;
	}

	public void setVlntrLname(String vlntrLname) {
		this.vlntrLname = vlntrLname;
	}

	public String getVlntrNtnly() {
		return vlntrNtnly;
	}

	public void setVlntrNtnly(String vlntrNtnly) {
		this.vlntrNtnly = vlntrNtnly;
	}

	public String getVlntrNtnlyLst() {
		return vlntrNtnlyLst;
	}

	public void setVlntrNtnlyLst(String vlntrNtnlyLst) {
		this.vlntrNtnlyLst = vlntrNtnlyLst;
	}

	public ArrayList getVlntrNtnlyLsts() {
		return vlntrNtnlyLsts;
	}

	public void setVlntrNtnlyLsts(String vlntrNtnly) {
		this.vlntrNtnlyLsts.add(vlntrNtnly);
	}

	public String getVlntrCstatus() {
		return vlntrCstatus;
	}

	public void setVlntrCstatus(String vlntrCstatus) {
		this.vlntrCstatus = vlntrCstatus;
	}

	public String getVlntrCstatusLst() {
		return vlntrCstatusLst;
	}

	public void setVlntrCstatusLst(String vlntrCstatusLst) {
		this.vlntrCstatusLst = vlntrCstatusLst;
	}

	public ArrayList getVlntrCstatusLsts() {
		return vlntrCstatusLsts;
	}

	public void setVlntrCstatusLsts(ArrayList vlntrCstatusLsts) {
		this.vlntrCstatusLsts = vlntrCstatusLsts;
	}

	public String getVlntrDateOfBirth() {
		return vlntrDateOfBirth;
	}

	public void setVlntrDateOfBirth(String vlntrDateOfBirth) {
		this.vlntrDateOfBirth = vlntrDateOfBirth;
	}

	public String getVlntrVisaExpiration() {
		return vlntrVisaExpiration;
	}

	public void setVlntrVisaExpiration(String vlntrVisaExpiration) {
		this.vlntrVisaExpiration = vlntrVisaExpiration;
	}

	public String getVlntrPrjct() {
		return vlntrPrjct;
	}

	public void setVlntrPrjct(String vlntrPrjct) {
		this.vlntrPrjct = vlntrPrjct;
	}

	public String getVlntrPrjPm() {
		return vlntrPrjPm;
	}

	public void setVlntrPrjPm(String vlntrPrjPm) {
		this.vlntrPrjPm = vlntrPrjPm;
	}

	public String getVlntrPrjDateFrom() {
		return vlntrPrjDateFrom;
	}

	public void setVlntrPrjDateFrom(String vlntrPrjDateFrom) {
		this.vlntrPrjDateFrom = vlntrPrjDateFrom;
	}

	public String getVlntrPrjDateTo() {
		return vlntrPrjDateTo;
	}

	public void setVlntrPrjDateTo(String vlntrPrjDateTo) {
		this.vlntrPrjDateTo = vlntrPrjDateTo;
	}

	public String getVlntrUserName() {
		return vlntrUserName;
	}

	public void setVlntrUserName(String vlntrUserName) {
		this.vlntrUserName = vlntrUserName;
	}

	public String getVlntrUserPass() {
		return vlntrUserPass;
	}

	public void setVlntrUserPass(String vlntrUserPass) {
		this.vlntrUserPass = vlntrUserPass;
	}

	public String getVlntrUserPassConfirm() {
		return vlntrUserPassConfirm;
	}

	public void setVlntrUserPassConfirm(String vlntrUserPassConfirm) {
		this.vlntrUserPassConfirm = vlntrUserPassConfirm;
	}

	public String getVlntrUserEmail() {
		return vlntrUserEmail;
	}

	public void setVlntrUserEmail(String vlntrUserEmail) {
		this.vlntrUserEmail = vlntrUserEmail;
	}

	public String getVlntrUserSecondEmail() {
		return vlntrUserSecondEmail;
	}

	public void setVlntrUserSecondEmail(String vlntrUserSecondEmail) {
		this.vlntrUserSecondEmail = vlntrUserSecondEmail;
	}

	public void reset() {
		vlntrCd = 0;
		prjCd = 0;
		pmCd = 0;
		vlntrId = "";
		vlntrFname = "";
		vlntrMname = "";
		vlntrLname = "";

		vlntrNtnly = "0";
		vlntrNtnlyLst = "";
		vlntrNtnlyLsts = new ArrayList();
		vlntrNtnlyLsts.add("Afghan");
		vlntrNtnlyLsts.add("Albanian");
		vlntrNtnlyLsts.add("Algerian");
		vlntrNtnlyLsts.add("American");
		vlntrNtnlyLsts.add("Andorran");
		vlntrNtnlyLsts.add("Angolan");
		vlntrNtnlyLsts.add("Antiguans");
		vlntrNtnlyLsts.add("Argentinean");
		vlntrNtnlyLsts.add("Armenian");
		vlntrNtnlyLsts.add("Australian");
		vlntrNtnlyLsts.add("Austrian");
		vlntrNtnlyLsts.add("Azerbaijani");
		vlntrNtnlyLsts.add("Bahamian");
		vlntrNtnlyLsts.add("Bahraini");
		vlntrNtnlyLsts.add("Bangladeshi");
		vlntrNtnlyLsts.add("Barbadian");
		vlntrNtnlyLsts.add("Barbudans");
		vlntrNtnlyLsts.add("Batswana");
		vlntrNtnlyLsts.add("Belarusian");
		vlntrNtnlyLsts.add("Belgian");
		vlntrNtnlyLsts.add("Belizean");
		vlntrNtnlyLsts.add("Beninese");
		vlntrNtnlyLsts.add("Bhutanese");
		vlntrNtnlyLsts.add("Bolivian");
		vlntrNtnlyLsts.add("Bosnian");
		vlntrNtnlyLsts.add("Brazilian");
		vlntrNtnlyLsts.add("British");
		vlntrNtnlyLsts.add("Bruneian");
		vlntrNtnlyLsts.add("Bulgarian");
		vlntrNtnlyLsts.add("Burkinabe");
		vlntrNtnlyLsts.add("Burmese");
		vlntrNtnlyLsts.add("Burundian");
		vlntrNtnlyLsts.add("Cambodian");
		vlntrNtnlyLsts.add("Cameroonian");
		vlntrNtnlyLsts.add("Canadian");
		vlntrNtnlyLsts.add("Cape Verdean");
		vlntrNtnlyLsts.add("Central African");
		vlntrNtnlyLsts.add("Chadian");
		vlntrNtnlyLsts.add("Chilean");
		vlntrNtnlyLsts.add("Chinese");
		vlntrNtnlyLsts.add("Colombian");
		vlntrNtnlyLsts.add("Comoran");
		vlntrNtnlyLsts.add("Congolese");
		vlntrNtnlyLsts.add("Congolese");
		vlntrNtnlyLsts.add("Costa Rican");
		vlntrNtnlyLsts.add("Croatian");
		vlntrNtnlyLsts.add("Cuban");
		vlntrNtnlyLsts.add("Cypriot");
		vlntrNtnlyLsts.add("Czech");
		vlntrNtnlyLsts.add("Danish");
		vlntrNtnlyLsts.add("Djibouti");
		vlntrNtnlyLsts.add("Dominican");
		vlntrNtnlyLsts.add("Dominican");
		vlntrNtnlyLsts.add("Dutch");
		vlntrNtnlyLsts.add("Dutchman");
		vlntrNtnlyLsts.add("Dutchwoman");
		vlntrNtnlyLsts.add("East Timorese");
		vlntrNtnlyLsts.add("Ecuadorean");
		vlntrNtnlyLsts.add("Egyptian");
		vlntrNtnlyLsts.add("Emirian");
		vlntrNtnlyLsts.add("Equatorial Guinean");
		vlntrNtnlyLsts.add("Eritrean");
		vlntrNtnlyLsts.add("Estonian");
		vlntrNtnlyLsts.add("Ethiopian");
		vlntrNtnlyLsts.add("Fijian");
		vlntrNtnlyLsts.add("Filipino");
		vlntrNtnlyLsts.add("Finnish");
		vlntrNtnlyLsts.add("French");
		vlntrNtnlyLsts.add("Gabonese");
		vlntrNtnlyLsts.add("Gambian");
		vlntrNtnlyLsts.add("Georgian");
		vlntrNtnlyLsts.add("German");
		vlntrNtnlyLsts.add("Ghanaian");
		vlntrNtnlyLsts.add("Greek");
		vlntrNtnlyLsts.add("Grenadian");
		vlntrNtnlyLsts.add("Guatemalan");
		vlntrNtnlyLsts.add("Guinea-Bissauan");
		vlntrNtnlyLsts.add("Guinean");
		vlntrNtnlyLsts.add("Guyanese");
		vlntrNtnlyLsts.add("Haitian");
		vlntrNtnlyLsts.add("Herzegovinian");
		vlntrNtnlyLsts.add("Honduran");
		vlntrNtnlyLsts.add("Hungarian");
		vlntrNtnlyLsts.add("I-Kiribati");
		vlntrNtnlyLsts.add("Icelander");
		vlntrNtnlyLsts.add("Indian");
		vlntrNtnlyLsts.add("Indonesian");
		vlntrNtnlyLsts.add("Iranian");
		vlntrNtnlyLsts.add("Iraqi");
		vlntrNtnlyLsts.add("Irish");
		vlntrNtnlyLsts.add("Irish");
		vlntrNtnlyLsts.add("Israeli");
		vlntrNtnlyLsts.add("Italian");
		vlntrNtnlyLsts.add("Ivorian");
		vlntrNtnlyLsts.add("Jamaican");
		vlntrNtnlyLsts.add("Japanese");
		vlntrNtnlyLsts.add("Jordanian");
		vlntrNtnlyLsts.add("Kazakhstani");
		vlntrNtnlyLsts.add("Kenyan");
		vlntrNtnlyLsts.add("Kittian and Nevisian");
		vlntrNtnlyLsts.add("Kuwaiti");
		vlntrNtnlyLsts.add("Kyrgyz");
		vlntrNtnlyLsts.add("Laotian");
		vlntrNtnlyLsts.add("Latvian");
		vlntrNtnlyLsts.add("Lebanese");
		vlntrNtnlyLsts.add("Liberian");
		vlntrNtnlyLsts.add("Libyan");
		vlntrNtnlyLsts.add("Liechtensteiner");
		vlntrNtnlyLsts.add("Lithuanian");
		vlntrNtnlyLsts.add("Luxembourger");
		vlntrNtnlyLsts.add("Macedonian");
		vlntrNtnlyLsts.add("Malagasy");
		vlntrNtnlyLsts.add("Malawian");
		vlntrNtnlyLsts.add("Malaysian");
		vlntrNtnlyLsts.add("Maldivan");
		vlntrNtnlyLsts.add("Malian");
		vlntrNtnlyLsts.add("Maltese");
		vlntrNtnlyLsts.add("Marshallese");
		vlntrNtnlyLsts.add("Mauritanian");
		vlntrNtnlyLsts.add("Mauritian");
		vlntrNtnlyLsts.add("Mexican");
		vlntrNtnlyLsts.add("Micronesian");
		vlntrNtnlyLsts.add("Moldovan");
		vlntrNtnlyLsts.add("Monacan");
		vlntrNtnlyLsts.add("Mongolian");
		vlntrNtnlyLsts.add("Moroccan");
		vlntrNtnlyLsts.add("Mosotho");
		vlntrNtnlyLsts.add("Motswana");
		vlntrNtnlyLsts.add("Mozambican");
		vlntrNtnlyLsts.add("Namibian");
		vlntrNtnlyLsts.add("Nauruan");
		vlntrNtnlyLsts.add("Nepalese");
		vlntrNtnlyLsts.add("Netherlander");
		vlntrNtnlyLsts.add("New Zealander");
		vlntrNtnlyLsts.add("Ni-Vanuatu");
		vlntrNtnlyLsts.add("Nicaraguan");
		vlntrNtnlyLsts.add("Nigerian");
		vlntrNtnlyLsts.add("Nigerien");
		vlntrNtnlyLsts.add("North Korean");
		vlntrNtnlyLsts.add("Northern Irish");
		vlntrNtnlyLsts.add("Norwegian");
		vlntrNtnlyLsts.add("Omani");
		vlntrNtnlyLsts.add("Pakistani");
		vlntrNtnlyLsts.add("Palauan");
		vlntrNtnlyLsts.add("Panamanian");
		vlntrNtnlyLsts.add("Papua New Guinean");
		vlntrNtnlyLsts.add("Paraguayan");
		vlntrNtnlyLsts.add("Peruvian");
		vlntrNtnlyLsts.add("Polish");
		vlntrNtnlyLsts.add("Portuguese");
		vlntrNtnlyLsts.add("Qatari");
		vlntrNtnlyLsts.add("Romanian");
		vlntrNtnlyLsts.add("Russian");
		vlntrNtnlyLsts.add("Rwandan");
		vlntrNtnlyLsts.add("Saint Lucian");
		vlntrNtnlyLsts.add("Salvadoran");
		vlntrNtnlyLsts.add("Samoan");
		vlntrNtnlyLsts.add("San Marinese");
		vlntrNtnlyLsts.add("Sao Tomean");
		vlntrNtnlyLsts.add("Saudi");
		vlntrNtnlyLsts.add("Scottish");
		vlntrNtnlyLsts.add("Senegalese");
		vlntrNtnlyLsts.add("Serbian");
		vlntrNtnlyLsts.add("Seychellois");
		vlntrNtnlyLsts.add("Sierra Leonean");
		vlntrNtnlyLsts.add("Singaporean");
		vlntrNtnlyLsts.add("Slovakian");
		vlntrNtnlyLsts.add("Slovenian");
		vlntrNtnlyLsts.add("Solomon Islander");
		vlntrNtnlyLsts.add("Somali");
		vlntrNtnlyLsts.add("South African");
		vlntrNtnlyLsts.add("South Korean");
		vlntrNtnlyLsts.add("Spanish");
		vlntrNtnlyLsts.add("Sri Lankan");
		vlntrNtnlyLsts.add("Sudanese");
		vlntrNtnlyLsts.add("Surinamer");
		vlntrNtnlyLsts.add("Swazi");
		vlntrNtnlyLsts.add("Swedish");
		vlntrNtnlyLsts.add("Swiss");
		vlntrNtnlyLsts.add("Syrian");
		vlntrNtnlyLsts.add("Taiwanese");
		vlntrNtnlyLsts.add("Tajik");
		vlntrNtnlyLsts.add("Tanzanian");
		vlntrNtnlyLsts.add("Thai");
		vlntrNtnlyLsts.add("Togolese");
		vlntrNtnlyLsts.add("Tongan");
		vlntrNtnlyLsts.add("Trinidadian or Tobagonian");
		vlntrNtnlyLsts.add("Tunisian");
		vlntrNtnlyLsts.add("Turkish");
		vlntrNtnlyLsts.add("Tuvaluan");
		vlntrNtnlyLsts.add("Ugandan");
		vlntrNtnlyLsts.add("Ukrainian");
		vlntrNtnlyLsts.add("Uruguayan");
		vlntrNtnlyLsts.add("Uzbekistani");
		vlntrNtnlyLsts.add("Venezuelan");
		vlntrNtnlyLsts.add("Vietnamese");
		vlntrNtnlyLsts.add("Welsh");
		vlntrNtnlyLsts.add("Welsh");
		vlntrNtnlyLsts.add("Yemenite");
		vlntrNtnlyLsts.add("Zambian");
		vlntrNtnlyLsts.add("Zimbabwean");

		vlntrCstatus = "0";
		vlntrCstatusLst = "";
		vlntrCstatusLsts = new ArrayList();
		vlntrCstatusLsts.add("Married");
		vlntrCstatusLsts.add("Living common law");
		vlntrCstatusLsts.add("Widowed");
		vlntrCstatusLsts.add("Separated");
		vlntrCstatusLsts.add("Divorced");
		vlntrCstatusLsts.add("Single");

		vlntrDateOfBirth = "";
		vlntrVisaExpiration = "";

		vlntrPrjct = "";
		vlntrPrjPm = "";
		vlntrPrjDateFrom = "";
		vlntrPrjDateTo = "";

		vlntrUserName = "";
		vlntrUserPass = "";
		vlntrUserPassConfirm = "";
		vlntrUserEmail = "";
		vlntrUserSecondEmail = "";

		clearGenderLists();

	}

}
