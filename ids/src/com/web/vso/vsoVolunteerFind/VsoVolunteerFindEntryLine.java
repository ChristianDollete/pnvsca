package com.web.vso.vsoVolunteerFind;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class VsoVolunteerFindEntryLine extends ApplicationMessage {

	private int vlntrCd;
	private String vlntrId;
	private String vlntrName;
	private String vlntrVisaExpiration;

	private String vlntrPrjct;
	private int prjCd;
	private String vlntrPrjPm;
	private int pmCd;
	private String vlntrPrjDateFrom;
	private String vlntrPrjDateTo;
	public int getVlntrCd() {
		return vlntrCd;
	}
	public void setVlntrCd(int vlntrCd) {
		this.vlntrCd = vlntrCd;
	}
	public String getVlntrId() {
		return vlntrId;
	}
	public void setVlntrId(String vlntrId) {
		this.vlntrId = vlntrId;
	}
	public String getVlntrName() {
		return vlntrName;
	}
	public void setVlntrName(String vlntrName) {
		this.vlntrName = vlntrName;
	}
	public String getVlntrVisaExpiration() {
		return vlntrVisaExpiration;
	}
	public void setVlntrVisaExpiration(String vlntrVisaExpiration) {
		this.vlntrVisaExpiration = vlntrVisaExpiration;
	}
	public String getVlntrPrjct() {
		return vlntrPrjct;
	}
	public void setVlntrPrjct(String vlntrPrjct) {
		this.vlntrPrjct = vlntrPrjct;
	}
	public int getPrjCd() {
		return prjCd;
	}
	public void setPrjCd(int prjCd) {
		this.prjCd = prjCd;
	}
	public String getVlntrPrjPm() {
		return vlntrPrjPm;
	}
	public void setVlntrPrjPm(String vlntrPrjPm) {
		this.vlntrPrjPm = vlntrPrjPm;
	}
	public int getPmCd() {
		return pmCd;
	}
	public void setPmCd(int pmCd) {
		this.pmCd = pmCd;
	}
	public String getVlntrPrjDateFrom() {
		return vlntrPrjDateFrom;
	}
	public void setVlntrPrjDateFrom(String vlntrPrjDateFrom) {
		this.vlntrPrjDateFrom = vlntrPrjDateFrom;
	}
	public String getVlntrPrjDateTo() {
		return vlntrPrjDateTo;
	}
	public void setVlntrPrjDateTo(String vlntrPrjDateTo) {
		this.vlntrPrjDateTo = vlntrPrjDateTo;
	}
	
	
	
	
	

	

}
