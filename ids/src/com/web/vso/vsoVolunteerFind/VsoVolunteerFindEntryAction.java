package com.web.vso.vsoVolunteerFind;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.LpiRequestVolunteerLineControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.txn.VsoVolunteerFindControllerBean;
import com.web.UserSession;
import com.web.lpi.lpiProjectManager.LpiProjectManagerEntryForm;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;

public class VsoVolunteerFindEntryAction extends ActionSupport {
	
	private VsoVolunteerFindEntryForm frm;
	public VsoVolunteerFindEntryForm getFrm() {return frm;}
	public void setFrm(VsoVolunteerFindEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		frm.reset();
		frm.clearVlntrPrjLsts();
	}
	
	public String execute() {
		System.out.println("VsoVolunteerFindEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				
				if ((frm.getLink() != null && frm.getLink().equals("5")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("VsoVolunteerEntryAction execute LOAD");
					rtrn = "onLoad";
					this.reload();
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						VsoVolunteerModDetails details = new VsoVolunteerModDetails();
						VsoVolunteerFindControllerBean vlntrFC = new VsoVolunteerFindControllerBean();
						details.setVlntr_id(frm.getVlntrId());
						details.setVlntr_fname(frm.getVlntrFname());
						details.setVlntr_mname(frm.getVlntrMname());
						details.setVlntr_lname(frm.getVlntrLname());
						details.setVlntr_vs_exprtn(frm.getVlntrVisaExpiration());
						details.setVlntr_ntnly(frm.getVlntrNtnlyLst());
						
						details.setVlntr_cmp_cd(user.getUserCmpCode());// get only for VSO
						System.out.println("USER PROFILE >> "+user.getUserProfile() );
						if(user.getUserProfile()==1){//VSO
							details.setVlntr_cmp_cd(0);
						}
						
						ArrayList vl = vlntrFC.getByCriteria(details);//new ArrayList();
						//vl = vlntrFC.getByCriteria(details);
						
						if(vl.size()==0){
							frm.setAppMessage("No Records Found.");
						}else{
							this.reload();
							Iterator vli = vl.iterator();
							frm.clearVlntrList();
							while(vli.hasNext()){
								VsoVolunteerModDetails vll = (VsoVolunteerModDetails)vli.next();
								VsoVolunteerFindEntryLine vlf = new VsoVolunteerFindEntryLine();
								vlf.setVlntrCd(vll.getVlntr_cd());
								vlf.setVlntrId(vll.getVlntr_id());
								vlf.setVlntrName(vll.getVlntr_lname() +", "+vll.getVlntr_fname() +" "+vll.getVlntr_mname());
								vlf.setVlntrVisaExpiration(vll.getVlntr_vs_exprtn());
							
								
								try {
									LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
									LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(vll.getVlntr_cd());
									int rv_cd = rvlD.getRvl_rv_cd();
									
									LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
									LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd,0);
									LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
									LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
									vlf.setVlntrPrjct(prjMD.getPrjOrgName());
									vlf.setVlntrPrjDateFrom(prjMD.getPrjDurationFrom());
									vlf.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
									
									LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
									LpiProjectManagerModDetails pmMD = pmC.getPmByPmCode(prjMD.getPrjProjectManager());
									vlf.setVlntrPrjPm(pmMD.getPmName());
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								
								frm.setVlntrList(vlf);
							}
						}
						rtrn = "onLoad";
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("VsoVolunteerFindEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	public String validateFrm(VsoVolunteerFindEntryForm vfrm){
		System.out.println("VsoVolunteerFindEntryAction execute validateFrm");
		
		String errMsg = "";
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}
	
	

}
