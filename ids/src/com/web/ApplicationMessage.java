package com.web;

import java.util.ArrayList;

public class ApplicationMessage {

	public String sessionKey = "sessionKey";
	private String appMessage;
	

	public String getAppMessage() {
		return appMessage;
	}

	public void setAppMessage(String appMessage) {
		this.appMessage = appMessage;
	}
	
	public String btnName="";
		
	public String getBtnName() {
		return btnName;
	}

	public void setBtnName(String btnName) {
		this.btnName = btnName;
	}

	public String link="";
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	


	private String address;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public void clearAddress(){
		this.address = "";
	}
	

	private String gender;
	private String genderList;
	private ArrayList genderLists = new ArrayList();
	
	
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGenderList() {
		return genderList;
	}

	public void setGenderList(String genderList) {
		this.genderList = genderList;
	}

	public ArrayList getGenderLists() {
		return genderLists;
	}

	public void setGenderLists(String gender) {
		this.genderLists.add(gender);
	}
	
	public void clearGenderLists(){
		this.genderLists = new ArrayList();
		genderLists.add("MALE");
		genderLists.add("FEMALE");
	}

	private String country;
	private String countryList;
	private ArrayList countryLists;
	

	private String region;
	private String regionList;
	public ArrayList regionLists;

	public String province;
	private String provinceList;
	private ArrayList provinceLists;

	public String municipality;
	private String municipalityList;
	private ArrayList municipalityLists;

	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryList() {
		return countryList;
	}

	public void setCountryList(String countryList) {
		this.countryList = countryList;
	}

	public ArrayList getCountryLists() {
		return countryLists;
	}

	public void setCountryLists(String country) {
		this.countryLists.add(country);
	}

	public void clearCountryLists() {
		this.countryLists = new ArrayList();
	}

	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegionList() {
		return regionList;
	}

	public void setRegionList(String regionList) {
		this.regionList = regionList;
	}

	public ArrayList getRegionLists() {
		return regionLists;
	}

	public void setRegionLists(String region) {
		this.regionLists.add(region);
	}

	public void clearRegionLists() {
		this.regionLists = new ArrayList();
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvinceList() {
		return provinceList;
	}

	public void setProvinceList(String provinceList) {
		this.provinceList = provinceList;
	}

	public ArrayList getProvinceLists() {
		return provinceLists;
	}

	public void setProvinceLists(String province) {
		this.provinceLists.add(province);
	}

	public void clearProvinceLists() {
		//this.provinceLists.clear();
		this.provinceLists = new ArrayList();
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getMunicipalityList() {
		return municipalityList;
	}

	public void setMunicipalityList(String municipalityList) {
		this.municipalityList = municipalityList;
	}

	public ArrayList getMunicipalityLists() {
		return municipalityLists;
	}

	public void setMunicipalityLists(String municipality) {
		this.municipalityLists.add(municipality);
	}

	public void clearMunicipalityLists() {
		this.municipalityLists = new ArrayList();
	}

}
