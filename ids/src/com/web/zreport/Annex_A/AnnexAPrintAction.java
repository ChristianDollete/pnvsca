package com.web.zreport.Annex_A;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.VsoVsoModDetails;
import com.web.vso.vsoSignup.VsoSignupForm;

public class AnnexAPrintAction extends ActionSupport  {
	
	private AnnexAPrintEntryForm frm;
	public AnnexAPrintEntryForm getFrm() {return frm;}
	public void setFrm(AnnexAPrintEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	

	private Map<String, Object> param=null;
	
	
	public Map<String, Object> getParam() {
		return param;
	}
	public void setParam(Map<String, Object> param) {
		this.param = param;
	}
	
	
	
	
	public void reload(){
		System.out.println("LpiProjectFindEntryAction reload");
		frm.reset();
		
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
		
	}
	
	
	public String execute() {
		System.out.println("AnnexAPrintAction execute");
		//String rtrn = "success";
		String rtrn = "failed";
		try{
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				
				if(frm.getLink()!=null && frm.getLink().equals("13") || frm.getBtnName().equalsIgnoreCase("Reset")){
					System.out.println("AnnexAPrintAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload();
					rtrn = "onLoad";
				} else if(frm.getLink()!=null && frm.getLink().equals("1313")){
					System.out.println("AnnexAPrintAction execute PRINT | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					param = new HashMap();
					
					VsoVsoControllerBean vsoCB = new VsoVsoControllerBean();
					VsoVsoModDetails details = vsoCB.getVsoByVsoId(frm.getVsoId());
					
					param.put("orgName", details.getVso_nm());
					
					String org_typ = "0";
					System.out.println("VSO TYPE >> "+ details.getVso_org_typ());
					if(details.getVso_org_typ() == 1){
						org_typ = "International Government Organization/ Program";
					}else if(details.getVso_org_typ() == 2){
						org_typ = "International Non-Government Organization";
					}else if(details.getVso_org_typ() == 3){
						org_typ = "Local Organization";
					}
					
					param.put("orgType", org_typ);
					param.put("orgTel", details.getVso_org_tlph_nmbr());
					param.put("orgFax", details.getVso_org_fx_nmbr());
					param.put("orgEmail", details.getVso_org_email());
					param.put("orgWebsite",details.getVso_org_wbst());
					param.put("orgHO", details.getVso_hd_offc_addrss());
					param.put("orgSEC", details.getVso_sec_rgstrtn_nmbr());
					param.put("orgDateEstablished", details.getVso_dt_estblishd());
					param.put("orgVision", details.getVso_vsn());
					param.put("orgMission",details.getVso_mssn());
					param.put("orgGoal", details.getVso_goal());
					param.put("orgRecruite", details.getVso_rcrtment());
					param.put("orgRecruite1", details.getVso_rcrtment());
					param.put("orgRecruite2", details.getVso_rcrtment());
					param.put("orgEngage", details.getVso_enggmnt());
					param.put("orgCapability", details.getVso_cpblty_bldng());
					param.put("orgPromotion", details.getVso_prmtn());
					param.put("orgNetworking", details.getVso_ntwrkng_of_vlntrs());
					param.put("orgOthers1", details.getVso_othrs1());
					param.put("orgOthers1Note", details.getVso_othrs_nt1());
					param.put("orgLiving", details.getVso_lvng());
					param.put("orgHousing", details.getVso_hsng());
					param.put("orgRecognition", details.getVso_rcgntn_or_awrd());
					param.put("orgTravel", details.getVso_trnng_and_ornttn());
					param.put("orgInsurance", details.getVso_insrnc());
					param.put("orgMeal", details.getVso_meal());
					param.put("orgTraining", details.getVso_trvl_allwnc());
					param.put("orgOthers2", details.getVso_othrs2());
					param.put("orgOthers2Note", details.getVso_othrs_nt2());
					param.put("orgBudget", details.getVso_annl_bdgt());
					param.put("orgSource", details.getVso_bdgt_scrc());
					param.put("orgHead", details.getVso_org_hd_nm());
					
					
					this.reload();
					rtrn = "success";
					
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					System.out.println("AnnexAPrintAction execute SEARCH | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					String errMsg = this.validateFrm(frm);
					if(errMsg.equals("")){
						
						VsoVsoModDetails details = new VsoVsoModDetails();
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						//this.reload();
						int org_type = 0;
						
						if(frm.getVsoTypeList().equals("International Government Organization/ Program")){
							org_type = 1;
						}else if(frm.getVsoTypeList().equals("International Non-Government Organization")){
							org_type = 2;
						}else if(frm.getVsoTypeList().equals("Local Organization")){
							org_type = 3;
						}
						details.setVso_org_typ(org_type);
						
						try {
							AdAddRegionModDetails rgnD = logonCB.getRegionByRgnName(frm.getRegionList());
							details.setVso_org_rgn(rgnD.getRgn_id());
						} catch (Exception e) {
							details.setVso_org_rgn(0);
						}
						try {
							AdAddProvinceModDetails prnvcD = logonCB.getProvinceByPrvcName(frm.getProvinceList());
							details.setVso_org_prvnc(prnvcD.getPrvnc_id());
						} catch (Exception e) {
							details.setVso_org_prvnc(0);
						}
						try {
							AdAddMunicipalityModDetails munDB = logonCB.getMunicipalityByMunName(frm.getMunicipalityList());
							details.setVso_org_mncplty(munDB.getMun_id());
						} catch (Exception e) {
							details.setVso_org_mncplty(0);
						}
						details.setVso_org_addrss(frm.getAddress());
						details.setVso_dt_estblishd(frm.getVsoDateEstablished());;
						
						this.reload();
						VsoVsoControllerBean vso = new VsoVsoControllerBean();
						ArrayList list = vso.getAllVsoByCriteria(details);
						
						if(list.size()==0){
							frm.setAppMessage("No Records Found. ");
						}
						Iterator i = list.iterator();
						frm.clearVsoLists();
						while(i.hasNext()){
							VsoVsoModDetails vsoMD = (VsoVsoModDetails)i.next();
							AnnexAPrintEntryLine aLn = new AnnexAPrintEntryLine();
							aLn.setVsoId(vsoMD.getVso_id());
							aLn.setVsoName(vsoMD.getVso_nm());
							
							String org_typ = "0";
							System.out.println("VSO TYPE >> "+ vsoMD.getVso_org_typ());
							if(vsoMD.getVso_org_typ() == 1){
								org_typ = "International Government Organization/ Program";
							}else if(vsoMD.getVso_org_typ() == 2){
								org_typ = "International Non-Government Organization";
							}else if(vsoMD.getVso_org_typ() == 3){
								org_typ = "Local Organization";
							}
							
							aLn.setVsoType(org_typ);
							aLn.setVsoDateEstablished(vsoMD.getVso_dt_estblishd());
							aLn.setVsoEmail(vsoMD.getVso_org_email());
							aLn.setVsoCoordinatorName(vsoMD.getVso_crdntr_nm());
							frm.setVsoLists(aLn);
							
						}
					}else{
						frm.clearRegionLists();
						frm.clearProvinceLists();
						frm.clearMunicipalityLists();
						frm.setAppMessage(errMsg);
					}
				
					rtrn = "onLoad";
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("AnnexAPrintAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				}
				
				
			}else{
				rtrn="login";
			}
		}catch (Exception e){
			e.printStackTrace();
			rtrn = "failed";
		}
		return rtrn;
		
		
	}
	
	
	public String validateFrm(VsoSignupForm frm){
		System.out.println("AnnexAPrintAction Validate Form");
		String errMsg = "";
		
		if(frm.getVsoDateEstablished()!=null || frm.getVsoDateEstablished().trim().length() >= 0){
			boolean date = common.validateDateFormat(frm.getVsoDateEstablished());
			if(date == false){

				errMsg += "Date Established is Invalid Format. ";
			}
		}
		
		return errMsg;
	}
	
	
	
	public void test(){
		/*HttpServletRequest request = (HttpServletRequest)ServletActionContext.getRequest();
		HttpServletResponse response = (HttpServletResponse)ServletActionContext.getResponse();
		HttpSession session = request.getSession();
		
		Map parameters = new HashMap();
		
		parameters.put("company", "PNVSCA");
		String filename = "/opt/pnvsca/ids/WebContent/resources/Annex_A_VSO_Registration.jasper";
		
		
		Report report = new Report();
*/
		
		//report.setBytes(JasperRunManager.
						//runReportToPdf(filename, parameters, 
						//new ApRepCheckVoucherPrintDS(list, ejbCVP.getAdPrfApCheckVoucherDataSource(user.getCmpCode()))));				     

		/*try {
			
			File f = new File(filename);
			if(f.exists()){
				System.out.println("EXIST >> "+ filename);
			}else{
				System.out.println("NOT EXIST >> "+filename);
			}*/
			/*
			JRDataSource jrDataSource = null;
			report.setBytes(JasperRunManager.runReportToPdf(filename, parameters, jrDataSource));
			report.setViewType("PDF");
			HttpSession sessionReport = request.getSession();
			*/
			/*
			
			OutputStream outStream = null;
			try {
				outStream = response.getOutputStream();
				outStream.write(report.getBytes(), 0, report.getBytes().length);
				response.getOutputStream().flush();
				response.getOutputStream().close();
			} catch (IOException e) {
				System.out.println("ERROR HERE 1");
				e.printStackTrace();
			}
			*/
			
			
			//sessionReport.setAttribute("PDF"+request.getSession().getId(), report);
			/*//HttpServletRequest request = (HttpServletRequest)ServletActionContext.getRequest();
			HttpServletResponse resp= (HttpServletResponse)ServletActionContext.getResponse();
			
			
			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(filename, parameters);
			//bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
			//resp.reset();
			resp.resetBuffer();
			resp.setContentType("application/pdf");
			resp.setContentLength(bytes.length);
			ServletOutputStream ouputStream;
			try {
				ouputStream = resp.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			
		/*} catch (JRException e) {
			System.out.println("ERROR HERE 2");
			e.printStackTrace();*/
		/*} catch (IllegalStateException e){
			System.out.println("ERROR HERE 3");
		} catch (Exception e) {
			System.out.println("ERROR HERE 4");
		} 
		*/
		
		
		
	}
	
	
	
}
