package com.web.zreport.Annex_A;

import java.util.ArrayList;

import com.web.vso.vsoSignup.VsoSignupForm;


public class AnnexAPrintEntryForm extends VsoSignupForm {
	
	private ArrayList vsoLists = new ArrayList();

	public void clearVsoLists(){
		this.vsoLists = new ArrayList();
	}
	
	public ArrayList getVsoLists() {
		return vsoLists;
	}

	public void setVsoLists(AnnexAPrintEntryLine vso) {
		this.vsoLists.add(vso);
	}
	
	
}
