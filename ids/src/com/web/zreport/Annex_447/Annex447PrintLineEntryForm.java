package com.web.zreport.Annex_447;

import com.web.vso.vsoVolunteer.VsoVolunteerEntryForm;


public class Annex447PrintLineEntryForm extends VsoVolunteerEntryForm {
	
	private String vsoName;

	public String getVsoName() {
		return vsoName;
	}

	public void setVsoName(String vsoName) {
		this.vsoName = vsoName;
	}
	
	
}
