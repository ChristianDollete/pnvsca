package com.web.zreport.Annex_447;

import java.util.ArrayList;
import java.util.Iterator;

import com.txn.AdLogonControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.common;
import com.web.util.VsoVsoModDetails;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class Annex447PrintDS implements JRDataSource {

	private ArrayList data = new ArrayList();
	private int index = -1;

	
	
	public Annex447PrintDS(ArrayList list){
		Iterator i = list.iterator();
		while(i.hasNext()){
			Annex447PrintLineEntryForm rvLine1 = (Annex447PrintLineEntryForm)i.next();
			Annex447PrintLineEntryForm rvLine2 = new Annex447PrintLineEntryForm();
			rvLine2 = rvLine1;
			data.add(rvLine2);
		}
	}
	
	
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		  Object value = null;

	      String fieldName = field.getName();
	      if(fieldName.equals("vol_id")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrId();
	      }else  if(fieldName.equals("vol_name")){
	    	  String Name = "";
	    	  Name = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrFname();
	    	  Name += " "+((Annex447PrintLineEntryForm)data.get(index)).getVlntrMname();
	    	  Name += " "+((Annex447PrintLineEntryForm)data.get(index)).getVlntrLname();
	    	  value = Name;
	    	  
	      }else  if(fieldName.equals("vol_fname")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrFname();
	      }else  if(fieldName.equals("vol_mname")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrMname();
	      }else  if(fieldName.equals("vol_lname")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrLname();
	      }else if(fieldName.equals("vol_duration_to")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrPrjDateTo();
	      }else  if(fieldName.equals("vol_nationality")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrNtnly();
	      }else  if(fieldName.equals("vol_birthDate")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrDateOfBirth();
	      }else  if(fieldName.equals("vol_gender")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getGender();
	      }else  if(fieldName.equals("vol_civilStatus")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrCstatus();
	      }else  if(fieldName.equals("vol_email")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVlntrUserEmail();
	      }else  if(fieldName.equals("vol_vsoName")){
	    	  value = ((Annex447PrintLineEntryForm)data.get(index)).getVsoName();
	      }
	      

			
	      
		return value;
	}

	
	
	
	@Override
	public boolean next() throws JRException {
		  index++;
	      return (index < data.size());
	}

}
