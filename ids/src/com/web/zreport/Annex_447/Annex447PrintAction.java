package com.web.zreport.Annex_447;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.LpiRequestVolunteerLineControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.txn.VsoVolunteerFindControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;
import com.web.util.VsoVsoModDetails;
import com.web.vso.vsoVolunteerFind.VsoVolunteerFindEntryLine;

public class Annex447PrintAction extends ActionSupport  {
	
	private Annex447PrintEntryForm frm;
	public Annex447PrintEntryForm getFrm() {return frm;}
	public void setFrm(Annex447PrintEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	
	
	private Map<String, Object> param=null;
	
	
	public Map<String, Object> getParam() {
		return param;
	}
	public void setParam(Map<String, Object> param) {
		this.param = param;
	}
	
	private ArrayList rvlList = new ArrayList();
	
	
	public ArrayList getRvlList() {
		return rvlList;
	}
	public void setRvlList(ArrayList rvlList) {
		this.rvlList = rvlList;
	}
	
	public void reload(){
		System.out.println("Annex447PrintAction reload");
		frm.reset();
		frm.clearVlntrPrjLsts();
	
	}
	
	
	
	public String execute() {
		System.out.println("Annex447PrintAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				
				if ((frm.getLink() != null && frm.getLink().equals("15")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("Annex447PrintAction execute LOAD");
					rtrn = "onLoad";
					this.reload();
					
					
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					
					String errmsg = this.validateFrm(frm);
					
					if(errmsg.equalsIgnoreCase("")){
						
						VsoVolunteerModDetails details = new VsoVolunteerModDetails();
						VsoVolunteerFindControllerBean vlntrFC = new VsoVolunteerFindControllerBean();
						
						details.setVlntr_id(frm.getVlntrId());
						details.setVlntr_fname(frm.getVlntrFname());
						details.setVlntr_mname(frm.getVlntrMname());
						details.setVlntr_lname(frm.getVlntrLname());
						details.setVlntr_vs_exprtn(frm.getVlntrVisaExpiration());
						details.setVlntr_ntnly(frm.getVlntrNtnlyLst());
						
						details.setVlntr_cmp_cd(user.getUserCmpCode());// get only for VSO
						System.out.println("USER PROFILE >> "+user.getUserProfile() );
						if(user.getUserProfile()==1){//VSO
							details.setVlntr_cmp_cd(0);
						}
						
						ArrayList vl = vlntrFC.getByCriteria(details);//new ArrayList();
						//vl = vlntrFC.getByCriteria(details);
						
						if(vl.size()==0){
							frm.setAppMessage("No Records Found.");
						}else{
							this.reload();
							Iterator vli = vl.iterator();
							frm.clearVlntrList();
							while(vli.hasNext()){
								VsoVolunteerModDetails vll = (VsoVolunteerModDetails)vli.next();
								VsoVolunteerFindEntryLine vlf = new VsoVolunteerFindEntryLine();
								vlf.setVlntrCd(vll.getVlntr_cd());
								vlf.setVlntrId(vll.getVlntr_id());
								vlf.setVlntrName(vll.getVlntr_lname() +", "+vll.getVlntr_fname() +" "+vll.getVlntr_mname());
								vlf.setVlntrVisaExpiration(vll.getVlntr_vs_exprtn());
							
								
								try {
									LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
									LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(vll.getVlntr_cd());
									int rv_cd = rvlD.getRvl_rv_cd();
									
									LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
									LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd,0);
									LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
									LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
									vlf.setVlntrPrjct(prjMD.getPrjOrgName());
									vlf.setVlntrPrjDateFrom(prjMD.getPrjDurationFrom());
									vlf.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
									
									LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
									LpiProjectManagerModDetails pmMD = pmC.getPmByPmCode(prjMD.getPrjProjectManager());
									vlf.setVlntrPrjPm(pmMD.getPmName());
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								
								frm.setVlntrList(vlf);
							}
						}
						rtrn = "onLoad";
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("Annex447PrintAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				} else if ((frm.getLink() != null && (frm.getLink().equals("1515") || frm.getLink().equals("1515B")))  ){
					System.out.println("Annex447PrintAction execute PRINT | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					param = new HashMap();
					ArrayList vlntrList = new ArrayList();
					
					String vlntrId = frm.getVlntrId();
					VsoVolunteerEntryControllerBean vntrCB = new VsoVolunteerEntryControllerBean();
					VsoVolunteerModDetails vlntrMD =  vntrCB.getByVlntrId(vlntrId);
					Annex447PrintLineEntryForm pline = new Annex447PrintLineEntryForm();
					pline.setVlntrId(vlntrMD.getVlntr_id());
					pline.setVlntrLname(vlntrMD.getVlntr_lname());
					pline.setVlntrMname(vlntrMD.getVlntr_mname());
					pline.setVlntrFname(vlntrMD.getVlntr_fname());
					pline.setVlntrPrjDateTo("");
					
					try {
						LpiRequestVolunteerLineControllerBean rvlC = new LpiRequestVolunteerLineControllerBean();
						LpiRequestVolunteerLineModDetails rvlD = rvlC.getRvlByVlntrCode(vlntrMD.getVlntr_cd());
						int rv_cd = rvlD.getRvl_rv_cd();
						
						LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
						LpiRequestVolunteerModDetails rvD = rvC.getRvByRvCd(rv_cd,0);
						LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
						LpiProjectModDetails prjMD =  prjC.getPrjByPrjCode(rvD.getRv_prj_cd());
						
						pline.setVlntrPrjDateTo(prjMD.getPrjDurationTo());
						
						
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					pline.setVlntrNtnly(vlntrMD.getVlntr_ntnly());
					pline.setVlntrDateOfBirth(vlntrMD.getVlntr_brth_dt());
					pline.setGender(vlntrMD.getVlntr_sex());
					pline.setVlntrCstatus(vlntrMD.getVlntr_cvl_stts());
					pline.setVlntrUserEmail(vlntrMD.getVlntr_usr_email());
					
					VsoVsoControllerBean vsoCB = new VsoVsoControllerBean();
					VsoVsoModDetails vsoMD = vsoCB.getVsoByVsoCmpCd(vlntrMD.getVlntr_cmp_cd());
					pline.setVsoName(vsoMD.getVso_nm());
					
					
					
					int annex = 1;
					if(frm.getLink().equals("1515B")){
						annex = 2;
					}
					
					param.put("PRINT", annex);
					
					
					vlntrList.add(pline);
					param.put("LIST", vlntrList);
					
					this.reload();
					rtrn = "success";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(Annex447PrintEntryForm frm){
		System.out.println("Annex447PrintAction Validate Form");
		String errMsg = "";
		
		/*if(frm.getVsoDateEstablished()!=null || frm.getVsoDateEstablished().trim().length() >= 0){
			boolean date = common.validateDateFormat(frm.getVsoDateEstablished());
			if(date == false){

				errMsg += "Date Established is Invalid Format. ";
			}
		}
		*/
		return errMsg;
	}

		
	
	
	
}
