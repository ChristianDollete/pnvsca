package com.web.zreport.Annex_31;

import java.util.ArrayList;

import com.web.lpi.lpiRequestVolunteer.LpiRequestVolunteerEntryForm;


public class Annex31PrintEntryForm extends LpiRequestVolunteerEntryForm {
	
	private ArrayList lpiRvlList = new ArrayList();

	public void clearLpiRvlList(){
		lpiRvlList = new ArrayList();
	}
	public ArrayList getLpiRvlList() {
		return lpiRvlList;
	}

	public void setLpiRvlList(Annex31PrintEntryLine lpiRvlList) {
		this.lpiRvlList.add(lpiRvlList);
	}

	
	
	
}
