package com.web.zreport.Annex_31;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.StrutsResultSupport;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.util.ValueStack;



public class Annex31Print extends StrutsResultSupport  {
	
	protected String parameters;
    protected String rptFormat;
    
    protected ArrayList list = new ArrayList();
 
    public String getRptFormat() {
        return rptFormat;
    }
 
    public void setRptFormat(String rptFormat) {
        this.rptFormat = rptFormat;
    }
 
    public String getParameters() {
        return parameters;
    }
 
    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
 
    

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}
	
	

	private JasperReport getCompiledFile(String fileName, HttpServletRequest request) throws JRException {
		System.out.println("AnnexAPrint getCompiledFile");
		File reportFile = new File(fileName);//( request.getSession().getServletContext().getRealPath(fileName+ ".jasper")/*"/jasper/" + fileName + ".jasper")*/);
		// If compiled file is not found, then compile XML template
		if (!reportFile.exists()) {
			//JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath(/*"/jasper/" + fileName + */fileName+".jrxml"),request.getSession().getServletContext().getRealPath("/jasper/" + fileName + ".jasper"));
			//JasperCompileManager.compileReportToFile(fileName,(fileName+ ".jasper"));
			//JasperCompileManager.compileReport(fileName);
		}
		System.out.println("reportFile >> "+ reportFile.getPath());
	    
		JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
		
		return jasperReport;
	    
	} 
	
	/*
	private void generateReportHtml( JasperPrint jasperPrint, HttpServletRequest req, HttpServletResponse resp) throws IOException, JRException {
		System.out.println("AnnexAPrint generateReportHtml");
		
		Map imagesMap = new HashMap();
		JRHtmlExporter exporter = new JRHtmlExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter( JRExporterParameter.OUTPUT_WRITER, resp.getWriter());
		exporter.exportReport();
	}*/
	 
	    
	
	private void generateReportPDF (HttpServletResponse resp, Map parameters, JasperReport jasperReport, Connection conn)throws JRException, NamingException, SQLException, IOException {
		System.out.println("Annex31Print generateReportPDF");

		byte[] bytes = null;
		//bytes = JasperRunManager.runReportToPdf(jasperReport, parameters);
		System.out.println("jasperReport >> "+ jasperReport);
		bytes = JasperRunManager.runReportToPdf(jasperReport, parameters);
		//bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
		resp.reset();
		resp.resetBuffer();
		resp.setContentType("application/pdf");
		resp.setContentLength(bytes.length);
		ServletOutputStream ouputStream = resp.getOutputStream();
		System.out.println("BYTES >> "+ bytes);
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		ouputStream.close();
	}
/*
	private void generateReportPDF2 (HttpServletResponse resp,String fileName, Map parameters, JRDataSource jrDataSource)throws JRException, NamingException, SQLException, IOException {
		System.out.println("AnnexAPrint generateReportPDF2");

		byte[] bytes = null;
		bytes = JasperRunManager.runReportToPdf(fileName, parameters, jrDataSource);//(sourceFileName, parameters, jrDataSource)//(jasperReport, parameters);
		//bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
		resp.reset();
		resp.resetBuffer();
		resp.setContentType("application/pdf");
		resp.setContentLength(bytes.length);
		ServletOutputStream ouputStream = resp.getOutputStream();
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		ouputStream.close();
	}
	*/
	

	private void generateReportPDF3 (HttpServletResponse resp,String fileName, Map parameters, ArrayList list)throws JRException, NamingException, SQLException, IOException {
		System.out.println("AnnexAPrint generateReportPDF3");

		byte[] bytes = null;
		
		/*
		File file = new File("C:\\opt\\Reports\\Annex31_sub3.jasper");
		JasperReport subreport = (JasperReport)JRLoader.loadObject(file);
		parameters.put("prvLinePath", subreport);
		parameters.put("prvLineSub",  new Annex31PrintDS(list));
		JasperFillManager.fillReport("C:\\opt\\Reports\\Annex31_sub3.jasper", parameters, new Annex31PrintDS(list));
		//jasperPrint = JasperFillManager.fillReport(jasperReport, jasperParameter,new JREmptyDataSource() );
		*/
		
		
		bytes = JasperRunManager.runReportToPdf(fileName, parameters, new Annex31PrintDS(list));//(sourceFileName, parameters, jrDataSource)//(jasperReport, parameters);
		//bytes = JasperRunManager.runReportToPdf(jasperReport,parameters,conn);
		resp.reset();
		resp.resetBuffer();
		resp.setContentType("application/pdf");
		resp.setContentLength(bytes.length);
		ServletOutputStream ouputStream = resp.getOutputStream();
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		ouputStream.close();
	}
	
	@Override
	
	
	
	
	
	
	
	
	
	protected void doExecute(String arg0, ActionInvocation arg1)throws IOException, Exception {
		System.out.println("Annex31Print doExecute");

		
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			HttpServletResponse response = (HttpServletResponse) ServletActionContext.getResponse();
			
			String filename = "/opt/Reports/Annex_3.1_Request_for_Volunteer.jasper";//"C:/opt/reports/report1.jasper";
			String filename2 = request.getServletContext().getRealPath(filename);
			//servlet.getServletContext().getRealPath
			System.out.println("FILE PATH >> "+filename2);
			
			
			ValueStack stack = arg1.getStack();
			HashMap hmParams=(HashMap)stack.findValue(parameters);
			parameters = conditionalParse(parameters, arg1);
			
			ArrayList rvList = (ArrayList)hmParams.get("LIST");
			
			
			
			
			//System.out.println("LIST SIZE >> "+ hmParams.get("LIST"));
			/*
			Iterator i = list.iterator();
			while(i.hasNext()){
				String xxx = (String)i.next();
				System.out.println("LIST >>> "+ xxx);
			}*/
			
			
			
		
			
			
            
			JasperReport jasperReport = getCompiledFile(filename, request);
			generateReportPDF3(response, filename, hmParams, rvList);
			//generateReportPDF(response, hmParams, jasperReport, null); // For PDF report
			/*JRDataSource jrdata = new JRDataSource() {
				
				@Override
				public boolean next() throws JRException {
					// TODO Auto-generated method stub
					return false;
				}
				
				@Override
				public Object getFieldValue(JRField arg0) throws JRException {
					// TODO Auto-generated method stub
					return null;
				}
			};*/
			
			//generateReportPDF2(response, filename, parameter2, null);
			
			
			/*Reports report = new Reports();
			JRDataSource jrDataSource = null;
			report.setBytes(JasperRunManager.runReportToPdf(filename, parameter2, jrDataSource));
					//runReportToPdf(filename, parameters, jrDataSource));
			report.setViewType("PDF");
			response.reset();
			response.resetBuffer();
			response.setContentType("application/pdf");
			response.setContentLength(report.getBytes().length);
			ServletOutputStream ouputStream = response.getOutputStream();
			
			System.out.println("BYTE >> "+ report.getBytes());
			ouputStream.write(report.getBytes(), 0, report.getBytes().length);
			ouputStream.flush();
			ouputStream.close();*/
		} catch (IOException e){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		
		 
		
	}

	
}
