package com.web.zreport.Annex_31;

import com.web.lpi.lpiRequestVolunteer.LpiRequestVolunteerLineEntryForm;


public class Annex31PrintLineEntryForm extends LpiRequestVolunteerLineEntryForm {
	
	private int no_of_vols_needed;
	private String rvl_dt_of_arrival;
	
	public int getNo_of_vols_needed() {
		return no_of_vols_needed;
	}

	public void setNo_of_vols_needed(int no_of_vols_needed) {
		this.no_of_vols_needed = no_of_vols_needed;
	}

	public String getRvl_dt_of_arrival() {
		return rvl_dt_of_arrival;
	}

	public void setRvl_dt_of_arrival(String rvl_dt_of_arrival) {
		this.rvl_dt_of_arrival = rvl_dt_of_arrival;
	}
	
	
	
	
	
}
