package com.web.zreport.Annex_31;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVsoModDetails;

public class Annex31PrintAction extends ActionSupport  {
	
	private Annex31PrintEntryForm frm;
	public Annex31PrintEntryForm getFrm() {return frm;}
	public void setFrm(Annex31PrintEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	
	
	private Map<String, Object> param=null;
	
	
	public Map<String, Object> getParam() {
		return param;
	}
	public void setParam(Map<String, Object> param) {
		this.param = param;
	}
	
	private ArrayList rvlList = new ArrayList();
	
	
	public ArrayList getRvlList() {
		return rvlList;
	}
	public void setRvlList(ArrayList rvlList) {
		this.rvlList = rvlList;
	}
	
	
	
	
	public void reload(int cmp_cd){
		System.out.println("Annex31PrintAction reload");
		
		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
		Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
		UserSession user = (UserSession)sessionsMap.get(session.getId());
		
		frm.reset();

		LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
		try {
			
			ArrayList prjL = new ArrayList();
			if(user.getUserProfile() == 3 || user.getUserProfile() == 31){
				prjL = prjFC.getPrjAssignByCmpCode(cmp_cd);
			}else if(user.getUserProfile()==2 || user.getUserProfile()==21){
				prjL = prjFC.getPrjByCmpCode(cmp_cd);
			}else{
				prjL = prjFC.getPrjAll();
			}
			Iterator prji = prjL.iterator();
			frm.clearRv_prjLists();
			while(prji.hasNext()){
				LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
				System.out.println("prjName >> "+ prjD2.getPrjOrgName());
				frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
			}
			frm.setRv_prj_nm("0");
			frm.clearRv_ln();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public String execute() {
		System.out.println("Annex31PrintAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				if(frm.getBtnName() == null){
					frm.setBtnName("");
				}
				
				if (frm.getLink()!=null && frm.getLink().equals("14")){
					System.out.println("Annex31PrintAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload(user.getUserCmpCode());
					rtrn = "onLoad";
				}else if ((frm.getLink() != null && frm.getLink().equals("141")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("Annex31PrintAction execute LOAD RESET | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload(user.getUserCmpCode());
					rtrn = "onLoad";
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					System.out.println("Annex31PrintAction execute Search | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						
						LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
						LpiRequestVolunteerModDetails details = new LpiRequestVolunteerModDetails();
						
						details.setRv_id(frm.getRv_id());
						details.setRv_status(frm.getRv_statusList());
						details.setRv_drtn_mnths(frm.getRv_no_vlntr());
						try {
							details.setRv_no_vlntr(Integer.parseInt(frm.getRv_no_vlntr()));
						} catch (Exception e) {
							details.setRv_no_vlntr(0);
						}
						details.setRv_prj_cd(frm.getRv_prj_cd());
						
						if(user.getUserProfile() == 2 ){//  if lpi login 
							details.setRv_cmp_cd(user.getUserCmpCode());
						}else if(user.getUserProfile() == 3){ // if vso login
							details.setRvl_vso_cd(user.getUserCmpCode());
						}
						
						this.reload(user.getUserCmpCode());
						
						ArrayList rlList = rvC.getRvByCriteria(details);
						Iterator i = rlList.iterator();
						frm.clearRv_ln();
						while(i.hasNext()){
							LpiRequestVolunteerModDetails rvlBean = (LpiRequestVolunteerModDetails)i.next();
							Annex31PrintEntryLine rqstLine = new Annex31PrintEntryLine();
							//System.out.println(">> "+ rvlBean.getRv_id());
							rqstLine.setRv_cd(rvlBean.getRv_cd());
							rqstLine.setRv_id(rvlBean.getRv_id());
							rqstLine.setRv_prj_drtn_mnths(rvlBean.getRv_drtn_mnths());
							rqstLine.setRv_no_vlntr(String.valueOf(rvlBean.getRv_no_vlntr()));
							rqstLine.setRv_statusList(rvlBean.getRv_status());
							if(rvlBean.getRv_id()!=null){
								frm.setLpiRvlList(rqstLine);
							}
						}
		
						rtrn = "onLoad";
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("Annex31PrintAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				} else if (frm.getBtnName().equalsIgnoreCase("Return")){
					System.out.println("Annex31PrintAction execute RETURN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn = "onLoad";
				} else if (frm.getLink()!=null && frm.getLink().equals("1414")){
					System.out.println("Annex31PrintAction execute PRINT | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					param = new HashMap();
					
					
					LpiRequestVolunteerModDetails rvMD = new LpiRequestVolunteerModDetails();
					rvMD.setRv_id(frm.getRv_id());
					LpiRequestVolunteerControllerBean lpiRV = new LpiRequestVolunteerControllerBean();
					ArrayList rvList = lpiRV.getRvByCriteria(rvMD);
					Iterator i1 = rvList.iterator();
					while(i1.hasNext()){
						LpiRequestVolunteerModDetails rvMDi = (LpiRequestVolunteerModDetails)i1.next();
						
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						System.out.println("rvMDi.getRv_cmp_cd() >> "+rvMDi.getRv_cmp_cd());
						LpiLpiModDetails lpiMD = logonCB.getLpiByLpiCode(rvMDi.getRv_cmp_cd());
						
						param.put("lpiName", lpiMD.getLpi_nm());
						String lpiAddress = "";
						lpiAddress+=lpiMD.getLpi_addrss() +" ";
						
						
						ArrayList rgnList = new ArrayList();
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							String rgn_nm = "0";
							while (rl.hasNext()) {
								AdAddRegionModDetails detailsR = (AdAddRegionModDetails) rl.next();
								if(detailsR.getRgn_id() ==  lpiMD.getLpi_rgn()){
									rgn_nm = detailsR.getRgn_nm();
								}
							}
							
							AdAddRegionModDetails rgnd= logonCB .getRegionByRgnName(rgn_nm);
							ArrayList prnvcList =  logonCB.getAllProvinceByRgnId(rgnd.getRgn_nm());
							Iterator pl = prnvcList.iterator();
							String prnvc_nm = "0";
							while(pl.hasNext()){
								AdAddProvinceModDetails detailsP = (AdAddProvinceModDetails)pl.next();
								if(detailsP.getPrvnc_id() == lpiMD.getLpi_prvnc()){
									prnvc_nm = detailsP.getPrvnc_nm();
								}
							}
							
							ArrayList munList = new ArrayList();
							munList =  logonCB.getAllMunicipalityByPrvncId(prnvc_nm);
							Iterator muni = munList.iterator();
							String mun_nm="0";
							while(muni.hasNext()){
								AdAddMunicipalityModDetails detailsM = (AdAddMunicipalityModDetails)muni.next();
								if(detailsM.getMun_id() == lpiMD.getLpi_mncplty()){
									mun_nm = detailsM.getMun_nm();
								}
							}
							lpiAddress=lpiMD.getLpi_addrss() +" "+mun_nm+" "+prnvc_nm+" "+rgn_nm;
						} catch (Exception e) {
						}
						
						
						
						param.put("lpiAddress", lpiAddress);
						param.put("lpiTel", lpiMD.getLpi_tlphn_nmbr());
						param.put("lpiFax", lpiMD.getLpi_fx_nmbr());
						param.put("lpiWebsite", lpiMD.getLpi_wbst());
						param.put("lpiEmail", lpiMD.getLpi_email());
						String lpiType = "0";
						try {
							if(lpiMD.getLpi_typ() == 1){
								lpiType = "National Government Agency";
							}else if(lpiMD.getLpi_typ() == 2){
								lpiType = "Local Government Unit";
							}else if(lpiMD.getLpi_typ() == 3){
								lpiType = "Non-Government Organization";
							}else if(lpiMD.getLpi_typ() == 4){
								lpiType = "Academe";
							}else if(lpiMD.getLpi_typ() == 5){
								lpiType = "Private Sector";
							} 
						} catch (Exception e1) {
						}
						
						param.put("lpiOrgType", lpiType);
						param.put("lpiMandate", lpiMD.getLpi_mndt());
						
						
						
						LpiProjectEntryControllerBean prjCB = new LpiProjectEntryControllerBean();
						LpiProjectModDetails prjMD = prjCB.getPrjByPrjCode(rvMDi.getRv_prj_cd());
						param.put("projectName", prjMD.getPrjOrgName());
						param.put("prjObjective", prjMD.getPrjProjectObjective());
						
						String prjLocation ="";
						
						rgnList = new ArrayList();
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							String rgn_nm = "0";
							while (rl.hasNext()) {
								AdAddRegionModDetails detailsR = (AdAddRegionModDetails) rl.next();
								if(detailsR.getRgn_id() ==  prjMD.getPrjRgn()){
									rgn_nm = detailsR.getRgn_nm();
								}
							}
							
							AdAddRegionModDetails rgnd= logonCB .getRegionByRgnName(rgn_nm);
							ArrayList prnvcList =  logonCB.getAllProvinceByRgnId(rgnd.getRgn_nm());
							Iterator pl = prnvcList.iterator();
							String prnvc_nm = "0";
							while(pl.hasNext()){
								AdAddProvinceModDetails detailsP = (AdAddProvinceModDetails)pl.next();
								if(detailsP.getPrvnc_id() == prjMD.getPrjPrvnc()){
									prnvc_nm = detailsP.getPrvnc_nm();
								}
							}
							
							ArrayList munList = new ArrayList();
							munList =  logonCB.getAllMunicipalityByPrvncId(prnvc_nm);
							Iterator muni = munList.iterator();
							String mun_nm="0";
							while(muni.hasNext()){
								AdAddMunicipalityModDetails detailsM = (AdAddMunicipalityModDetails)muni.next();
								if(detailsM.getMun_id() == prjMD.getPrjMncplty()){
									mun_nm = detailsM.getMun_nm();
								}
							}
							prjLocation=prjMD.getPrjAddress() +" "+mun_nm+" "+prnvc_nm+" "+rgn_nm;
						} catch (Exception e) {
						}
						param.put("prjLocation", prjLocation);
						param.put("prjBeneficiaries", prjMD.getPrjTargetBenefits());
						param.put("prjBudgetSource", prjMD.getPrjBudgetSource());
						param.put("prjStatus", "N/A");
						param.put("prjVolunteerDuration", prjMD.getPrjDurationFrom() +" to "+prjMD.getPrjDurationTo());
						
						LpiProjectManagerEntryControllerBean pmCB = new LpiProjectManagerEntryControllerBean();
						
						LpiProjectManagerModDetails pmMD = pmCB.getPmByPmCode(prjMD.getPrjProjectManager());
						
						param.put("prjManager", pmMD.getPmName());
						param.put("pmPosition", pmMD.getPmPosition());
						param.put("pmContact", pmMD.getPmEmail());
						
				
						LpiRequestVolunteerModDetails rvMD2 = lpiRV.getRvByRvCd(rvMDi.getRv_cd(), 0);
						
						ArrayList rvlList =  rvMD2.getRvlList();
						Iterator il = rvlList.iterator();
						ArrayList rvlLineList = new ArrayList();
						while(il.hasNext()){
							LpiRequestVolunteerLineBean rvlLine = (LpiRequestVolunteerLineBean)il.next();
							
							Annex31PrintLineEntryForm pline = new Annex31PrintLineEntryForm();
							
							pline.setRvl_ln_no(rvlLine.getRvl_ln_no());
							
							pline.setRvl_fld_spcfctn(rvlLine.getRvl_fld_spcfctn());
							pline.setRvl_edctn(rvlLine.getRvl_edctn());
							pline.setRvl_exprnc_trnnng(rvlLine.getRvl_exprnc_trnnng());
							
							AdLogonControllerBean aCB = new AdLogonControllerBean();
							VsoVsoModDetails vsoMD = aCB.getVsoByVsoCode(rvlLine.getRvl_vso_cd());
							//vsoCB.getAllVsoByCriteria(details)
							pline.setRvl_vso_nm(vsoMD.getVso_nm());
							pline.setNo_of_vols_needed(rvMDi.getRv_no_vlntr());
							pline.setRvl_prfrd_dt_arrvl(rvlLine.getRvl_prfrd_dt_arrvl());
							
							
							
							pline.setRvl_fclty(common.IntToBoolean(rvlLine.getRvl_fclty()));
							pline.setRvl_trnsprt(common.IntToBoolean(rvlLine.getRvl_trnsprt()));
							pline.setRvl_housing(common.IntToBoolean(rvlLine.getRvl_housing()));
							pline.setRvl_emrgncy(common.IntToBoolean(rvlLine.getRvl_emrgncy()));
							pline.setRvl_othrs(common.IntToBoolean(rvlLine.getRvl_othrs()));
							pline.setRvl_othrs_spcfy(rvlLine.getRvl_othrs_spcfy());
							
							
							
							pline.setRvl_objctv(rvlLine.getRvl_objctv());
							pline.setRvl_output(rvlLine.getRvl_output());
							pline.setRvl_actvts(rvlLine.getRvl_actvts());
							pline.setRvl_implmttn_prd(rvlLine.getRvl_implmttn_prd());
							pline.setRvl_fndng_src(rvlLine.getRvl_fndng_src());
							
							
							rvlLineList.add(pline);
							
						}
						
						
						
						
						
						
						
						
						param.put("LIST", rvlLineList);
						
						
						
						
						
						
					}//END WHILE
					
					
					
					param.put("orgName", "");
					
					
					
					
					this.reload(user.getUserCmpCode());
					rtrn = "success";
				}
				
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(Annex31PrintEntryForm frm){
		System.out.println("AnnexAPrintAction Validate Form");
		String errMsg = "";
		
		/*if(frm.getVsoDateEstablished()!=null || frm.getVsoDateEstablished().trim().length() >= 0){
			boolean date = common.validateDateFormat(frm.getVsoDateEstablished());
			if(date == false){

				errMsg += "Date Established is Invalid Format. ";
			}
		}
		*/
		return errMsg;
	}

		
	
	
	
}
