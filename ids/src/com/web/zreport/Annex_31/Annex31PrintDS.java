package com.web.zreport.Annex_31;

import java.util.ArrayList;
import java.util.Iterator;

import com.txn.AdLogonControllerBean;
import com.web.common;
import com.web.util.VsoVsoModDetails;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class Annex31PrintDS implements JRDataSource {

	private ArrayList data = new ArrayList();
	private int index = -1;

	
	
	public Annex31PrintDS(ArrayList list){
		
		Iterator i = list.iterator();
		while(i.hasNext()){
			Annex31PrintLineEntryForm rvLine1 = (Annex31PrintLineEntryForm)i.next();
			Annex31PrintLineEntryForm rvLine2 = new Annex31PrintLineEntryForm();
			rvLine2 = rvLine1;
			data.add(rvLine2);
		}
	}
	
	
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		  Object value = null;

	      String fieldName = field.getName();
	      if(fieldName.equals("RVLine")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_ln_no();
	      }else if(fieldName.equals("specification")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_fld_spcfctn();
	      }else if(fieldName.equals("education")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_edctn();
	      }else if(fieldName.equals("experience")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_exprnc_trnnng();
	      }else if(fieldName.equals("vsoName")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_vso_nm();
	      }else if(fieldName.equals("noOfVolRequested")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getNo_of_vols_needed();
	      }else if(fieldName.equals("preferedDateArrival")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_prfrd_dt_arrvl();
	      }else if(fieldName.equals("facilities")){//boolean
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_fclty();
	      }else if(fieldName.equals("transport")){//boolean
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_trnsprt();
	      }else if(fieldName.equals("housing")){//boolean
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_housing();
	      }else if(fieldName.equals("emergency")){//boolean
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_emrgncy();
	      }else if(fieldName.equals("others")){//boolean
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_othrs();
	      }else if(fieldName.equals("othersSpecify")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_othrs_spcfy();
	      }else if(fieldName.equals("objective")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_objctv();
	      }else if(fieldName.equals("output")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_output();
	      }else if(fieldName.equals("activities")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_actvts();
	      }else if(fieldName.equals("implementation")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_implmttn_prd();
	      }else if(fieldName.equals("fundingSource")){
	    	  value = ((Annex31PrintLineEntryForm)data.get(index)).getRvl_fndng_src();
	      }
	      
	      
	      
	      
	      /*
	      
	      
	      
	      	pline.setRvl_edctn(rvlLine.getRvl_edctn());
			pline.setRvl_exprnc_trnnng(rvlLine.getRvl_exprnc_trnnng());
			
			AdLogonControllerBean aCB = new AdLogonControllerBean();
			VsoVsoModDetails vsoMD = aCB.getVsoByVsoCode(rvlLine.getRvl_vso_cd());
			//vsoCB.getAllVsoByCriteria(details)
			pline.setRvl_vso_nm(vsoMD.getVso_nm());
			pline.setNo_of_vols_needed(rvMD.getRv_no_vlntr());
			pline.setRvl_prfrd_dt_arrvl(rvlLine.getRvl_prfrd_dt_arrvl());
			
			pline.setRvl_fclty(common.IntToBoolean(rvlLine.getRvl_fclty()));
			pline.setRvl_trnsprt(common.IntToBoolean(rvlLine.getRvl_trnsprt()));
			pline.setRvl_housing(common.IntToBoolean(rvlLine.getRvl_housing()));
			pline.setRvl_emrgncy(common.IntToBoolean(rvlLine.getRvl_emrgncy()));
			pline.setRvl_othrs(common.IntToBoolean(rvlLine.getRvl_othrs()));
			pline.setRvl_othrs_spcfy(rvlLine.getRvl_othrs_spcfy());
			
			
			
			pline.setRvl_objctv(rvlLine.getRvl_objctv());
			pline.setRvl_output(rvlLine.getRvl_output());
			pline.setRvl_actvts(rvlLine.getRvl_actvts());
			pline.setRvl_implmttn_prd(rvlLine.getRvl_implmttn_prd());
			pline.setRvl_fndng_src(rvlLine.getRvl_fndng_src());
	      
	      
	      */
	      
		return value;
	}

	
	
	
	@Override
	public boolean next() throws JRException {
		  index++;
	      return (index < data.size());
	}

}
