package com.web.lpi.lpiRequestVolunteerExtensionAssignment;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerExtensionAssignmentBean;
import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdCompanyControllerBean;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.LpiRequestVolunteerExtensionAssignmentControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.txn.VsoVolunteerFindControllerBean;
import com.web.UserSession;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;
import com.web.vso.vsoVolunteerFind.VsoVolunteerFindEntryForm;

public class LpiRequestVolunteerExtensionAssignmentEntryAction extends ActionSupport {
	
	private LpiRequestVolunteerExtensionAssignmentEntryForm frm;
	public LpiRequestVolunteerExtensionAssignmentEntryForm getFrm() {return frm;}
	public void setFrm(LpiRequestVolunteerExtensionAssignmentEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		frm.reset();
		
		
	}
	
	public String execute() {
		System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				if(frm.getBtnName() == null){
					frm.setBtnName("");
				}
			
				
				if (frm.getLink()!=null && frm.getLink().equals("99")){
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute Load");
					
					try {
						AdLogonControllerBean lpiC = new AdLogonControllerBean();
						LpiLpiModDetails lpiMD = lpiC.getLpiByLpiCode(user.getUserCmpCode());
						frm.setLpi_nm(lpiMD.getLpi_nm());
						frm.setLpi_addrss(lpiMD.getLpi_addrss());
						frm.setLpi_email(lpiMD.getLpi_prmry_email());
						frm.setLpi_cntct_no(lpiMD.getLpi_crdntr_tlphn_nmbr());
						frm.setRva_lpi_cd(lpiMD.getLpi_cd());
						
						
						
						

						rtrn = "onLoad";
						
					} catch (Exception e) {
						e.printStackTrace();
						rtrn="failed";
					}
					
				}else if ((frm.getLink() != null && frm.getLink().equals("9")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute Load");
					try {
						
						this.reload();
						
						AdLogonControllerBean lpiC = new AdLogonControllerBean();
						LpiLpiModDetails lpiMD = lpiC.getLpiByLpiCode(user.getUserCmpCode());
						frm.setLpi_nm(lpiMD.getLpi_nm());
						frm.setLpi_addrss(lpiMD.getLpi_addrss());
						frm.setLpi_email(lpiMD.getLpi_prmry_email());
						frm.setLpi_cntct_no(lpiMD.getLpi_crdntr_tlphn_nmbr());
						frm.setRva_lpi_cd(lpiMD.getLpi_cd());
						
						
						LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
						ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
						Iterator prji = prjL.iterator();
						frm.clearRva_prjct_lists();
						while(prji.hasNext()){
							LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
							frm.setRva_prjct_lists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
							/*if(prjD2.getPrjCd()==rvd.getRv_prj_cd()){
								rv_prj_nm = prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName();
							}*/
						}
						frm.setRva_prjct("0");
						
						
						rtrn = "onLoad";
						
					} catch (Exception e) {
						e.printStackTrace();
						rtrn="failed";
					}
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute Save");
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						LpiRequestVolunteerExtensionAssignmentBean lpveaBean = new LpiRequestVolunteerExtensionAssignmentBean();
						lpveaBean.setRva_cd(frm.getRva_cd());
						lpveaBean.setRva_id(frm.getRva_id());
						lpveaBean.setRva_lpi_cd(frm.getRva_lpi_cd());
						lpveaBean.setRva_lpi_prjct_cd(frm.getRva_lpi_prjct_cd());
						
						System.out.println("Volunteer Name Selected >>"+frm.getRva_vlntr_nm_list() );
						int vlntr_cd = 0;
						if(frm.getRva_vlntr_nm_list().equals("")){
							try {
																
								String vlntr_lst =  frm.getRva_vlntr_nm_list();
								String[] vlntr_nm = vlntr_lst.split(" --- ");
								vlntr_lst = vlntr_nm[0].toString();
								
								VsoVolunteerEntryControllerBean vlntrC = new VsoVolunteerEntryControllerBean();
								VsoVolunteerModDetails vlntrMD =  vlntrC.getByVlntrId(vlntr_lst);
								vlntr_cd =  vlntrMD.getVlntr_cd();
							} catch (Exception e1) {
								e1.printStackTrace();
								throw new Exception();
							}
						}
						
						lpveaBean.setRva_vlntr_cd(vlntr_cd);
						lpveaBean.setRva_drtn_vlntr_assgnmnt(frm.getRva_drtn_vlntr_assgnmnt());
						lpveaBean.setRva_prd_extnsn_rqst(frm.getRva_prd_extnsn_rqst());
						lpveaBean.setRva_rsn_rqst_extnsn(frm.getRva_rsn_rqst_extnsn());
						lpveaBean.setRva_dfrd_due_rsn(frm.getRva_dfrd_due_rsn());//int
						lpveaBean.setRva_dfrd_due(frm.getRva_dfrd_due());
						lpveaBean.setRva_ds_apprvd_rsn(frm.getRva_ds_apprvd_rsn());
						lpveaBean.setRva_apprvd_to(frm.getRva_apprvd_to());
						lpveaBean.setRva_apprvd_prd(frm.getRva_apprvd_prd());
						lpveaBean.setRva_dt(frm.getRva_dt());
						lpveaBean.setRva_cmp_cd(user.getUserCmpCode());
						
						
						if(lpveaBean.getRva_cd()==0){
							LpiRequestVolunteerExtensionAssignmentControllerBean rvaC = new LpiRequestVolunteerExtensionAssignmentControllerBean();
							int rva_cd = rvaC.createRVA(lpveaBean);
							
							if(rva_cd != 0){
								frm.setAppMessage("Request for Assignment Extension Has Bean Save.");
							}
							
							
						}else{
							
						}
					
						
						
						rtrn = "onLoad";
						
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				} else if (frm.getBtnName().equalsIgnoreCase("Return")){
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute RETURN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					 
					 
					
					rtrn = "onLoad";
				}else{
					System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute ELSE | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					
					rtrn = "onLoad";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(LpiRequestVolunteerExtensionAssignmentEntryForm vfrm){
		System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction execute validateFrm");
		
		String errMsg = "";
		
		int rvln = 0;
		
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}
	
	public String prjLocation;
	public String getPrjLocation() {return prjLocation;}
	
	
	public String getProjectLocation(){
		System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction getProjectLocation");
		String prjAddress = "";
		String prjName = "";
		try {
			LpiProjectEntryControllerBean prjF= new LpiProjectEntryControllerBean();
			try {
				if(!frm.getRva_prjct().equals("0")){
					String[] prjList = frm.getRva_prjct().split(" --- ");
					prjName = prjList[0].toString();
					System.out.println("prjName ID >> "+prjName);
					LpiProjectModDetails prjD = prjF.getPrjByPrjId(prjName);
					prjAddress = prjD.getPrjAddress();
				}
				prjLocation = prjAddress;
			} catch (Exception e) {
				prjName="";
				System.out.println("CANNOT SPLT PRJ ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	

	
	public ArrayList<String> volunteers;
	public ArrayList<String> getVolunteers() {return volunteers;}
	
	public String getVolunteerName() {
		System.out.println("LpiRequestVolunteerExtensionAssignmentEntryAction getVolunteerName");
		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
		System.out.println("session ID >> "+ request.getSession().getId());
		UserSession user = (UserSession)session.getAttribute(request.getSession().getId());
		
		VsoVolunteerFindControllerBean vcf = new VsoVolunteerFindControllerBean();
		
		ArrayList volList = new ArrayList();
		try {
			String[] prjList = frm.getRva_prjct().split(" --- ");
			volList = vcf.getByProjectStatusAndCmpCode("On-going",prjList[0].toString(), user.getUserCmpCode());
			Iterator i = volList.iterator();
			volunteers =  new ArrayList<String>();
			frm.clearRva_vlntr_nm_lists();
			while(i.hasNext()){
				VsoVolunteerModDetails details = (VsoVolunteerModDetails)i.next();
				String vlntr_nm = details.getVlntr_id()+ " --- " + details.getVlntr_lname()+", "+ details.getVlntr_fname() +" "+ details.getVlntr_mname();
				frm.setRva_vlntr_nm_lists(vlntr_nm);
				System.out.println("VLNTR NAME >> "+vlntr_nm);
				volunteers.add(vlntr_nm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

}
