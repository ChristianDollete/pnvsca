package com.web.lpi.lpiRequestVolunteerExtensionAssignment;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiRequestVolunteerExtensionAssignmentEntryForm extends
		ApplicationMessage {

	private int rva_cd;
	private String rva_id;
	private int rva_lpi_cd;
	private String lpi_nm;
	private String lpi_addrss;
	private String lpi_email;
	private String lpi_cntct_no;
	private int rva_lpi_prjct_cd;
	private int rva_vlntr_cd;
	String rva_vlntr_nm;
	String rva_vlntr_nm_list;
	private ArrayList rva_vlntr_nm_lists = new ArrayList();
	private String rva_drtn_vlntr_assgnmnt;
	private String rva_prd_extnsn_rqst;
	private String rva_rsn_rqst_extnsn;
	private int rva_dfrd_due;
	private int rva_dfrd_due_rsn;
	private int rva_ds_apprvd;
	private int rva_ds_apprvd_rsn;
	String rva_apprvd_to;
	String rva_apprvd_prd;
	String rva_dt;
	private int rva_cmp_cd;
	
	
	private String rva_prjct;
	private String rva_prjct_list;
	private ArrayList rva_prjct_lists = new ArrayList();
	private String rva_prjct_lctn;
	
		
	

	public String getRva_vlntr_nm() {
		return rva_vlntr_nm;
	}

	public void setRva_vlntr_nm(String rva_vlntr_nm) {
		this.rva_vlntr_nm = rva_vlntr_nm;
	}

	public String getRva_vlntr_nm_list() {
		return rva_vlntr_nm_list;
	}

	public void setRva_vlntr_nm_list(String rva_vlntr_nm_list) {
		this.rva_vlntr_nm_list = rva_vlntr_nm_list;
	}
	
	public void clearRva_vlntr_nm_lists(){
		this.rva_vlntr_nm_lists = new ArrayList();
	}

	public ArrayList getRva_vlntr_nm_lists() {
		return rva_vlntr_nm_lists;
	}

	public void setRva_vlntr_nm_lists(String rva_vlntr_nm_list) {
		this.rva_vlntr_nm_lists.add(rva_vlntr_nm_list);
	}

	public String getRva_prjct_lctn() {
		return rva_prjct_lctn;
	}

	public void setRva_prjct_lctn(String rva_prjct_lctn) {
		this.rva_prjct_lctn = rva_prjct_lctn;
	}

	public String getRva_prjct() {
		return rva_prjct;
	}

	public void setRva_prjct(String rva_prjct) {
		this.rva_prjct = rva_prjct;
	}

	public String getRva_prjct_list() {
		return rva_prjct_list;
	}

	public void setRva_prjct_list(String rva_prjct_list) {
		this.rva_prjct_list = rva_prjct_list;
	}

	public void clearRva_prjct_lists(){
		this.rva_prjct_lists = new ArrayList();
	}
	
	public ArrayList getRva_prjct_lists() {
		return rva_prjct_lists;
	}

	public void setRva_prjct_lists(String rva_prjct_list) {
		this.rva_prjct_lists.add(rva_prjct_list);
	}

	public int getRva_cd() {
		return rva_cd;
	}

	public void setRva_cd(int rva_cd) {
		this.rva_cd = rva_cd;
	}

	public String getRva_id() {
		return rva_id;
	}

	public void setRva_id(String rva_id) {
		this.rva_id = rva_id;
	}

	public int getRva_lpi_cd() {
		return rva_lpi_cd;
	}

	public void setRva_lpi_cd(int rva_lpi_cd) {
		this.rva_lpi_cd = rva_lpi_cd;
	}

	public String getLpi_nm() {
		return lpi_nm;
	}

	public void setLpi_nm(String lpi_nm) {
		this.lpi_nm = lpi_nm;
	}

	public String getLpi_addrss() {
		return lpi_addrss;
	}

	public void setLpi_addrss(String lpi_addrss) {
		this.lpi_addrss = lpi_addrss;
	}

	public String getLpi_email() {
		return lpi_email;
	}

	public void setLpi_email(String lpi_email) {
		this.lpi_email = lpi_email;
	}

	public String getLpi_cntct_no() {
		return lpi_cntct_no;
	}

	public void setLpi_cntct_no(String lpi_cntct_no) {
		this.lpi_cntct_no = lpi_cntct_no;
	}

	public int getRva_lpi_prjct_cd() {
		return rva_lpi_prjct_cd;
	}

	public void setRva_lpi_prjct_cd(int rva_lpi_prjct_cd) {
		this.rva_lpi_prjct_cd = rva_lpi_prjct_cd;
	}

	public int getRva_vlntr_cd() {
		return rva_vlntr_cd;
	}

	public void setRva_vlntr_cd(int rva_vlntr_cd) {
		this.rva_vlntr_cd = rva_vlntr_cd;
	}

	public String getRva_drtn_vlntr_assgnmnt() {
		return rva_drtn_vlntr_assgnmnt;
	}

	public void setRva_drtn_vlntr_assgnmnt(String rva_drtn_vlntr_assgnmnt) {
		this.rva_drtn_vlntr_assgnmnt = rva_drtn_vlntr_assgnmnt;
	}

	public String getRva_prd_extnsn_rqst() {
		return rva_prd_extnsn_rqst;
	}

	public void setRva_prd_extnsn_rqst(String rva_prd_extnsn_rqst) {
		this.rva_prd_extnsn_rqst = rva_prd_extnsn_rqst;
	}

	public String getRva_rsn_rqst_extnsn() {
		return rva_rsn_rqst_extnsn;
	}

	public void setRva_rsn_rqst_extnsn(String rva_rsn_rqst_extnsn) {
		this.rva_rsn_rqst_extnsn = rva_rsn_rqst_extnsn;
	}

	public int getRva_dfrd_due() {
		return rva_dfrd_due;
	}

	public void setRva_dfrd_due(int rva_dfrd_due) {
		this.rva_dfrd_due = rva_dfrd_due;
	}

	public int getRva_dfrd_due_rsn() {
		return rva_dfrd_due_rsn;
	}

	public void setRva_dfrd_due_rsn(int rva_dfrd_due_rsn) {
		this.rva_dfrd_due_rsn = rva_dfrd_due_rsn;
	}

	public int getRva_ds_apprvd() {
		return rva_ds_apprvd;
	}

	public void setRva_ds_apprvd(int rva_ds_apprvd) {
		this.rva_ds_apprvd = rva_ds_apprvd;
	}

	public int getRva_ds_apprvd_rsn() {
		return rva_ds_apprvd_rsn;
	}

	public void setRva_ds_apprvd_rsn(int rva_ds_apprvd_rsn) {
		this.rva_ds_apprvd_rsn = rva_ds_apprvd_rsn;
	}

	public String getRva_apprvd_to() {
		return rva_apprvd_to;
	}

	public void setRva_apprvd_to(String rva_apprvd_to) {
		this.rva_apprvd_to = rva_apprvd_to;
	}

	public String getRva_apprvd_prd() {
		return rva_apprvd_prd;
	}

	public void setRva_apprvd_prd(String rva_apprvd_prd) {
		this.rva_apprvd_prd = rva_apprvd_prd;
	}

	public String getRva_dt() {
		return rva_dt;
	}

	public void setRva_dt(String rva_dt) {
		this.rva_dt = rva_dt;
	}

	public int getRva_cmp_cd() {
		return rva_cmp_cd;
	}

	public void setRva_cmp_cd(int rva_cmp_cd) {
		this.rva_cmp_cd = rva_cmp_cd;
	}

	public void reset() {
		rva_cd = 0;
		rva_id = "";
		rva_lpi_cd = 0;
		lpi_nm = "";
		lpi_addrss = "";
		lpi_email = "";
		lpi_cntct_no = "";
		rva_lpi_prjct_cd = 0;
		rva_vlntr_cd = 0;
		rva_drtn_vlntr_assgnmnt = "";
		rva_prd_extnsn_rqst = "";
		rva_rsn_rqst_extnsn = "";
		rva_dfrd_due = 0;
		rva_dfrd_due_rsn = 0;
		rva_ds_apprvd = 0;
		rva_ds_apprvd_rsn = 0;
		rva_apprvd_to = "";
		rva_apprvd_prd = "";
		rva_dt = "";
		rva_cmp_cd = 0;
		this.clearRva_prjct_lists();
		rva_prjct="0";
		this.clearRva_vlntr_nm_lists();
		this.rva_vlntr_nm = "0";

	}

}
