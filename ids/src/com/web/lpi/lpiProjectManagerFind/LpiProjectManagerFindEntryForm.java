package com.web.lpi.lpiProjectManagerFind;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiProjectManagerFindEntryForm extends ApplicationMessage  {
	
	private int pmCd;
	private String pmId;
	private String pmName;
	private String pmUserName;
	private String pmUserPass;
	private String pmUserConfirmPass;
	private String pmPosition;
	private String pmEmail;
	private String pmTelNo;
	private String pmMobNo;
	private String pmFaxNo;
	
	public ArrayList pmList;
		
	public ArrayList getPmList() {
		return pmList;
	}
	public void setPmList(LpiProjectManagerFindEntryLine lfrm) {
		this.pmList.add(lfrm);
	}
	
	public void clearPmList(){
		this.pmList = new ArrayList();
	}
	
	public int getPmCd() {
		return pmCd;
	}
	public void setPmCd(int pmCd) {
		this.pmCd = pmCd;
	}
	public String getPmId() {
		return pmId;
	}
	public void setPmId(String pmId) {
		this.pmId = pmId;
	}
	public String getPmName() {
		return pmName;
	}
	public void setPmName(String pmName) {
		this.pmName = pmName;
	}
	public String getPmUserName() {
		return pmUserName;
	}
	public void setPmUserName(String pmUserName) {
		this.pmUserName = pmUserName;
	}
	public String getPmUserPass() {
		return pmUserPass;
	}
	public void setPmUserPass(String pmUserPass) {
		this.pmUserPass = pmUserPass;
	}
	public String getPmUserConfirmPass() {
		return pmUserConfirmPass;
	}
	public void setPmUserConfirmPass(String pmUserConfirmPass) {
		this.pmUserConfirmPass = pmUserConfirmPass;
	}
	public String getPmPosition() {
		return pmPosition;
	}
	public void setPmPosition(String pmPosition) {
		this.pmPosition = pmPosition;
	}
	public String getPmEmail() {
		return pmEmail;
	}
	public void setPmEmail(String pmEmail) {
		this.pmEmail = pmEmail;
	}
	public String getPmTelNo() {
		return pmTelNo;
	}
	public void setPmTelNo(String pmTelNo) {
		this.pmTelNo = pmTelNo;
	}
	public String getPmMobNo() {
		return pmMobNo;
	}
	public void setPmMobNo(String pmMobNo) {
		this.pmMobNo = pmMobNo;
	}
	public String getPmFaxNo() {
		return pmFaxNo;
	}
	public void setPmFaxNo(String pmFaxNo) {
		this.pmFaxNo = pmFaxNo;
	}
	
	
	public void reset(){
		

		try {
			pmCd=0;
			pmId="";
			pmName="";
			pmUserName="";
			pmUserPass="";
			pmUserConfirmPass="";
			pmPosition="";
			pmEmail="";
			pmTelNo="";
			pmMobNo="";
			pmFaxNo="";
			clearGenderLists();
			clearPmList();
			
		} catch (Exception e) {
			System.out.println("RESET << ");
			
			e.printStackTrace();
		}
		
	}
	
	
	

}
