package com.web.lpi.lpiProjectManagerFind;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.web.UserSession;
import com.web.lpi.lpiProjectManager.LpiProjectManagerEntryForm;
import com.web.util.LpiProjectManagerModDetails;

public class LpiProjectManagerFindEntryAction extends ActionSupport {

	private LpiProjectManagerFindEntryForm frm;
	public LpiProjectManagerFindEntryForm getFrm() {return frm;}
	public void setFrm(LpiProjectManagerFindEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		System.out.println("LpiProjectManagerFindEntryAction reload");
		frm.reset();
	}
	
	
	public String execute() {
		System.out.println("LpiProjectManagerFindEntryAction execute");
		String rtrn;
	
		try{
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				rtrn = "onLoad";
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.link = "0";
					//rtrn="failed";
				}
			
				
				if(frm.getLink()!=null && frm.getLink().equals("1") || frm.getBtnName().equalsIgnoreCase("Reset")){
					System.out.println("LpiProjectManagerFindEntryAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload();
					rtrn = "onLoad";
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					System.out.println("LpiProjectManagerFindEntryAction execute SAVE | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
						details.setPmName(frm.getPmName());
						details.setPmPosition(frm.getPmPosition());
						details.setPmEmail(frm.getPmEmail());
						details.setPm_cmpny_id(user.getUserCmpCode());
						LpiProjectManagerFindControllerBean pmF = new LpiProjectManagerFindControllerBean();
						ArrayList pmL = pmF.getPmByCriteria(details);
						if(pmL.size()==0){
							frm.setAppMessage("No Records Found.");
						}else{
							Iterator i = pmL.iterator();
							frm.clearPmList();
							while(i.hasNext()){
								LpiProjectManagerModDetails pmi = (LpiProjectManagerModDetails)i.next();
								LpiProjectManagerFindEntryLine pmLine = new LpiProjectManagerFindEntryLine();
								pmLine.setPmlCd(pmi.getPmCd());
								System.out.println("PM ID >> "+pmi.getPmId() );
								pmLine.setPmlId(pmi.getPmId());
								pmLine.setPmlName(pmi.getPmName());
								pmLine.setPmlPosition(pmi.getPmPosition());
								pmLine.setPmlEmail(pmi.getPmEmail());
								frm.setPmList(pmLine);
							}
						}
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiProjectManagerFindEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					rtrn="main";
				}
			}else{
				rtrn="login";
			}
			
		}catch (Exception e){
			e.printStackTrace();
			rtrn = "failed";
		}
		return rtrn;
	}
	
	
	public String validateFrm(LpiProjectManagerFindEntryForm frm){
		System.out.println("LpiProjectManagerFindEntryAction execute validateFrm");
		
		String errMsg = "";
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}

}