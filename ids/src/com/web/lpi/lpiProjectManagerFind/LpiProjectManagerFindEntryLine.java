package com.web.lpi.lpiProjectManagerFind;

import com.web.ApplicationMessage;

public class LpiProjectManagerFindEntryLine {
	
	private int pmlCd;
	private String pmlId;
	private String pmlName;
	private String pmlEmail;
	private String pmlPosition;
	
	
	public int getPmlCd() {
		return pmlCd;
	}
	public void setPmlCd(int pmlCd) {
		this.pmlCd = pmlCd;
	}
	public String getPmlId() {
		return pmlId;
	}
	public void setPmlId(String pmlId) {
		this.pmlId = pmlId;
	}
	public String getPmlName() {
		return pmlName;
	}
	public void setPmlName(String pmlName) {
		this.pmlName = pmlName;
	}
	public String getPmlEmail() {
		return pmlEmail;
	}
	public void setPmlEmail(String pmlEmail) {
		this.pmlEmail = pmlEmail;
	}
	public String getPmlPosition() {
		return pmlPosition;
	}
	public void setPmlPosition(String pmlPosition) {
		this.pmlPosition = pmlPosition;
	}
	
	
	
	
	
	
	
}
