package com.web.lpi.lpiProjectManager;

import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.txn.AdUserControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.web.UserAutoEmailSession;
import com.web.UserSession;
import com.web.common;
import com.web.lpi.lpiSignup.LpiSignupForm;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;

public class LpiProjectManagerEntryAction extends ActionSupport {

	private LpiProjectManagerEntryForm frm;
	public LpiProjectManagerEntryForm getFrm() {return frm;}
	public void setFrm(LpiProjectManagerEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		System.out.println("LpiProjectManagerEntryAction reload");
		frm.reset();
	}
	
	
	public String execute() {
		System.out.println("LpiProjectManagerEntryAction execute");
		String rtrn;
		try{
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				rtrn = "onLoad";
				try {
					if(frm.link==null){
						frm.link = "0";
					}
					System.out.println("link >> "+ frm.link);
					if(frm.getBtnName()==null){
						frm.setBtnName("");
					}
					
				}catch (NullPointerException e){
					frm.link = "0";
				} catch (Exception e) {
					frm.setLink("0");
					//rtrn="failed";
				} 
			
			 
				if (frm.getLink()!=null && frm.getLink().equals("22")){
					System.out.println("LpiProjectManagerEntryAction execute FIND | pmId >> "+frm.getLinkPmId()+" | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					System.out.println("PM ID >>"+frm.pmId+"<<");
					String pmID = frm.getPmId();
					try {
						LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
						LpiProjectManagerModDetails pmD= pmC.getPmByPmId(pmID);
						this.reload();
						frm.setPmCd(pmD.getPmCd());
						frm.setPmId(pmD.getPmId());
						frm.setPmName(pmD.getPmName());
						frm.setPmPosition(pmD.getPmPosition());
						frm.setGender(pmD.getSex());
						frm.setPmEmail(pmD.getPmEmail());
						frm.setPmTelNo(pmD.getPmTelNo());
						frm.setPmMobNo(pmD.getPmMobNo());
						frm.setPmFaxNo(pmD.getPmFaxNo());
						frm.setPmUserName(pmD.getPmUserName());
						rtrn = "onLoad";
					} catch (Exception e) {
						e.printStackTrace();
						rtrn = "failed";
					}
				} else if(frm.getLink()!=null && frm.getLink().equals("2") || frm.getBtnName().equalsIgnoreCase("Reset")){
					System.out.println("LpiProjectManagerEntryAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload();
					rtrn = "onLoad";
				
				} else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("LpiProjectManagerEntryAction execute SAVE | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					
					AdEmailControllerBean emailCB = new AdEmailControllerBean();
					AdEmailBean emlBean = emailCB.getEmail(4);
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					LpiLpiModDetails lpiMD =  logonCB.getLpiByLpiCode(user.getUserCmpCode());
					
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						
						String msg = "";
						
						if(frm.getPmId()!=null && !frm.getPmId().equals("")){
							LpiProjectManagerModDetails pmD = new LpiProjectManagerModDetails();
							
							pmD.setPmId(frm.getPmId());
							pmD.setPmName(frm.getPmName());
							pmD.setSex(frm.getGenderList());
							pmD.setPmEmail(frm.getPmEmail());
							pmD.setPmPosition(frm.getPmPosition());
							pmD.setPmTelNo(frm.getPmTelNo());
							pmD.setPmMobNo(frm.getPmMobNo());
							pmD.setPmFaxNo(frm.getPmFaxNo());
							pmD.setPmUserName(frm.getPmUserName());
							pmD.setPmUserPassword(frm.getPmUserPass());
							pmD.setPm_cmpny_id(user.getUserCmpCode());
							
							LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
							pmC.updatePM(pmD);
							this.reload();
							//frm.setAppMessage("Project Manager was SUCCESSFULLY Updated!");
							msg+="Project Manager was SUCCESSFULLY Updated!.";
							
							
							try {
								UserAutoEmailSession email = new UserAutoEmailSession();
								String Body = "<html><head></head><body>"
								+"<p>HI</p>"
								+ "<p>Good Day!</p>"
								+ "<p>Your Local Partner Institute has been update your Account.</p>"
								//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
								+ "<p>&nbsp;</p>"
								+ "<p>Please use the following information below to log-in and Edit your Profile.</p>"
																	
								+ "<p>Institute Code : "+lpiMD.getLpi_nm()+"</p>"
								+ "<p>User Name : "+pmD.getPmUserName()+"</p>"
								+ "<p>Password : "+pmD.getPmUserPassword()+"</p>"
								
								+ "<p>&nbsp;</p>"
								+ "<p>Thanks,</p>"
								+ "<p>PNVSCA</p>"
								+ "</body></html>";
								email.SendEmail(pmD.getPmEmail(), "LPI Project Manager Notification", Body);
								
								msg+=" Email has sent to PM Email Address.";
							} catch (Exception e) {
								
								e.printStackTrace();
							}
							
							
							
						}else{
							LpiProjectManagerModDetails pmD = new LpiProjectManagerModDetails();
							pmD.setPmId(frm.getPmId());
							pmD.setPmName(frm.getPmName());
							pmD.setSex(frm.getGenderList());
							pmD.setPmEmail(frm.getPmEmail());
							pmD.setPmPosition(frm.getPmPosition());
							pmD.setPmTelNo(frm.getPmTelNo());
							pmD.setPmMobNo(frm.getPmMobNo());
							pmD.setPmFaxNo(frm.getPmFaxNo());
							pmD.setPmUserName(frm.getPmUserName());
							pmD.setPmUserPassword(frm.getPmUserPass());
							pmD.setPm_cmpny_id(user.getUserCmpCode()); 	
							pmD.setPm_usr_cd(user.getUserCode());
							LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
							pmC.createPM(pmD);
							this.reload();
							//frm.setAppMessage("Project Manager was SUCCESSFULLY Added!");
							msg+="Project Manager was SUCCESSFULLY Added!.";
							
							
							if(emlBean.getEmlEnabled() == 1){
								
								try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>Your Local Partner Institute Account has been created new Project Manager.</p>"
									//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to Edit PM Profile.</p>"
									+ "<p>Project Manager Name : "+pmD.getPmName()+"</p>"
									//+ "<p>User Name : "+details.getLpi_usr_nm()+"</p>"
									//+ "<p>Password : "+details.getLpi_usr_psswrd()+"</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(lpiMD.getLpi_email(), "LPI Project Manager Notification", Body);
									
									msg+=" Email has sent to your LPI Email Address.";
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								
								
								
								try {
									UserAutoEmailSession email = new UserAutoEmailSession();
									String Body = "<html><head></head><body>"
									+"<p>HI</p>"
									+ "<p>Good Day!</p>"
									+ "<p>Your Local Partner Institute Account has been assigned you as new Project Manager.</p>"
									//+ "<p>PNVSCA will validate the information for you to use the application.</p>"
									+ "<p>&nbsp;</p>"
									+ "<p>Please use the following information below to log-in and Edit your Profile.</p>"
																		
									+ "<p>Institute Code : "+lpiMD.getLpi_nm()+"</p>"
									+ "<p>User Name : "+pmD.getPmUserName()+"</p>"
									+ "<p>Password : "+pmD.getPmUserPassword()+"</p>"
									
									+ "<p>&nbsp;</p>"
									+ "<p>Thanks,</p>"
									+ "<p>PNVSCA</p>"
									+ "</body></html>";
									email.SendEmail(pmD.getPmEmail(), "LPI Project Manager Notification", Body);
									
									msg+=" Email has sent to your PM Email Address.";
								} catch (Exception e) {
									
									e.printStackTrace();
								}
								
								
							}
							
							
							
						}
						rtrn = "onLoad";
						frm.setAppMessage(msg);
					}else{
						frm.clearGenderLists();
						frm.setGender(frm.getGenderList());
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiProjectManagerEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					rtrn="main";
				}
				
			}else{
				rtrn="login";
			}
			
		}catch (Exception e){
			e.printStackTrace();
			rtrn = "failed";
		}
		return rtrn;
	}
	
	
	public String validateFrm(LpiProjectManagerEntryForm frm){
		System.out.println("LpiProjectManagerEntryAction execute validateFrm");
		
		String errMsg = "";
		
		if(frm.getPmName() == null || frm.getPmName().trim().length()==0){
			errMsg+="PM Full Name is Required.  ";
		}
		
		if(frm.getGenderList().equals("0")){
			errMsg+="Gender is Required.  ";
		}
		
		if(frm.getPmEmail() == null || frm.getPmEmail().trim().length() == 0){
			errMsg+="Email is Required.  ";
		}else{
			boolean eml = common.validateEmail(frm.getPmEmail());
			if(eml == false){
				errMsg+="Email is Invalid Format.  ";
			}
		}
		
		
		if(frm.getPmTelNo() == null || frm.getPmTelNo().trim().length()==0){
			errMsg+="Telephone No. is Required.  ";
		}
		
		
		if(frm.getPmMobNo() == null || frm.getPmMobNo().trim().length()==0){
			errMsg+="Mobile No. is Required.  ";
		}
		
		if(frm.getPmFaxNo() == null || frm.getPmFaxNo().trim().length()==0){
			errMsg+="Fax No. is Required.  ";
		}
		
		if(frm.getPmUserName() == null || frm.getPmUserName().trim().length()==0){
			errMsg += "User Name is Required.  ";
		}
		
		if(frm.getPmUserPass() == null || frm.getPmUserPass().trim().length()==0){
			errMsg+="Password is Required.  ";
		}else if(!frm.getPmUserPass().equals(frm.getPmUserConfirmPass())){
			errMsg+="Passwrod not Matched.  ";
		}
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}

}