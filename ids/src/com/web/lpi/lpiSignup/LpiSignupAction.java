package com.web.lpi.lpiSignup;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.web.UserAutoEmailSession;
import com.web.common;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;

public class LpiSignupAction extends ActionSupport {
	private LpiSignupForm frm;
	public LpiSignupForm getFrm() {return frm;}
	public void setFrm(LpiSignupForm frm) {this.frm = frm;}

	public String onload = "";

	
	public void reload(){
		
		frm.reset();
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
	}
	
	
	public String execute() {
		System.out.println("LpiSignupAction execute");
		String rtrn = "onLoad";

		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
		
		try {
			frm.getLink();
		} catch (Exception e) {
			frm.setLink("0");
			rtrn="failed";
			
		}
		
		
	
		if ((frm.getLink() != null && frm.getLink().equals("1")) || frm.lpiBtnName.equalsIgnoreCase("Reset")) {
			System.out.println("LpiSignupAction execute LOAD");
			rtrn = "onLoad";
			this.reload();

		} else {
			
			if (frm.lpiBtnName != null && frm.lpiBtnName.equals("LogIn")) {
				System.out.println("LpiSignupAction execute LogIn");
				rtrn = "login";
			} else if (frm.lpiBtnName != null	&& frm.lpiBtnName.equals("Sign-Up")) {
				System.out.println("LpiSignupAction execute Sign-Up");
				String errMsg = this.validateFrm(frm);
				if(!errMsg.equalsIgnoreCase("")){
					frm.setAppMessage(errMsg);
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					
					frm.populateType();
					frm.setLpiType(frm.getLpiTypeList());
					
					
					
					ArrayList rgnList = new ArrayList();
					try {
						rgnList = logonCB.getAllRegion();
						Iterator rl = rgnList.iterator();
						frm.clearRegionLists();
						while (rl.hasNext()) {
							AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
							frm.setRegionLists(rgnD.getRgn_nm());
						}
					} catch (Exception e) {
						
						e.printStackTrace();
					}
					frm.setRegion(frm.getRegionList());
					
					
					ArrayList prvncList = new ArrayList();
					Iterator i;
					try {
						prvncList =  logonCB.getAllProvinceByRgnId(frm.getRegion());
						i = prvncList.iterator();
						provinces =  new ArrayList<String>();
						frm.clearProvinceLists();
						while(i.hasNext()){
							AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
							frm.setProvinceLists(details.getPrvnc_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					frm.setProvince(frm.getProvinceList());
					
					ArrayList munList = new ArrayList();
					try {
						munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
						i = munList.iterator();
						municipalities =  new ArrayList<String>();
						frm.clearMunicipalityLists();
						while(i.hasNext()){
							AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
							frm.setMunicipalityLists(details.getMun_nm().toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					frm.setMunicipality(frm.getMunicipalityList());
			
				}else{
					System.out.println("LpiSignupAction execute Sign-Up Save Form");
					try {
						LpiLpiModDetails details = new LpiLpiModDetails();
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						details.setLpi_id(frm.getLpiId());
						details.setLpi_nm(frm.getLpiName());
						int ltyp = 0;
						try {
							if(frm.getLpiTypeList().equalsIgnoreCase("National Government Agency")){
								ltyp = 1;
							}else if(frm.getLpiTypeList().equalsIgnoreCase("Local Government Unit")){
								ltyp = 2;
							}else if(frm.getLpiTypeList().equalsIgnoreCase("Non-Government Organization")){
								ltyp = 3;
							}else if(frm.getLpiTypeList().equalsIgnoreCase("Academe")){
								ltyp = 4;
							}else if(frm.getLpiTypeList().equalsIgnoreCase("Private Sector")){
								ltyp = 5;
							}
						} catch (Exception e1) {
							
						}
						
						
						
						
						
						details.setLpi_typ(ltyp);
						details.setLpi_mndt(frm.getLpiMandate());
						try {
							AdAddRegionModDetails rgnD = logonCB.getRegionByRgnName(frm.getRegionList());
							details.setLpi_rgn(rgnD.getRgn_id());
						} catch (Exception e) {
							details.setLpi_rgn(0);
						}
						try {
							AdAddProvinceModDetails prnvcD = logonCB.getProvinceByPrvcName(frm.getProvinceList());
							details.setLpi_prvnc(prnvcD.getPrvnc_id());
						} catch (Exception e) {
							details.setLpi_prvnc(0);
						}
						try {
							System.out.println("frm.getMunicipalityList() >> "+frm.getMunicipalityList());
							AdAddMunicipalityModDetails munDB = logonCB.getMunicipalityByMunName(frm.getMunicipalityList());
							details.setLpi_mncplty(munDB.getMun_id());
						} catch (Exception e) {
							details.setLpi_mncplty(0);
						}
						
						details.setLpi_addrss(frm.getAddress());
						details.setLpi_tlphn_nmbr(frm.getLpiTelephoneNum());
						details.setLpi_email(frm.getLpiEmail());
						details.setLpi_fx_nmbr(frm.getLpiFaxNum());
						details.setLpi_wbst(frm.getLpiWebsite());
						
						details.setLpi_usr_nm(frm.getLpiUserName());
						details.setLpi_usr_psswrd(frm.getLpiUserPass());
						details.setLpi_prmry_email(frm.getLpiUserPrimaryEmail());
						details.setLpi_scndry_email(frm.getLpiUserSecondaryEmail());
						details.setLpi_crdntr_nm(frm.getLpiCoordinatorName());
						details.setLpi_crdntr_pstn(frm.getLpiCoordinatorPosition());
						details.setLpi_crdntr_dprtmnt(frm.getLpiCoordinatorDepartment());
						details.setLpi_crdntr_tlphn_nmbr(frm.getLpiCoordinatorTelNum());
						details.setLpi_crdntr_mble_nmbr(frm.getLpiCoordinatorMobileNum());
						int LpiCode = logonCB.saveLpiDetails(details);
						
						String msg = "Your LPI Account was successfully created."
								+ "You can now input your ID and password to Login.";
						
						AdEmailControllerBean emailCB = new AdEmailControllerBean();
						AdEmailBean emlBean = emailCB.getEmail(2);
						
						if(emlBean.getEmlEnabled() == 1){
							
							try {
								UserAutoEmailSession email = new UserAutoEmailSession();
								String Body = "<html><head></head><body>"
								+"<p>HI</p>"
								+ "<p>Good Day!</p>"
								+ "<p>Your Local Partner Institute Account has been Created.</p>"
								+ "<p>PnVSCA will validate the information for you to use the application.</p>"
								+ "<p>&nbsp;</p>"
								+ "<p>Please use the following information below to log-in and Edit your Profile.</p>"
								+ "<p>Institute Code : "+details.getLpi_nm()+"</p>"
								+ "<p>User Name : "+details.getLpi_usr_nm()+"</p>"
								+ "<p>Password : "+details.getLpi_usr_psswrd()+"</p>"
								+ "<p>&nbsp;</p>"
								+ "<p>Thanks,</p>"
								+ "<p>PNVSCA</p>"
								+ "</body></html>";
								email.SendEmail(details.getLpi_email(), "LPI Sign-up Notification", Body);
								
								msg+=" Email has sent to your Email Address.";
							} catch (Exception e) {
								
								e.printStackTrace();
							}
							
							
						}
						
						
						
						this.reload();
						rtrn = "login";
						//frm.setAppMessage("Sign-up for LPI Account was Succesfully Created, Please see your Email for Confirmation.");
						
						/*
						UserAutoEmailSession email = new UserAutoEmailSession();
						String Body = "Your account has been login";
						email.SendEmail("chan.dollete08@gmail.com", "PNVSCA LOG-IN Notification", Body);
						*/
						
						
						frm.setAppMessage(msg);
						
						
					} catch (NumberFormatException e) {
						e.printStackTrace();
						rtrn="failed";
					} catch (Exception e) {
						e.printStackTrace();
						rtrn="failed";
					}
			
				}
			}
		}
		return rtrn;
	}
	
	public String validateFrm(LpiSignupForm frm){
		System.out.println("LpiSignupAction Validate Form");
		String errMsg = "";
		
		if(frm.getLpiName() == null || frm.getLpiName().trim().length()==0){
			errMsg +="LPI Organization Name is Required. ";
		}
		
		if(frm.getLpiTypeList() == null || frm.getLpiTypeList().equals("0")){
			errMsg +="LPI Type is Required. ";
		}
		
		if(frm.getRegionList() == null || frm.getRegionList().equals("0")){
			errMsg +="Region is Required. ";
		}
		
		if(frm.getProvinceList() == null || frm.getProvinceList().equals("0")){
			errMsg +="Province is Required. ";
		}
		
		if(frm.getMunicipalityList() == null || frm.getMunicipalityList().equals("0")){
			errMsg +="Province is Required. ";
		}
		
		if(frm.getAddress() == null || frm.getAddress().trim().length()==0){
			errMsg +="Address is Required. ";
		}
		
		if(frm.getLpiTelephoneNum() == null || frm.getLpiTelephoneNum().trim().length()==0){
			errMsg +="Telephone No. is Required. ";
		}
		
		if(frm.getLpiEmail() == null || frm.getLpiEmail().trim().length()==0){
			errMsg +="Lpi Email Address is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getLpiEmail());
			if(yes == false){
				errMsg += "LPI Email is Invalid Format. ";
			}
			
		}
		
		if(frm.getLpiFaxNum() == null || frm.getLpiFaxNum().trim().length()==0){
			errMsg +="Fax No. is Required. ";
		}
		
		if(frm.getLpiFaxNum() == null || frm.getLpiFaxNum().trim().length()==0){
			errMsg +="Fax No. is Required. ";
		}
		
		if(frm.getLpiId() == null || frm.getLpiId().trim().length()==0){
			errMsg +="Lpi ID / UACS Code is Required. ";
		}else{
			AdLogonControllerBean adlCB = new AdLogonControllerBean();
			try {
				LpiLpiModDetails lpiMD =  adlCB.getLpiByLpiUACS(frm.getLpiId().trim());
				
				if(lpiMD.getLpi_cd() >  0){
					errMsg +="Lpi ID / UACS Code is already Registered. ";
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		
		if(frm.getLpiUserName() == null || frm.getLpiUserName().trim().length()==0){
			errMsg +="UserName is Required. ";
		}
		
		if(frm.getLpiUserPass() == null || frm.getLpiUserPass().trim().length()==0){
			errMsg +="User Password is Required. ";
		}
		
		if(!frm.getLpiUserPass().equals(frm.getLpiUserPassConfirm())){
			errMsg +="User Password is not matched. ";
		}
		

		if(frm.getLpiUserPrimaryEmail() == null || frm.getLpiUserPrimaryEmail().trim().length()==0){
			errMsg +="User Primary Email is Required. ";
		}else{
			boolean yes = common.validateEmail(frm.getLpiUserPrimaryEmail());
			if(yes == false){
				errMsg += "User Primaryl is Invalid Format. ";
			}
			
		}
		
		if(frm.getLpiCoordinatorName() == null || frm.getLpiCoordinatorName().trim().length()==0){
			errMsg +="PNVSCA Coordinator is Required. ";
		}
		
		
		
		
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}
	
	
	

	private ArrayList<String> provinces;
	private ArrayList<String> municipalities;

	public ArrayList<String> getProvinces() {
		return provinces;
	}

	public ArrayList<String> getMunicipalities() {
		return municipalities;
	}

	public String getProvince() {
		System.out.println("LpiSignupAction getProvince");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList prvncList =  new ArrayList();
		
		try {
			prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
			Iterator i = prvncList.iterator();
			provinces =  new ArrayList<String>();
			frm.clearProvinceLists();
			while(i.hasNext()){
				AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
				System.out.println("prvnc >> "+ details.getPrvnc_nm());
				frm.setProvinceLists(details.getPrvnc_nm().toString());
				provinces.add(details.getPrvnc_nm().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	
	public String getMunicipality() {
		System.out.println("LpiSignupAction getMunicipality");
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		System.out.println("frm.getProvince() 2 >> "+frm.getProvince());
		ArrayList munList = new ArrayList();
		try {
			munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
			Iterator i = munList.iterator();
			municipalities =  new ArrayList<String>();
			frm.clearMunicipalityLists();
			while(i.hasNext()){
				AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
				frm.setMunicipalityLists(details.getMun_nm().toString());
				municipalities.add(details.getMun_nm().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	

}
