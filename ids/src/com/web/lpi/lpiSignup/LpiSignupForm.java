package com.web.lpi.lpiSignup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.web.ApplicationMessage;

public class LpiSignupForm extends ApplicationMessage {

	private String lpiId;
	private String lpiName;
	// -- type
	private ArrayList lpiTypeLists = new ArrayList();
	private String lpiTypeList;
	private String lpiType;
	
	private Map<Integer, String> lpiTypeListsMap = null;
	// --

	private String lpiHeadName;
	private String lpiMandate;

	private String lpiTelephoneNum;
	private String lpiEmail;
	private String lpiFaxNum;
	private String lpiWebsite;

	private String lpiUserName;
	private String lpiUserPass;
	private String lpiUserPassConfirm;

	private String lpiUserPrimaryEmail;
	private String lpiUserSecondaryEmail;

	private String lpiCoordinatorName;
	private String lpiCoordinatorPosition;
	private String lpiCoordinatorTelNum;
	private String lpiCoordinatorDepartment;
	private String lpiCoordinatorMobileNum;

	
	public String lpiBtnName;
	public String getLpiBtnName() {
		return lpiBtnName;
	}

	public void setLpiBtnName(String lpiBtnName) {
		this.lpiBtnName = lpiBtnName;
	}
	
	
	
	public String getLpiMandate() {
		return lpiMandate;
	}

	public void setLpiMandate(String lpiMandate) {
		this.lpiMandate = lpiMandate;
	}

	public String getLpiTelephoneNum() {
		return lpiTelephoneNum;
	}

	public void setLpiTelephoneNum(String lpiTelephoneNum) {
		this.lpiTelephoneNum = lpiTelephoneNum;
	}

	public String getLpiEmail() {
		return lpiEmail;
	}

	public void setLpiEmail(String lpiEmail) {
		this.lpiEmail = lpiEmail;
	}

	public String getLpiFaxNum() {
		return lpiFaxNum;
	}

	public void setLpiFaxNum(String lpiFaxNum) {
		this.lpiFaxNum = lpiFaxNum;
	}

	public String getLpiWebsite() {
		return lpiWebsite;
	}

	public void setLpiWebsite(String lpiWebsite) {
		this.lpiWebsite = lpiWebsite;
	}

	public String getLpiUserName() {
		return lpiUserName;
	}

	public void setLpiUserName(String lpiUserName) {
		this.lpiUserName = lpiUserName;
	}

	public String getLpiUserPass() {
		return lpiUserPass;
	}

	public void setLpiUserPass(String lpiUserPass) {
		this.lpiUserPass = lpiUserPass;
	}

	public String getLpiUserPassConfirm() {
		return lpiUserPassConfirm;
	}

	public void setLpiUserPassConfirm(String lpiUserPassConfirm) {
		this.lpiUserPassConfirm = lpiUserPassConfirm;
	}

	public String getLpiUserPrimaryEmail() {
		return lpiUserPrimaryEmail;
	}

	public void setLpiUserPrimaryEmail(String lpiUserPrimaryEmail) {
		this.lpiUserPrimaryEmail = lpiUserPrimaryEmail;
	}

	public String getLpiUserSecondaryEmail() {
		return lpiUserSecondaryEmail;
	}

	public void setLpiUserSecondaryEmail(String lpiUserSecondaryEmail) {
		this.lpiUserSecondaryEmail = lpiUserSecondaryEmail;
	}

	public String getLpiCoordinatorName() {
		return lpiCoordinatorName;
	}

	public void setLpiCoordinatorName(String lpiCoordinatorName) {
		this.lpiCoordinatorName = lpiCoordinatorName;
	}

	public String getLpiCoordinatorPosition() {
		return lpiCoordinatorPosition;
	}

	public void setLpiCoordinatorPosition(String lpiCoordinatorPosition) {
		this.lpiCoordinatorPosition = lpiCoordinatorPosition;
	}

	public String getLpiCoordinatorTelNum() {
		return lpiCoordinatorTelNum;
	}

	public void setLpiCoordinatorTelNum(String lpiCoordinatorTelNum) {
		this.lpiCoordinatorTelNum = lpiCoordinatorTelNum;
	}

	public String getLpiCoordinatorDepartment() {
		return lpiCoordinatorDepartment;
	}

	public void setLpiCoordinatorDepartment(String lpiCoordinatorDepartment) {
		this.lpiCoordinatorDepartment = lpiCoordinatorDepartment;
	}

	public String getLpiCoordinatorMobileNum() {
		return lpiCoordinatorMobileNum;
	}

	public void setLpiCoordinatorMobileNum(String lpiCoordinatorMobileNum) {
		this.lpiCoordinatorMobileNum = lpiCoordinatorMobileNum;
	}

	public void setLpiTypeLists(ArrayList lpiTypeLists) {
		this.lpiTypeLists = lpiTypeLists;
	}

	public void clearLpiTypeLists() {
		lpiTypeLists = new ArrayList();
	}

	public void setLpiTypeLists(String LpiType) {
		System.out.println("lpiType >> " + LpiType);

		this.lpiTypeLists.add(LpiType);
	}
	

	public List<String> getLpiTypeLists() {
		return lpiTypeLists;
	}
	
	public Map<Integer,String> getLpiTypeListsMap() {
		return lpiTypeListsMap;
	}


	public void setLpiTypeList(String LpiTypeList) {
		this.lpiTypeList = LpiTypeList;
	}

	public String getLpiTypeList() {
		return lpiTypeList;
	}

	public String getLpiType() {
		return lpiType;
	}

	public void setLpiType(String lpiType) {
		this.lpiType = lpiType;
	}

	public String getLpiId() {
		return lpiId;
	}

	public void setLpiId(String lpiId) {
		this.lpiId = lpiId;
	}

	public String getLpiName() {
		return lpiName;
	}

	public void setLpiName(String lpiName) {
		this.lpiName = lpiName;
	}

	public String getLpiHeadName() {
		return lpiHeadName;
	}

	public void setLpiHeadName(String lpiHeadName) {
		this.lpiHeadName = lpiHeadName;
	}
	
	public void populateType(){
		lpiTypeLists = new ArrayList();
		lpiTypeLists.add("National Government Agency");
		lpiTypeLists.add("Local Government Unit");
		lpiTypeLists.add("Non-Government Organization");
		lpiTypeLists.add("Academe");
		lpiTypeLists.add("Private Sector");
		lpiType = "0";
		
		
	}

	public void reset() {

		lpiId = "";
		lpiName = "";
		populateType();
		lpiTypeList = "";
		lpiType = "";
		
		
		lpiHeadName = "";
		lpiMandate = "";

		lpiTelephoneNum = "";
		lpiEmail = "";
		lpiFaxNum = "";
		lpiWebsite = "";

		lpiUserName = "";
		lpiUserPass = "";
		lpiUserPassConfirm = "";

		lpiUserPrimaryEmail = "";
		lpiUserSecondaryEmail = "";

		lpiCoordinatorName = "";
		lpiCoordinatorPosition = "";
		lpiCoordinatorTelNum = "";
		lpiCoordinatorDepartment = "";
		lpiCoordinatorMobileNum = "";

		clearRegionLists();
		clearProvinceLists();
		clearMunicipalityLists();
		clearAddress();
		

	}
}
