package com.web.lpi.lpiRequestVolunteerFind;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.web.UserSession;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.vso.vsoVolunteerFind.VsoVolunteerFindEntryForm;

public class LpiRequestVolunteerFindEntryAction extends ActionSupport {
	
	private LpiRequestVolunteerFindEntryForm frm;
	public LpiRequestVolunteerFindEntryForm getFrm() {return frm;}
	public void setFrm(LpiRequestVolunteerFindEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(int cmp_cd){
		System.out.println("LpiRequestVolunteerFindEntryAction reload");
		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
		Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
		UserSession user = (UserSession)sessionsMap.get(session.getId());
		
		frm.reset();

		LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
		try {
			
			ArrayList prjL = new ArrayList();
			if(user.getUserProfile() == 3 || user.getUserProfile() == 31){
				prjL = prjFC.getPrjAssignByCmpCode(cmp_cd);
			}else if(user.getUserProfile()==2 || user.getUserProfile()==21){
				prjL = prjFC.getPrjByCmpCode(cmp_cd);
			}else{
				prjL = prjFC.getPrjAll();
			}
			Iterator prji = prjL.iterator();
			frm.clearRv_prjLists();
			while(prji.hasNext()){
				LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
				System.out.println("prjName >> "+ prjD2.getPrjOrgName());
				frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
			}
			frm.setRv_prj_nm("0");
			frm.clearRv_ln();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String execute() {
		System.out.println("LpiRequestVolunteerFindEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				if(frm.getBtnName() == null){
					frm.setBtnName("");
				}
			
				
				if (frm.getLink()!=null && frm.getLink().equals("88")){
					System.out.println("LpiRequestVolunteerFindEntryAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload(user.getUserCmpCode());
					rtrn = "onLoad";
				}else if ((frm.getLink() != null && frm.getLink().equals("8")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("LpiRequestVolunteerFindEntryAction execute LOAD RESET | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload(user.getUserCmpCode());
					rtrn = "onLoad";
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					System.out.println("LpiRequestVolunteerFindEntryAction execute Search | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						
						LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
						LpiRequestVolunteerModDetails details = new LpiRequestVolunteerModDetails();
						
						details.setRv_id(frm.getRv_id());
						details.setRv_status(frm.getRv_statusList());
						details.setRv_drtn_mnths(frm.getRv_no_vlntr());
						try {
							details.setRv_no_vlntr(Integer.parseInt(frm.getRv_no_vlntr()));
						} catch (Exception e) {
							details.setRv_no_vlntr(0);
						}
						details.setRv_prj_cd(frm.getRv_prj_cd());
						
						if(user.getUserProfile() == 2 ){//  if lpi login 
							details.setRv_cmp_cd(user.getUserCmpCode());
						}else if(user.getUserProfile() == 3){ // if vso login
							details.setRvl_vso_cd(user.getUserCmpCode());
						}
						
						this.reload(user.getUserCmpCode());
						
						ArrayList rlList = rvC.getRvByCriteria(details);
						Iterator i = rlList.iterator();
						frm.clearRv_ln();
						while(i.hasNext()){
							LpiRequestVolunteerModDetails rvlBean = (LpiRequestVolunteerModDetails)i.next();
							LpiRequestVolunteerLineEntryForm rvLine = new LpiRequestVolunteerLineEntryForm();
							//System.out.println(">> "+ rvlBean.getRv_id());
							rvLine.setRv_cd(rvlBean.getRv_cd());
							rvLine.setRv_id(rvlBean.getRv_id());
							rvLine.setRv_prj_drtn_mnths(rvlBean.getRv_drtn_mnths());
							rvLine.setRv_no_vlntr(String.valueOf(rvlBean.getRv_no_vlntr()));
							rvLine.setRv_statusList(rvlBean.getRv_status());
							if(rvlBean.getRv_id()!=null){
								frm.setRv_ln(rvLine);	
							}
						}
						/*
						Iterator x = frm.getRv_ln().iterator();
						while(x.hasNext()){
							LpiRequestVolunteerLineEntryForm xx = (LpiRequestVolunteerLineEntryForm)x.next();
							System.out.println(xx.getRv_cd());
						}
						*/						
						
						
						rtrn = "onLoad";
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiRequestVolunteerFindEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				} else if (frm.getBtnName().equalsIgnoreCase("Return")){
					System.out.println("LpiRequestVolunteerFindEntryAction execute RETURN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn = "onLoad";
				}
				
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(LpiRequestVolunteerFindEntryForm vfrm){
		System.out.println("LpiRequestVolunteerFindEntryAction execute validateFrm");
		
		String errMsg = "";
		
		int rvln = 0;
		if(frm.getRv_no_vlntr().toString().trim().length()>0){
			try {
				rvln =  Integer.parseInt(frm.getRv_no_vlntr());
			} catch (NumberFormatException e) {
				errMsg ="Invalid Number Format";
			}
		}
		/*if(frm.getRv_statusList().toString().trim().equalsIgnoreCase("0")){
			errMsg += "Invalid Selected Status;";
		}
		*/
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		
		
		
		HttpSession session = ServletActionContext.getRequest().getSession();
		HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
		System.out.println("session ID >> "+ request.getSession().getId());
		UserSession user = (UserSession)session.getAttribute(request.getSession().getId());
		this.reload(user.getUserCmpCode());
		return errMsg;
		
	}
	
	public String prjMngrName;
	public String getPrjMngrName() {return prjMngrName;}
	public String getProjectManager(){
		System.out.println("LpiRequestVolunteerFindEntryAction getProjectManager");
		String pmName = "";
		try {
			LpiProjectManagerFindControllerBean pmf= new LpiProjectManagerFindControllerBean();
			try {
				if(!frm.getRv_prj_nm().equals("0")){
					String[] prjList = frm.getRv_prj_nm().split(" --- ");
					pmName = prjList[0].toString();
					System.out.println("pmName ID >> "+pmName);
					LpiProjectManagerModDetails pmm = pmf.getPmByPrjName(pmName);
					pmName = pmm.getPmName();
				}
				prjMngrName = pmName;;
			} catch (Exception e) {
				pmName="";
				System.out.println("CANNOT SPLT PRJ ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	
		
	}

}
