package com.web.lpi.lpiProject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.AdEmailBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdEmailControllerBean;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.web.UserAutoEmailSession;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddCountryModDetails;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;


public class LpiProjectEntryAction extends ActionSupport {
	
	private LpiProjectEntryForm frm;
	public LpiProjectEntryForm getFrm() {return frm;}
	public void setFrm(LpiProjectEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		frm.reset();
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
	}
	
	public String execute() {
		System.out.println("LpiProjectEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			rtrn = "onLoad";
			
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				
				if (frm.getLink()!=null && frm.getLink().equals("33")){
					System.out.println("LpiProjectEntryAction execute FIND | prjCd >> "+frm.getLink()+" | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					int prjCd = frm.getPrjCd();
					rtrn = "onLoad";
					this.reload();
					System.out.println("PRJ CODE >> "+prjCd);
					
					try {
						
						LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
						LpiProjectModDetails details = prjC.getPrjByPrjCode(prjCd);
						frm.setPrjCd(details.getPrjCd());
						frm.setPrjId(details.getPrjId());
						frm.setPrjName(details.getPrjOrgName());
						frm.setPrjDateFrom(details.getPrjDurationFrom());
						frm.setPrjDateTo(details.getPrjDurationTo());
						frm.setPrjTarget(details.getPrjTargetBenefits());
						frm.setPrjBudget(details.getPrjBudgetSource());
						frm.setPrjSource(details.getPrjSource());
						frm.setPrjObjective(details.getPrjProjectObjective());
						frm.setPrjDescription(details.getPrjProjectDesc());
						frm.setAddress(details.getPrjAddress());
						
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						ArrayList rgnList = new ArrayList();
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							String rgn_nm = "0";
							
							frm.clearRegionLists();
							while (rl.hasNext()) {
								AdAddRegionModDetails detailsR = (AdAddRegionModDetails) rl.next();
								System.out.println("RGN ID >>>>> "+detailsR.getRgn_id() + " >> "+details.getPrjRgn());
								if(detailsR.getRgn_id() ==  details.getPrjRgn()){
									System.out.println("XXX");
									rgn_nm = detailsR.getRgn_nm();
								}
								frm.setRegionLists(detailsR.getRgn_nm());
							}
							frm.setRegion(rgn_nm);
							
							
							AdAddRegionModDetails rgnd= logonCB .getRegionByRgnName(rgn_nm);
							ArrayList prnvcList =  logonCB.getAllProvinceByRgnId(rgnd.getRgn_nm());
							Iterator pl = prnvcList.iterator();
							String prnvc_nm = "0";
							frm.clearProvinceLists();
							while(pl.hasNext()){
								AdAddProvinceModDetails detailsP = (AdAddProvinceModDetails)pl.next();
								if(detailsP.getPrvnc_id() == details.getPrjPrvnc()){
									prnvc_nm = detailsP.getPrvnc_nm();
								}
								frm.setProvinceLists(detailsP.getPrvnc_nm().toString());
							}
							frm.setProvince(prnvc_nm);
							
							
							logonCB.getAllMunicipalityByPrvncId(prnvc_nm);
							ArrayList munList = new ArrayList();
							munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
							Iterator muni = munList.iterator();
							frm.clearMunicipalityLists();
							String mun_nm="0";
							while(muni.hasNext()){
								AdAddMunicipalityModDetails detailsM = (AdAddMunicipalityModDetails)muni.next();
								if(detailsM.getMun_id() == details.getPrjMncplty()){
									mun_nm = detailsM.getMun_nm();
								}
								frm.setMunicipalityLists(detailsM.getMun_nm().toString());
							}
							frm.setMunicipality(mun_nm);
							
							LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
							ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
							frm.clearPrjPmAssignLists();
							Iterator i = pmlList.iterator();
							String pm_nm = "0";
							while(i.hasNext()){
								LpiProjectManagerModDetails detailsPm = (LpiProjectManagerModDetails)i.next();
								frm.setPrjPmAssignLists(detailsPm.getPmId() + " --- "+ detailsPm.getPmName());
								if(detailsPm.getPmCd() == details.getPrjProjectManager()){
									pm_nm = detailsPm.getPmId() + " --- "+ detailsPm.getPmName();
								}
							}
							frm.setPrjPmAssign(pm_nm);
							
							
						} catch (Exception e) {
						}
						
						
						
						/*
						LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
						LpiProjectManagerModDetails pmD= pmC.getPmByPmId(pmID);
						this.reload();
						frm.setPmCd(pmD.getPmCd());
						frm.setPmId(pmD.getPmId());
						frm.setPmName(pmD.getPmName());
						frm.setPmPosition(pmD.getPmPosition());
						frm.setGender(pmD.getSex());
						frm.setPmEmail(pmD.getPmEmail());
						frm.setPmTelNo(pmD.getPmTelNo());
						frm.setPmMobNo(pmD.getPmMobNo());
						frm.setPmFaxNo(pmD.getPmFaxNo());
						frm.setPmUserName(pmD.getPmUserName());*/
						rtrn = "onLoad";
					} catch (Exception e) {
						e.printStackTrace();
						rtrn = "failed";
					}
					
					
					
				
				}else if ((frm.getLink() != null && frm.getLink().equals("4")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("LpiProjectEntryAction execute LOAD");
					rtrn = "onLoad";
					this.reload();
					LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
					ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
					Iterator i = pmlList.iterator();
					while(i.hasNext()){
						LpiProjectManagerModDetails details = (LpiProjectManagerModDetails)i.next();
						frm.setPrjPmAssignLists(details.getPmId() + " --- "+ details.getPmName());
					}
					
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("LpiProjectEntryAction execute Save");
					
					
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						
						LpiProjectModDetails details = new LpiProjectModDetails();
						details.setPrjCd(frm.getPrjCd());
						details.setPrjId(frm.getPrjId());
						details.setPrjOrgName(frm.getPrjName());
						details.setPrjTargetBenefits(frm.getPrjTarget());
						details.setPrjDurationFrom(frm.getPrjDateFrom());
						details.setPrjDurationTo(frm.getPrjDateTo());
						details.setPrjBudgetSource(frm.getPrjBudget());
						details.setPrjSource(frm.getPrjSource());
						details.setPrjProjectObjective(frm.getPrjObjective());
						details.setPrjProjectDesc(frm.getPrjDescription());
						details.setPrjCmpCode(user.getUserCmpCode());
						
						int rgn=0;
						AdLogonControllerBean logC = new AdLogonControllerBean();
						try {
							AdAddRegionModDetails rgnd= logC.getRegionByRgnName(frm.getRegionList());
							rgn = rgnd.getRgn_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						details.setPrjRgn(rgn);
						
						int prnvc = 0;
						try {
							AdAddProvinceModDetails prvd = logC.getProvinceByPrvcName(frm.getProvinceList());
							prnvc = prvd.getPrvnc_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						details.setPrjPrvnc(prnvc);
						
						int mncpl = 0;
						try{
							AdAddMunicipalityModDetails mund = logC.getMunicipalityByMunName(frm.getMunicipalityList());
							mncpl = mund.getMun_id();
						} catch (Exception e){
							e.printStackTrace();
						}
						details.setPrjMncplty(mncpl);
						details.setPrjAddress(frm.getAddress());
						
						String pmId = "0";
						LpiProjectManagerModDetails pmD = null;
						try {
							String[] pmIdLst = frm.getPrjPmAssignList().split(" --- ");
							pmId = pmIdLst[0].toString();
							System.out.println("PMD ID >> "+pmId);
							LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
							pmD = pmC.getPmByPmId(pmId);
							details.setPrjProjectManager(pmD.getPmCd());
							
						} catch (Exception e) {
							details.setPrjProjectManager(0);
							System.out.println("CANNOT SPLT PM ID");
						}
						
						LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
						int prj_cd = 0;
						this.reload();
						String msg = "";
						
						
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						LpiLpiModDetails lpiMD =  logonCB.getLpiByLpiCode(user.getUserCmpCode());
						AdEmailControllerBean emailCB = new AdEmailControllerBean();
						AdEmailBean emlBean = emailCB.getEmail(5);
						
						
						if(details.getPrjCd()==0){
							prj_cd = prjC.create(details);
							
							
							if(prj_cd==0){
								//frm.setAppMessage("Invalid Project");
								msg+="Invalid Project. ";
							}else{
								//frm.setAppMessage("Project was Successfully Created!");
								msg+="Project was Successfully Created!. ";
								
								
								if(emlBean.getEmlEnabled() == 1){
									
									try {
										UserAutoEmailSession email = new UserAutoEmailSession();
										String Body = "<html><head></head><body>"
										+"<p>HI</p>"
										+ "<p>Good Day!</p>"
										+ "<p>New Poject has been Created.</p>"
										+ "<p>PNVSCA will validate the information for APPROVAL.</p>"
										+ "<p>&nbsp;</p>"
										+ "<p>Please use the following information below to Search and Edit your Project.</p>"
										+ "<p>Project ID : "+details.getPrjId()+"</p>"
										+ "<p>Project Nae : "+details.getPrjOrgName()+"</p>"
										+ "<p>Project Manager : "+frm.getPrjPmAssignList()+"</p>"
										+ "<p>&nbsp;</p>"
										+ "<p>Thanks,</p>"
										+ "<p>PNVSCA</p>"
										+ "</body></html>";
										email.SendEmail(lpiMD.getLpi_email(), "LPI PROJECT Notification", Body);
										
										msg+=" Email has sent to LPI Email Address. ";
									} catch (Exception e) {
										
										e.printStackTrace();
									}
									
									
									
									
									try {
										UserAutoEmailSession email = new UserAutoEmailSession();
										String Body = "<html><head></head><body>"
										+"<p>HI</p>"
										+ "<p>Good Day!</p>"
										+ "<p>New Poject has been Created.</p>"
										+ "<p>Your Local Partner Institute Account has been assigned you as new Project Manager.</p>"
										+ "<p>&nbsp;</p>"
										+ "<p>Please use the following information below to Search and View your Project information. </p>"
										+ "<p>Project ID : "+details.getPrjId()+"</p>"
										+ "<p>Project Nae : "+details.getPrjOrgName()+"</p>"
										+ "<p>Project Manager : "+frm.getPrjPmAssignList()+"</p>"
										+ "<p>&nbsp;</p>"
										+ "<p>Thanks,</p>"
										+ "<p>PNVSCA</p>"
										+ "</body></html>";
										email.SendEmail(pmD.getPmEmail(), "LPI PROJECT Notification", Body);
										
										msg+=" Email has sent to PM Email Address. ";
									} catch (Exception e) {
										
										e.printStackTrace();
									}
									
									
									
								}
								
							}
						}else{
								
							prj_cd = prjC.update(details);
							
							if(prj_cd==0){
							
								msg+="Invalid Project. ";
								
							}else{
							
								msg+="Project was Successfully Updated!. ";
										if(emlBean.getEmlEnabled() == 1){
										
											try {
												UserAutoEmailSession email = new UserAutoEmailSession();
												String Body = "<html><head></head><body>"
												+"<p>HI</p>"
												+ "<p>Good Day!</p>"
												+ "<p>New Poject has been UPDATE.</p>"
												+ "<p>PNVSCA will validate the information for APPROVAL.</p>"
												+ "<p>&nbsp;</p>"
												+ "<p>Please use the following information below to Search and Edit your Project.</p>"
												+ "<p>Project ID : "+details.getPrjId()+"</p>"
												+ "<p>Project Nae : "+details.getPrjOrgName()+"</p>"
												+ "<p>Project Manager : "+frm.getPrjPmAssignList()+"</p>"
												+ "<p>&nbsp;</p>"
												+ "<p>Thanks,</p>"
												+ "<p>PNVSCA</p>"
												+ "</body></html>";
												email.SendEmail(lpiMD.getLpi_email(), "LPI PROJECT Notification", Body);
												
												msg+=" Email has sent to your Email Address. ";
											} catch (Exception e) {
												
												e.printStackTrace();
											}
										
										
										}
									
								}
						}
						
						
					
						this.reload();
						LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
						ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
						Iterator i = pmlList.iterator();
						while(i.hasNext()){
							LpiProjectManagerModDetails details2 = (LpiProjectManagerModDetails)i.next();
							frm.setPrjPmAssignLists(details2.getPmId() + " --- "+ details2.getPmName());
						}
						
						frm.setAppMessage(msg);
						
						rtrn = "onLoad";
					}else{
						
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						ArrayList cntryList = new ArrayList();
						frm.clearCountryLists();
						try {
							cntryList = logonCB.getAllCountry();
							Iterator cl = cntryList.iterator();
							while (cl.hasNext()) {
								AdAddCountryModDetails cntrD = (AdAddCountryModDetails)cl.next();
								frm.setCountryLists(cntrD.getCntry_nm());
							}
						} catch (Exception e) {
						}
						frm.setCountry(frm.getCountryList());

						ArrayList rgnList = new ArrayList();
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							frm.clearRegionLists();
							while (rl.hasNext()) {
								AdAddRegionModDetails rgnD = (AdAddRegionModDetails)rl.next();
								frm.setRegionLists(rgnD.getRgn_nm());
							}
						} catch (Exception e) {

							e.printStackTrace();
						}
						frm.setRegion(frm.getRegionList());

						ArrayList prvncList = new ArrayList();
						Iterator i;
						try {
							prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
							i = prvncList.iterator();
							frm.clearProvinceLists();
							while (i.hasNext()) {
								AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
								frm.setProvinceLists(details.getPrvnc_nm().toString());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						frm.setProvince(frm.getProvinceList());

						ArrayList munList = new ArrayList();
						try {
							munList = logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
							i = munList.iterator();
							frm.clearMunicipalityLists();
							while (i.hasNext()) {
								AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
								frm.setMunicipalityLists(details.getMun_nm().toString());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						frm.setMunicipality(frm.getMunicipalityList());
						
						
						
						LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
						ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
						Iterator i2 = pmlList.iterator();
						while(i2.hasNext()){
							LpiProjectManagerModDetails details2 = (LpiProjectManagerModDetails)i2.next();
							frm.setPrjPmAssignLists(details2.getPmId() + " --- "+ details2.getPmName());
						}
						frm.setPrjPmAssign(frm.getPrjPmAssignList());
						
						
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
					
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiProjectEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(LpiProjectEntryForm vfrm){
		System.out.println("LpiProjectEntryAction execute validateFrm");
		
		String errMsg = "";
		if((frm.getPrjName() == null || frm.getPrjName().trim().length()==0) && frm.getPrjCd()==0){
			errMsg+="Project Name is Required.  ";
			
		}
		
		if(frm.getPrjDateFrom() == null){
			errMsg+="Duration From is Required. ";
		}else{
			boolean date = common.validateDateFormat(frm.getPrjDateFrom());
			if(date == false){
				errMsg+="Duration From is Invalid Format.  ";
			}
		}
		
		if(frm.getPrjDateTo() == null){
			errMsg+="Duration To is Required. ";
		}else{
			boolean date = common.validateDateFormat(frm.getPrjDateTo());
			if(date == false){
				errMsg+="Duration To is Invalid Format.  ";
			}
		}
		
		if(frm.getPrjTarget() == null || frm.getPrjTarget().trim().length()==0  ){
			errMsg+="Target Beneficiaries is Required.  ";
		}
		

		if(frm.getPrjBudget() == null || frm.getPrjBudget().trim().length()==0  ){
			errMsg+="Budget is Required.  ";
		}
		
		
		if(frm.getPrjSource() == null || frm.getPrjSource().trim().length()==0  ){
			errMsg+="Source is Required.  ";
		}
		

		if(frm.getPrjDescription() == null || frm.getPrjDescription().trim().length()==0  ){
			errMsg+="Project Description is Required.  ";
		}
		

		if(frm.getAddress() == null || frm.getAddress().trim().length()==0  ){
			errMsg+="Address is Required.  ";
		}
		
		if(frm.getRegionList().equals("0")){
			errMsg+="Region is Required.  ";
		}
		
		if(frm.getProvinceList().equals("0")){
			errMsg+="Province is Required.  ";
		}
		
		if(frm.getMunicipalityList().equals("0")){
			errMsg+="Municipality is Required.  ";
		}
		
		if(frm.getPrjPmAssignList().equals("0")){
			errMsg+="Project Manager is Required.  ";
		}
		
		return errMsg;
		
	}

}
