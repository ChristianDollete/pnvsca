package com.web.lpi.lpiProjectFind;

import com.web.ApplicationMessage;

public class LpiProjectFindEntryLine {
	
	private int prjCd;
	private String prjId;
	private String prjName;
	private String prjDescription;
	private String prjDateFrom;
	private String prjDateTo;
	private String prjProjectManager;
	
	
	
	public String getPrjProjectManager() {
		return prjProjectManager;
	}
	public void setPrjProjectManager(String prjProjectManager) {
		this.prjProjectManager = prjProjectManager;
	}
	public int getPrjCd() {
		return prjCd;
	}
	public void setPrjCd(int prjCd) {
		this.prjCd = prjCd;
	}
	public String getPrjId() {
		return prjId;
	}
	public void setPrjId(String prjId) {
		this.prjId = prjId;
	}
	public String getPrjName() {
		return prjName;
	}
	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}
	public String getPrjDescription() {
		return prjDescription;
	}
	public void setPrjDescription(String prjDescription) {
		this.prjDescription = prjDescription;
	}
	public String getPrjDateFrom() {
		return prjDateFrom;
	}
	public void setPrjDateFrom(String prjDateFrom) {
		this.prjDateFrom = prjDateFrom;
	}
	public String getPrjDateTo() {
		return prjDateTo;
	}
	public void setPrjDateTo(String prjDateTo) {
		this.prjDateTo = prjDateTo;
	}
	
	
	
	
	
	
}
