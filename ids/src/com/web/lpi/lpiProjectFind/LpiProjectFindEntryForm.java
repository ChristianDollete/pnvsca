package com.web.lpi.lpiProjectFind;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiProjectFindEntryForm extends ApplicationMessage  {
	

	
	private int prjCd;
	private String prjId;
	private String prjName;
	private String prjDateFrom;
	private String prjDateTo;
	private String prjTarget;
	private String prjBudget;
	private String prjSource;
	private String prjObjective;
	private String prjDescription;
	
	private String prjPmAssign;
	private String prjPmAssignList;
	private ArrayList prjPmAssignLists = new ArrayList();
	
	private String prjStat;
	private String prjStatList;
	private ArrayList prjStatLists = new ArrayList();
	
	public ArrayList prjList;
	
	
	
	public ArrayList getPrjList() {
		return prjList;
	}
	public void setPrjList(LpiProjectFindEntryLine prjLine) {
		this.prjList.add(prjLine);
	}
	
	public void clearPrjList(){
		this.prjList = new ArrayList();
	}
	
	public int getPrjCd() {
		return prjCd;
	}
	public void setPrjCd(int prjCd) {
		this.prjCd = prjCd;
	}
	public String getPrjId() {
		return prjId;
	}
	public void setPrjId(String prjId) {
		this.prjId = prjId;
	}
	public String getPrjName() {
		return prjName;
	}
	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}
	public String getPrjDateFrom() {
		return prjDateFrom;
	}
	public void setPrjDateFrom(String prjDateFrom) {
		this.prjDateFrom = prjDateFrom;
	}
	public String getPrjDateTo() {
		return prjDateTo;
	}
	public void setPrjDateTo(String prjDateTo) {
		this.prjDateTo = prjDateTo;
	}
	public String getPrjTarget() {
		return prjTarget;
	}
	public void setPrjTarget(String prjTarget) {
		this.prjTarget = prjTarget;
	}
	public String getPrjBudget() {
		return prjBudget;
	}
	public void setPrjBudget(String prjBudget) {
		this.prjBudget = prjBudget;
	}
	
	public String getPrjSource() {
		return prjSource;
	}
	public void setPrjSource(String prjSource) {
		this.prjSource = prjSource;
	}
	public String getPrjObjective() {
		return prjObjective;
	}
	public void setPrjObjective(String prjObjective) {
		this.prjObjective = prjObjective;
	}
	public String getPrjDescription() {
		return prjDescription;
	}
	public void setPrjDescription(String prjDescription) {
		this.prjDescription = prjDescription;
	}
	public String getPrjPmAssign() {
		return prjPmAssign;
	}
	public void setPrjPmAssign(String prjPmAssign) {
		this.prjPmAssign = prjPmAssign;
	}
	public String getPrjPmAssignList() {
		return prjPmAssignList;
	}
	public void setPrjPmAssignList(String prjPmAssignList) {
		this.prjPmAssignList = prjPmAssignList;
	}
	public ArrayList getPrjPmAssignLists() {
		return prjPmAssignLists;
	}
	public void setPrjPmAssignLists(String prjPmAssign) {
		this.prjPmAssignLists.add(prjPmAssign);
	}
	
	public void clearPrjPmAssignLists(){
		this.prjPmAssignLists = new ArrayList();
	}
	
	public String getPrjStat() {
		return prjStat;
	}
	public void setPrjStat(String prjStat) {
		this.prjStat = prjStat;
	}
	public String getPrjStatList() {
		return prjStatList;
	}
	public void setPrjStatList(String prjStatList) {
		this.prjStatList = prjStatList;
	}
	public ArrayList getPrjStatLists() {
		return prjStatLists;
	}
	public void setPrjStatLists(String prjStat) {
		this.prjStatLists.add(prjStat);
	}

	public void clearPrjStatLists(){
		this.prjStatLists = new ArrayList();
	}
	
	
	public void populateStat(){
		prjStat="0";
		prjStatList="";
		prjStatLists = new ArrayList();
		prjStatLists.add("Not yet Started");
		prjStatLists.add("Inception Phase");
		prjStatLists.add("On-going");
	}
	
	
	public void reset(){
		
		clearCountryLists();
		clearRegionLists();
		clearProvinceLists();
		clearMunicipalityLists();
		setAddress("");
		
		prjCd=0;
		prjId="";
		prjName="";
		prjDateFrom="";
		prjDateTo="";
		prjTarget="";
		prjBudget="";
		prjSource="";
		prjObjective="";
		prjDescription="";
		
		prjPmAssign="";
		prjPmAssignList="";
		prjPmAssignLists = new ArrayList();
		
		prjList = new ArrayList();
		
		
	}
	
	
	
}
