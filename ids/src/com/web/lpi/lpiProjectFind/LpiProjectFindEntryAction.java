package com.web.lpi.lpiProjectFind;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.web.UserSession;
import com.web.lpi.lpiProjectManager.LpiProjectManagerEntryForm;
import com.web.lpi.lpiProjectManagerFind.LpiProjectManagerFindEntryLine;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;

public class LpiProjectFindEntryAction extends ActionSupport {

	private LpiProjectFindEntryForm frm;
	public LpiProjectFindEntryForm getFrm() {return frm;}
	public void setFrm(LpiProjectFindEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		System.out.println("LpiProjectFindEntryAction reload");
		frm.reset();
		
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
		
	}
	
	
	public String execute() {
		System.out.println("LpiProjectFindEntryAction execute");
		String rtrn;
	
		try{
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			if(user!=null){
				rtrn = "onLoad";
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.link = "0";
					//rtrn="failed";
				}
			
				
				if(frm.getLink()!=null && frm.getLink().equals("3") || frm.getBtnName().equalsIgnoreCase("Reset")){
					System.out.println("LpiProjectFindEntryAction execute LOAD | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					this.reload();
					LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
					ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
					Iterator i = pmlList.iterator();
					while(i.hasNext()){
						LpiProjectManagerModDetails details = (LpiProjectManagerModDetails)i.next();
						frm.setPrjPmAssignLists(details.getPmId() + " --- "+ details.getPmName());
					}
					
					rtrn = "onLoad";
				}else if (frm.getBtnName().equalsIgnoreCase("Search")){
					System.out.println("LpiProjectFindEntryAction execute SAVE | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
		
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){

						
						LpiProjectModDetails details = new LpiProjectModDetails();
						details.setPrjOrgName(frm.getPrjName());
						details.setPrjProjectDesc(frm.getPrjDescription());
						details.setPrjProjectObjective(frm.getPrjObjective());
						details.setPrjDurationFrom(frm.getPrjDateFrom());
						details.setPrjDurationTo(frm.getPrjDateTo());
						
						String pmId = "0";
						
						try {
							String[] pmIdLst = frm.getPrjPmAssignList().split(" --- ");
							pmId = pmIdLst[0].toString();
							System.out.println("PMD ID >> "+pmId);
							LpiProjectManagerEntryControllerBean pmC = new LpiProjectManagerEntryControllerBean();
							LpiProjectManagerModDetails pmD = pmC.getPmByPmId(pmId);
							details.setPrjProjectManager(pmD.getPmCd());
							
						} catch (Exception e) {
							details.setPrjProjectManager(0);
							System.out.println("CANNOT SPLT PM ID");
						}
						
						
						details.setPrjCmpCode(user.getUserCmpCode());
						
						int rgn=0;
						AdLogonControllerBean logC = new AdLogonControllerBean();
						try {
							AdAddRegionModDetails rgnd= logC.getRegionByRgnName(frm.getRegionList());
							rgn = rgnd.getRgn_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						details.setPrjRgn(rgn);
						
						int prnvc = 0;
					
						try {
							AdAddProvinceModDetails prvd = logC.getProvinceByPrvcName(frm.getProvinceList());
							prnvc = prvd.getPrvnc_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						details.setPrjPrvnc(prnvc);
						
						int mncpl = 0;
						try{
							AdAddMunicipalityModDetails mund = logC.getMunicipalityByMunName(frm.getMunicipalityList());
							mncpl = mund.getMun_id();
						} catch (Exception e){
							e.printStackTrace();
						}
						details.setPrjMncplty(mncpl);
						details.setPrjAddress(frm.getAddress());
						
						LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
						ArrayList prjLst = prjFC.getPrjByCriteria(details);
						this.reload();
						LpiProjectManagerEntryControllerBean  pmC = new LpiProjectManagerEntryControllerBean();
						ArrayList pmlList =  pmC.getPmByLpiCd(user.getUserCmpCode());
						Iterator ii = pmlList.iterator();
						while(ii.hasNext()){
							LpiProjectManagerModDetails detailsi = (LpiProjectManagerModDetails)ii.next();
							frm.setPrjPmAssignLists(detailsi.getPmId() + " --- "+ detailsi.getPmName());
						}
						
						
						if(prjLst.size()==0){
							frm.setAppMessage("No Records Found.");
						}else{
							Iterator i = prjLst.iterator();
							frm.clearPrjList();
							while(i.hasNext()){
								LpiProjectModDetails prji = (LpiProjectModDetails)i.next();
								LpiProjectFindEntryLine prjLine = new LpiProjectFindEntryLine();
								prjLine.setPrjCd(prji.getPrjCd());
								prjLine.setPrjId(prji.getPrjId());
								prjLine.setPrjName(prji.getPrjOrgName());
								prjLine.setPrjDescription(prji.getPrjProjectDesc());
								prjLine.setPrjDateFrom(prji.getPrjDurationFrom());
								prjLine.setPrjDateTo(prji.getPrjDurationTo());
								prjLine.setPrjProjectManager(prji.getPmName());
								frm.setPrjList(prjLine);
							}
						}
						rtrn = "onLoad";
					}else{
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiProjectFindEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					
					rtrn="main";
				}
			}else{
				rtrn="login";
			}
			
		}catch (Exception e){
			e.printStackTrace();
			rtrn = "failed";
		}
		return rtrn;
	}
	
	
	public String validateFrm(LpiProjectFindEntryForm frm){
		System.out.println("LpiProjectFindEntryAction execute validateFrm");
		
		String errMsg = "";
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}

}