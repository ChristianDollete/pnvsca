package com.web.lpi.lpiRequestVolunteer;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectEntryControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.web.UserSession;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.vso.vsoVolunteerFind.VsoVolunteerFindEntryForm;

public class LpiRequestVolunteerEntryAction extends ActionSupport {
	
	private LpiRequestVolunteerEntryForm frm;
	public LpiRequestVolunteerEntryForm getFrm() {return frm;}
	public void setFrm(LpiRequestVolunteerEntryForm frm) {this.frm = frm;}
	public String onload = "";
	
	public void reload(){
		frm.reset();
		
		
	}
	
	public String execute() {
		System.out.println("LpiRequestVolunteerEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			
			rtrn = "onLoad";
			
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				if(frm.getBtnName() == null){
					frm.setBtnName("");
				}
			
				int vso_cmp_cd = 0;
				if(user.getUserProfile() == 3){
					vso_cmp_cd = user.getUserCmpCode();
				}
				
				
				if (frm.getLink()!=null && frm.getLink().equals("77")){
					System.out.println("LpiRequestVolunteerEntryAction execute Load 77");
					
					
					
					
					LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
					LpiRequestVolunteerModDetails rvd = rvC.getRvByRvCd(frm.getRv_cd(),vso_cmp_cd);
					
					frm.setRv_cd(rvd.getRv_cd());
					frm.setRv_id(rvd.getRv_id());
					//frm.clearRv_statusLists();
					frm.setRv_status(rvd.getRv_status());
					
					this.reload();
					
					String rv_prj_nm="0";
					frm.clearRv_prjLists();
					
					if(user.getUserProfile()==1 || user.getUserProfile()==3 || user.getUserProfile() == 31){
						LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
						LpiProjectModDetails prjD2 = prjC.getPrjByPrjCode(rvd.getRv_prj_cd());
						frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
						rv_prj_nm = prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName();
					}else{
						LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
						ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
						Iterator prji = prjL.iterator();
						while(prji.hasNext()){
							LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
							frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
							if(prjD2.getPrjCd()==rvd.getRv_prj_cd()){
								rv_prj_nm = prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName();
							}
						}
						
					}
					
					frm.setRv_prj_nm(rv_prj_nm);
					
					LpiProjectManagerFindControllerBean pmf= new LpiProjectManagerFindControllerBean();
					String pmName="";
					try {
						if(!frm.getRv_prj_nm().equals("0")){
							String[] prml = rv_prj_nm.split(" --- ");
							pmName = prml[0].toString();
							System.out.println("pmName ID >> "+pmName);
							LpiProjectManagerModDetails pmm = pmf.getPmByPrjName(pmName);
							pmName = pmm.getPmName();
						}
					}catch (Exception e){
						pmName="";
					}
					
					frm.setRv_prj_mngr_nm(pmName);
					String rvln2 = "0";
					if(rvd.getRv_no_vlntr()>0){
						rvln2 =  String.valueOf(rvd.getRv_no_vlntr());
					}
					frm.setRv_no_vlntr(rvln2);
					frm.setRv_prj_drtn_mnths(rvd.getRv_drtn_mnths());
					frm.clearRv_ln();
					ArrayList rlList = rvd.getRvlList();
					Iterator i = rlList.iterator();
					while(i.hasNext()){
						LpiRequestVolunteerLineBean rvlBean = (LpiRequestVolunteerLineBean)i.next();
						LpiRequestVolunteerEntryLine rvLine = new LpiRequestVolunteerEntryLine();
						rvLine.setRvl_ln_no(rvlBean.getRvl_ln_no());
						rvLine.setRvl_addrss(rvlBean.getRvl_addrss());
						rvLine.setRvl_dt_frm(rvlBean.getRvl_dt_frm());
						rvLine.setRvl_dt_to(rvlBean.getRvl_dt_to());
						rvLine.setRvl_objctv(rvlBean.getRvl_objctv());
						rvLine.setRvl_prfrrd_dt(rvlBean.getRvl_prfrd_dt_arrvl());
						rvLine.setRvl_cd(rvlBean.getRvl_cd());
						rvLine.setRvl_rv_cd(rvlBean.getRvl_rv_cd());
						frm.setRv_ln(rvLine);
					}
					rtrn = "onLoad";
					
				}else if ((frm.getLink() != null && frm.getLink().equals("7")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("LpiRequestVolunteerEntryAction execute Load Reset");
					this.reload();
					LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
					ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
					Iterator prji = prjL.iterator();
					frm.clearRv_prjLists();
					while(prji.hasNext()){
						LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
						System.out.println("prjName >> "+ prjD2.getPmName());
						frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
					}
					frm.setRv_prj_nm("0");
					frm.clearRv_ln();
					rtrn = "onLoad";
				
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("LpiRequestVolunteerEntryAction execute Save");
					
					String errmsg = this.validateFrm(frm);
					if(errmsg.equalsIgnoreCase("")){
						
						LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
						LpiRequestVolunteerModDetails details = new LpiRequestVolunteerModDetails();
						details.setRv_id(frm.getRv_id());
						details.setRv_status(frm.getRv_statusList());
						LpiProjectModDetails prjD;
						int prj_cd = 0;
						try {
							String prj_nm = frm.getRv_prjList();
							String[] prjList = prj_nm.split(" --- ");
							prj_nm = prjList[0].toString();
							System.out.println("prj_nm ID >> "+prj_nm);
							
							LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
							prjD = prjC.getPrjByPrjId(prj_nm);
							prj_cd = prjD.getPrjCd();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						details.setRv_prj_cd(prj_cd);
						details.setRv_cmp_cd(user.getUserCmpCode());
						
						int rvln = 0;
						System.out.println("no. of volunteer >> "+frm.getRv_no_vlntr());
						if(frm.getRv_no_vlntr().toString().trim().length()>0){
							
							rvln =  Integer.parseInt(frm.getRv_no_vlntr());
						}
						details.setRv_no_vlntr(rvln);
						details.setRv_drtn_mnths(frm.getRv_prj_drtn_mnthsLst());
						int rvCd = 0;
						if(frm.getRv_cd()==0){
							rvCd = rvC.createRV(details);
							this.reload();
							frm.setAppMessage("Request Has Bean Save. Please Update Request Line Details.");
						}else{
							details.setRv_cd(frm.getRv_cd());
							rvCd = rvC.updateRV(details);
							this.reload();
							frm.setAppMessage("Request Has Bean Updated. Please Update Request Line Details.");
						}
						LpiRequestVolunteerModDetails rvd = rvC.getRvByRvCd(rvCd,vso_cmp_cd);
						frm.setRv_cd(rvd.getRv_cd());
						frm.setRv_id(rvd.getRv_id());
						//frm.clearRv_statusLists();
						frm.setRv_status(rvd.getRv_status());
						
						LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
						ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
						Iterator prji = prjL.iterator();
						frm.clearRv_prjLists();
						String rv_prj_nm="0";
						while(prji.hasNext()){
							LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
							frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
							if(prjD2.getPrjCd()==rvd.getRv_prj_cd()){
								rv_prj_nm = prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName();
							}
						}
						frm.setRv_prj_nm(rv_prj_nm);
						LpiProjectManagerFindControllerBean pmf= new LpiProjectManagerFindControllerBean();
						String pmName="";
						try {
							if(!frm.getRv_prj_nm().equals("0")){
								String[] prml = rv_prj_nm.split(" --- ");
								pmName = prml[0].toString();
								System.out.println("pmName ID >> "+prml);
								LpiProjectManagerModDetails pmm = pmf.getPmByPrjName(pmName);
								pmName = pmm.getPmName();
							}
						}catch (Exception e){
							pmName="";
						}
						
						frm.setRv_prj_mngr_nm(pmName);
						String rvln2 = "0";
						if(rvd.getRv_no_vlntr()>0){
							rvln2 =  String.valueOf(rvd.getRv_no_vlntr());
						}
						frm.setRv_no_vlntr(rvln2);
						frm.setRv_prj_drtn_mnths(rvd.getRv_drtn_mnths());
						frm.clearRv_ln();
						ArrayList rlList = rvd.getRvlList();
						Iterator i = rlList.iterator();
						while(i.hasNext()){
							LpiRequestVolunteerLineBean rvlBean = (LpiRequestVolunteerLineBean)i.next();
							LpiRequestVolunteerEntryLine rvLine = new LpiRequestVolunteerEntryLine();
							rvLine.setRvl_ln_no(rvlBean.getRvl_ln_no());
							rvLine.setRvl_addrss(rvlBean.getRvl_addrss());
							rvLine.setRvl_dt_frm(rvlBean.getRvl_dt_frm());
							rvLine.setRvl_dt_to(rvlBean.getRvl_dt_to());
							rvLine.setRvl_objctv(rvlBean.getRvl_objctv());
							rvLine.setRvl_prfrrd_dt(rvlBean.getRvl_prfrd_dt_arrvl());
							rvLine.setRvl_cd(rvlBean.getRvl_cd());
							rvLine.setRvl_rv_cd(rvlBean.getRvl_rv_cd());
							frm.setRv_ln(rvLine);
						}
						rtrn = "onLoad";
					}else{
						
						this.reload();
						LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
						ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
						Iterator prji = prjL.iterator();
						frm.clearRv_prjLists();
						while(prji.hasNext()){
							LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
							System.out.println("prjName >> "+ prjD2.getPmName());
							frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
						}
						frm.setRv_prj_nm("0");
						frm.clearRv_ln();
						
						frm.setAppMessage(errmsg);
						rtrn = "onLoad";
					}
				} else if (frm.getBtnName().equalsIgnoreCase("Close")){
					System.out.println("LpiRequestVolunteerEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					rtrn="main";
				} else if (frm.getBtnName().equalsIgnoreCase("Return")){
					System.out.println("LpiRequestVolunteerEntryAction execute RETURN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					 
					 
					
					rtrn = "onLoad";
				}else{
					
					System.out.println("LpiRequestVolunteerEntryAction execute Load ELSE");
									
					LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
					
					System.out.println("REQUEST ID >> "+ frm.getRv_id());
					LpiRequestVolunteerModDetails rvd = rvC.getRvByRvId(frm.getRv_id(),vso_cmp_cd);
					frm.clearRv_statusLists();
					this.reload();
					frm.setRv_cd(rvd.getRv_cd());
					frm.setRv_id(rvd.getRv_id());
					
					frm.setRv_status(rvd.getRv_status());
					
					LpiProjectFindControllerBean prjFC = new LpiProjectFindControllerBean();
					ArrayList prjL = prjFC.getPrjByCmpCode(user.getUserCmpCode());
					Iterator prji = prjL.iterator();
					frm.clearRv_prjLists();
					String rv_prj_nm="0";
					while(prji.hasNext()){
						LpiProjectModDetails prjD2 = (LpiProjectModDetails)prji.next();
						frm.setRv_prjLists(prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName());
						if(prjD2.getPrjCd()==rvd.getRv_prj_cd()){
							rv_prj_nm = prjD2.getPrjId() + " --- "+ prjD2.getPrjOrgName();
						}
					}
					frm.setRv_prj_nm(rv_prj_nm);
					LpiProjectManagerFindControllerBean pmf= new LpiProjectManagerFindControllerBean();
					String pmName="";
					try {
						if(!frm.getRv_prj_nm().equals("0")){
							String[] prml = rv_prj_nm.split(" --- ");
							pmName = prml[0].toString();
							System.out.println("pmName ID >> "+prml);
							LpiProjectManagerModDetails pmm = pmf.getPmByPrjName(pmName);
							pmName = pmm.getPmName();
						}
					}catch (Exception e){
						pmName="";
					}
					
					frm.setRv_prj_mngr_nm(pmName);
					String rvln2 = "0";
					if(rvd.getRv_no_vlntr()>0){
						rvln2 =  String.valueOf(rvd.getRv_no_vlntr());
					}
					frm.setRv_no_vlntr(rvln2);
					frm.setRv_prj_drtn_mnths(rvd.getRv_drtn_mnths());
					frm.clearRv_ln();
					ArrayList rlList = rvd.getRvlList();
					Iterator i = rlList.iterator();
					while(i.hasNext()){
						LpiRequestVolunteerLineBean rvlBean = (LpiRequestVolunteerLineBean)i.next();
						LpiRequestVolunteerEntryLine rvLine = new LpiRequestVolunteerEntryLine();
						rvLine.setRvl_ln_no(rvlBean.getRvl_ln_no());
						rvLine.setRvl_addrss(rvlBean.getRvl_addrss());
						rvLine.setRvl_dt_frm(rvlBean.getRvl_dt_frm());
						rvLine.setRvl_dt_to(rvlBean.getRvl_dt_to());
						rvLine.setRvl_objctv(rvlBean.getRvl_objctv());
						rvLine.setRvl_prfrrd_dt(rvlBean.getRvl_prfrd_dt_arrvl());
						rvLine.setRvl_cd(rvlBean.getRvl_cd());
						rvLine.setRvl_rv_cd(rvlBean.getRvl_rv_cd());
						frm.setRv_ln(rvLine);
					}
					
					
					rtrn = "onLoad";
					
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(LpiRequestVolunteerEntryForm vfrm){
		System.out.println("LpiRequestVolunteerEntryAction execute validateFrm");
		
		String errMsg = "";
		
		
		if(frm.getRv_id().trim().length()>0 && frm.getRv_cd() == 0){
			
			try {
				LpiRequestVolunteerControllerBean rvCB = new LpiRequestVolunteerControllerBean();
				LpiRequestVolunteerModDetails rvMD = new LpiRequestVolunteerModDetails();
				rvMD.setRv_id(frm.getRv_id());
				ArrayList list = rvCB.getRvByCriteria(rvMD);
				if(list.size() > 0){
					errMsg ="Request No. is already registered. ";
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(frm.getRv_id().trim().length()>0 && frm.getRv_cd() > 0){
			try {
				LpiRequestVolunteerControllerBean rvCB = new LpiRequestVolunteerControllerBean();
				LpiRequestVolunteerModDetails rvMD = new LpiRequestVolunteerModDetails();
				rvMD.setRv_id(frm.getRv_id());
				ArrayList list = rvCB.getRvByCriteria(rvMD);
				if(list.size() > 0){
					Iterator i = list.iterator();
					while(i.hasNext()){
						LpiRequestVolunteerModDetails rvMDi = (LpiRequestVolunteerModDetails)i.next();
						if(rvMDi.getRv_cd() != frm.getRv_cd()){
							errMsg ="Request No. is already registered. ";
						}
						break;
					}
					
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		System.out.println("frm.getRv_prjList() >> "+frm.getRv_prjList());
		if(frm.getRv_prjList() == null || frm.getRv_prjList().equals("0")){
			errMsg += "Project is Required.  ";
		}
		
		int rvln = 0;
		if(frm.getRv_no_vlntr().toString().trim().length()>0){
			try {
				rvln =  Integer.parseInt(frm.getRv_no_vlntr());
			} catch (NumberFormatException e) {
				errMsg ="Invalid Number Format. ";
			}
		}else{
			errMsg +="Request No. of Volunteer is Required.  ";
		}
		
		
		
		if(frm.getRv_statusList().toString().trim().equalsIgnoreCase("0")){
			errMsg += "Invalid Selected Status.  ";
		}
		
		
		
		if(frm.getRv_prj_drtn_mnthsLst().equals("0")){
			errMsg += "Project Duration is Required.  ";
		}
		
		
		/*errMsg = "Organization Name is Required";
		errMsg +=", ";
		errMsg += "Type of Organization is Required";*/
		return errMsg;
		
	}
	
	public String prjMngrName;
	public String getPrjMngrName() {return prjMngrName;}
	public String getProjectManager(){
		System.out.println("LpiRequestVolunteerEntryAction getProjectManager");
		String pmName = "";
		try {
			LpiProjectManagerFindControllerBean pmf= new LpiProjectManagerFindControllerBean();
			try {
				if(!frm.getRv_prj_nm().equals("0")){
					String[] prjList = frm.getRv_prj_nm().split(" --- ");
					pmName = prjList[0].toString();
					System.out.println("pmName ID >> "+pmName);
					LpiProjectManagerModDetails pmm = pmf.getPmByPrjName(pmName);
					pmName = pmm.getPmName();
				}
				prjMngrName = pmName;;
			} catch (Exception e) {
				pmName="";
				System.out.println("CANNOT SPLT PRJ ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	
		
	}

}
