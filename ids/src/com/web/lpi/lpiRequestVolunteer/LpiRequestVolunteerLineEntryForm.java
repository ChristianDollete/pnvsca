package com.web.lpi.lpiRequestVolunteer;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiRequestVolunteerLineEntryForm extends ApplicationMessage {

	private int rvl_cd;
	public int rvl_ln_no;
	private String rvl_prfrd_dt_arrvl;
	private String rvl_dt_frm;
	private String rvl_dt_to;
	private String rvl_objctv;
	private String rvl_objectvLst;
	private ArrayList rvl_objectvLsts = new ArrayList();

	private String rvl_output;
	private String rvl_outputLst;
	private ArrayList rvl_outputLsts = new ArrayList();

	private String rvl_actvts;
	private String rvl_actvtsLst;
	private ArrayList rvl_actvtsLsts = new ArrayList();

	private String rvl_implmttn_prd;
	private String rvl_implmttn_prdLst;
	private ArrayList rvl_implmttn_prdLsts = new ArrayList();

	private String rvl_fndng_src;
	private String rvl_fndng_srcLst;
	private ArrayList rvl_fndng_srcLsts = new ArrayList();

	private String rvl_fld_spcfctn;
	private String rvl_fld_spcfctnLst;
	private ArrayList rvl_fld_spcfctnLsts = new ArrayList();

	private String rvl_exprnc_trnnng;
	private String rvl_exprnc_trnnngLst;
	private ArrayList rvl_exprnc_trnnngLsts = new ArrayList();

	private String rvl_edctn;
	private String rvl_edctnLst;
	private ArrayList rvl_edctnLsts = new ArrayList();

	private int rvl_vso_cd;
	private int rvl_vlntr_cd;
	private String rvl_dt_arrvl;
	private boolean rvl_emrgncy;
	private boolean rvl_trnsprt;
	private boolean rvl_fclty;
	private boolean rvl_housing;
	private boolean rvl_othrs;
	private String rvl_othrs_spcfy;
	public int rvl_rv_cd;
	public String rvl_rv_rqst_id;

	public String rvl_vso_nm;
	public String rvl_vso_nmLst;
	public ArrayList rvl_vso_nmLsts = new ArrayList();
		
	private boolean rvl_tecH_admin=false;
	
	private String rvl_vlntr_nm;
	private String rvl_vlntr_nmLst;
	private ArrayList rvl_vlntr_nmLsts = new ArrayList();
	
	
	
	public void clearRvl_edctnLsts() {
		this.rvl_edctnLsts = new ArrayList();
	}

	public void clearRvl_vso_nmLsts() {
		this.rvl_vso_nmLsts = new ArrayList();
	}

	public String getRvl_actvts() {
		return rvl_actvts;
	}

	public String getRvl_actvtsLst() {
		return rvl_actvtsLst;
	}

	public ArrayList getRvl_actvtsLsts() {
		return rvl_actvtsLsts;
	}

	public int getRvl_cd() {
		return rvl_cd;
	}

	public String getRvl_dt_arrvl() {
		return rvl_dt_arrvl;
	}

	public String getRvl_dt_frm() {
		return rvl_dt_frm;
	}

	public String getRvl_dt_to() {
		return rvl_dt_to;
	}

	public String getRvl_edctn() {
		return rvl_edctn;
	}

	public String getRvl_edctnLst() {
		return rvl_edctnLst;
	}

	public ArrayList getRvl_edctnLsts() {
		return rvl_edctnLsts;
	}

	public boolean getRvl_emrgncy() {
		return rvl_emrgncy;
	}

	public String getRvl_exprnc_trnnng() {
		return rvl_exprnc_trnnng;
	}

	public String getRvl_exprnc_trnnngLst() {
		return rvl_exprnc_trnnngLst;
	}

	public ArrayList getRvl_exprnc_trnnngLsts() {
		return rvl_exprnc_trnnngLsts;
	}

	public boolean getRvl_fclty() {
		return rvl_fclty;
	}

	public String getRvl_fld_spcfctn() {
		return rvl_fld_spcfctn;
	}

	public String getRvl_fld_spcfctnLst() {
		return rvl_fld_spcfctnLst;
	}

	public ArrayList getRvl_fld_spcfctnLsts() {
		return rvl_fld_spcfctnLsts;
	}

	public String getRvl_fndng_src() {
		return rvl_fndng_src;
	}

	public String getRvl_fndng_srcLst() {
		return rvl_fndng_srcLst;
	}

	public ArrayList getRvl_fndng_srcLsts() {
		return rvl_fndng_srcLsts;
	}

	public boolean getRvl_housing() {
		return rvl_housing;
	}

	public String getRvl_implmttn_prd() {
		return rvl_implmttn_prd;
	}

	public String getRvl_implmttn_prdLst() {
		return rvl_implmttn_prdLst;
	}

	public ArrayList getRvl_implmttn_prdLsts() {
		return rvl_implmttn_prdLsts;
	}

	public int getRvl_ln_no() {
		return rvl_ln_no;
	}

	public String getRvl_objctv() {
		return rvl_objctv;
	}

	public String getRvl_objectvLst() {
		return rvl_objectvLst;
	}

	public ArrayList getRvl_objectvLsts() {
		return rvl_objectvLsts;
	}

	public boolean getRvl_othrs() {
		return rvl_othrs;
	}

	public String getRvl_othrs_spcfy() {
		return rvl_othrs_spcfy;
	}

	public String getRvl_output() {
		return rvl_output;
	}

	public String getRvl_outputLst() {
		return rvl_outputLst;
	}

	public ArrayList getRvl_outputLsts() {
		return rvl_outputLsts;
	}

	public String getRvl_prfrd_dt_arrvl() {
		return rvl_prfrd_dt_arrvl;
	}

	public int getRvl_rv_cd() {
		return rvl_rv_cd;
	}

	public String getRvl_rv_rqst_id() {
		return rvl_rv_rqst_id;
	}

	public boolean getRvl_trnsprt() {
		return rvl_trnsprt;
	}

	public int getRvl_vlntr_cd() {
		return rvl_vlntr_cd;
	}

	public String getRvl_vlntr_nm() {
		return rvl_vlntr_nm;
	}

	public String getRvl_vlntr_nmLst() {
		return rvl_vlntr_nmLst;
	}

	public ArrayList getRvl_vlntr_nmLsts() {
		return rvl_vlntr_nmLsts;
	}

	public int getRvl_vso_cd() {
		return rvl_vso_cd;
	}

	public String getRvl_vso_nm() {
		return rvl_vso_nm;
	}

	public String getRvl_vso_nmLst() {
		return rvl_vso_nmLst;
	}

	public ArrayList getRvl_vso_nmLsts() {
		return rvl_vso_nmLsts;
	}

	public boolean isRvl_tecH_admin() {
		return rvl_tecH_admin;
	}

	public void reset() {

		rvl_dt_frm = "";
		rvl_dt_to = "";
		rvl_objctv = "";
		rvl_output = "";
		rvl_actvts = "";
		rvl_implmttn_prd = "";
		rvl_fndng_src = "";
		rvl_fld_spcfctn = "";
		rvl_exprnc_trnnng = "";
		rvl_edctn = "";
		rvl_vso_cd = 0;
		rvl_vlntr_cd = 0;
		rvl_dt_arrvl = "";
		rvl_emrgncy = false;
		rvl_trnsprt = false;
		rvl_fclty = false;
		rvl_housing = false;
		rvl_othrs = false;
		rvl_othrs_spcfy = "";
		clearGenderLists();
		rvl_objectvLsts.add("");

		clearRvl_edctnLsts();
		populateEducation();
		rvl_edctn = "0";
		
		rvl_tecH_admin=false;
		
		rvl_vlntr_nm="0";
		rvl_vlntr_nmLst="";
		rvl_vlntr_nmLsts = new ArrayList();
		
		clearRegionLists();
		clearProvinceLists();
		clearMunicipalityLists();
		clearCountryLists();
		
		
		
		

	}
	
	
	public void populateEducation(){
		clearRvl_edctnLsts();
		setRvl_edctnLsts("Undergraduate");
		setRvl_edctnLsts("Graduate");
		setRvl_edctnLsts("Masteral");
		setRvl_edctnLsts("Doctorate");
		setRvl_edctnLsts("Technical/Vocational/Certificate and Associate Degree");
	}

	public void setRvl_actvts(String rvl_actvts) {
		this.rvl_actvts = rvl_actvts;
	}

	public void setRvl_actvtsLst(String rvl_actvtsLst) {
		this.rvl_actvtsLst = rvl_actvtsLst;
	}

	public void setRvl_actvtsLsts(String rvl_actvtsLsts) {
		this.rvl_actvtsLsts.add(rvl_actvtsLsts);
	}

	public void setRvl_cd(int rvl_cd) {
		this.rvl_cd = rvl_cd;
	}

	public void setRvl_dt_arrvl(String rvl_dt_arrvl) {
		this.rvl_dt_arrvl = rvl_dt_arrvl;
	}

	public void setRvl_dt_frm(String rvl_dt_frm) {
		this.rvl_dt_frm = rvl_dt_frm;
	}

	public void setRvl_dt_to(String rvl_dt_to) {
		this.rvl_dt_to = rvl_dt_to;
	}

	public void setRvl_edctn(String rvl_edctn) {
		this.rvl_edctn = rvl_edctn;
	}

	public void setRvl_edctnLst(String rvl_edctnLst) {
		this.rvl_edctnLst = rvl_edctnLst;
	}

	public void setRvl_edctnLsts(String rvl_edctnLsts) {
		this.rvl_edctnLsts.add(rvl_edctnLsts);
	}

	public void setRvl_emrgncy(boolean rvl_emrgncy) {
		this.rvl_emrgncy = rvl_emrgncy;
	}

	public void setRvl_exprnc_trnnng(String rvl_exprnc_trnnng) {
		this.rvl_exprnc_trnnng = rvl_exprnc_trnnng;
	}

	public void setRvl_exprnc_trnnngLst(String rvl_exprnc_trnnngLst) {
		this.rvl_exprnc_trnnngLst = rvl_exprnc_trnnngLst;
	}

	public void setRvl_exprnc_trnnngLsts(String rvl_exprnc_trnnngLsts) {
		this.rvl_exprnc_trnnngLsts.add(rvl_exprnc_trnnngLsts);
	}

	public void setRvl_fclty(boolean rvl_fclty) {
		this.rvl_fclty = rvl_fclty;
	}

	public void setRvl_fld_spcfctn(String rvl_fld_spcfctn) {
		this.rvl_fld_spcfctn = rvl_fld_spcfctn;
	}

	public void setRvl_fld_spcfctnLst(String rvl_fld_spcfctnLst) {
		this.rvl_fld_spcfctnLst = rvl_fld_spcfctnLst;
	}

	public void setRvl_fld_spcfctnLsts(String rvl_fld_spcfctnLsts) {
		this.rvl_fld_spcfctnLsts.add(rvl_fld_spcfctnLsts);
	}

	public void setRvl_fndng_src(String rvl_fndng_src) {
		this.rvl_fndng_src = rvl_fndng_src;
	}

	public void setRvl_fndng_srcLst(String rvl_fndng_srcLst) {
		this.rvl_fndng_srcLst = rvl_fndng_srcLst;
	}

	public void setRvl_fndng_srcLsts(String rvl_fndng_srcLsts) {
		this.rvl_fndng_srcLsts.add(rvl_fndng_srcLsts);
	}

	public void setRvl_housing(boolean rvl_housing) {
		this.rvl_housing = rvl_housing;
	}

	public void setRvl_implmttn_prd(String rvl_implmttn_prd) {
		this.rvl_implmttn_prd = rvl_implmttn_prd;
	}

	public void setRvl_implmttn_prdLst(String rvl_implmttn_prdLst) {
		this.rvl_implmttn_prdLst = rvl_implmttn_prdLst;
	}

	public void setRvl_implmttn_prdLsts(String rvl_implmttn_prdLsts) {
		this.rvl_implmttn_prdLsts.add(rvl_implmttn_prdLsts);
	}

	public void setRvl_ln_no(int rvl_ln_no) {
		this.rvl_ln_no = rvl_ln_no;
	}

	public void setRvl_objctv(String rvl_objctv) {
		this.rvl_objctv = rvl_objctv;
	}

	public void setRvl_objectvLst(String rvl_objectvLst) {
		this.rvl_objectvLst = rvl_objectvLst;
	}

	public void setRvl_objectvLsts(String rvl_objectvLst) {
		this.rvl_objectvLsts.add(rvl_objectvLst);
	}

	public void setRvl_othrs(boolean rvl_othrs) {
		this.rvl_othrs = rvl_othrs;
	}

	public void setRvl_othrs_spcfy(String rvl_othrs_spcfy) {
		this.rvl_othrs_spcfy = rvl_othrs_spcfy;
	}

	public void setRvl_output(String rvl_output) {
		this.rvl_output = rvl_output;
	}

	public void setRvl_outputLst(String rvl_outputLst) {
		this.rvl_outputLst = rvl_outputLst;
	}

	public void setRvl_outputLsts(String rvl_outputLsts) {
		this.rvl_outputLsts.add(rvl_outputLsts);
	}

	public void setRvl_prfrd_dt_arrvl(String rvl_prfrd_dt_arrvl) {
		this.rvl_prfrd_dt_arrvl = rvl_prfrd_dt_arrvl;
	}

	public void setRvl_rv_cd(int rvl_rv_cd) {
		this.rvl_rv_cd = rvl_rv_cd;
	}

	public void setRvl_rv_rqst_id(String rvl_rv_rqst_id) {
		this.rvl_rv_rqst_id = rvl_rv_rqst_id;
	}

	public void setRvl_tecH_admin(boolean rvl_tecH_admin) {
		this.rvl_tecH_admin = rvl_tecH_admin;
	}

	public void setRvl_trnsprt(boolean rvl_trnsprt) {
		this.rvl_trnsprt = rvl_trnsprt;
	}

	public void setRvl_vlntr_cd(int rvl_vlntr_cd) {
		this.rvl_vlntr_cd = rvl_vlntr_cd;
	}

	public void setRvl_vlntr_nm(String rvl_vlntr_nm) {
		this.rvl_vlntr_nm = rvl_vlntr_nm;
	}

	public void setRvl_vlntr_nmLst(String rvl_vlntr_nmLst) {
		this.rvl_vlntr_nmLst = rvl_vlntr_nmLst;
	}

	public void setRvl_vlntr_nmLsts(String rvl_vlntr_nmLsts) {
		this.rvl_vlntr_nmLsts.add(rvl_vlntr_nmLsts);
	}

	public void setRvl_vso_cd(int rvl_vso_cd) {
		this.rvl_vso_cd = rvl_vso_cd;
	}

	public void setRvl_vso_nm(String rvl_vso_nm) {
		this.rvl_vso_nm = rvl_vso_nm;
	}

	public void setRvl_vso_nmLst(String rvl_vso_nmLst) {
		this.rvl_vso_nmLst = rvl_vso_nmLst;
	}

	public void setRvl_vso_nmLsts(String rvl_vso_nmLsts) {
		this.rvl_vso_nmLsts.add(rvl_vso_nmLsts);
	}

}
