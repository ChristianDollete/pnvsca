package com.web.lpi.lpiRequestVolunteer;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiRequestVolunteerEntryForm extends ApplicationMessage {
	private int rv_cd;
	private String rv_id;
	private String rv_ln_no;
	private String rv_status;
	private String rv_statusList;
	private ArrayList rv_statusLists = new ArrayList();

	private int rv_prj_cd;
	private String rv_prjList;
	private ArrayList rv_prjLists = new ArrayList();

	private String rv_prj_nm;
	private String rv_prj_mngr_nm;
	private int rv_cmp_cd;
	private String rv_no_vlntr;
	
	private String rv_prj_drtn_mnths;
	private String rv_prj_drtn_mnthsLst;
	
	private ArrayList rv_prj_drtn_mnthsLsts = new ArrayList();
	
	
	
	
	
	public String getRv_prj_drtn_mnths() {
		return rv_prj_drtn_mnths;
	}

	public void setRv_prj_drtn_mnths(String rv_prj_drtn_mnths) {
		this.rv_prj_drtn_mnths = rv_prj_drtn_mnths;
	}

	public String getRv_prj_drtn_mnthsLst() {
		return rv_prj_drtn_mnthsLst;
	}

	public void setRv_prj_drtn_mnthsLst(String rv_prj_drtn_mnthsLst) {
		this.rv_prj_drtn_mnthsLst = rv_prj_drtn_mnthsLst;
	}

	public ArrayList getRv_prj_drtn_mnthsLsts() {
		return rv_prj_drtn_mnthsLsts;
	}

	public void setRv_prj_drtn_mnthsLsts(String rv_prj_drtn_mnthsLsts) {
		this.rv_prj_drtn_mnthsLsts.add(rv_prj_drtn_mnthsLsts);
	}
	

	public String getRv_no_vlntr() {
		return rv_no_vlntr;
	}

	public void setRv_no_vlntr(String rv_no_vlntr) {
		this.rv_no_vlntr = rv_no_vlntr;
	}

	public String getRv_statusList() {
		return rv_statusList;
	}

	public void setRv_statusList(String rv_statusList) {
		this.rv_statusList = rv_statusList;
	}

	public ArrayList getRv_statusLists() {
		return rv_statusLists;
	}

	public void setRv_statusLists(String rv_statusList) {
		this.rv_statusLists.add(rv_statusList);
	}
	
	public void clearRv_statusLists(){
		this.rv_statusLists = new ArrayList();
	}

	public String getRv_id() {
		return rv_id;
	}

	public void setRv_id(String rv_id) {
		this.rv_id = rv_id;
	}

	public String getRv_prjList() {
		return rv_prjList;
	}

	public void setRv_prjList(String rv_prjList) {
		this.rv_prjList = rv_prjList;
	}

	public ArrayList getRv_prjLists() {
		return rv_prjLists;
	}

	public void setRv_prjLists(String rv_prjList) {
		this.rv_prjLists.add(rv_prjList);
	}

	public void clearRv_prjLists() {
		this.rv_prjLists = new ArrayList();
	}

	private ArrayList rv_ln = new ArrayList();

	public ArrayList getRv_ln() {
		return rv_ln;
	}

	public void setRv_ln(LpiRequestVolunteerEntryLine rvl) {
		this.rv_ln.add(rvl);
	}

	public void clearRv_ln() {
		this.rv_ln = new ArrayList();
	}

	public int getRv_cd() {
		return rv_cd;
	}

	public void setRv_cd(int rv_cd) {
		this.rv_cd = rv_cd;
	}

	public String getRv_ln_no() {
		return rv_ln_no;
	}

	public void setRv_ln_no(String rv_ln_no) {
		this.rv_ln_no = rv_ln_no;
	}

	public String getRv_status() {
		return rv_status;
	}

	public void setRv_status(String rv_status) {
		this.rv_status = rv_status;
	}

	public int getRv_prj_cd() {
		return rv_prj_cd;
	}

	public void setRv_prj_cd(int rv_prj_cd) {
		this.rv_prj_cd = rv_prj_cd;
	}

	public String getRv_prj_nm() {
		return rv_prj_nm;
	}

	public void setRv_prj_nm(String rv_prj_nm) {
		this.rv_prj_nm = rv_prj_nm;
	}

	public String getRv_prj_mngr_nm() {
		return rv_prj_mngr_nm;
	}

	public void setRv_prj_mngr_nm(String rv_prj_mngr_nm) {
		this.rv_prj_mngr_nm = rv_prj_mngr_nm;
	}

	public int getRv_cmp_cd() {
		return rv_cmp_cd;
	}

	public void setRv_cmp_cd(int rv_cmp_cd) {
		this.rv_cmp_cd = rv_cmp_cd;
	}

	public void reset() {
		rv_cd = 0;
		rv_ln_no = "0";
		rv_prj_cd = 0;
		rv_prj_nm = "";
		rv_prj_mngr_nm = "";
		rv_cmp_cd = 0;
		rv_statusLists = new ArrayList();
		rv_statusLists.add("For-Approval");
		rv_statusLists.add("Approved");
		rv_statusLists.add("Disapproved");
		rv_statusLists.add("On-going");
		rv_statusLists.add("Cancelled");
		rv_statusLists.add("Closed");
		rv_status = "For-Approval";
		
		rv_prjLists = new ArrayList();
		rv_prj_nm="0";
		
		
		rv_prj_drtn_mnthsLsts = new ArrayList();
		rv_prj_drtn_mnthsLsts.add("1");
		rv_prj_drtn_mnthsLsts.add("2");
		rv_prj_drtn_mnthsLsts.add("3");
		rv_prj_drtn_mnthsLsts.add("4");
		rv_prj_drtn_mnthsLsts.add("5");
		rv_prj_drtn_mnthsLsts.add("6");
		rv_prj_drtn_mnthsLsts.add("7");
		rv_prj_drtn_mnthsLsts.add("8");
		rv_prj_drtn_mnthsLsts.add("9");
		rv_prj_drtn_mnthsLsts.add("10");
		rv_prj_drtn_mnthsLsts.add("11");
		rv_prj_drtn_mnthsLsts.add("12");
		rv_prj_drtn_mnthsLsts.add("13");
		rv_prj_drtn_mnthsLsts.add("14");
		rv_prj_drtn_mnthsLsts.add("15");
		rv_prj_drtn_mnthsLsts.add("16");
		rv_prj_drtn_mnthsLsts.add("17");
		rv_prj_drtn_mnthsLsts.add("18");
		rv_prj_drtn_mnthsLsts.add("19");
		rv_prj_drtn_mnthsLsts.add("20");
		rv_prj_drtn_mnthsLsts.add("21");
		rv_prj_drtn_mnthsLsts.add("22");
		rv_prj_drtn_mnthsLsts.add("23");
		rv_prj_drtn_mnthsLsts.add("24");
		rv_prj_drtn_mnthsLsts.add("25");
		rv_prj_drtn_mnthsLsts.add("26");
		rv_prj_drtn_mnthsLsts.add("27");
		rv_prj_drtn_mnthsLsts.add("28");
		rv_prj_drtn_mnthsLsts.add("29");
		rv_prj_drtn_mnthsLsts.add("30");
		rv_prj_drtn_mnthsLsts.add("31");
		rv_prj_drtn_mnthsLsts.add("32");
		rv_prj_drtn_mnthsLsts.add("33");
		rv_prj_drtn_mnthsLsts.add("34");
		rv_prj_drtn_mnthsLsts.add("35");
		rv_prj_drtn_mnthsLsts.add("36");
		rv_prj_drtn_mnthsLsts.add("37");
		rv_prj_drtn_mnthsLsts.add("38");
		rv_prj_drtn_mnthsLsts.add("39");
		rv_prj_drtn_mnthsLsts.add("40");
		rv_prj_drtn_mnthsLsts.add("41");
		rv_prj_drtn_mnthsLsts.add("42");
		rv_prj_drtn_mnthsLsts.add("43");
		rv_prj_drtn_mnthsLsts.add("44");
		rv_prj_drtn_mnthsLsts.add("45");
		rv_prj_drtn_mnthsLsts.add("46");
		rv_prj_drtn_mnthsLsts.add("47");
		rv_prj_drtn_mnthsLsts.add("48");
		rv_prj_drtn_mnthsLsts.add("49");
		rv_prj_drtn_mnthsLsts.add("50");
		rv_prj_drtn_mnthsLsts.add("51");
		rv_prj_drtn_mnthsLsts.add("52");
		rv_prj_drtn_mnthsLsts.add("53");
		rv_prj_drtn_mnthsLsts.add("54");
		rv_prj_drtn_mnthsLsts.add("55");
		rv_prj_drtn_mnthsLsts.add("56");
		rv_prj_drtn_mnthsLsts.add("57");
		rv_prj_drtn_mnthsLsts.add("58");
		rv_prj_drtn_mnthsLsts.add("59");
		rv_prj_drtn_mnthsLsts.add("60");
		rv_prj_drtn_mnthsLsts.add("61");
		rv_prj_drtn_mnthsLsts.add("62");
		rv_prj_drtn_mnthsLsts.add("63");
		rv_prj_drtn_mnthsLsts.add("64");
		rv_prj_drtn_mnthsLsts.add("65");
		rv_prj_drtn_mnthsLsts.add("66");
		rv_prj_drtn_mnthsLsts.add("67");
		rv_prj_drtn_mnthsLsts.add("68");
		rv_prj_drtn_mnthsLsts.add("69");
		rv_prj_drtn_mnthsLsts.add("70");
		rv_prj_drtn_mnthsLsts.add("71");
		rv_prj_drtn_mnthsLsts.add("72");
		rv_prj_drtn_mnths = "0";
	}

}
