package com.web.lpi.lpiRequestVolunteer;

import java.util.ArrayList;

import com.web.ApplicationMessage;

public class LpiRequestVolunteerEntryLine extends ApplicationMessage {

	private int rvl_cd;
	private int rvl_ln_no;
	private int rvl_rgn;
	private int rvl_prnvc;
	private int rvl_mncplty;
	private String rvl_addrss;
	private String rvl_prfrrd_dt;
	private String rvl_dt_frm;
	private String rvl_dt_to;
	private String rvl_objctv;
	private String rvl_output;
	private String rvl_actvts;
	private String rvl_implmttn_prd;
	private String rvl_fndng_src;
	private String rvl_fld_spcfctn;
	private String rvl_exprnc_trnnng;
	private String rvl_edctn;
	private int rvl_vso_cd;
	private int rvl_vlntr_cd;
	private String rvl_dt_arrvl;
	private int rvl_emrgncy;
	private int rvl_trnsprt;
	private int rvl_fclty;
	private int rvl_housing;
	private int rvl_othrs;
	private String rvl_othrs_spcfy;
	private int rvl_rv_cd;
	
	

	public String getRvl_prfrrd_dt() {
		return rvl_prfrrd_dt;
	}

	public void setRvl_prfrrd_dt(String rvl_prfrrd_dt) {
		this.rvl_prfrrd_dt = rvl_prfrrd_dt;
	}

	public int getRvl_cd() {
		return rvl_cd;
	}

	public void setRvl_cd(int rvl_cd) {
		this.rvl_cd = rvl_cd;
	}

	public int getRvl_ln_no() {
		return rvl_ln_no;
	}

	public void setRvl_ln_no(int rvl_ln_no) {
		this.rvl_ln_no = rvl_ln_no;
	}

	public int getRvl_rgn() {
		return rvl_rgn;
	}

	public void setRvl_rgn(int rvl_rgn) {
		this.rvl_rgn = rvl_rgn;
	}

	public int getRvl_prnvc() {
		return rvl_prnvc;
	}

	public void setRvl_prnvc(int rvl_prnvc) {
		this.rvl_prnvc = rvl_prnvc;
	}

	public int getRvl_mncplty() {
		return rvl_mncplty;
	}

	public void setRvl_mncplty(int rvl_mncplty) {
		this.rvl_mncplty = rvl_mncplty;
	}

	public String getRvl_addrss() {
		return rvl_addrss;
	}

	public void setRvl_addrss(String rvl_addrss) {
		this.rvl_addrss = rvl_addrss;
	}

	public String getRvl_dt_frm() {
		return rvl_dt_frm;
	}

	public void setRvl_dt_frm(String rvl_dt_frm) {
		this.rvl_dt_frm = rvl_dt_frm;
	}

	public String getRvl_dt_to() {
		return rvl_dt_to;
	}

	public void setRvl_dt_to(String rvl_dt_to) {
		this.rvl_dt_to = rvl_dt_to;
	}

	public String getRvl_objctv() {
		return rvl_objctv;
	}

	public void setRvl_objctv(String rvl_objctv) {
		this.rvl_objctv = rvl_objctv;
	}

	public String getRvl_output() {
		return rvl_output;
	}

	public void setRvl_output(String rvl_output) {
		this.rvl_output = rvl_output;
	}

	public String getRvl_actvts() {
		return rvl_actvts;
	}

	public void setRvl_actvts(String rvl_actvts) {
		this.rvl_actvts = rvl_actvts;
	}

	public String getRvl_implmttn_prd() {
		return rvl_implmttn_prd;
	}

	public void setRvl_implmttn_prd(String rvl_implmttn_prd) {
		this.rvl_implmttn_prd = rvl_implmttn_prd;
	}

	public String getRvl_fndng_src() {
		return rvl_fndng_src;
	}

	public void setRvl_fndng_src(String rvl_fndng_src) {
		this.rvl_fndng_src = rvl_fndng_src;
	}

	public String getRvl_fld_spcfctn() {
		return rvl_fld_spcfctn;
	}

	public void setRvl_fld_spcfctn(String rvl_fld_spcfctn) {
		this.rvl_fld_spcfctn = rvl_fld_spcfctn;
	}

	public String getRvl_exprnc_trnnng() {
		return rvl_exprnc_trnnng;
	}

	public void setRvl_exprnc_trnnng(String rvl_exprnc_trnnng) {
		this.rvl_exprnc_trnnng = rvl_exprnc_trnnng;
	}

	public String getRvl_edctn() {
		return rvl_edctn;
	}

	public void setRvl_edctn(String rvl_edctn) {
		this.rvl_edctn = rvl_edctn;
	}

	public int getRvl_vso_cd() {
		return rvl_vso_cd;
	}

	public void setRvl_vso_cd(int rvl_vso_cd) {
		this.rvl_vso_cd = rvl_vso_cd;
	}

	public int getRvl_vlntr_cd() {
		return rvl_vlntr_cd;
	}

	public void setRvl_vlntr_cd(int rvl_vlntr_cd) {
		this.rvl_vlntr_cd = rvl_vlntr_cd;
	}

	public String getRvl_dt_arrvl() {
		return rvl_dt_arrvl;
	}

	public void setRvl_dt_arrvl(String rvl_dt_arrvl) {
		this.rvl_dt_arrvl = rvl_dt_arrvl;
	}

	public int getRvl_emrgncy() {
		return rvl_emrgncy;
	}

	public void setRvl_emrgncy(int rvl_emrgncy) {
		this.rvl_emrgncy = rvl_emrgncy;
	}

	public int getRvl_trnsprt() {
		return rvl_trnsprt;
	}

	public void setRvl_trnsprt(int rvl_trnsprt) {
		this.rvl_trnsprt = rvl_trnsprt;
	}

	public int getRvl_fclty() {
		return rvl_fclty;
	}

	public void setRvl_fclty(int rvl_fclty) {
		this.rvl_fclty = rvl_fclty;
	}

	public int getRvl_housing() {
		return rvl_housing;
	}

	public void setRvl_housing(int rvl_housing) {
		this.rvl_housing = rvl_housing;
	}

	public int getRvl_othrs() {
		return rvl_othrs;
	}

	public void setRvl_othrs(int rvl_othrs) {
		this.rvl_othrs = rvl_othrs;
	}

	public String getRvl_othrs_spcfy() {
		return rvl_othrs_spcfy;
	}

	public void setRvl_othrs_spcfy(String rvl_othrs_spcfy) {
		this.rvl_othrs_spcfy = rvl_othrs_spcfy;
	}

	public int getRvl_rv_cd() {
		return rvl_rv_cd;
	}

	public void setRvl_rv_cd(int rvl_rv_cd) {
		this.rvl_rv_cd = rvl_rv_cd;
	}

}
