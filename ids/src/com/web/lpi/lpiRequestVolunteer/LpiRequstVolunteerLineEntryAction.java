package com.web.lpi.lpiRequestVolunteer;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.LpiRequestVolunteerLineBean;
import com.opensymphony.xwork2.ActionSupport;
import com.txn.AdLogonControllerBean;
import com.txn.LpiProjectFindControllerBean;
import com.txn.LpiProjectManagerEntryControllerBean;
import com.txn.LpiProjectManagerFindControllerBean;
import com.txn.LpiRequestVolunteerControllerBean;
import com.txn.VsoVolunteerEntryControllerBean;
import com.txn.VsoVolunteerFindControllerBean;
import com.txn.VsoVsoControllerBean;
import com.web.UserSession;
import com.web.common;
import com.web.util.AdAddCountryModDetails;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;
import com.web.util.VsoVolunteerModDetails;
import com.web.util.VsoVsoModDetails;

public class LpiRequstVolunteerLineEntryAction extends ActionSupport {
	
	private LpiRequestVolunteerLineEntryForm frm;
	public LpiRequestVolunteerLineEntryForm getFrm() {return frm;}
	public void setFrm(LpiRequestVolunteerLineEntryForm frm) {this.frm = frm;}

	public String onload = "";
	int rqst_cd = 0;
	int rqst_ln_no = 0;
	int rvl_cd=0;
	public void reload() throws Exception{
		frm.reset();
		AdLogonControllerBean logonCB = new AdLogonControllerBean();
		ArrayList rgnList = new ArrayList();
		frm.clearRegionLists();
		try {
			rgnList = logonCB.getAllRegion();
			Iterator rl = rgnList.iterator();
			while (rl.hasNext()) {
				AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
				frm.setRegionLists(rgnD.getRgn_nm());
			}
		} catch (Exception e) {
		}
		frm.setRegion("0");
		frm.setRvl_rv_cd(rqst_cd);
		frm.setRvl_ln_no(rqst_ln_no);
		frm.setRvl_cd(rvl_cd);
		
		VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
		ArrayList list = vsoC.getAllVso();
		Iterator i = list.iterator();
		while(i.hasNext()){
			VsoVsoModDetails vsoD = (VsoVsoModDetails)i.next();
			frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
		}
		frm.setRvl_vso_nm("0");
	}
	
	public String execute() {
		System.out.println("LpiRequstVolunteerLineEntryAction execute");
		
		String rtrn;
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
			Hashtable sessionsMap = (Hashtable)ServletActionContext.getServletContext().getAttribute("sessionKey");
			UserSession user = (UserSession)sessionsMap.get(session.getId());
			rtrn = "onLoad";
			if(user!=null){
								
				
				try {
					frm.getLink();
				} catch (Exception e) {
					frm.setLink("0");
					rtrn="failed";
				}
				
				System.out.println("frm.getBtnName() >> "+frm.getBtnName());
				LpiRequestVolunteerControllerBean rvC = new LpiRequestVolunteerControllerBean();
				
				if (frm.getLink()!=null && frm.getLink().equals("711")){
					
					System.out.println("LpiRequstVolunteerLineEntryAction execute Load");
					System.out.println("RV CODE >> " + frm.getRvl_rv_cd());
					rqst_cd = frm.getRvl_rv_cd();
					System.out.println("RV LINE NO >> " + frm.getRvl_ln_no());
					rqst_ln_no = frm.getRvl_ln_no();
					rvl_cd = frm.getRvl_cd();
					this.reload();
				
					LpiRequestVolunteerLineBean rvl = rvC.getRvlByRvlCd(rvl_cd);
					
					

					frm.setRvl_ln_no(rvl.getRvl_ln_no());
					frm.setRvl_prfrd_dt_arrvl(rvl.getRvl_prfrd_dt_arrvl());
					frm.setRvl_objctv(rvl.getRvl_objctv());
					frm.setRvl_output(rvl.getRvl_output());
					frm.setRvl_actvts(rvl.getRvl_actvts());
					frm.setRvl_implmttn_prd(rvl.getRvl_implmttn_prd());
					frm.setRvl_fndng_src(rvl.getRvl_fndng_src());
					frm.setRvl_fld_spcfctn(rvl.getRvl_fld_spcfctn());
					frm.setRvl_exprnc_trnnng(rvl.getRvl_exprnc_trnnng());
					frm.setRvl_edctn(rvl.getRvl_edctn());
					frm.setRvl_dt_arrvl(rvl.getRvl_dt_arrvl());
					frm.setRvl_emrgncy(common.IntToBoolean(rvl.getRvl_emrgncy()));
					frm.setRvl_trnsprt(common.IntToBoolean(rvl.getRvl_trnsprt()));
					frm.setRvl_fclty(common.IntToBoolean(rvl.getRvl_fclty()));
					frm.setRvl_housing(common.IntToBoolean(rvl.getRvl_housing()));
					frm.setRvl_othrs(common.IntToBoolean(rvl.getRvl_othrs()));
					frm.setRvl_othrs_spcfy(rvl.getRvl_othrs_spcfy());
					
					int rgn=0;
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					
					ArrayList rgnList = new ArrayList();
					frm.clearRegionLists();
					frm.setRegion("0");
					try {
						rgnList = logonCB.getAllRegion();
						Iterator rl = rgnList.iterator();
						while (rl.hasNext()) {
							AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
							frm.setRegionLists(rgnD.getRgn_nm());
							if(rgnD.getRgn_id() == rvl.getRvl_rgn()){
								frm.setRegion(rgnD.getRgn_nm());
							}
						}
					} catch (Exception e) {
					}
					
					ArrayList prvncList =  new ArrayList();
					
					try {
						prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
						Iterator i = prvncList.iterator();
						frm.clearProvinceLists();
						frm.setProvince("0");
						while(i.hasNext()){
							AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
							frm.setProvinceLists(details.getPrvnc_nm().toString());
							if(details.getPrvnc_id() == rvl.getRvl_prnvc()){
								frm.setProvince(details.getPrvnc_nm());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					ArrayList munList = new ArrayList();
					try {
						munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
						Iterator i = munList.iterator();
						frm.clearMunicipalityLists();
						frm.setMunicipality("0");
						while(i.hasNext()){
							AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
							frm.setMunicipalityLists(details.getMun_nm().toString());
							if(details.getMun_id() == rvl.getRvl_mncplty()){
								frm.setMunicipality(details.getMun_nm());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					frm.setAddress(rvl.getRvl_addrss());
					
					try {
						VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
						frm.setRvl_vso_nm("0");
						frm.clearRvl_vso_nmLsts();
						if(user.getUserProfile()==3 || user.getUserProfile() == 1 || (user.getUserCmpCode() == rvl.getRvl_vso_cd())){
							VsoVsoModDetails vsoD = vsoC.getVsoByVsoCmpCd(user.getUserCmpCode());
							frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
							frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
						}else{
							ArrayList list = vsoC.getAllVso();
							Iterator i = list.iterator();
							while(i.hasNext()){
								VsoVsoModDetails vsoD = (VsoVsoModDetails)i.next();
								frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								if(vsoD.getVso_cmp_cd() == rvl.getRvl_vso_cd()){
									frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								}
							}	
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					try {
						VsoVolunteerEntryControllerBean vlntrCc = new VsoVolunteerEntryControllerBean();
						frm.setRvl_vlntr_nm("0");
						if(user.getUserProfile()==31){
							VsoVolunteerModDetails vl = vlntrCc.getByVlntrUsrCode(user.getUserCode());
							frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
							frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
						}else{
							
							ArrayList vltL = vlntrCc.getVlntrByCmpCd(user.getUserCmpCode());
							Iterator i = vltL.iterator();
							while(i.hasNext()){
								VsoVolunteerModDetails vl = (VsoVolunteerModDetails)i.next();
								
								if(vl.getVlntr_cd()  == rvl.getRvl_vlntr_cd()){
									frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								}
								frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					rvl.setRvl_rv_cd(frm.getRvl_rv_cd());
					rvl.setRvl_cd(frm.getRvl_cd());
					rtrn = "onLoad";
				}else if ((frm.getLink() != null && frm.getLink().equals("71")) || frm.getBtnName().equalsIgnoreCase("Reset")) {
					System.out.println("LpiRequstVolunteerLineEntryAction execute Reset");
					
								
					System.out.println("RV CODE >> " + frm.getRvl_rv_cd());
					rqst_cd = frm.getRvl_rv_cd();
					System.out.println("RV LINE NO >> " + frm.getRvl_ln_no());
					rqst_ln_no = frm.getRvl_ln_no();
					rvl_cd = frm.getRvl_cd();
					this.reload();
				
					LpiRequestVolunteerLineBean rvl = rvC.getRvlByRvlCd(rvl_cd);
					
					

					frm.setRvl_ln_no(rvl.getRvl_ln_no());
					frm.setRvl_prfrd_dt_arrvl(rvl.getRvl_prfrd_dt_arrvl());
					frm.setRvl_objctv(rvl.getRvl_objctv());
					frm.setRvl_output(rvl.getRvl_output());
					frm.setRvl_actvts(rvl.getRvl_actvts());
					frm.setRvl_implmttn_prd(rvl.getRvl_implmttn_prd());
					frm.setRvl_fndng_src(rvl.getRvl_fndng_src());
					frm.setRvl_fld_spcfctn(rvl.getRvl_fld_spcfctn());
					frm.setRvl_exprnc_trnnng(rvl.getRvl_exprnc_trnnng());
					frm.setRvl_edctn(rvl.getRvl_edctn());
					frm.setRvl_dt_arrvl(rvl.getRvl_dt_arrvl());
					frm.setRvl_emrgncy(common.IntToBoolean(rvl.getRvl_emrgncy()));
					frm.setRvl_trnsprt(common.IntToBoolean(rvl.getRvl_trnsprt()));
					frm.setRvl_fclty(common.IntToBoolean(rvl.getRvl_fclty()));
					frm.setRvl_housing(common.IntToBoolean(rvl.getRvl_housing()));
					frm.setRvl_othrs(common.IntToBoolean(rvl.getRvl_othrs()));
					frm.setRvl_othrs_spcfy(rvl.getRvl_othrs_spcfy());
					
					int rgn=0;
					AdLogonControllerBean logonCB = new AdLogonControllerBean();
					
					ArrayList rgnList = new ArrayList();
					frm.clearRegionLists();
					frm.setRegion("0");
					try {
						rgnList = logonCB.getAllRegion();
						Iterator rl = rgnList.iterator();
						while (rl.hasNext()) {
							AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
							frm.setRegionLists(rgnD.getRgn_nm());
							if(rgnD.getRgn_id() == rvl.getRvl_rgn()){
								frm.setRegion(rgnD.getRgn_nm());
							}
						}
					} catch (Exception e) {
					}
					
					ArrayList prvncList =  new ArrayList();
					
					try {
						prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
						Iterator i = prvncList.iterator();
						frm.clearProvinceLists();
						frm.setProvince("0");
						while(i.hasNext()){
							AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
							frm.setProvinceLists(details.getPrvnc_nm().toString());
							if(details.getPrvnc_id() == rvl.getRvl_prnvc()){
								frm.setProvince(details.getPrvnc_nm());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					ArrayList munList = new ArrayList();
					try {
						munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
						Iterator i = munList.iterator();
						frm.clearMunicipalityLists();
						frm.setMunicipality("0");
						while(i.hasNext()){
							AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
							frm.setMunicipalityLists(details.getMun_nm().toString());
							if(details.getMun_id() == rvl.getRvl_mncplty()){
								frm.setMunicipality(details.getMun_nm());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					frm.setAddress(rvl.getRvl_addrss());
					
					try {
						VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
						frm.setRvl_vso_nm("0");
						frm.clearRvl_vso_nmLsts();
						if(user.getUserProfile()==3 || user.getUserProfile() == 1){
							VsoVsoModDetails vsoD = vsoC.getVsoByVsoCmpCd(user.getUserCmpCode());
							frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
							frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
						}else{
							ArrayList list = vsoC.getAllVso();
							Iterator i = list.iterator();
							while(i.hasNext()){
								VsoVsoModDetails vsoD = (VsoVsoModDetails)i.next();
								frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								if(vsoD.getVso_cd() == rvl.getRvl_vso_cd()){
									frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								}
							}	
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					try {
						VsoVolunteerEntryControllerBean vlntrCc = new VsoVolunteerEntryControllerBean();
						frm.setRvl_vlntr_nm("0");
						if(user.getUserProfile()==31){
							VsoVolunteerModDetails vl = vlntrCc.getByVlntrUsrCode(user.getUserCode());
							frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
							frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
						}else{
							ArrayList vltL = vlntrCc.getVlntrByCmpCd(user.getUserCmpCode());
							Iterator i = vltL.iterator();
							while(i.hasNext()){
								VsoVolunteerModDetails vl = (VsoVolunteerModDetails)i.next();
								frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								if(vl.getVlntr_cd()  == rvl.getRvl_vlntr_cd()){
									frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					rvl.setRvl_rv_cd(frm.getRvl_rv_cd());
					rvl.setRvl_cd(frm.getRvl_cd());
					
					
					
					
					
					rtrn = "onLoad";
				}else if (frm.getBtnName().equalsIgnoreCase("Save")){
					System.out.println("LpiRequstVolunteerLineEntryAction execute Save");
					
					String errmsg = this.validateFrm(frm,user);
					
					if(errmsg.equalsIgnoreCase("")){
						LpiRequestVolunteerLineBean rvl = new LpiRequestVolunteerLineBean();
						
						rvl.setRvl_ln_no(frm.getRvl_ln_no());
						rvl.setRvl_prfrd_dt_arrvl(frm.getRvl_prfrd_dt_arrvl());
						rvl.setRvl_objctv(frm.getRvl_objctv());
						rvl.setRvl_output(frm.getRvl_output());
						rvl.setRvl_actvts(frm.getRvl_actvts());
						rvl.setRvl_implmttn_prd(frm.getRvl_implmttn_prd());
						rvl.setRvl_fndng_src(frm.getRvl_fndng_src());
						rvl.setRvl_fld_spcfctn(frm.getRvl_fld_spcfctn());
						rvl.setRvl_exprnc_trnnng(frm.getRvl_exprnc_trnnng());
						rvl.setRvl_edctn(frm.getRvl_edctnLst());
						rvl.setRvl_dt_arrvl(frm.getRvl_dt_arrvl());
						rvl.setRvl_emrgncy(common.booleanToInt(frm.getRvl_emrgncy()));
						rvl.setRvl_trnsprt(common.booleanToInt(frm.getRvl_trnsprt()));
						rvl.setRvl_fclty(common.booleanToInt(frm.getRvl_fclty()));
						rvl.setRvl_housing(common.booleanToInt(frm.getRvl_housing()));
						rvl.setRvl_othrs(common.booleanToInt(frm.getRvl_othrs()));
						rvl.setRvl_othrs_spcfy(frm.getRvl_othrs_spcfy());
						
						int rgn=0;
						AdLogonControllerBean logC = new AdLogonControllerBean();
						try {
							AdAddRegionModDetails rgnd= logC.getRegionByRgnName(frm.getRegionList());
							rgn = rgnd.getRgn_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						rvl.setRvl_rgn(rgn);
						
						int prnvc = 0;
						try {
							AdAddProvinceModDetails prvd = logC.getProvinceByPrvcName(frm.getProvinceList());
							prnvc = prvd.getPrvnc_id();
						} catch (Exception e) {
							e.printStackTrace();
						}
						rvl.setRvl_prnvc(prnvc);
						
						int mncpl = 0;
						try{
							AdAddMunicipalityModDetails mund = logC.getMunicipalityByMunName(frm.getMunicipalityList());
							mncpl = mund.getMun_id();
						} catch (Exception e){
							e.printStackTrace();
						}
						rvl.setRvl_mncplty(mncpl);
						rvl.setRvl_addrss(frm.getAddress());
						
						String vso_id="0";
						try {
							String[] vsoList = frm.getRvl_vso_nmLst().split(" --- ");
							vso_id = vsoList[0].toString();
							System.out.println("VSO ID >> "+vso_id);
							VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
							VsoVsoModDetails vsoD = vsoC.getVsoByVsoId(vso_id);
							rvl.setRvl_vso_cd(vsoD.getVso_cmp_cd());
							
						} catch (Exception e) {
							rvl.setRvl_vso_cd(0);
							System.out.println("CANNOT SPLT VSO ID");
						}
						
						String vlntr_id="0";
						try {
							String[] vlntrList = frm.getRvl_vlntr_nmLst().split(" --- ");
							vlntr_id = vlntrList[0].toString();
							System.out.println("VLNTR ID >> "+vlntr_id);
							VsoVolunteerEntryControllerBean vlntrC = new VsoVolunteerEntryControllerBean();
							VsoVolunteerModDetails vvmd = vlntrC.getByVlntrId(vlntr_id);
							rvl.setRvl_vlntr_cd(vvmd.getVlntr_cd());
							
						} catch (Exception e) {
							rvl.setRvl_vlntr_cd(0);
							System.out.println("CANNOT SPLT VLNTR ID");
						}
						rvl.setRvl_rv_cd(frm.getRvl_rv_cd());
						System.out.println("RVL RV CODE >> "+ frm.getRvl_rv_cd());
						rvl.setRvl_cd(frm.getRvl_cd());
						
						
						rvC.updateRVL(rvl);
						
						
					
						//---------------- RELOAD DATA ----------------//
						
		
						System.out.println("//---------------- RELOAD DATA ----------------//");
						System.out.println("LpiRequstVolunteerLineEntryAction execute RE-Load");
						System.out.println("RV CODE >> " + frm.getRvl_rv_cd());
						rqst_cd = frm.getRvl_rv_cd();
						System.out.println("RV LINE NO >> " + frm.getRvl_ln_no());
						rqst_ln_no = frm.getRvl_ln_no();
						rvl_cd = frm.getRvl_cd();
						this.reload();
					
						LpiRequestVolunteerLineBean rvl_reload = rvC.getRvlByRvlCd(rvl_cd);
						
						

						frm.setRvl_ln_no(rvl_reload.getRvl_ln_no());
						frm.setRvl_prfrd_dt_arrvl(rvl_reload.getRvl_prfrd_dt_arrvl());
						frm.setRvl_objctv(rvl_reload.getRvl_objctv());
						frm.setRvl_output(rvl_reload.getRvl_output());
						frm.setRvl_actvts(rvl_reload.getRvl_actvts());
						frm.setRvl_implmttn_prd(rvl_reload.getRvl_implmttn_prd());
						frm.setRvl_fndng_src(rvl_reload.getRvl_fndng_src());
						frm.setRvl_fld_spcfctn(rvl_reload.getRvl_fld_spcfctn());
						frm.setRvl_exprnc_trnnng(rvl_reload.getRvl_exprnc_trnnng());
						frm.setRvl_edctn(rvl_reload.getRvl_edctn());
						frm.setRvl_dt_arrvl(rvl_reload.getRvl_dt_arrvl());
						frm.setRvl_emrgncy(common.IntToBoolean(rvl_reload.getRvl_emrgncy()));
						frm.setRvl_trnsprt(common.IntToBoolean(rvl_reload.getRvl_trnsprt()));
						frm.setRvl_fclty(common.IntToBoolean(rvl_reload.getRvl_fclty()));
						frm.setRvl_housing(common.IntToBoolean(rvl_reload.getRvl_housing()));
						frm.setRvl_othrs(common.IntToBoolean(rvl_reload.getRvl_othrs()));
						frm.setRvl_othrs_spcfy(rvl_reload.getRvl_othrs_spcfy());
						
						int rgn1=0;
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						
						ArrayList rgnList = new ArrayList();
						frm.clearRegionLists();
						frm.setRegion("0");
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							while (rl.hasNext()) {
								AdAddRegionModDetails rgnD = (AdAddRegionModDetails) rl.next();
								frm.setRegionLists(rgnD.getRgn_nm());
								if(rgnD.getRgn_id() == rvl_reload.getRvl_rgn()){
									frm.setRegion(rgnD.getRgn_nm());
								}
							}
						} catch (Exception e) {
						}
						
						ArrayList prvncList =  new ArrayList();
						
						try {
							prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
							Iterator i = prvncList.iterator();
							frm.clearProvinceLists();
							frm.setProvince("0");
							while(i.hasNext()){
								AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
								frm.setProvinceLists(details.getPrvnc_nm().toString());
								if(details.getPrvnc_id() == rvl_reload.getRvl_prnvc()){
									frm.setProvince(details.getPrvnc_nm());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						ArrayList munList = new ArrayList();
						try {
							munList =  logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
							Iterator i = munList.iterator();
							frm.clearMunicipalityLists();
							frm.setMunicipality("0");
							while(i.hasNext()){
								AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
								frm.setMunicipalityLists(details.getMun_nm().toString());
								if(details.getMun_id() == rvl_reload.getRvl_mncplty()){
									frm.setMunicipality(details.getMun_nm());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						frm.setAddress(rvl_reload.getRvl_addrss());
						
						try {
							VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
							frm.setRvl_vso_nm("0");
							frm.clearRvl_vso_nmLsts();
							if(user.getUserProfile()==3 || user.getUserProfile() == 1 || (user.getUserCmpCode() == rvl_reload.getRvl_vso_cd())){
								VsoVsoModDetails vsoD = vsoC.getVsoByVsoCmpCd(user.getUserCmpCode());
								frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
							}else{
								ArrayList list = vsoC.getAllVso();
								Iterator i = list.iterator();
								while(i.hasNext()){
									VsoVsoModDetails vsoD = (VsoVsoModDetails)i.next();
									frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
									if(vsoD.getVso_cd() == rvl_reload.getRvl_vso_cd()){
										frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
									}
								}	
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						try {
							VsoVolunteerEntryControllerBean vlntrCc = new VsoVolunteerEntryControllerBean();
							frm.setRvl_vlntr_nm("0");
							if(user.getUserProfile()==31){
								VsoVolunteerModDetails vl = vlntrCc.getByVlntrUsrCode(user.getUserCode());
								frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
							}else{
								ArrayList vltL = vlntrCc.getVlntrByCmpCd(user.getUserCmpCode());
								Iterator i = vltL.iterator();
								while(i.hasNext()){
									VsoVolunteerModDetails vl = (VsoVolunteerModDetails)i.next();
									frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
									if(vl.getVlntr_cd()  == rvl_reload.getRvl_vlntr_cd()){
										frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						rvl_reload.setRvl_rv_cd(frm.getRvl_rv_cd());
						rvl_reload.setRvl_cd(frm.getRvl_cd());
						
						System.out.println("//----------------END RELOAD DATA ----------------//");
						
						
						frm.setAppMessage("Request Line has been Updated, Click Return to Go back.");
						
					}else{
						
						AdLogonControllerBean logonCB = new AdLogonControllerBean();
						ArrayList cntryList = new ArrayList();
						frm.clearCountryLists();
						try {
							cntryList = logonCB.getAllCountry();
							Iterator cl = cntryList.iterator();
							while (cl.hasNext()) {
								AdAddCountryModDetails cntrD = (AdAddCountryModDetails)cl.next();
								frm.setCountryLists(cntrD.getCntry_nm());
							}
						} catch (Exception e) {
						}
						frm.setCountry(frm.getCountryList());

						ArrayList rgnList = new ArrayList();
						try {
							rgnList = logonCB.getAllRegion();
							Iterator rl = rgnList.iterator();
							frm.clearRegionLists();
							while (rl.hasNext()) {
								AdAddRegionModDetails rgnD = (AdAddRegionModDetails)rl.next();
								frm.setRegionLists(rgnD.getRgn_nm());
							}
						} catch (Exception e) {

							e.printStackTrace();
						}
						frm.setRegion(frm.getRegionList());

						ArrayList prvncList = new ArrayList();
						Iterator i;
						try {
							prvncList = logonCB.getAllProvinceByRgnId(frm.getRegion());
							i = prvncList.iterator();
							frm.clearProvinceLists();
							while (i.hasNext()) {
								AdAddProvinceModDetails details = (AdAddProvinceModDetails)i.next();
								frm.setProvinceLists(details.getPrvnc_nm().toString());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						frm.setProvince(frm.getProvinceList());

						ArrayList munList = new ArrayList();
						try {
							munList = logonCB.getAllMunicipalityByPrvncId(frm.getProvince());
							i = munList.iterator();
							frm.clearMunicipalityLists();
							while (i.hasNext()) {
								AdAddMunicipalityModDetails details = (AdAddMunicipalityModDetails)i.next();
								frm.setMunicipalityLists(details.getMun_nm().toString());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						frm.setMunicipality(frm.getMunicipalityList());
						
						
						frm.clearRvl_edctnLsts();
						frm.populateEducation();
						frm.setRvl_edctn(frm.getRvl_edctnLst());
						
						
						LpiRequestVolunteerLineBean rvl = rvC.getRvlByRvlCd(frm.getRvl_cd());
						
						try {
							VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
							frm.setRvl_vso_nm("0");
							frm.clearRvl_vso_nmLsts();
							if(user.getUserProfile()==3 || user.getUserProfile() == 1){
								VsoVsoModDetails vsoD = vsoC.getVsoByVsoCmpCd(user.getUserCmpCode());
								frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
								frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
							}else{
								ArrayList list = vsoC.getAllVso();
								Iterator i1 = list.iterator();
								while(i1.hasNext()){
									VsoVsoModDetails vsoD = (VsoVsoModDetails)i1.next();
									frm.setRvl_vso_nmLsts(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
									if(vsoD.getVso_cd() == rvl.getRvl_vso_cd()){
										frm.setRvl_vso_nm(vsoD.getVso_id() + " --- "+ vsoD.getVso_nm());
									}
								}	
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						try {
							VsoVolunteerEntryControllerBean vlntrCc = new VsoVolunteerEntryControllerBean();
							frm.setRvl_vlntr_nm("0");
							if(user.getUserProfile()==31){
								VsoVolunteerModDetails vl = vlntrCc.getByVlntrUsrCode(user.getUserCode());
								frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
								frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
							}else{
								ArrayList vltL = vlntrCc.getVlntrByCmpCd(user.getUserCmpCode());
								Iterator i2 = vltL.iterator();
								while(i2.hasNext()){
									VsoVolunteerModDetails vl = (VsoVolunteerModDetails)i2.next();
									frm.setRvl_vlntr_nmLsts(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
									if(vl.getVlntr_cd()  == rvl.getRvl_vlntr_cd()){
										frm.setRvl_vlntr_nm(vl.getVlntr_id() + " --- "+ (vl.getVlntr_lname()+", "+vl.getVlntr_fname()+" "+vl.getVlntr_mname()));
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						rvl.setRvl_rv_cd(frm.getRvl_rv_cd());
						rvl.setRvl_cd(frm.getRvl_cd());
						
						
						
						frm.setAppMessage(errmsg);
					}
					rtrn = "onLoad";
				} else if (frm.getBtnName().equalsIgnoreCase("Return")){
					//System.out.println("LpiRequstVolunteerLineEntryAction execute MAIN | userName >> "+user.getUserName() +"  | session ID >> "+user.getSessionId());
					System.out.println("LpiRequstVolunteerLineEntryAction execute Return");
					rtrn="request";
				}
			}else{
				rtrn="login";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtrn = "failed";
		}

		return rtrn;
	}
	
	
	public String validateFrm(LpiRequestVolunteerLineEntryForm vfrm, UserSession user){
		System.out.println("LpiRequstVolunteerLineEntryAction execute validateFrm");
		
		String errMsg = "";
		
		if(frm.getRegionList().equals("0")){
			
			errMsg+="Region is Required.  ";
		}
		
		if(frm.getProvinceList().equals("0")){
			errMsg+="Province is Required.  ";
		}
		
		if(frm.getMunicipalityList().equals("0")){
			errMsg+="Municipality is Required.  ";
		}
		
		if(frm.getAddress() == null || frm.getAddress().trim().length()==0){
			errMsg+="Address is Required. ";
		}
		
		if(frm.getRvl_prfrd_dt_arrvl() == null || frm.getRvl_prfrd_dt_arrvl().trim().length()==0){
			errMsg+="Preferred Date of Arrival is Required.  ";
		}else{
			boolean date = common.validateDateFormat(frm.getRvl_prfrd_dt_arrvl());
			if(date==false){
				errMsg+="Preferred Date of Arrival is Invalid Format";
			}
		}
		
		
		
		if(frm.getRvl_objctv() == null || frm.getRvl_objctv().trim().length()==0){
			errMsg+="Objective of Volunteer Assignment is Required. ";
		}
		
		if(frm.getRvl_output() == null || frm.getRvl_output().trim().length()==0){
			errMsg+="Expected Output is Required. ";
		}
		
		if(frm.getRvl_actvts() == null || frm.getRvl_actvts().trim().length()==0){
			errMsg+="Volunteer Activities is Required. ";
		}
		
		if(frm.getRvl_implmttn_prd() == null || frm.getRvl_implmttn_prd().trim().length()==0){
			errMsg+="Implementation Period is Required. ";
		}
		
		if(frm.getRvl_fndng_src() == null || frm.getRvl_fndng_src().trim().length()==0){
			errMsg+="Funding Source is Required. ";
		}
		
		if(frm.getRvl_fld_spcfctn() == null || frm.getRvl_fld_spcfctn().trim().length()==0){
			errMsg+="Specialization is Required. ";
		}
		
		if(frm.getRvl_exprnc_trnnng() == null || frm.getRvl_exprnc_trnnng().trim().length()==0){
			errMsg+="Experience and Training is Required. ";
		}
		
		if(frm.getRvl_edctnLst().equals("0")){
			errMsg+="Education is Required. ";
		}
		
		if(frm.getRvl_vso_nmLst().equals("0")){
			errMsg+="VSO is Required.  ";
		}
		
		if(frm.getRvl_dt_arrvl() == null || frm.getRvl_dt_arrvl().trim().length()==0){
			errMsg+="Date of Arrival is Required.  ";
		}else{
			boolean date = common.validateDateFormat(frm.getRvl_dt_arrvl());
			if(date==false){
				errMsg+="Date of Arrival is Invalid Format";
			}
		}
		
		
		if(user.getUserProfile()==3){
			if(frm.getRvl_vlntr_nmLst().equals("0")){
				errMsg+="Volunteer is Required. ";
			}
			
		}
		
		return errMsg;
		
	}
	
	
	private ArrayList<String> volunteers;
	public ArrayList<String> getVolunteers() {
		return volunteers;
	}
	
	public String getVsoVolunteerList(){
		System.out.println("LpiRequstVolunteerLineEntryAction getVsoVolunteerList");
		String vsoName = "";
		System.out.println("VSO NAME >> "+ frm.getRvl_vso_nm());
		try {
			try {
				if(!frm.getRvl_vso_nm().equals("0")){
					HttpSession session = ServletActionContext.getRequest().getSession();
					HttpServletRequest request = (HttpServletRequest) ServletActionContext.getRequest();
					System.out.println("session ID >> "+ request.getSession().getId());
					UserSession user = (UserSession)session.getAttribute(request.getSession().getId());
					
					
					String[] vsoNameList = frm.getRvl_vso_nm().split(" --- ");
					vsoName = vsoNameList[0].toString();
					
					VsoVsoControllerBean vsoC = new VsoVsoControllerBean();
					VsoVsoModDetails vsoMD = vsoC.getVsoByVsoId(vsoName);
					VsoVolunteerFindControllerBean vsoVlntrFC = new VsoVolunteerFindControllerBean();
					ArrayList vlntrLst = vsoVlntrFC.getVolunteerWithoutProject(/*user.getUserCmpCode()*/vsoMD.getVso_cmp_cd());
					Iterator i = vlntrLst.iterator();
					volunteers = new ArrayList();
					while(i.hasNext()){
						VsoVolunteerModDetails details = (VsoVolunteerModDetails)i.next();
						volunteers.add(details.getVlntr_id()+" --- "+details.getVlntr_lname() +", "+details.getVlntr_fname() +" "+details.getVlntr_mname());
					}
				}
				
			} catch (Exception e) {
				vsoName="";
				e.printStackTrace();
				System.out.println("CANNOT SPLT VSO ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	
		
	}

}
