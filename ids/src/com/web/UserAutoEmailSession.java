package com.web;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.opensymphony.xwork2.ActionSupport;

public class UserAutoEmailSession extends ActionSupport {

	String from = "helpdesk.pnvscagov@gmail.com";//"helpdesk.pnvsca@gmail.com"; // email of user will send an email
	String userName  = "helpdesk.pnvscagov";//"helpdesk.pnvsca"; // account user name
	String password = "@admin123";//"P@ssw0rd123"; // user email account password
	private String to; // email of user to will be send
	private String subject; // email Subject
	private String body; // email body

	
	
	
	
	static Properties properties = new Properties();
	static {
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.port", "465"); // defaul port for gmail
		properties.put("mail.smtp.socketFactory.class",	"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");
	}

	public String UserAutoEmailSession(String fromUserEmailAddress,String UserName, String UserPassword, String toEmailAddress, final String Subject, final String Body) {
		
		String rturn = SUCCESS;
		
		
		/*fromUserEmailAddress = "helpdesk.pnvsca@gmail.com";
		UserName = "helpdesk.pnvsca";
		UserPassword = "P@ssw0rd123";
		toEmailAddress = "dollete.christian@yahoo.com";*/
		/*final String userNm =  UserName;
		final String userPass = UserPassword;*/
		
		 try
         {
            Session sessionMail = Session.getDefaultInstance(properties, 
            		new javax.mail.Authenticator() {
            	    	protected PasswordAuthentication getPasswordAuthentication() {
            	    		return new PasswordAuthentication(userName, password);
            	    	}
            	    }
            );

            
            Message message = new MimeMessage(sessionMail);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));
            message.setSubject(Subject);
            message.setContent(Body, "text/html; charset=utf-8");
            //message.setText(Body);
            Transport.send(message);
         }
         catch(Exception e)
         {
        	 rturn = ERROR;
        	e.printStackTrace();
         }
		 
		 return rturn;

	}
	
	
	
	public String SendEmail(String toEmailAddress, final String Subject, final String Body) throws Exception {
		
		String rturn = SUCCESS;
		
		
		 try
         {
            Session sessionMail = Session.getDefaultInstance(properties, 
            		new javax.mail.Authenticator() {
            	    	protected PasswordAuthentication getPasswordAuthentication() {
            	    		return new PasswordAuthentication(userName, password);
            	    	}
            	    }
            );

            
            Message message = new MimeMessage(sessionMail);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));
            message.setSubject(Subject);
            message.setContent(Body, "text/html; charset=utf-8");
            //message.setText(Body);
            Transport.send(message);
         }
         catch(Exception e)
         {
        	 rturn = ERROR;
        	e.printStackTrace();
         }
		 
		 return rturn;

	}
	

}
