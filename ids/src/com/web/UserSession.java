package com.web;

public class UserSession {

	private String sessionId;
	private String userName;
	private String userFName;
	private String userMName;
	private String userLName;
	private String userDescription;
	private String userCurrentResName;
	private String userCurrentResDescription;
	private String userCurrentBranchName;
	private String userCurrentBranchDescription;

	private int userResCode;
	private int userCmpCode;
	private int userCode;
	private String userCmpName;

	private int userProfile;

	private int prjct;
	private int prjctMngr;
	private int rqstVlntr;
	private int vlntr;

	private int lpiNo; 
	private int vsoNo;

	public int getLpiNo() {
		return lpiNo;
	}

	public void setLpiNo(int lpiNo) {
		this.lpiNo = lpiNo;
	}

	public int getVsoNo() {
		return vsoNo;
	}

	public void setVsoNo(int vsoNo) {
		this.vsoNo = vsoNo;
	}

	public String getUserCmpName() {
		return userCmpName;
	}

	public void setUserCmpName(String userCmpName) {
		this.userCmpName = userCmpName;
	}

	public int getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(int userProfile) {
		this.userProfile = userProfile;
	}

	public int getPrjct() {
		return prjct;
	}

	public void setPrjct(int prjct) {
		this.prjct = prjct;
	}

	public int getPrjctMngr() {
		return prjctMngr;
	}

	public void setPrjctMngr(int prjctMngr) {
		this.prjctMngr = prjctMngr;
	}

	public int getRqstVlntr() {
		return rqstVlntr;
	}

	public void setRqstVlntr(int rqstVlntr) {
		this.rqstVlntr = rqstVlntr;
	}

	public int getVlntr() {
		return vlntr;
	}

	public void setVlntr(int vlntr) {
		this.vlntr = vlntr;
	}

	public int getUserCode() {
		return userCode;
	}

	public void setUserCode(int userCode) {
		this.userCode = userCode;
	}

	public int getUserResCode() {
		return userResCode;
	}

	public void setUserResCode(int userResCode) {
		this.userResCode = userResCode;
	}

	public int getUserCmpCode() {
		return userCmpCode;
	}

	public void setUserCmpCode(int userCmpCode) {
		this.userCmpCode = userCmpCode;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFName() {
		return userFName;
	}

	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}

	public String getUserMName() {
		return userMName;
	}

	public void setUserMName(String userMName) {
		this.userMName = userMName;
	}

	public String getUserLName() {
		return userLName;
	}

	public void setUserLName(String userLName) {
		this.userLName = userLName;
	}

	public String getUserDescription() {
		return userDescription;
	}

	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	public String getUserCurrentResName() {
		return userCurrentResName;
	}

	public void setUserCurrentResName(String userCurrentResName) {
		this.userCurrentResName = userCurrentResName;
	}

	public String getUserCurrentResDescription() {
		return userCurrentResDescription;
	}

	public void setUserCurrentResDescription(String userCurrentResDescription) {
		this.userCurrentResDescription = userCurrentResDescription;
	}

	public String getUserCurrentBranchName() {
		return userCurrentBranchName;
	}

	public void setUserCurrentBranchName(String userCurrentBranchName) {
		this.userCurrentBranchName = userCurrentBranchName;
	}

	public String getUserCurrentBranchDescription() {
		return userCurrentBranchDescription;
	}

	public void setUserCurrentBranchDescription(
			String userCurrentBranchDescription) {
		this.userCurrentBranchDescription = userCurrentBranchDescription;
	}

}
