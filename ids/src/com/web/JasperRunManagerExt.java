package com.web;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspWriter;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

public class JasperRunManagerExt {

    public static byte[] runReportToXls(
		String sourceFileName, 
		Map parameters, 
		JRDataSource jrDataSource
		) throws Exception
	{
		
		try {

			JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, jrDataSource);
	
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
	
			JRXlsExporter exporter = new JRXlsExporter();
			
			HashMap formatPatternMap = new HashMap();
			formatPatternMap.put("#,##0.00;-#,##0.00", "($#,##0.00_);[Red]($#,##0.00)");
			formatPatternMap.put("MMMMM dd, yyyy", "MMMM dd, yyyy");

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.FORMAT_PATTERNS_MAP, formatPatternMap); 
			exporter.exportReport();
				    	
			return baos.toByteArray();
		
		} catch (Exception ex) {
			
			throw ex;
			
		}

	}
	
	public static byte[] runReportToXls(
		JasperReport jasperReport, 
		Map parameters, 
		JRDataSource jrDataSource
		) throws Exception
	{
		
		try {

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);
	
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
	
			JRXlsExporter exporter = new JRXlsExporter();
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			
			exporter.exportReport();
				    	
			return baos.toByteArray();
		
		} catch (Exception ex) {
			
			throw ex;
			
		}

	}
	
	public static void runReportToHtml(JasperPrint jasperPrint,
	    JspWriter out, Map imagesMap, String imageUri) throws Exception {
		
		try {
			
			JRHtmlExporter exporter = new JRHtmlExporter();
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);				
			exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
	   	    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, imageUri);
	   	    
			exporter.exportReport();
			
		} catch (Exception ex) {
			
			throw ex;
			
		}
		
		
	}	    

}
