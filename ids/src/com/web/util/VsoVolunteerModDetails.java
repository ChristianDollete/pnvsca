package com.web.util;

import com.bean.VsoVolunteerBean;

public class VsoVolunteerModDetails extends VsoVolunteerBean {
	private String prjDateFrom;
	private String prjDateTo;
	private int usr_cd;
	
	
	public int getUsr_cd() {
		return usr_cd;
	}
	public void setUsr_cd(int usr_cd) {
		this.usr_cd = usr_cd;
	}
	public String getPrjDateFrom() {
		return prjDateFrom;
	}
	public void setPrjDateFrom(String prjDateFrom) {
		this.prjDateFrom = prjDateFrom;
	}
	public String getPrjDateTo() {
		return prjDateTo;
	}
	public void setPrjDateTo(String prjDateTo) {
		this.prjDateTo = prjDateTo;
	}
	

}
