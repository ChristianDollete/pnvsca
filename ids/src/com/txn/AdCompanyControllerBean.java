package com.txn;

import com.bean.AdCompanyBean;
import com.databaseBean.AdCompanyDB;
import com.databaseBean.DatabaseService;
import com.web.util.AdCompanyModDetails;

public class AdCompanyControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public AdCompanyControllerBean(){;}
	
	
	private AdCompanyModDetails toModDetails(AdCompanyBean cmpBean){
		AdCompanyModDetails details = new AdCompanyModDetails();
		details.setCmp_id(cmpBean.getCmp_id());
		details.setCmp_nm(cmpBean.getCmp_nm());
		details.setCmp_dscrptn(cmpBean.getCmp_dscrptn());
		details.setCmp_typ(cmpBean.getCmp_typ());
		return details;
		
	}
	
	
	public AdCompanyModDetails getCompanyByCmpName(String Cmp_Name) throws Exception {
		System.out.println("AdCompanyControllerBean getCompanyByCmpName");
		AdCompanyModDetails details = new AdCompanyModDetails();
		try {
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = cmpDB.getCompanyByCmpName(Cmp_Name);
			details = this.toModDetails(cmpBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	
	

}
