package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.AdAddCountryBean;
import com.bean.AdAddMunicipalityBean;
import com.bean.AdAddProvinceBean;
import com.bean.AdAddRegionBean;
import com.bean.AdCompanyBean;
import com.bean.LpiLpiBean;
import com.databaseBean.AdCompanyDB;
import com.databaseBean.AdCountryDB;
import com.databaseBean.AdMunicipalityDB;
import com.databaseBean.AdProvinceDB;
import com.databaseBean.AdRegionDB;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiLpiDB;
import com.databaseBean.VsoVsoDB;
import com.web.util.AdAddCountryModDetails;
import com.web.util.AdAddMunicipalityModDetails;
import com.web.util.AdAddProvinceModDetails;
import com.web.util.AdAddRegionModDetails;
import com.web.util.AdCompanyModDetails;
import com.web.util.AdUserModDetails;
import com.web.util.AdUserResponsibilityModDetails;
import com.web.util.LpiLpiModDetails;
import com.web.util.VsoVsoModDetails;

public class AdLogonControllerBean {
	DatabaseService databaseService = new DatabaseService();
	DatabaseService databaseService2 = new DatabaseService();
	public AdLogonControllerBean(){;}
	
	
	public ArrayList getAllCountry()throws Exception{
		System.out.println("AdLogonControllerBean getAllCountry");
		ArrayList list = new ArrayList();
		try {
			AdCountryDB cntryDB = new AdCountryDB(databaseService);
			ArrayList cntryList = cntryDB.getAllCountry();
			Iterator i = cntryList.iterator();
			while(i.hasNext()){
				AdAddCountryBean cntryBean = (AdAddCountryBean)i.next();
				AdAddCountryModDetails details = new AdAddCountryModDetails();
				details.setCntry_cd(cntryBean.getCntry_cd());
				details.setCntry_nm(cntryBean.getCntry_nm());
				details.setCntry_dscrptn(cntryBean.getCntry_dscrptn());
				list.add(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	public AdAddCountryModDetails getAllCountryByCntryName(String cntry_nm)throws Exception{
		System.out.println("AdLogonControllerBean getAllCountryByCntryName");
		AdAddCountryModDetails details = new AdAddCountryModDetails();
		try {
			AdCountryDB cntryDB = new AdCountryDB(databaseService);
			AdAddCountryBean cntryBean = cntryDB.getAllCountryByCntryName(cntry_nm);
			details.setCntry_cd(cntryBean.getCntry_cd());
			details.setCntry_nm(cntryBean.getCntry_nm());
			details.setCntry_dscrptn(cntryBean.getCntry_dscrptn());
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	
	
	public ArrayList getAllRegion()throws Exception{
		System.out.println("AdLogonControllerBean getAllRegion");
		ArrayList list = new ArrayList();
		try {
			AdRegionDB rgnDb = new AdRegionDB(databaseService);
			ArrayList rgnList = rgnDb.getAllRegion();
			Iterator i = rgnList.iterator();
			while(i.hasNext()){
				AdAddRegionBean rgnBean = (AdAddRegionBean)i.next();
				AdAddRegionModDetails detail = new AdAddRegionModDetails();
				detail.setRgn_id(rgnBean.getRgn_id());
				detail.setRgn_nm(rgnBean.getRgn_nm());
				detail.setRgn_dscrptn(rgnBean.getRgn_dscrptn());
				list.add(detail);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	
	
	
	public AdAddRegionModDetails getRegionByRgnName(String rgn_nm)throws Exception{
		AdAddRegionModDetails detail = new AdAddRegionModDetails();
		try {
			AdRegionDB rgnDb = new AdRegionDB(databaseService);
			AdAddRegionBean rgnBean = rgnDb.getRegionByRgnNm(rgn_nm);
			
			detail.setRgn_id(rgnBean.getRgn_id());
			detail.setRgn_nm(rgnBean.getRgn_nm());
			detail.setRgn_dscrptn(rgnBean.getRgn_dscrptn());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return detail;
	}
	
	public ArrayList getAllProvinceByRgnId(String rgn_nm)throws Exception{
		System.out.println("AdLogonControllerBean getAllProvinceByRgnId");
		ArrayList list = new ArrayList();
		try {
			AdRegionDB rgnDB = new AdRegionDB(databaseService);
			AdAddRegionBean rgnDetail = rgnDB.getRegionByRgnNm(rgn_nm);
			AdProvinceDB rgnDb = new AdProvinceDB(databaseService2);
			ArrayList prvncList = rgnDb.getAllProvinceByRegionId(rgnDetail.getRgn_id());
			Iterator i = prvncList.iterator();
			while(i.hasNext()){
				AdAddProvinceBean prvncBean = (AdAddProvinceBean)i.next();
				AdAddProvinceModDetails details = new AdAddProvinceModDetails();
				details.setPrvnc_id(prvncBean.getPrvnc_id());
				details.setPrvnc_nm(prvncBean.getPrvnc_nm());
				details.setPrvnc_dscrptn(prvncBean.getPrvnc_dscrptn());
				details.setPrvnc_rgn_id(prvncBean.getPrvnc_rgn_id());
				
				list.add(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	public AdAddProvinceModDetails getProvinceByPrvcName(String prvnc_nm) throws Exception{
		System.out.println("AdLogonControllerBean getProvinceByPrvcName");
		AdAddProvinceModDetails details = new AdAddProvinceModDetails();
		try {
			AdProvinceDB prvncDb = new AdProvinceDB(databaseService);
			AdAddProvinceBean prvncBean = prvncDb.getProvinceByName(prvnc_nm);
			details.setPrvnc_id(prvncBean.getPrvnc_id());
			details.setPrvnc_nm(prvncBean.getPrvnc_nm());
			details.setPrvnc_dscrptn(prvncBean.getPrvnc_dscrptn());
			details.setPrvnc_rgn_id(prvncBean.getPrvnc_rgn_id());
				
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	public ArrayList getAllMunicipalityByPrvncId(String prnvc_nm)throws Exception{
		System.out.println("AdLogonControllerBean getAllMunicipalityByPrvncId");
		ArrayList list = new ArrayList();
		
		try {
			AdProvinceDB prvncDB = new AdProvinceDB(databaseService);
			AdAddProvinceBean prvncBean =  prvncDB.getProvinceByName(prnvc_nm);
			AdMunicipalityDB munDB = new AdMunicipalityDB(databaseService2);
			ArrayList munList =  munDB.getAllMunicipalityByProvinceId(prvncBean.getPrvnc_id());
			Iterator i = munList.iterator();
			while(i.hasNext()){
				AdAddMunicipalityBean munBean = (AdAddMunicipalityBean)i.next();
				AdAddMunicipalityModDetails details = new AdAddMunicipalityModDetails();
				details.setMun_id(munBean.getMun_id());
				details.setMun_nm(munBean.getMun_nm());
				details.setMun_dscrptn(munBean.getMun_dscrptn());
				details.setMun_prvnc_id(munBean.getMun_prvnc_id());
				list.add(details);
			}
			
			
		} catch (Exception e) {
		
			e.printStackTrace();
			throw new Exception();
		}
		
		
		return list;
	}
	
	public AdAddMunicipalityModDetails getMunicipalityByMunName(String mun_nm)throws Exception{
		System.out.println("AdLogonControllerBean getMunicipalityByMunName");
		AdAddMunicipalityModDetails details = new AdAddMunicipalityModDetails();
		
		try {
			AdMunicipalityDB munDB = new AdMunicipalityDB(databaseService);
			AdAddMunicipalityBean munBean =  munDB.getAllMunicipalityByMunName(mun_nm);
			details.setMun_id(munBean.getMun_id());
			details.setMun_nm(munBean.getMun_nm());
			details.setMun_dscrptn(munBean.getMun_dscrptn());
			details.setMun_prvnc_id(munBean.getMun_prvnc_id());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		
		return details;
	}
	
	
	
	public int saveLpiDetails(LpiLpiModDetails details) throws Exception{
		System.out.println("AdLogonControllerBean saveLpiDetails");
		int lpi_cd = 0;
		DatabaseService databaseService3 = new DatabaseService();
		try {
			
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = new AdCompanyBean();
			cmpBean.setCmp_nm(details.getLpi_nm());
			cmpBean.setCmp_dscrptn(details.getLpi_nm());
			cmpBean.setCmp_typ(2);
			
			boolean cmpIns = cmpDB.CREATE(cmpBean);
			if(cmpIns==true){
				LpiLpiDB lpiDB = new LpiLpiDB(databaseService2);
				cmpBean =  cmpDB.getCompanyByCmpName(details.getLpi_nm());
				details.setLpi_cmp_id(cmpBean.getCmp_id());
				
				
				String sqnc = details.getLpi_id();
				
				if(sqnc==null && sqnc.toString().trim().equals("")){
					AdSequence lpiSqnc = new AdSequence(databaseService3);
					sqnc = lpiSqnc.getLpiId();
				}
				details.setLpi_id(sqnc);//LPI unique ID;
				int created = lpiDB.CREATE(details);
				if(created>0){
					lpi_cd = created;
				}else{
					throw new Exception();
				}
				
				AdUserControllerBean usrCbean = new AdUserControllerBean();
				AdUserModDetails usr = new AdUserModDetails();
				usr.setUsr_usr_nm(details.getLpi_usr_nm());
				usr.setUsr_usr_psswrd(details.getLpi_usr_psswrd());
				usr.setUsr_usr_prmry_email(details.getLpi_prmry_email());
				usr.setUsr_usr_scndry_email(details.getLpi_scndry_email());
				usr.setUsr_usr_dscrptn(details.getLpi_usr_nm());
				usr.setAd_usr_clsfctn(2);//lpi profile
				usr.setAd_cmpny(details.getLpi_cmp_id());
				int usr_id = usrCbean.createUser(usr);
				if(usr_id>0){
				}else{
					throw new Exception();
				}
				
				AdUserResponsibilityControllerBean usrResCbean = new AdUserResponsibilityControllerBean();
				AdUserResponsibilityModDetails resMd = new AdUserResponsibilityModDetails();
				resMd.setRes_usr_id(usr_id);
				resMd.setRes_lpi_prjct(1);
				resMd.setRes_lpi_prjct_mngr(1);
				resMd.setRes_lpi_rqst_vlntr(1);
				resMd.setRes_vso_vlntr(0);
				int res_id = usrResCbean.createRes(resMd);
				if(res_id>0){
				}else{
					throw new Exception();
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		return lpi_cd;
	}
	
	

	
	public int updateLpiDetails(LpiLpiModDetails details) throws Exception{
		System.out.println("AdLogonControllerBean updateLpiDetails");
		int lpi_cd = 0;
		
		try {
			
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = new AdCompanyBean();
			cmpBean.setCmp_nm(details.getLpi_nm());
			cmpBean.setCmp_dscrptn(details.getLpi_nm());
			cmpBean.setCmp_typ(2);
			
			boolean cmpIns = true;//cmpDB.CREATE(cmpBean);
			if(cmpIns==true){
				LpiLpiDB lpiDB = new LpiLpiDB(databaseService2);
				cmpBean =  cmpDB.getCompanyByCmpName(details.getLpi_nm());
				details.setLpi_cmp_id(cmpBean.getCmp_id());
				
				
				String sqnc = details.getLpi_id();
				
				/*if(sqnc==null && sqnc.toString().trim().equals("")){
					AdSequence lpiSqnc = new AdSequence(databaseService);
					sqnc = lpiSqnc.getLpiId();
				}*/
				details.setLpi_id(sqnc);//LPI unique ID;
				int created = lpiDB.UPDATE(details);
				if(created>0){
					lpi_cd = created;
				}else{
					throw new Exception();
				}
				
				AdUserControllerBean usrCbean = new AdUserControllerBean();
				AdUserModDetails usr = new AdUserModDetails();
				usr.setUsr_usr_nm(details.getLpi_usr_nm());
				usr.setUsr_usr_psswrd(details.getLpi_usr_psswrd());
				usr.setUsr_usr_prmry_email(details.getLpi_prmry_email());
				usr.setUsr_usr_scndry_email(details.getLpi_scndry_email());
				usr.setUsr_usr_dscrptn(details.getLpi_usr_nm());
				usr.setAd_cmpny(details.getLpi_cmp_id());
				usr.setUsr_id(details.getUsr_cd());
				int usr_id = usrCbean.update(usr);
				if(usr_id>0){
				}else{
					throw new Exception();
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		return lpi_cd;
	}
	
	
	
	
	public LpiLpiModDetails getLpiByLpiCode(int lpi_cmp_id)throws Exception{
		System.out.println("AdLogonControllerBean getLpiByLpiCode");
		LpiLpiModDetails details = new LpiLpiModDetails();
		try{
			LpiLpiDB lpiDB = new LpiLpiDB(databaseService);
			LpiLpiBean lpiBean = lpiDB.getLpiByLpiCmpCd(lpi_cmp_id);
			
			
			details.setLpi_cd(lpiBean.getLpi_cd());
			details.setLpi_id(lpiBean.getLpi_id());
			details.setLpi_nm(lpiBean.getLpi_nm());
			details.setLpi_typ(lpiBean.getLpi_typ());
			details.setLpi_mndt(lpiBean.getLpi_mndt());
			details.setLpi_addrss(lpiBean.getLpi_addrss());
			details.setLpi_rgn(lpiBean.getLpi_rgn());
			details.setLpi_prvnc(lpiBean.getLpi_prvnc());
			details.setLpi_mncplty(lpiBean.getLpi_mncplty());
			details.setLpi_tlphn_nmbr(lpiBean.getLpi_tlphn_nmbr());
			details.setLpi_email(lpiBean.getLpi_email());
			details.setLpi_fx_nmbr(lpiBean.getLpi_fx_nmbr());
			details.setLpi_wbst(lpiBean.getLpi_wbst());
			details.setLpi_usr_nm(lpiBean.getLpi_usr_nm());
			details.setLpi_usr_psswrd(lpiBean.getLpi_usr_psswrd());
			details.setLpi_prmry_email(lpiBean.getLpi_prmry_email());
			details.setLpi_scndry_email(lpiBean.getLpi_scndry_email());
			details.setLpi_crdntr_nm(lpiBean.getLpi_crdntr_nm());
			details.setLpi_crdntr_pstn(lpiBean.getLpi_crdntr_pstn());
			details.setLpi_crdntr_tlphn_nmbr(lpiBean.getLpi_crdntr_tlphn_nmbr());
			details.setLpi_crdntr_dprtmnt(lpiBean.getLpi_crdntr_dprtmnt());
			details.setLpi_crdntr_mble_nmbr(lpiBean.getLpi_crdntr_mble_nmbr());
			details.setLpi_cmp_id(lpiBean.getLpi_cmp_id());
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	public ArrayList getAllLpi() throws Exception{
		System.out.println("LpiProjectEntryControllerBean getAllLpi");
		ArrayList list = new ArrayList();
		try{
			
			LpiLpiDB lpiDB = new LpiLpiDB(databaseService);
			ArrayList lst = lpiDB.getAllLpi();
			Iterator i = lst.iterator();
			while(i.hasNext()){
				LpiLpiBean lpiBean = (LpiLpiBean)i.next();
				LpiLpiModDetails details = new LpiLpiModDetails();
				details.setLpi_cd(lpiBean.getLpi_cd());
				details.setLpi_id(lpiBean.getLpi_id());
				details.setLpi_nm(lpiBean.getLpi_nm());
				details.setLpi_typ(lpiBean.getLpi_typ());
				details.setLpi_mndt(lpiBean.getLpi_mndt());
				details.setLpi_addrss(lpiBean.getLpi_addrss());
				details.setLpi_rgn(lpiBean.getLpi_rgn());
				details.setLpi_prvnc(lpiBean.getLpi_prvnc());
				details.setLpi_mncplty(lpiBean.getLpi_mncplty());
				details.setLpi_tlphn_nmbr(lpiBean.getLpi_tlphn_nmbr());
				details.setLpi_email(lpiBean.getLpi_email());
				details.setLpi_fx_nmbr(lpiBean.getLpi_fx_nmbr());
				details.setLpi_wbst(lpiBean.getLpi_wbst());
				details.setLpi_usr_nm(lpiBean.getLpi_usr_nm());
				details.setLpi_usr_psswrd(lpiBean.getLpi_usr_psswrd());
				details.setLpi_prmry_email(lpiBean.getLpi_prmry_email());
				details.setLpi_scndry_email(lpiBean.getLpi_scndry_email());
				details.setLpi_crdntr_nm(lpiBean.getLpi_crdntr_nm());
				details.setLpi_crdntr_pstn(lpiBean.getLpi_crdntr_pstn());
				details.setLpi_crdntr_tlphn_nmbr(lpiBean.getLpi_crdntr_tlphn_nmbr());
				details.setLpi_crdntr_dprtmnt(lpiBean.getLpi_crdntr_dprtmnt());
				details.setLpi_crdntr_mble_nmbr(lpiBean.getLpi_crdntr_mble_nmbr());
				details.setLpi_cmp_id(lpiBean.getLpi_cmp_id());
				list.add(details);	
				
			}
			
		
		
		}catch(Exception e){
			throw new Exception();
		}
		return list;
	}
	
	public int getAllLpiSize() throws Exception{
		System.out.println("LpiProjectEntryControllerBean getAllLpiSize");
		int lpiSize = 0;
		try{
			
			LpiLpiDB lpiDB = new LpiLpiDB(databaseService);
			ArrayList lst = lpiDB.getAllLpi();
			lpiSize = lst.size();
		
		}catch(Exception e){
			throw new Exception();
		}
		return lpiSize;
	}
	
	public LpiLpiModDetails getLpiByLpiUACS(String lpi_id)throws Exception{
		System.out.println("AdLogonControllerBean getLpiByLpiUACS");
		LpiLpiModDetails details = new LpiLpiModDetails();
		try{
			LpiLpiDB lpiDB = new LpiLpiDB(databaseService);
			LpiLpiBean lpiBean = lpiDB.getLpiByLpiId(lpi_id);
			details.setLpi_cd(lpiBean.getLpi_cd());
			details.setLpi_id(lpiBean.getLpi_id());
			details.setLpi_nm(lpiBean.getLpi_nm());
			details.setLpi_typ(lpiBean.getLpi_typ());
			details.setLpi_mndt(lpiBean.getLpi_mndt());
			details.setLpi_addrss(lpiBean.getLpi_addrss());
			details.setLpi_rgn(lpiBean.getLpi_rgn());
			details.setLpi_prvnc(lpiBean.getLpi_prvnc());
			details.setLpi_mncplty(lpiBean.getLpi_mncplty());
			details.setLpi_tlphn_nmbr(lpiBean.getLpi_tlphn_nmbr());
			details.setLpi_email(lpiBean.getLpi_email());
			details.setLpi_fx_nmbr(lpiBean.getLpi_fx_nmbr());
			details.setLpi_wbst(lpiBean.getLpi_wbst());
			details.setLpi_usr_nm(lpiBean.getLpi_usr_nm());
			details.setLpi_usr_psswrd(lpiBean.getLpi_usr_psswrd());
			details.setLpi_prmry_email(lpiBean.getLpi_prmry_email());
			details.setLpi_scndry_email(lpiBean.getLpi_scndry_email());
			details.setLpi_crdntr_nm(lpiBean.getLpi_crdntr_nm());
			details.setLpi_crdntr_pstn(lpiBean.getLpi_crdntr_pstn());
			details.setLpi_crdntr_tlphn_nmbr(lpiBean.getLpi_crdntr_tlphn_nmbr());
			details.setLpi_crdntr_dprtmnt(lpiBean.getLpi_crdntr_dprtmnt());
			details.setLpi_crdntr_mble_nmbr(lpiBean.getLpi_crdntr_mble_nmbr());
			details.setLpi_cmp_id(lpiBean.getLpi_cmp_id());
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	
	public int saveVsoDetails(VsoVsoModDetails details) throws Exception{
		System.out.println("AdLogonControllerBean saveVsoDetails");
		int vso_cd = 0;
		try {
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = new AdCompanyBean();
			cmpBean.setCmp_nm(details.getVso_nm());
			cmpBean.setCmp_dscrptn(details.getVso_nm());
			cmpBean.setCmp_typ(3);
			boolean cmpIns = cmpDB.CREATE(cmpBean);
			if(cmpIns==true){
				VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
				cmpBean = new AdCompanyBean();
				cmpBean =  cmpDB.getCompanyByCmpName(details.getVso_nm());
				details.setVso_cmp_cd(cmpBean.getCmp_id());
				
				AdSequence lpiSqnc = new AdSequence(databaseService);
				details.setVso_id(lpiSqnc.getVsoId());//LPI unique ID;
				
				int created = vsoDB.CREATE(details);
				if(created>0){
					vso_cd = created;
				}else{
					throw new Exception();
				}
				
				AdUserControllerBean usrCbean = new AdUserControllerBean();
				AdUserModDetails usr = new AdUserModDetails();
				usr.setUsr_usr_nm(details.getVso_usr_nm());
				usr.setUsr_usr_psswrd(details.getVso_psswrd());
				usr.setUsr_usr_prmry_email(details.getVso_prmry_email());
				usr.setUsr_usr_scndry_email(details.getVso_scndry_email());
				usr.setUsr_usr_dscrptn(details.getVso_usr_nm());
				usr.setAd_usr_clsfctn(3);
				usr.setAd_cmpny(cmpBean.getCmp_id());
				int usr_id = usrCbean.createUser(usr);
				if(usr_id>0){
				}else{
					throw new Exception();
				}
				
				
				AdUserResponsibilityControllerBean usrResCbean = new AdUserResponsibilityControllerBean();
				AdUserResponsibilityModDetails resMd = new AdUserResponsibilityModDetails();
				resMd.setRes_usr_id(usr_id);
				resMd.setRes_lpi_prjct(1);
				resMd.setRes_lpi_prjct_mngr(1);
				resMd.setRes_lpi_rqst_vlntr(1);
				resMd.setRes_vso_vlntr(1);
				int res_id = usrResCbean.createRes(resMd);
				if(res_id>0){
				}else{
					throw new Exception();
				}
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return vso_cd;
	}


	
	public int updateVsoDetails(VsoVsoModDetails details) throws Exception{
		System.out.println("AdLogonControllerBean updateVsoDetails");
		int vso_cd = 0;
		try {
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = new AdCompanyBean();
			cmpBean.setCmp_nm(details.getVso_nm());
			cmpBean.setCmp_dscrptn(details.getVso_nm());
			cmpBean.setCmp_typ(3);
			boolean cmpIns = true;//cmpDB.CREATE(cmpBean);
			if(cmpIns==true){
				databaseService = new DatabaseService();
				VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
				cmpBean = new AdCompanyBean();
				cmpBean =  cmpDB.getCompanyByCmpName(details.getVso_nm());
				details.setVso_cmp_cd(details.getVso_cmp_cd());
				details.setVso_id(details.getVso_id());//LPI unique ID;
				
				int created = vsoDB.UPDATE(details);
				if(created>0){
					vso_cd = created;
				}else{
					throw new Exception();
				}
				
				AdUserControllerBean usrCbean = new AdUserControllerBean();
				AdUserModDetails usr = new AdUserModDetails();
				usr.setUsr_usr_nm(details.getVso_usr_nm());
				usr.setUsr_usr_psswrd(details.getVso_psswrd());
				usr.setUsr_usr_prmry_email(details.getVso_prmry_email());
				usr.setUsr_usr_scndry_email(details.getVso_scndry_email());
				usr.setUsr_usr_dscrptn(details.getVso_usr_nm());
				usr.setAd_usr_clsfctn(3);
				usr.setAd_cmpny(cmpBean.getCmp_id());
				usr.setUsr_id(details.getUsr_cd());
				int usr_id = usrCbean.update(usr);
				if(usr_id>0){
				}else{
					throw new Exception();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return vso_cd;
	}

	
	
	
	
	
	
	public VsoVsoModDetails getVsoByVsoCode(int vso_cmp_id)throws Exception{
		System.out.println("AdLogonControllerBean getVsoByVsoCode");
		VsoVsoModDetails details = new VsoVsoModDetails();
		try{
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			
			details = vsoDB.getVsoByVsoCmpCode(vso_cmp_id);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	public int getAllVsoSize()throws Exception{
		System.out.println("AdLogonControllerBean getAllVsoSize");
		int vsoNo = 0;
		try{
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			ArrayList vsoList = vsoDB.getAllVso();
			vsoNo = vsoList.size();
			//VsoVsoModDetails details = new VsoVsoModDetails();
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return vsoNo;
	}
	
	
	
	public AdUserModDetails excecuteLogin(String Cmp_Name, String UsrName, String Psswrd)throws Exception {
		System.out.println("AdLogonControllerBean excecuteLogin");
		AdUserModDetails details =  null;
		
		AdCompanyControllerBean cmpCbean = new AdCompanyControllerBean();
		AdCompanyModDetails cmpD = null;
		try {
			cmpD = cmpCbean.getCompanyByCmpName(Cmp_Name);
			System.out.println("cmpD.getCmp_id() >> " +cmpD.getCmp_id());
			AdUserControllerBean usrCbean = new AdUserControllerBean();
			details = usrCbean.getUserByUserNameAndPasswordAndCompanyCode(cmpD.getCmp_id(), UsrName, Psswrd);
			System.out.println("details.getUsr_id() >> " +details.getUsr_id());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		return details;
		
	}
	
	
	public int getAllLpiByYear(String Date){
		System.out.println("AdLogonControllerBean getAllLpiByYear");
		int rtrn = 0;
		
		return rtrn;
	}
	
	
	
	
	
	
	
	

}
