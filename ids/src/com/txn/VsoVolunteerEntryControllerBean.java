package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.VsoVolunteerBean;
import com.databaseBean.AdRegionDB;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.VsoVolunteerDB;
import com.web.util.AdUserModDetails;
import com.web.util.AdUserResponsibilityModDetails;
import com.web.util.VsoVolunteerModDetails;

public class VsoVolunteerEntryControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public VsoVolunteerEntryControllerBean(){;}
	
	public int Create(VsoVolunteerModDetails details) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean Create");
		int vlntr_cd = 0;
		try{
			VsoVolunteerBean vdetails = new VsoVolunteerBean();
			
			
			String VlntrId  = details.getVlntr_id();
			if(details.getVlntr_id() == null || details.getVlntr_id().trim().equals("")){
				AdSequence sqnc = new AdSequence(databaseService);
				VlntrId = sqnc.getVsoVlntrId();
			}
			

			AdUserControllerBean userC = new AdUserControllerBean();
			AdUserModDetails udetails = new AdUserModDetails();
			udetails.setUsr_usr_nm(details.getVlntr_usr_nm());
			udetails.setUsr_usr_dscrptn(details.getVlntr_lname() + ", "+ details.getVlntr_fname() + " "+ details.getVlntr_mname());
			udetails.setUsr_usr_psswrd(details.getVlntr_usr_psswrd());
			udetails.setUsr_usr_prmry_email(details.getVlntr_usr_email());
			udetails.setUsr_usr_scndry_email(details.getVlntr_usr_scnd_email());
			udetails.setAd_cmpny(details.getVlntr_cmp_cd());
			udetails.setAd_usr_clsfctn(31);// VOLUNTEER PROFILE CODE
			int usr_cd = userC.createUser(udetails);
			
			vdetails.setVlntr_id(VlntrId);
			vdetails.setVlntr_cmp_cd(details.getVlntr_cmp_cd());
			vdetails.setVlntr_fname(details.getVlntr_fname());
			vdetails.setVlntr_mname(details.getVlntr_mname());
			vdetails.setVlntr_lname(details.getVlntr_lname());
			vdetails.setVlntr_ntnly(details.getVlntr_ntnly());
			vdetails.setVlntr_cvl_stts(details.getVlntr_cvl_stts());
			System.out.println("date of birth >> "+ details.getVlntr_brth_dt());
			vdetails.setVlntr_sex(details.getVlntr_sex());
			vdetails.setVlntr_brth_dt(details.getVlntr_brth_dt());
			vdetails.setVlntr_vs_exprtn(details.getVlntr_vs_exprtn());
			vdetails.setVlntr_usr_nm(details.getVlntr_usr_nm());
			vdetails.setVlntr_usr_psswrd(details.getVlntr_usr_psswrd());
			vdetails.setVlntr_usr_email(details.getVlntr_usr_email());
			vdetails.setVlntr_usr_scnd_email(details.getVlntr_usr_scnd_email());
			vdetails.setVlntr_usr_cd(usr_cd);
			
			VsoVolunteerDB vlntrDB = new VsoVolunteerDB(databaseService);
			vlntr_cd = vlntrDB.Create(vdetails);
			
			
			AdUserResponsibilityControllerBean uresC = new AdUserResponsibilityControllerBean();
			AdUserResponsibilityModDetails resdetails = new AdUserResponsibilityModDetails();
			resdetails.setRes_usr_id(usr_cd);
			resdetails.setRes_vso_vlntr(1);
			uresC.createRes(resdetails);
			
			
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return vlntr_cd;
	}
	
	
	public int Update(VsoVolunteerModDetails details) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean Update");
		int vlntr_cd = 0;
		try{
			VsoVolunteerEntryControllerBean vsoVlntrCB = new VsoVolunteerEntryControllerBean();
			
			VsoVolunteerBean vdetails = new VsoVolunteerBean();
			vdetails.setVlntr_id(details.getVlntr_id());
			vdetails.setVlntr_cmp_cd(details.getVlntr_cmp_cd());
			vdetails.setVlntr_fname(details.getVlntr_fname());
			vdetails.setVlntr_mname(details.getVlntr_mname());
			vdetails.setVlntr_lname(details.getVlntr_lname());
			vdetails.setVlntr_ntnly(details.getVlntr_ntnly());
			vdetails.setVlntr_cvl_stts(details.getVlntr_cvl_stts());
			
			vdetails.setVlntr_sex(details.getVlntr_sex());
			vdetails.setVlntr_brth_dt(details.getVlntr_brth_dt());
			vdetails.setVlntr_vs_exprtn(details.getVlntr_vs_exprtn());
			vdetails.setVlntr_usr_nm(details.getVlntr_usr_nm());
			vdetails.setVlntr_usr_psswrd(details.getVlntr_usr_psswrd());
			vdetails.setVlntr_usr_email(details.getVlntr_usr_email());
			vdetails.setVlntr_usr_scnd_email(details.getVlntr_usr_scnd_email());
			
			VsoVolunteerModDetails vlntr = vsoVlntrCB.getByVlntrCd(details.getVlntr_cd());
			System.out.println("details.getVlntr_cd() >> "+ details.getVlntr_cd());
			vdetails.setVlntr_cd(details.getVlntr_cd());
			vdetails.setVlntr_usr_cd(vlntr.getVlntr_usr_cd());
			System.out.println("details.getVlntr_usr_cd() >> "+ vlntr.getVlntr_usr_cd());
			VsoVolunteerDB vlntrDB = new VsoVolunteerDB(databaseService);
			
			vlntr_cd = vlntrDB.Update(vdetails);
			
			
			
			AdUserControllerBean userC = new AdUserControllerBean();
			AdUserModDetails udetails = new AdUserModDetails();
			udetails.setUsr_usr_nm(details.getVlntr_usr_nm());
			udetails.setUsr_usr_dscrptn(details.getVlntr_lname() + ", "+ details.getVlntr_fname() + " "+ details.getVlntr_mname());
			udetails.setUsr_usr_psswrd(details.getVlntr_usr_psswrd());
			udetails.setUsr_usr_prmry_email(details.getVlntr_usr_email());
			udetails.setUsr_usr_scndry_email(details.getVlntr_usr_scnd_email());
			udetails.setAd_cmpny(details.getVlntr_cmp_cd());
			System.out.println("VLNTR USR CD >> "+ vlntr.getVlntr_usr_cd());
			udetails.setUsr_id(vlntr.getVlntr_usr_cd());
			int usr_cd = userC.update(udetails);
			
			
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return vlntr_cd;
	}
	
	
	
	public ArrayList getVlntrByCmpCd(int vlntr_cmp_cd) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean getVlntrByCmpCd");
		ArrayList list = new ArrayList();
		try{
			VsoVolunteerDB vlDB = new VsoVolunteerDB(databaseService);
			
			ArrayList vtrL = vlDB.getVlntrByCmpCd(vlntr_cmp_cd);
			Iterator i = vtrL.iterator();
			while(i.hasNext()){
				VsoVolunteerBean vBean =  (VsoVolunteerBean)i.next();
				VsoVolunteerModDetails details = new VsoVolunteerModDetails();
				details = this.beanToModDetails(vBean);
				list.add(details);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
		
	}
	
	
	
	
	public VsoVolunteerModDetails getByVlntrCd(int vlntr_cd) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean getByVlntrCd");
		VsoVolunteerModDetails details = new VsoVolunteerModDetails();
		try{
			VsoVolunteerDB vlDB = new VsoVolunteerDB(databaseService);
			VsoVolunteerBean vBean =  vlDB.getByVlntrCd(vlntr_cd);
			details = this.beanToModDetails(vBean);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
		
	}
	
	public VsoVolunteerModDetails getByVlntrId(String vlntr_Id) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean getByVlntrId");
		VsoVolunteerModDetails details = new VsoVolunteerModDetails();
		try{
			VsoVolunteerDB vlDB = new VsoVolunteerDB(databaseService);
			VsoVolunteerBean vBean =  vlDB.getByVlntrId(vlntr_Id);
			details = this.beanToModDetails(vBean);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
		
	}
	
	public VsoVolunteerModDetails getByVlntrUsrCode(int vlntr_usr_cd) throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean getByVlntrUsrCode");
		VsoVolunteerModDetails details = new VsoVolunteerModDetails();
		try{
			VsoVolunteerDB vlDB = new VsoVolunteerDB(databaseService);
			VsoVolunteerBean vBean =  vlDB.getVlntrByUsrCode(vlntr_usr_cd);
			details = this.beanToModDetails(vBean);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
		
	}
	
	
	private VsoVolunteerModDetails beanToModDetails(VsoVolunteerBean vBean)throws Exception{
		System.out.println("VsoVolunteerEntryControllerBean beanToModDetails");
		VsoVolunteerModDetails details = new VsoVolunteerModDetails();
		try{
			details.setVlntr_cd(vBean.getVlntr_cd());
			details.setVlntr_id(vBean.getVlntr_id());
			details.setVlntr_fname(vBean.getVlntr_fname());
			details.setVlntr_mname(vBean.getVlntr_mname());
			details.setVlntr_lname(vBean.getVlntr_lname());
			details.setVlntr_sex(vBean.getVlntr_sex());
			details.setVlntr_ntnly(vBean.getVlntr_ntnly());
			details.setVlntr_cvl_stts(vBean.getVlntr_cvl_stts());
			details.setVlntr_brth_dt(vBean.getVlntr_brth_dt());
			details.setVlntr_vs_exprtn(vBean.getVlntr_vs_exprtn());
			details.setVlntr_usr_nm(vBean.getVlntr_usr_nm());
			details.setVlntr_usr_email(vBean.getVlntr_usr_email());
			details.setVlntr_usr_scnd_email(vBean.getVlntr_usr_scnd_email());
			details.setVlntr_prjct_cd(vBean.getVlntr_prjct_cd());
			details.setVlntr_cmp_cd(vBean.getVlntr_cmp_cd());
			details.setVlntr_usr_cd(vBean.getVlntr_usr_cd());
			
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	

}
