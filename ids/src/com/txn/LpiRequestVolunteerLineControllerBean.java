package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.LpiRequestVolunteerBean;
import com.bean.LpiRequestVolunteerLineBean;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiRequestVolunteerDB;
import com.databaseBean.LpiRequestVolunteerLineDB;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;

public class LpiRequestVolunteerLineControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiRequestVolunteerLineControllerBean(){;}
	
	public LpiRequestVolunteerLineModDetails getRvlByVlntrCode(int rvl_rv_cd) throws Exception{
		System.out.println("LpiRequestVolunteerLineControllerBean getRvlByVlntrCode");
		LpiRequestVolunteerLineModDetails details = new LpiRequestVolunteerLineModDetails();
		try{
			LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
			LpiRequestVolunteerLineBean rvlBean = rvlDB.getRvlByVLNTR_CD(rvl_rv_cd);
			details.setRvl_rv_cd(rvlBean.getRvl_rv_cd());
			System.out.println("requst line >> "+ rvlBean.getRvl_cd());
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
}
