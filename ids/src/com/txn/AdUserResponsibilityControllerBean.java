package com.txn;

import com.bean.AdUserResponsibilityBean;
import com.databaseBean.AdUserDB;
import com.databaseBean.AdUserResponsiblityDB;
import com.databaseBean.DatabaseService;
import com.web.util.AdUserResponsibilityModDetails;

public class AdUserResponsibilityControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public AdUserResponsibilityControllerBean(){;}
	
	
	public int createRes(AdUserResponsibilityModDetails res) throws Exception{
		int res_cd = 0;
		
		AdUserResponsiblityDB resDB = new AdUserResponsiblityDB(databaseService);
		AdUserResponsibilityBean details = new AdUserResponsibilityBean();
		details.setRes_id(res.getRes_id());
		details.setRes_lpi_prjct(res.getRes_lpi_prjct());
		details.setRes_lpi_prjct_mngr(res.getRes_lpi_prjct_mngr());
		details.setRes_lpi_rqst_vlntr(res.getRes_lpi_rqst_vlntr());
		details.setRes_vso_vlntr(res.getRes_vso_vlntr());
		details.setRes_usr_id(res.getRes_usr_id());
		try {
			res_cd=resDB.CREATE(details);
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return res_cd;
	}
	
	
	public int getUserByUserNameAndPassword(String UsrName, String UsrPassword){
		int usr_cd = 0;
			AdUserDB usrDB = new AdUserDB(databaseService);
			
		return usr_cd;
	}
	
	
	public AdUserResponsibilityModDetails getUserRes(int usr_cd) throws Exception{
		System.out.println("AdUserResponsibilityControllerBean getUserRes");
		AdUserResponsibilityModDetails details = new AdUserResponsibilityModDetails();
		int res_cd = 0;
		try {
			AdUserResponsiblityDB resDB = new AdUserResponsiblityDB(databaseService);
			AdUserResponsibilityBean res = resDB.getResByUsrCd(usr_cd);
			
			details.setRes_id(res.getRes_id());
			details.setRes_lpi_prjct(res.getRes_lpi_prjct());
			details.setRes_lpi_prjct_mngr(res.getRes_lpi_prjct_mngr());
			details.setRes_lpi_rqst_vlntr(res.getRes_lpi_rqst_vlntr());
			details.setRes_vso_vlntr(res.getRes_vso_vlntr());
			details.setRes_usr_id(res.getRes_usr_id());
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	
}
