package com.txn;

import java.sql.SQLException;

import com.bean.AdUserBean;
import com.databaseBean.AdUserDB;
import com.databaseBean.DatabaseService;
import com.web.util.AdUserModDetails;

public class AdUserControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public AdUserControllerBean(){;}
	
	
	
	public int createUser(AdUserModDetails usr) throws Exception{
		System.out.println("AdUserControllerBean createUser");
		int usr_id = 0;
		
		AdUserDB usrDB = new  AdUserDB(databaseService);
		try {
			AdUserBean usrBean = new AdUserBean();
			usrBean.setUsr_usr_nm(usr.getUsr_usr_nm());
			usrBean.setUsr_usr_psswrd(usr.getUsr_usr_psswrd());
			usrBean.setUsr_usr_dscrptn(usr.getUsr_usr_dscrptn());
			usrBean.setUsr_usr_prmry_email(usr.getUsr_usr_prmry_email());
			usrBean.setUsr_usr_scndry_email(usr.getUsr_usr_scndry_email());
			usrBean.setAd_usr_clsfctn(usr.getAd_usr_clsfctn());
			usrBean.setAd_cmpny(usr.getAd_cmpny());
			usr_id = usrDB.CREATE(usrBean);
			System.out.println("USER ID >> "+ usr_id);
			if(usr_id == 0){
				throw new Exception();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return usr_id;
	}
	
	
	public int update(AdUserModDetails usr) throws Exception{
		System.out.println("AdUserControllerBean update");
		int usr_id = 0;
		
		AdUserDB usrDB = new  AdUserDB(databaseService);
		try {
			AdUserBean usrBean = new AdUserBean();
			usrBean.setUsr_usr_nm(usr.getUsr_usr_nm());
			usrBean.setUsr_usr_psswrd(usr.getUsr_usr_psswrd());
			usrBean.setUsr_usr_dscrptn(usr.getUsr_usr_dscrptn());
			usrBean.setUsr_usr_prmry_email(usr.getUsr_usr_prmry_email());
			usrBean.setUsr_usr_scndry_email(usr.getUsr_usr_scndry_email());
			usrBean.setAd_cmpny(usr.getAd_cmpny());
			usrBean.setUsr_id(usr.getUsr_id());
			usrBean.setUsr_email(usr.getUsr_email());
			usrBean.setUsr_email_psswrd(usr.getUsr_email_psswrd());
			usr_id = usrDB.UPDATE(usrBean);
			System.out.println("USER ID >> "+ usr_id);
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return usr_id;
	}


	public AdUserModDetails getUserByUserNameAndPasswordAndCompanyCode(int Cmp_Code, String Usr_Name, String Usr_Passwrd) throws Exception{
		AdUserModDetails details = new AdUserModDetails();
		
		try {
			AdUserDB usrDb = new AdUserDB(databaseService);
			AdUserBean usrBean = usrDb.getUserByUserNameAndPassword(Cmp_Code, Usr_Name, Usr_Passwrd);
			details = toModDetails(usrBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	public AdUserModDetails getAdmin(int usr_cd) throws Exception{
		AdUserModDetails details = new AdUserModDetails();
		
		try {
			AdUserDB usrDb = new AdUserDB(databaseService);
			AdUserBean usrBean = usrDb.getAdmin(usr_cd);
			details = toModDetails(usrBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	
	private AdUserModDetails toModDetails(AdUserBean usrBean){
		AdUserModDetails details = new AdUserModDetails();
		details.setAd_cmpny(usrBean.getAd_cmpny());
		details.setUsr_id(usrBean.getUsr_id());
		details.setUsr_usr_nm(usrBean.getUsr_usr_nm());
		details.setUsr_usr_psswrd(usrBean.getUsr_usr_psswrd());
		details.setUsr_usr_dscrptn(usrBean.getUsr_usr_dscrptn());
		details.setUsr_usr_prmry_email(usrBean.getUsr_usr_prmry_email());
		details.setUsr_usr_scndry_email(usrBean.getUsr_usr_scndry_email());
		details.setAd_usr_clsfctn(usrBean.getAd_usr_clsfctn());
		details.setUsr_email(usrBean.getUsr_email());
		details.setUsr_email_psswrd(usrBean.getUsr_email_psswrd());
		
		return details;
		
	}
}
