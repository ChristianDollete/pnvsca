package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.LpiProjectManagerBean;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiProjectManagerDB;
import com.web.util.LpiProjectManagerModDetails;

public class LpiProjectManagerFindControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiProjectManagerFindControllerBean(){;}
	
	public ArrayList getPmByCriteria(LpiProjectManagerModDetails details)throws Exception{
		System.out.println("LpiProjectManagerFindControllerBean getPmByCriteria");
		ArrayList pmList = new ArrayList();
		try {
			String criteria = "SELECT * FROM lpi_prjct_mngr WHERE pm_cd>0 AND pm_ad_cmpny="+details.getPm_cmpny_id()+" ";
			if(details.getPmName()!=null && !details.getPmName().equals("")){
				criteria += "AND pm_nm like '%"+details.getPmName()+"%' ";
			}
			if(details.getPmPosition()!=null && !details.getPmPosition().equals("")){
				criteria += "AND pm_pstn='"+details.getPmPosition()+"' ";
			}
			if(details.getPmEmail()!=null && !details.getPmEmail().equals("")){
				criteria += "AND pm_eml='"+details.getPmEmail()+"'";
			}
			System.out.println("criteria >> "+criteria);
			
			LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
			ArrayList pmListDB = pmDB.getByCriteria(criteria);
			Iterator i = pmListDB.iterator();
			while(i.hasNext()){
				LpiProjectManagerBean pmBean = (LpiProjectManagerBean)i.next();
				LpiProjectManagerModDetails pmMod = new LpiProjectManagerModDetails();
				pmMod.setPm_cmpny_id(pmBean.getPm_cmpny_id());
				pmMod.setPmName(pmBean.getPmName());
				pmMod.setPmPosition(pmBean.getPmPosition());
				pmMod.setPmCd(pmBean.getPmCd());
				pmMod.setPmEmail(pmBean.getPmEmail());
				pmMod.setPmId(pmBean.getPmId());
				pmList.add(pmMod);
			}
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		
		
		return pmList;
	}
	
	
	public LpiProjectManagerModDetails getPmByPrjName(String prj_id) throws Exception{
		System.out.println("LpiProjectManagerFindControllerBean getPmByPrjName");
		
		String criteria = "SELECT lpi_prjct_mngr.* FROM lpi_prjct_mngr , lpi_prjct WHERE "
				+ "lpi_prjct_mngr.pm_cd = lpi_prjct.prj_pm_cd AND lpi_prjct_mngr.pm_cd>0 AND lpi_prjct.prj_id='"+prj_id+"'";
		
		LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
		LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
		try {
			ArrayList pmListDB = pmDB.getByCriteria(criteria);
			System.out.println("criteria >> "+criteria);
			Iterator i = pmListDB.iterator();
			while(i.hasNext()){
				LpiProjectManagerBean pmBean = (LpiProjectManagerBean)i.next();
				details.setPm_cmpny_id(pmBean.getPm_cmpny_id());
				details.setPmName(pmBean.getPmName());
				details.setPmPosition(pmBean.getPmPosition());
				details.setPmCd(pmBean.getPmCd());
				details.setPmEmail(pmBean.getPmEmail());
				details.setPmId(pmBean.getPmId());

			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	
	

}
