package com.txn;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.bean.AdEmailBean;
import com.bean.AdUserBean;
import com.databaseBean.AdEmailDB;
import com.databaseBean.AdUserDB;
import com.databaseBean.DatabaseService;
import com.web.util.AdUserModDetails;

public class AdEmailControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public AdEmailControllerBean(){;}
	
	
	
	
	
	
	public int update(AdEmailBean eml) throws Exception{
		System.out.println("AdEmailControllerBean update");
		int eml_cd = 0;
		
		
		AdEmailDB emlDB = new AdEmailDB(databaseService);
		try {
			System.out.println("EMAIL ID >> "+ eml.getEmlName());
			eml_cd = emlDB.UPDATE(eml);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return eml_cd;
	}


	public ArrayList getAllEmail() throws Exception{
		System.out.println("AdEmailControllerBean getAllEmail");
		ArrayList emailList = new ArrayList();
		
		try {
			AdEmailDB emlDB = new AdEmailDB(databaseService);
			ArrayList list = emlDB.getAllEmail();
			Iterator i = list.iterator();

			while(i.hasNext()){
				AdEmailBean details = (AdEmailBean)i.next();
				emailList.add(details);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return emailList;
	}
	
	public AdEmailBean getEmail(int eml_cd) throws Exception{
		System.out.println("AdEmailControllerBean getEmail");
		AdEmailBean details = new AdEmailBean();
		
		try {
			AdEmailDB emlDB = new AdEmailDB(databaseService);
			
			details = emlDB.getEmailbyEMLCode(eml_cd);
			if(details == null){
				throw new Exception();
			}
			
			
		} catch (Exception e) {
			System.out.println("AdEmailControllerBean getEmail Exception");
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	
}
