package com.txn;

import java.util.ArrayList;

import com.databaseBean.DatabaseService;
import com.databaseBean.VsoVsoDB;
import com.web.util.VsoVsoModDetails;

public class VsoVsoControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public VsoVsoControllerBean(){;}
	
	public ArrayList getAllVso()throws Exception{
		System.out.println("VsoVsoControllerBean getAllVso");
		ArrayList list = new ArrayList();
		try{
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			list = vsoDB.getAllVso();
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	public VsoVsoModDetails getVsoByVsoId(String vso_id)throws Exception{
		System.out.println("VsoVsoControllerBean getVsoByVsoId");
		VsoVsoModDetails details = new VsoVsoModDetails();
		try{
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			details = vsoDB.getVsoByVsoId(vso_id);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	public VsoVsoModDetails getVsoByVsoCmpCd(int vso_cmp_cd)throws Exception{
		System.out.println("VsoVsoControllerBean getVsoByVsoCmpCd");
		VsoVsoModDetails details = new VsoVsoModDetails();
		try{
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			details = vsoDB.getVsoByVsoCmpCode(vso_cmp_cd);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	public ArrayList getAllVsoByCriteria(VsoVsoModDetails details)throws Exception{
		System.out.println("VsoVsoControllerBean getAllVsoByCriteria");
		ArrayList list = new ArrayList();
		/*	
		AND a.vso_id=''  
				AND a.vso_nm like '%%' 
				AND a.vso_org_typ=''
				AND a.vso_org_rgn = 
				AND a.vso_org_prvnc = 
				AND a.vso_org_mncplty = 
				AND a.vso_org_addrss like '%%'
				AND a.vso_dt_estblishd = '';*/
		
		try{
			String criteria = "select * from vso_vso a where a.vso_cd > 0 ";
			if(details.getVso_nm()!=null && !details.getVso_nm().trim().equals("")){
				criteria+= "AND a.vso_id='"+details.getVso_nm()+"' ";
			}
			
			if(details.getVso_org_typ()>0){
				criteria+= "AND a.vso_org_typ="+details.getVso_org_typ()+" ";
			}
			
			if(details.getVso_org_rgn() > 0){
				criteria+= "AND a.vso_org_rgn="+details.getVso_org_rgn()+" ";
			}
			
			if(details.getVso_org_prvnc() > 0){
				criteria+= "AND a.vso_org_prvnc="+details.getVso_org_prvnc()+" ";
			}
			
			if(details.getVso_org_mncplty() > 0 ){
				criteria+= "AND a.vso_org_mncplty="+details.getVso_org_mncplty()+" ";
			}
			
			if(details.getVso_org_addrss()!=null && details.getVso_org_addrss().trim().length()>0){
				criteria+= "AND a.vso_org_addrss like '%"+details.getVso_org_addrss()+"%' ";
			}
			
			if(details.getVso_dt_estblishd() != null && details.getVso_dt_estblishd().trim().length()>0){
				criteria+= "AND a.vso_dt_estblishd = '"+details.getVso_dt_estblishd()+"' ";
			}

			System.out.println("CRITERIA >> "+ criteria);
			VsoVsoDB vsoDB = new VsoVsoDB(databaseService);
			list = vsoDB.getAllVsoByCriteria(criteria);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}	
		
		return list;
	}
	
	
}
