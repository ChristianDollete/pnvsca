package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.AdCompanyBean;
import com.bean.LpiProjectManagerBean;
import com.databaseBean.AdCompanyDB;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiProjectManagerDB;
import com.web.util.AdUserModDetails;
import com.web.util.AdUserResponsibilityModDetails;
import com.web.util.LpiProjectManagerModDetails;

public class LpiProjectManagerEntryControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiProjectManagerEntryControllerBean(){;}
	
	public int createPM(LpiProjectManagerModDetails pm) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean createPM");
		int pm_cd =0;
		try{
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean= cmpDB.getCompanyByCmpId(pm.getPm_cmpny_id());
			AdSequence lpiSqnc = new AdSequence(databaseService);
			String PM_ID = lpiSqnc.getLpiPmId();
			//CREATE PM
			
			AdUserControllerBean usrCbean = new AdUserControllerBean();
			AdUserModDetails usr = new AdUserModDetails();
			usr.setUsr_usr_nm(pm.getPmUserName());
			usr.setUsr_usr_psswrd(pm.getPmUserPassword());
			usr.setUsr_usr_prmry_email(pm.getPmEmail());
			usr.setUsr_usr_scndry_email("");
			usr.setUsr_usr_dscrptn(pm.getPmName());
			usr.setAd_cmpny(pm.getPm_cmpny_id());
			usr.setAd_usr_clsfctn(21);// PM PROFILE CODE
			int usr_id = usrCbean.createUser(usr);
			if(usr_id>0){
			}else{
				throw new Exception();
			}
			
			LpiProjectManagerBean details = new LpiProjectManagerBean();
			details.setPm_cmpny_id(pm.getPm_cmpny_id());
			details.setPmId(PM_ID);
			details.setPmName(pm.getPmName());
			details.setSex(pm.getSex());
			details.setPmPosition(pm.getPmPosition());
			details.setPmEmail(pm.getPmEmail());
			details.setPmTelNo(pm.getPmTelNo());
			details.setPmMobNo(pm.getPmMobNo());
			details.setPmFaxNo(pm.getPmFaxNo());
			details.setPmUserName(pm.getPmUserName());
			details.setPmUserPassword(pm.getPmUserPassword());
			details.setPm_cmpny_id(pm.getPm_cmpny_id());
			details.setPm_usr_cd(usr_id);
			databaseService = new DatabaseService();
			LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
			pm_cd = pmDB.CREATE(details);
			
			//CREATE USR
			
			
			
			AdUserResponsibilityControllerBean usrResCbean = new AdUserResponsibilityControllerBean();
			AdUserResponsibilityModDetails resMd = new AdUserResponsibilityModDetails();
			resMd.setRes_usr_id(usr_id);
			resMd.setRes_lpi_prjct(0);
			resMd.setRes_lpi_prjct_mngr(1);
			resMd.setRes_lpi_rqst_vlntr(0);
			resMd.setRes_vso_vlntr(0);
			int res_id = usrResCbean.createRes(resMd);
			if(res_id>0){
			}else{
				throw new Exception();
			}
			
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return pm_cd;
	}
	
	public int updatePM(LpiProjectManagerModDetails pm) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean updatePM");
		int pm_cd = 0;
		try {
			LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
			LpiProjectManagerBean details = new LpiProjectManagerBean();
			details.setPm_cmpny_id(pm.getPm_cmpny_id());
			details.setPmId(pm.getPmId());
			details.setPmName(pm.getPmName());
			details.setSex(pm.getSex());
			details.setPmPosition(pm.getPmPosition());
			details.setPmEmail(pm.getPmEmail());
			details.setPmTelNo(pm.getPmTelNo());
			details.setPmMobNo(pm.getPmMobNo());
			details.setPmFaxNo(pm.getPmFaxNo());
			details.setPmUserName(pm.getPmUserName());
			details.setPmUserPassword(pm.getPmUserPassword());
			pm_cd = pmDB.UPDATE(details);
			
			LpiProjectManagerModDetails pmMD = this.getPmByPmId(pm.getPmId());
			
			AdUserControllerBean usrCbean = new AdUserControllerBean();
			AdUserModDetails usr = new AdUserModDetails();
			usr.setUsr_usr_nm(pm.getPmUserName());
			usr.setUsr_usr_psswrd(pm.getPmUserPassword());
			usr.setUsr_usr_prmry_email(pm.getPmEmail());
			usr.setUsr_usr_scndry_email(pm.getPmEmail());
			usr.setUsr_usr_dscrptn(pm.getPmName());
			usr.setAd_cmpny(pm.getPm_cmpny_id());
			usr.setUsr_id(pmMD.getPm_usr_cd());
			int usr_id = usrCbean.update(usr);
			if(usr_id>0){
			}else{
				throw new Exception();
			}
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return pm_cd;
	}
	
	public LpiProjectManagerModDetails getPmByPmCode(int pm_cd) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean getPmByPmCode");
		LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
		LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
		try {
			LpiProjectManagerBean pmBean = pmDB.getPmByPmCode(pm_cd);
			details = this.beanToModDetail(pmBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	
	public LpiProjectManagerModDetails getPmByPmId(String pm_id) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean getPmByPmId");
		LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
		LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
		try {
			LpiProjectManagerBean pmBean = pmDB.getPmByPmId(pm_id);
			details = this.beanToModDetail(pmBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	
	
	public LpiProjectManagerModDetails getPmByUsrCode(int pm_usr_cd) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean getPmByUsrCode");
		LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
		LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
		try {
			LpiProjectManagerBean pmBean = pmDB.getPmByUsrCode(pm_usr_cd);
			details = this.beanToModDetail(pmBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
	}
	
	
	
	
	public ArrayList getPmByLpiCd(int lpi_cd) throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean getPmByLpiCd");
		ArrayList pmlist = new ArrayList();
		try {
			
			LpiProjectManagerDB pmDB = new LpiProjectManagerDB(databaseService);
			ArrayList pmLbean = pmDB.getPmByLpiCd(lpi_cd);
			Iterator i = pmLbean.iterator();
			while(i.hasNext()){
				LpiProjectManagerBean pmBean = (LpiProjectManagerBean)i.next();
				LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
				details = this.beanToModDetail(pmBean);
				pmlist.add(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return pmlist;
		
	}
	
	private LpiProjectManagerModDetails beanToModDetail(LpiProjectManagerBean pmBean)throws Exception{
		System.out.println("LpiProjectManagerEntryControllerBean beanToModDetail");
		LpiProjectManagerModDetails details = new LpiProjectManagerModDetails();
		try {
			details.setPmCd(pmBean.getPmCd());
			details.setPmId(pmBean.getPmId());
			details.setPmName(pmBean.getPmName());
			details.setSex(pmBean.getSex());
			details.setPmPosition(pmBean.getPmPosition());
			details.setPmEmail(pmBean.getPmEmail());
			details.setPmTelNo(pmBean.getPmTelNo());
			details.setPmMobNo(pmBean.getPmMobNo());
			details.setPmFaxNo(pmBean.getPmFaxNo());
			details.setPmUserName(pmBean.getPmUserName());
			details.setPm_cmpny_id(pmBean.getPm_cmpny_id());
			details.setPm_usr_cd(pmBean.getPm_usr_cd());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	
	
	

}
