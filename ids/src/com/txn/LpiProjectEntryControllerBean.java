package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.LpiLpiBean;
import com.bean.LpiProjectBean;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiLpiDB;
import com.databaseBean.LpiProjectDB;
import com.web.util.LpiProjectModDetails;

public class LpiProjectEntryControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiProjectEntryControllerBean(){;}
	
	public int create(LpiProjectModDetails prj) throws Exception{
		System.out.println("LpiProjectEntryControllerBean create");
		int prj_cd = 0;
		try{
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			LpiProjectBean prjBean = new LpiProjectBean();
			prjBean.setPrjCd(prj.getPrjCd());
			
			AdSequence spmId = new AdSequence(databaseService);
			String pmID = spmId.getLpiPrjId();
			prjBean.setPrjId(pmID);
			prjBean.setPrjOrgName(prj.getPrjOrgName());
			prjBean.setPrjDurationFrom(prj.getPrjDurationFrom());
			prjBean.setPrjDurationTo(prj.getPrjDurationTo());
			prjBean.setPrjTargetBenefits(prj.getPrjTargetBenefits());
			prjBean.setPrjBudgetSource(prj.getPrjBudgetSource());
			prjBean.setPrjSource(prj.getPrjSource());
			prjBean.setPrjProjectObjective(prj.getPrjProjectObjective());
			prjBean.setPrjProjectDesc(prj.getPrjProjectDesc());
			prjBean.setPrjRgn(prj.getPrjRgn());
			prjBean.setPrjPrvnc(prj.getPrjPrvnc());
			prjBean.setPrjMncplty(prj.getPrjMncplty());
			prjBean.setPrjAddress(prj.getPrjAddress());
			prjBean.setPrjProjectManager(prj.getPrjProjectManager());
			prjBean.setPrjCmpCode(prj.getPrjCmpCode());
			prj_cd = prjDB.CREATE(prjBean);
			
			if(prj_cd == 0){
				throw new Exception();
			}
			
		}catch(Exception e){
			throw new Exception(e);
		}
		return prj_cd;
	}
	
	
	
	
	public int update(LpiProjectModDetails prj) throws Exception{
		System.out.println("LpiProjectEntryControllerBean update");
		int prj_cd = 0;
		try{
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			LpiProjectBean prjBean = new LpiProjectBean();
			prjBean.setPrjCd(prj.getPrjCd());
			prjBean.setPrjId(prj.getPrjId());
			prjBean.setPrjOrgName(prj.getPrjOrgName());
			prjBean.setPrjDurationFrom(prj.getPrjDurationFrom());
			prjBean.setPrjDurationTo(prj.getPrjDurationTo());
			prjBean.setPrjTargetBenefits(prj.getPrjTargetBenefits());
			prjBean.setPrjBudgetSource(prj.getPrjBudgetSource());
			prjBean.setPrjSource(prj.getPrjSource());
			prjBean.setPrjProjectObjective(prj.getPrjProjectObjective());
			prjBean.setPrjProjectDesc(prj.getPrjProjectDesc());
			prjBean.setPrjRgn(prj.getPrjRgn());
			prjBean.setPrjPrvnc(prj.getPrjPrvnc());
			prjBean.setPrjMncplty(prj.getPrjMncplty());
			prjBean.setPrjAddress(prj.getPrjAddress());
			prjBean.setPrjProjectManager(prj.getPrjProjectManager());
			prjBean.setPrjCmpCode(prj.getPrjCmpCode());
			prj_cd = prjDB.UPDATE(prjBean);
			
			if(prj_cd == 0){
				throw new Exception();
			}
			
		}catch(Exception e){
			throw new Exception(e);
		}
		return prj_cd;
	}
	
	
	
	public LpiProjectModDetails getPrjByPrjCode(int prj_cd) throws Exception{
		System.out.println("LpiProjectEntryControllerBean getPrjByPrjCd");
		LpiProjectModDetails details = new LpiProjectModDetails();
		try{
		
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			LpiProjectBean prjBean = prjDB.getPrjByPrjCode(prj_cd);
			
			details.setPrjId(prjBean.getPrjId());
			details.setPrjOrgName(prjBean.getPrjOrgName());
			details.setPrjDurationFrom(prjBean.getPrjDurationFrom());
			details.setPrjDurationTo(prjBean.getPrjDurationTo());
			details.setPrjTargetBenefits(prjBean.getPrjTargetBenefits());
			details.setPrjBudgetSource(prjBean.getPrjBudgetSource());
			details.setPrjSource(prjBean.getPrjSource());
			details.setPrjProjectObjective(prjBean.getPrjProjectObjective());
			details.setPrjProjectDesc(prjBean.getPrjProjectDesc());
			details.setPrjRgn(prjBean.getPrjRgn());
			details.setPrjPrvnc(prjBean.getPrjPrvnc());
			details.setPrjMncplty(prjBean.getPrjMncplty());
			details.setPrjAddress(prjBean.getPrjAddress());
			details.setPrjProjectManager(prjBean.getPrjProjectManager());
			details.setPrjCmpCode(prjBean.getPrjCmpCode());
			details.setPrjCd(prjBean.getPrjCd());
			
			
		}catch (Exception e){
			e.printStackTrace();
			
		}
		return details;
		
	}
	
	public LpiProjectModDetails getPrjByPrjId(String prj_id) throws Exception{
		System.out.println("LpiProjectEntryControllerBean getPrjByPrjId");
		LpiProjectModDetails details = new LpiProjectModDetails();
		try{
		
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			LpiProjectBean prjBean = prjDB.getPrjByPrjId(prj_id);
			
			details.setPrjId(prjBean.getPrjId());
			details.setPrjOrgName(prjBean.getPrjOrgName());
			details.setPrjDurationFrom(prjBean.getPrjDurationFrom());
			details.setPrjDurationTo(prjBean.getPrjDurationTo());
			details.setPrjTargetBenefits(prjBean.getPrjTargetBenefits());
			details.setPrjBudgetSource(prjBean.getPrjBudgetSource());
			details.setPrjSource(prjBean.getPrjSource());
			details.setPrjProjectObjective(prjBean.getPrjProjectObjective());
			details.setPrjProjectDesc(prjBean.getPrjProjectDesc());
			details.setPrjRgn(prjBean.getPrjRgn());
			details.setPrjPrvnc(prjBean.getPrjPrvnc());
			details.setPrjMncplty(prjBean.getPrjMncplty());
			details.setPrjAddress(prjBean.getPrjAddress());
			details.setPrjProjectManager(prjBean.getPrjProjectManager());
			details.setPrjCmpCode(prjBean.getPrjCmpCode());
			details.setPrjCd(prjBean.getPrjCd());
			
			
		}catch (Exception e){
			e.printStackTrace();
			
		}
		return details;
		
	}
	
	
	
	
	

}
