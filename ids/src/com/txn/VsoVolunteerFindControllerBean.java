package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.VsoVolunteerBean;
import com.databaseBean.AdRegionDB;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.VsoVolunteerDB;
import com.web.util.AdUserModDetails;
import com.web.util.AdUserResponsibilityModDetails;
import com.web.util.LpiProjectModDetails;
import com.web.util.VsoVolunteerModDetails;

public class VsoVolunteerFindControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public VsoVolunteerFindControllerBean(){;}
	
	public ArrayList getByCriteria(VsoVolunteerModDetails vl) throws Exception{
		System.out.println("VsoVolunteerFindControllerBean getByCriteria");
		ArrayList list = new ArrayList();
		try{
			
			VsoVolunteerDB vBean = new VsoVolunteerDB(databaseService);
			String criteria = "SELECT * FROM vso_vlntr WHERE vlntr_cd > 0 ";
			if(!vl.getVlntr_id().equalsIgnoreCase("")){
				criteria += " AND vlntr_id like '%"+vl.getVlntr_id()+"%'";
			}
			if(vl.getVlntr_fname()!=null && !vl.getVlntr_fname().equalsIgnoreCase("")){
				criteria += " AND vlntr_fname like '%"+vl.getVlntr_fname()+"%'";
			}
			if(vl.getVlntr_mname()!=null && !vl.getVlntr_mname().equalsIgnoreCase("")){
				criteria += " AND vlntr_mname like '%"+vl.getVlntr_mname()+"%'";
			}
			if(vl.getVlntr_lname()!=null && !vl.getVlntr_lname().equalsIgnoreCase("")){
				criteria += " AND vlntr_lname like '%"+vl.getVlntr_lname()+"%'";
			}
			if(vl.getVlntr_vs_exprtn()!=null && !vl.getVlntr_vs_exprtn().equalsIgnoreCase("")){
				criteria += " AND vltnr_vs_exprtn like '%"+vl.getVlntr_vs_exprtn()+"%'";
			}
			if(vl.getVlntr_ntnly()!=null && !vl.getVlntr_ntnly().equalsIgnoreCase("")){
				criteria += " AND vlntr_ntnly like ='"+vl.getVlntr_ntnly()+"'";
			}
			
			if(vl.getVlntr_cmp_cd()==3){
				criteria += " AND vlntr_cmp_cd ="+vl.getVlntr_cmp_cd()+"";
			}
			criteria += " ORDER BY vlntr_id";
			System.out.println("CRITERIA >> "+ criteria);
			ArrayList vlntrList  = vBean.getByCriteria(criteria);
			Iterator i = vlntrList.iterator();
			while(i.hasNext()){
				VsoVolunteerBean vlBean = (VsoVolunteerBean)i.next();
				VsoVolunteerModDetails details = new VsoVolunteerModDetails();
				details.setVlntr_cd(vlBean.getVlntr_cd());
				details.setVlntr_id(vlBean.getVlntr_id());
				details.setVlntr_fname(vlBean.getVlntr_fname());
				details.setVlntr_mname(vlBean.getVlntr_mname());
				details.setVlntr_lname(vlBean.getVlntr_lname());
				details.setVlntr_prjct_cd(vlBean.getVlntr_prjct_cd());
				if(vlBean.getVlntr_prjct_cd()>0){
					LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
					LpiProjectModDetails prjD =prjC.getPrjByPrjCode(vlBean.getVlntr_prjct_cd());
					details.setVlntr_prjct_nm(prjD.getPrjOrgName());
					details.setPrjDateFrom(prjD.getPrjDurationFrom());
					details.setPrjDateTo(prjD.getPrjDurationTo());
					
				}
				details.setVlntr_vs_exprtn(vlBean.getVlntr_vs_exprtn());
				
				list.add(details);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	

	public ArrayList getByProjectStatusAndCmpCode(String pjct_status,String prjct_id,int cmp_cd) throws Exception{
		System.out.println("VsoVolunteerFindControllerBean getByProjectStatusAndCmpCode");
		ArrayList list = new ArrayList();
		try{
			
			VsoVolunteerDB vBean = new VsoVolunteerDB(databaseService);
			String criteria = "SELECT * FROM vso_vlntr a, lpi_rqst_vlntr_ln b, lpi_rqst_vlntr c, lpi_prjct d "
					+ "WHERE a.vlntr_cd = b.rvl_vlntr_cd "
					+ "AND b.rvl_rv_cd = c.rv_cd "
					+ "AND c.rv_prj_cd = d.prj_cd "
					+ "AND d.prj_cmp_cd ="+cmp_cd+" "
					+ "AND d.prj_id = '"+prjct_id+"' "
					+ "AND c.rv_status = '"+pjct_status+"'";
			criteria += " ORDER BY a.vlntr_id";
			System.out.println("CRITERIA >> "+ criteria);
			ArrayList vlntrList  = vBean.getByCriteria(criteria);
			Iterator i = vlntrList.iterator();
			while(i.hasNext()){
				VsoVolunteerBean vlBean = (VsoVolunteerBean)i.next();
				VsoVolunteerModDetails details = new VsoVolunteerModDetails();
				details.setVlntr_cd(vlBean.getVlntr_cd());
				details.setVlntr_id(vlBean.getVlntr_id());
				details.setVlntr_fname(vlBean.getVlntr_fname());
				details.setVlntr_mname(vlBean.getVlntr_mname());
				details.setVlntr_lname(vlBean.getVlntr_lname());
				details.setVlntr_prjct_cd(vlBean.getVlntr_prjct_cd());
				if(vlBean.getVlntr_prjct_cd()>0){
					LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
					LpiProjectModDetails prjD =prjC.getPrjByPrjCode(vlBean.getVlntr_prjct_cd());
					details.setVlntr_prjct_nm(prjD.getPrjOrgName());
					details.setPrjDateFrom(prjD.getPrjDurationFrom());
					details.setPrjDateTo(prjD.getPrjDurationTo());
					
				}
				details.setVlntr_vs_exprtn(vlBean.getVlntr_vs_exprtn());
				
				list.add(details);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	public ArrayList getVolunteerWithoutProject(int cmp_cd) throws Exception{
		System.out.println("VsoVolunteerFindControllerBean getVolunteerWithoutProject");
		ArrayList list = new ArrayList();
		try{
			
			VsoVolunteerDB vBean = new VsoVolunteerDB(databaseService);
			String criteria = "SELECT * FROM vso_vlntr a WHERE a.vlntr_cmp_cd="+cmp_cd+"";
			criteria += " ORDER BY a.vlntr_id";
			System.out.println("CRITERIA >> "+ criteria);
			ArrayList vlntrList  = vBean.getByCriteria(criteria);
			Iterator i = vlntrList.iterator();
			while(i.hasNext()){
				VsoVolunteerBean vlBean = (VsoVolunteerBean)i.next();
				VsoVolunteerModDetails details = new VsoVolunteerModDetails();
				details.setVlntr_cd(vlBean.getVlntr_cd());
				details.setVlntr_id(vlBean.getVlntr_id());
				details.setVlntr_fname(vlBean.getVlntr_fname());
				details.setVlntr_mname(vlBean.getVlntr_mname());
				details.setVlntr_lname(vlBean.getVlntr_lname());
				details.setVlntr_prjct_cd(vlBean.getVlntr_prjct_cd());
				if(vlBean.getVlntr_prjct_cd()>0){
					LpiProjectEntryControllerBean prjC = new LpiProjectEntryControllerBean();
					LpiProjectModDetails prjD =prjC.getPrjByPrjCode(vlBean.getVlntr_prjct_cd());
					details.setVlntr_prjct_nm(prjD.getPrjOrgName());
					details.setPrjDateFrom(prjD.getPrjDurationFrom());
					details.setPrjDateTo(prjD.getPrjDurationTo());
					
				}
				details.setVlntr_vs_exprtn(vlBean.getVlntr_vs_exprtn());
				
				list.add(details);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	
	
}
