package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.LpiProjectBean;
import com.bean.LpiProjectManagerBean;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiProjectDB;
import com.databaseBean.LpiProjectManagerDB;
import com.web.util.LpiProjectManagerModDetails;
import com.web.util.LpiProjectModDetails;

public class LpiProjectFindControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiProjectFindControllerBean(){;}
	
	public ArrayList getPrjByCriteria(LpiProjectModDetails details)throws Exception{
		System.out.println("LpiProjectFindControllerBean getPmByCriteria");
		ArrayList pmList = new ArrayList();
		try {
			String criteria = "SELECT * FROM lpi_prjct WHERE prj_cd>0 AND prj_cmp_cd="+details.getPrjCmpCode()+" ";
			if(details.getPrjOrgName()!=null && !details.getPrjOrgName().equals("")){
				criteria += " AND prj_nm like '%"+details.getPrjOrgName()+"%' ";
			}
			if(details.getPrjProjectDesc()!=null && !details.getPrjProjectDesc().equals("")){
				criteria += " AND prj_dscptn like '%"+details.getPrjProjectDesc().trim()+"%' ";
			}
			if(details.getPrjProjectObjective()!=null && !details.getPrjProjectObjective().equals("")){
				criteria += " AND prj_objctv like '%"+details.getPrjProjectObjective().trim()+"%'";
			}
			
			if(details.getPrjDurationFrom()!=null && !details.getPrjDurationFrom().equals("")){
				criteria += " AND STR_TO_DATE(prj_drtn_frm, '%m/%d/%Y') >= STR_TO_DATE('"+details.getPrjDurationFrom().trim()+"', '%m/%d/%Y')";
				//criteria += " AND str_to_date(prj_drtn_frm,%M/%D/%Y)    >= '"+details.getPrjDurationFrom().trim()+"'";
			}
			
			if(details.getPrjDurationTo()!=null && !details.getPrjDurationTo().equals("")){
				criteria += " AND STR_TO_DATE(prj_drtn_to, '%m/%d/%Y') <= STR_TO_DATE('"+details.getPrjDurationTo().trim()+"', '%m/%d/%Y')";
				
				//criteria += " AND prj_drtn_to <= '"+details.getPrjDurationTo().trim()+"'";
			}
			
			if(details.getPrjProjectManager()!=0){
				criteria += " AND prj_pm_cd = "+details.getPrjProjectManager()+"";
			}
			
			if(details.getPrjRgn()!=0){
				criteria += " AND prj_rgn = "+details.getPrjRgn()+"";
			}
			
			if(details.getPrjPrvnc()!=0){
				criteria += " AND prj_prvnc = "+details.getPrjPrvnc()+"";
			}
			

			if(details.getPrjMncplty()!=0){
				criteria += " AND prj_mncplty = "+details.getPrjMncplty()+"";
			}
			
			if(details.getPrjAddress()!=null && !details.getPrjAddress().equals("")){
				criteria += " AND prj_addrss like '%"+details.getPrjAddress().trim()+"%'";
			}
			
			
			
			System.out.println("criteria >> "+criteria);
			
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			ArrayList prjListDB = prjDB.getByCriteria(criteria);
			Iterator i = prjListDB.iterator();
			while(i.hasNext()){
				LpiProjectBean prjBean = (LpiProjectBean)i.next();
				LpiProjectModDetails prjMod = new LpiProjectModDetails();
				prjMod.setPrjCd(prjBean.getPrjCd());
				prjMod.setPrjId(prjBean.getPrjId());
				prjMod.setPrjOrgName(prjBean.getPrjOrgName());
				prjMod.setPrjProjectDesc(prjBean.getPrjProjectDesc());
				prjMod.setPrjProjectObjective(prjBean.getPrjProjectObjective());
				prjMod.setPrjDurationFrom(prjBean.getPrjDurationFrom());
				prjMod.setPrjDurationTo(prjBean.getPrjDurationTo());
				String pmName ="";
				DatabaseService databaseService3 = new DatabaseService();
				try {
					LpiProjectManagerDB pmDBDb = new LpiProjectManagerDB(databaseService3);
					LpiProjectManagerBean pmBean = pmDBDb.getPmByPmCode(prjBean.getPrjProjectManager());
					pmName = pmBean.getPmName();
					prjMod.setPmName(pmName);
					System.out.println("pmName >> "+ pmName);
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				
				pmList.add(prjMod);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return pmList;
	}
	
	public ArrayList getPrjByCmpCode(int cmp_cd)throws Exception{
		System.out.println("LpiProjectFindControllerBean getPrjByCmpCode");
		ArrayList pmList = new ArrayList();
		try {
			String criteria = "SELECT * FROM lpi_prjct WHERE prj_cd>0 AND prj_cmp_cd="+cmp_cd+"";
			System.out.println("criteria >> "+criteria);
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			ArrayList prjListDB = prjDB.getByCriteria(criteria);
			Iterator i = prjListDB.iterator();
			while(i.hasNext()){
				LpiProjectBean prjBean = (LpiProjectBean)i.next();
				LpiProjectModDetails prjMod = new LpiProjectModDetails();
				prjMod.setPrjCd(prjBean.getPrjCd());
				prjMod.setPrjId(prjBean.getPrjId());
				prjMod.setPrjOrgName(prjBean.getPrjOrgName());
				prjMod.setPrjProjectDesc(prjBean.getPrjProjectDesc());
				prjMod.setPrjProjectObjective(prjBean.getPrjProjectObjective());
				prjMod.setPrjDurationFrom(prjBean.getPrjDurationFrom());
				prjMod.setPrjDurationTo(prjBean.getPrjDurationTo());
				pmList.add(prjMod);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return pmList;
	}

	public ArrayList getPrjAll()throws Exception{
		System.out.println("LpiProjectFindControllerBean getPrjAll");
		ArrayList pmList = new ArrayList();
		try {
			String criteria = "SELECT * FROM lpi_prjct";
			System.out.println("criteria >> "+criteria);
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			ArrayList prjListDB = prjDB.getByCriteria(criteria);
			Iterator i = prjListDB.iterator();
			while(i.hasNext()){
				LpiProjectBean prjBean = (LpiProjectBean)i.next();
				LpiProjectModDetails prjMod = new LpiProjectModDetails();
				prjMod.setPrjCd(prjBean.getPrjCd());
				prjMod.setPrjId(prjBean.getPrjId());
				prjMod.setPrjOrgName(prjBean.getPrjOrgName());
				prjMod.setPrjProjectDesc(prjBean.getPrjProjectDesc());
				prjMod.setPrjProjectObjective(prjBean.getPrjProjectObjective());
				prjMod.setPrjDurationFrom(prjBean.getPrjDurationFrom());
				prjMod.setPrjDurationTo(prjBean.getPrjDurationTo());
				pmList.add(prjMod);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return pmList;
	}

	
	
	public ArrayList getPrjAssignByCmpCode(int cmp_cd)throws Exception{
		System.out.println("LpiProjectFindControllerBean getPrjAssignByCmpCode");
		ArrayList pmList = new ArrayList();
		try {
			String criteria = "SELECT * FROM lpi_prjct a, lpi_rqst_vlntr b, lpi_rqst_vlntr_ln c, vso_vso d  "
					+ "WHERE b.rv_prj_cd = a.prj_cd "
					+ "AND c.rvl_rv_cd = b.rv_cd "
					+ "AND c.rvl_vso_cd = d.vso_cmp_cd "
					+ "AND d.vso_cmp_cd = "+cmp_cd+"";
			
			System.out.println("criteria >> "+criteria);
			LpiProjectDB prjDB = new LpiProjectDB(databaseService);
			ArrayList prjListDB = prjDB.getByCriteria(criteria);
			Iterator i = prjListDB.iterator();
			while(i.hasNext()){
				LpiProjectBean prjBean = (LpiProjectBean)i.next();
				LpiProjectModDetails prjMod = new LpiProjectModDetails();
				prjMod.setPrjCd(prjBean.getPrjCd());
				prjMod.setPrjId(prjBean.getPrjId());
				prjMod.setPrjOrgName(prjBean.getPrjOrgName());
				prjMod.setPrjProjectDesc(prjBean.getPrjProjectDesc());
				prjMod.setPrjProjectObjective(prjBean.getPrjProjectObjective());
				prjMod.setPrjDurationFrom(prjBean.getPrjDurationFrom());
				prjMod.setPrjDurationTo(prjBean.getPrjDurationTo());
				
				pmList.add(prjMod);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return pmList;
	}
	
	
	
	

}
