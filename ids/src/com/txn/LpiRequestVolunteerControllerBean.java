package com.txn;

import java.util.ArrayList;
import java.util.Iterator;

import com.bean.LpiRequestVolunteerBean;
import com.bean.LpiRequestVolunteerLineBean;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiRequestVolunteerDB;
import com.databaseBean.LpiRequestVolunteerLineDB;
import com.web.util.LpiProjectModDetails;
import com.web.util.LpiRequestVolunteerLineModDetails;
import com.web.util.LpiRequestVolunteerModDetails;

public class LpiRequestVolunteerControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiRequestVolunteerControllerBean(){;}
	
	public int createRV(LpiRequestVolunteerModDetails rv) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean createRV");
		int rv_cd =0;
		try{
			LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService);
			LpiRequestVolunteerBean rvBean = new LpiRequestVolunteerBean();
			rvBean.setRv_cd(rv.getRv_cd());
			rvBean.setRv_cmp_cd(rv.getRv_cmp_cd());
			String rv_id=rv.getRv_id();
			if(rv.getRv_id().toString().trim().length()==0){
				databaseService = new DatabaseService();
				AdSequence sqnc = new AdSequence(databaseService);
				rv_id = sqnc.getLpiRvId();
			}
			rvBean.setRv_id(rv_id);
			rvBean.setRv_status(rv.getRv_status());
			rvBean.setRv_prj_cd(rv.getRv_prj_cd());
			rvBean.setRv_no_vlntr(rv.getRv_no_vlntr());
			rvBean.setRv_drtn_mnths(rv.getRv_drtn_mnths());
			rv_cd = rvDB.CREATE(rvBean);
			
			for(int i = 1; i<=rv.getRv_no_vlntr(); i++){
				DatabaseService databaseService3 = new DatabaseService();
				LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService3);
				LpiRequestVolunteerLineBean rvln = new LpiRequestVolunteerLineBean();
				rvln.setRvl_ln_no(i);
				rvln.setRvl_rv_cd(rv_cd);
				rvlDB.CREATE(rvln);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rv_cd;
	}
	
	public int updateRV(LpiRequestVolunteerModDetails rv) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean updateRV");
		int rv_cd =0;
		try{
			LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService);
			LpiRequestVolunteerBean rvBean = new LpiRequestVolunteerBean();
			
			rvBean.setRv_cd(rv.getRv_cd());
			rvBean.setRv_cmp_cd(rv.getRv_cmp_cd());
			rvBean.setRv_id(rv.getRv_id());
			rvBean.setRv_status(rv.getRv_status());
			rvBean.setRv_prj_cd(rv.getRv_prj_cd());
			rvBean.setRv_no_vlntr(rv.getRv_no_vlntr());
			rvBean.setRv_drtn_mnths(rv.getRv_drtn_mnths());
			rv_cd = rvDB.UPDATE(rvBean);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rv_cd;
	}
	
	
	public int updateRVL(LpiRequestVolunteerLineBean rvl) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean updateRVL");
		int rvl_cd =0;
		try{
			LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
			rvl_cd = rvlDB.UPDATE(rvl);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rvl_cd;
	}
	
	
	
	public LpiRequestVolunteerModDetails getRvByRvId(String rv_id , int vso_cmp_cd) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean getRvByRvId");
		LpiRequestVolunteerModDetails rvD = new LpiRequestVolunteerModDetails();
		int rv_cd =0;
		try{

			LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService);
			LpiRequestVolunteerBean rvBean = rvDB.getRvByRV_ID(rv_id);
			
			rvD.setRv_cd(rvBean.getRv_cd());
			rvD.setRv_cmp_cd(rvBean.getRv_cmp_cd());
			rvD.setRv_drtn_mnths(rvBean.getRv_drtn_mnths());
			rvD.setRv_id(rvBean.getRv_id());
			rvD.setRv_no_vlntr(rvBean.getRv_no_vlntr());
			rvD.setRv_prj_cd(rvBean.getRv_prj_cd());
			rvD.setRv_status(rvBean.getRv_status());
			
			databaseService = new DatabaseService();
			LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
			System.out.println("GET REQUEST LINE");
			ArrayList rvlList = rvlDB.getRvlByRVL_RV_CD(rvBean.getRv_cd());
			Iterator i = rvlList.iterator();
			rvD.clearRvlList();
			while(i.hasNext()){
				LpiRequestVolunteerLineBean rvlBean = (LpiRequestVolunteerLineBean)i.next();
				if(vso_cmp_cd !=0){
					if(rvlBean.getRvl_vso_cd() == vso_cmp_cd){
						rvD.setRvlList(rvlBean);
					}
				}else{
					rvD.setRvlList(rvlBean);
				}
				
				//rvD.setRvlList(rvlBean);
			}
		
			
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rvD;
	}
	
	
	public LpiRequestVolunteerLineBean getRvlByRvlCd(int rvl_cd) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean getRvlByRvlCd");
		LpiRequestVolunteerLineBean details = new LpiRequestVolunteerLineBean();
		int rv_cd =0;
		try{
			LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
			System.out.println("GET REQUEST LINE");
			details = rvlDB.getRvlByRVL_CD(rvl_cd);
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return details;
	}
	
	

	public LpiRequestVolunteerModDetails getRvByRvCd(int rv_cd, int vso_cmp_cd) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean getRvByRvCd");
		LpiRequestVolunteerModDetails rvD = new LpiRequestVolunteerModDetails();
		try{
			LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService);
			LpiRequestVolunteerBean rvBean = rvDB.getRvByRV_CD(rv_cd);
			
			rvD.setRv_cd(rvBean.getRv_cd());
			rvD.setRv_cmp_cd(rvBean.getRv_cmp_cd());
			rvD.setRv_drtn_mnths(rvBean.getRv_drtn_mnths());
			rvD.setRv_id(rvBean.getRv_id());
			rvD.setRv_no_vlntr(rvBean.getRv_no_vlntr());
			rvD.setRv_prj_cd(rvBean.getRv_prj_cd());
			rvD.setRv_status(rvBean.getRv_status());
			
			
			databaseService = new DatabaseService();
			LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
			ArrayList rvlList = rvlDB.getRvlByRVL_RV_CD(rvBean.getRv_cd());
			Iterator i = rvlList.iterator();
			rvD.clearRvlList();
			while(i.hasNext()){
				LpiRequestVolunteerLineBean rvlBean = (LpiRequestVolunteerLineBean)i.next();
				if(vso_cmp_cd !=0){
					if(rvlBean.getRvl_vso_cd() == vso_cmp_cd){
						rvD.setRvlList(rvlBean);
					}
				}else{
					rvD.setRvlList(rvlBean);
				}
				
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rvD;
	}
	
	public ArrayList getRvByCriteria(LpiRequestVolunteerModDetails details) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean getRvByCriteria");
		ArrayList list = new ArrayList();
		try{
			
			String criteria = "SELECT * FROM lpi_rqst_vlntr a , lpi_prjct b ,  lpi_rqst_vlntr_ln c  "
					+ "WHERE a.rv_prj_cd = b.prj_cd AND a.rv_cd = c.rvl_rv_cd ";
			
			if(details.getRv_id()!=null && !details.getRv_id().equals("")){
				criteria += "AND rv_id like '%"+details.getRv_id()+"%' ";
			}
			if(details.getRv_status()!=null && !details.getRv_status().equals("") && !details.getRv_status().equals("0")){
				criteria += "AND rv_status='"+details.getRv_status()+"' ";
			}
			if(details.getRv_drtn_mnths()!=null && !details.getRv_drtn_mnths().equals("")){
				criteria += "AND rv_drtn_mnths='"+details.getRv_drtn_mnths()+"'";
			}
			if(details.getRv_no_vlntr()>0){
				criteria += "AND a.rv_no_vlntr ="+details.getRv_no_vlntr()+" ";
			}
			
			if(details.getRv_prj_cd()!=0 ){
				criteria += "AND b.prj_id ="+details.getRv_prj_cd()+" ";
			}
			
			if(details.getRv_cmp_cd()!=0){
				criteria += "AND a.rv_cmp_cd ="+details.getRv_cmp_cd()+" ";
			}
			
			if(details.getRvl_vso_cd()!=0){
				criteria += "AND c.rvl_vso_cd ="+details.getRvl_vso_cd()+" ";
			}
			
			System.out.println("criteria >> "+criteria);
			
			 String orderBy = "GROUP BY a.rv_id ORDER BY a.rv_id;";
			
			 LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService); 
			 ArrayList rvlist = rvDB.getByCriteria(criteria + orderBy);
			 Iterator i = rvlist.iterator();
			 while(i.hasNext()){
				 LpiRequestVolunteerBean rvlBean = (LpiRequestVolunteerBean)i.next();
				 LpiRequestVolunteerModDetails rvd = new LpiRequestVolunteerModDetails();
				 rvd.setRv_cd(rvlBean.getRv_cd());
				 rvd.setRv_id(rvlBean.getRv_id());
				 rvd.setRv_drtn_mnths(rvlBean.getRv_drtn_mnths());
				 rvd.setRv_no_vlntr(rvlBean.getRv_no_vlntr());
				 rvd.setRv_status(rvlBean.getRv_status());
				 rvd.setRv_cmp_cd(rvlBean.getRv_cmp_cd());
				 rvd.setRv_prj_cd(rvlBean.getRv_prj_cd());
				 
				 
				 
				 list.add(rvd);
			 }
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	public ArrayList getRvByCriteriaPerVso(LpiRequestVolunteerModDetails details) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean getRvByCriteriaPerVso");
		ArrayList list = new ArrayList();
		try{
			
			String criteria = "SELECT * FROM lpi_rqst_vlntr a , lpi_prjct b WHERE a.rv_prj_cd = b.prj_cd ";
			if(details.getRv_id()!=null && !details.getRv_id().equals("")){
				criteria += "AND rv_id like '%"+details.getRv_id()+"%' ";
			}
			if(details.getRv_status()!=null && !details.getRv_status().equals("")){
				criteria += "AND rv_status='"+details.getRv_status()+"' ";
			}
			if(details.getRv_drtn_mnths()!=null && !details.getRv_drtn_mnths().equals("")){
				criteria += "AND rv_drtn_mnths='"+details.getRv_drtn_mnths()+"'";
			}
			if(details.getRv_no_vlntr()>0){
				criteria += "AND a.rv_no_vlntr ="+details.getRv_no_vlntr()+" ";
			}
			
			if(details.getRv_prj_cd()!=0 ){
				criteria += "AND b.prj_id ="+details.getRv_prj_cd()+" ";
			}
			System.out.println("criteria >> "+criteria);
			
			 String orderBy = " ORDER BY a.rv_id;";
			
			 LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService); 
			 ArrayList rvlist = rvDB.getByCriteria(criteria + orderBy);
			 Iterator i = rvlist.iterator();
			 while(i.hasNext()){
				 LpiRequestVolunteerBean rvlBean = (LpiRequestVolunteerBean)i.next();
				 LpiRequestVolunteerModDetails rvd = new LpiRequestVolunteerModDetails();
				 rvd.setRv_cd(rvlBean.getRv_cd());
				 rvd.setRv_id(rvlBean.getRv_id());
				 rvd.setRv_drtn_mnths(rvlBean.getRv_drtn_mnths());
				 rvd.setRv_no_vlntr(rvlBean.getRv_no_vlntr());
				 rvd.setRv_status(rvlBean.getRv_status());
				 list.add(rvd);
			 }
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return list;
	}
	
	
	
	
	
	

}
