package com.txn;

import com.bean.LpiRequestVolunteerExtensionAssignmentBean;
import com.databaseBean.AdSequence;
import com.databaseBean.DatabaseService;
import com.databaseBean.LpiRequestVolunteerExtensionAssignmentDB;

public class LpiRequestVolunteerExtensionAssignmentControllerBean {
	DatabaseService databaseService = new DatabaseService();
	public LpiRequestVolunteerExtensionAssignmentControllerBean(){;}
	
	
	
	public int createRVA(LpiRequestVolunteerExtensionAssignmentBean rva) throws Exception{
		System.out.println("LpiRequestVolunteerControllerBean createRVA");
		int rv_cd =0;
		try{
			
			LpiRequestVolunteerExtensionAssignmentDB rvaDB = new LpiRequestVolunteerExtensionAssignmentDB(databaseService);
			
			if(rva.getRva_id()==null || rva.getRva_id().equals("")){
				AdSequence sqnc = new AdSequence(databaseService);
				rva.setRva_id(sqnc.getLpiRvaId());
			}
			
			rv_cd = rvaDB.CREATE(rva);
			
			
			/*LpiRequestVolunteerDB rvDB = new LpiRequestVolunteerDB(databaseService);
			LpiRequestVolunteerBean rvBean = new LpiRequestVolunteerBean();
			rvBean.setRv_cd(rv.getRv_cd());
			rvBean.setRv_cmp_cd(rv.getRv_cmp_cd());
			String rv_id=rv.getRv_id();
			if(rv.getRv_id().toString().trim().length()==0){
				AdSequence sqnc = new AdSequence(databaseService);
				rv_id = sqnc.getLpiRvId();
			}
			rvBean.setRv_id(rv_id);
			rvBean.setRv_status(rv.getRv_status());
			rvBean.setRv_prj_cd(rv.getRv_prj_cd());
			rvBean.setRv_no_vlntr(rv.getRv_no_vlntr());
			rvBean.setRv_drtn_mnths(rv.getRv_drtn_mnths());
			rv_cd = rvDB.CREATE(rvBean);
			
			for(int i = 1; i<=rv.getRv_no_vlntr(); i++){
				LpiRequestVolunteerLineDB rvlDB = new LpiRequestVolunteerLineDB(databaseService);
				LpiRequestVolunteerLineBean rvln = new LpiRequestVolunteerLineBean();
				rvln.setRvl_ln_no(i);
				rvln.setRvl_rv_cd(rv_cd);
				rvlDB.CREATE(rvln);
			}*/
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		return rv_cd;
	} 
	
	/*
	
	private AdCompanyModDetails toModDetails(AdCompanyBean cmpBean){
		AdCompanyModDetails details = new AdCompanyModDetails();
		details.setCmp_id(cmpBean.getCmp_id());
		details.setCmp_nm(cmpBean.getCmp_nm());
		details.setCmp_dscrptn(cmpBean.getCmp_dscrptn());
		details.setCmp_typ(cmpBean.getCmp_typ());
		return details;
		
	}
	
	
	public AdCompanyModDetails getCompanyByCmpName(String Cmp_Name) throws Exception {
		System.out.println("AdCompanyControllerBean getCompanyByCmpName");
		AdCompanyModDetails details = new AdCompanyModDetails();
		try {
			AdCompanyDB cmpDB = new AdCompanyDB(databaseService);
			AdCompanyBean cmpBean = cmpDB.getCompanyByCmpName(Cmp_Name);
			details = this.toModDetails(cmpBean);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	*/
	

}
