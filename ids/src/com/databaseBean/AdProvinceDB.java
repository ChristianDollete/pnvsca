package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.AdAddProvinceBean;

public class AdProvinceDB extends JdbcDao{
	public AdProvinceDB(DatabaseService databaseService) {
		super(databaseService);
	}
	

	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private static final String GET_ALL_PRVNC ="SELECT * FROM ad_prvnc";
	private static final String GET_ALL_PRVNC_BY_RGN_ID ="SELECT * FROM ad_prvnc WHERE RGN_ID=?";
	private static final String GET_ALL_PRVNC_BY_PRVNC_NM ="SELECT * FROM ad_prvnc WHERE PRVNC_NM=?";
	
	public ArrayList getAllProvince() throws Exception{
		System.out.println("AdProvinceDB getAllProvince");
		ArrayList provinceList = new ArrayList();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_PRVNC);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddProvinceBean details = new AdAddProvinceBean();
				details.setPrvnc_id(resultSet.getInt("prvnc_id"));
				details.setPrvnc_nm(resultSet.getString("prvnc_nm"));
				details.setPrvnc_dscrptn(resultSet.getString("prvnc_dscrptn"));
				details.setPrvnc_rgn_id(resultSet.getInt("rgn_id"));
				provinceList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdProvinceDB getAllProvince SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return provinceList;
	}
	
	public ArrayList getAllProvinceByRegionId(int rgn_id) throws Exception{
		System.out.println("AdProvinceDB getAllProvinceByRegionId"); 
		ArrayList provinceList = new ArrayList();
		
		//System.out.println("RGN ID >> "+ rgn_id);
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_PRVNC_BY_RGN_ID);
			statement.setInt(1, rgn_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddProvinceBean details = new AdAddProvinceBean();
				//System.out.println("1 >>> ");
				details.setPrvnc_id(resultSet.getInt("prvnc_id"));
				details.setPrvnc_nm(resultSet.getString("prvnc_nm"));
				details.setPrvnc_dscrptn(resultSet.getString("prvnc_dscrptn"));
				details.setPrvnc_rgn_id(resultSet.getInt("rgn_id"));
				provinceList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdProvinceDB getAllProvinceByRegionId SQLException"); 
			e.printStackTrace();
			throw new Exception();
		} catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return provinceList;
	}
	
	
	
	public AdAddProvinceBean getProvinceByName(String prvnc_nm) throws Exception{
		System.out.println("AdProvinceDB getProvinceByName"); 
		AdAddProvinceBean details = new AdAddProvinceBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_PRVNC_BY_PRVNC_NM);
			statement.setString(1, prvnc_nm);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details.setPrvnc_id(resultSet.getInt("prvnc_id"));
				details.setPrvnc_nm(resultSet.getString("prvnc_nm"));
				details.setPrvnc_dscrptn(resultSet.getString("prvnc_dscrptn"));
				details.setPrvnc_rgn_id(resultSet.getInt("rgn_id"));
			}
		} catch (SQLException e) {
			System.out.println("AdProvinceDB getProvinceByName SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
		
	}

}
