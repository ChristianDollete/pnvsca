package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.AdAddProvinceBean;
import com.bean.AdUserBean;

public class AdUserDB  extends JdbcDao  {

	public AdUserDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	ResultSet generatedKeys = null;
    int generatedKey = -1;
    
	
	private static final String CREATE = "INSERT INTO ad_usr ("
			+ "usr_usr_nm, usr_usr_psswrd, usr_usr_dscrptn, usr_usr_prmry_email, usr_usr_scndry_email, ad_usr_clsfctn, "
			+ "ad_cmpny) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
	
	
	private static final String UPDATE = "UPDATE ad_usr SET "
			+ "usr_usr_nm=?, usr_usr_psswrd=?, usr_usr_dscrptn=?, usr_usr_prmry_email=?, usr_usr_scndry_email=?, "
			+ "ad_cmpny=?, ad_email=?, ad_email_psswrd=? WHERE usr_id=?";
	
	private static final String GET_USR_BY_USRNM_AND_PSSWRD_AND_CMP_ID = "SELECT * FROM ad_usr WHERE usr_usr_nm=? AND usr_usr_psswrd=? AND ad_cmpny=?";
	private static final String GET_ADMIN = "SELECT * FROM ad_usr WHERE usr_id=?";
		
	
	public int CREATE(AdUserBean usr) throws SQLException {
		System.out.println("AdUserDB  CREATE");
		boolean insert = false;
		int usr_id = 0;
		try {
			///getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"usr_id"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, usr.getUsr_usr_nm());
			statement.setString(2, usr.getUsr_usr_psswrd());
			statement.setString(3, usr.getUsr_usr_dscrptn());
			statement.setString(4, usr.getUsr_usr_prmry_email());
			statement.setString(5, usr.getUsr_usr_scndry_email());
			statement.setInt(6, usr.getAd_usr_clsfctn());
			statement.setInt(7, usr.getAd_cmpny());
			statement.executeUpdate();
			
			
			//getDatabaseService().getConnection().commit();
			
			insert = true;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			usr_id = generatedKey;
			
		} catch (SQLException e) {
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return usr_id;
	}
	
	
	public int UPDATE(AdUserBean usr) throws SQLException {
		System.out.println("AdUserDB  UPDATE");
		boolean insert = false;
		int usr_id = 0;
		try {
			///getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"usr_id"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE);
			statement.setString(1, usr.getUsr_usr_nm());
			statement.setString(2, usr.getUsr_usr_psswrd());
			statement.setString(3, usr.getUsr_usr_dscrptn());
			statement.setString(4, usr.getUsr_usr_prmry_email());
			statement.setString(5, usr.getUsr_usr_scndry_email());
			statement.setInt(6, usr.getAd_cmpny());
			statement.setString(7, usr.getUsr_email());
			statement.setString(8, usr.getUsr_email_psswrd());
			statement.setInt(9, usr.getUsr_id());
			System.out.println("UPDATE USR >>"+ statement);
			statement.executeUpdate();
			
			
			//getDatabaseService().getConnection().commit();
			
		/*	insert = true;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);*/
			usr_id = usr.getUsr_id();
			System.out.println("AdUserDB  UPDATE USER ID >> "+ usr_id);
			
			
		} catch (SQLException e) {
			//getDatabaseService().getConnection().rollback();
			
			e.printStackTrace();
			System.out.println("UPDATE USR >>"+ statement);
			throw new SQLException(e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
			System.out.println("UPDATE USR >>"+ statement);
		}
		statement.close();
		return usr_id;
	}
	
	
	public AdUserBean getUserByUserNameAndPassword(int Cmp_Code, String UsrName, String UsrPsswrd)throws Exception{
		System.out.println("AdUserDB getUserByUserNameAndPassword"); 
		int usr_cd = 0;
		AdUserBean details = new AdUserBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_USR_BY_USRNM_AND_PSSWRD_AND_CMP_ID);
			statement.setString(1, UsrName);
			statement.setString(2, UsrPsswrd);
			statement.setInt(3, Cmp_Code);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("AdUserDB getUserByUserNameAndPassword SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
		
	}
	
	public AdUserBean getAdmin(int usr_cd1)throws Exception{
		System.out.println("AdUserDB getAdmin"); 
		int usr_cd = 0;
		AdUserBean details = new AdUserBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ADMIN);
			statement.setInt(1, usr_cd1);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("AdUserDB getAdmin SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
		
	}
	
	private AdUserBean toEntity(ResultSet resultSet) throws Exception{
		AdUserBean details = new AdUserBean();
		try {
			details.setUsr_id(resultSet.getInt("usr_id"));
			details.setUsr_usr_nm(resultSet.getString("usr_usr_nm"));
			details.setUsr_usr_psswrd(resultSet.getString("usr_usr_psswrd"));
			details.setUsr_usr_dscrptn(resultSet.getString("usr_usr_dscrptn"));
			details.setUsr_usr_prmry_email(resultSet.getString("usr_usr_prmry_email"));
			details.setUsr_usr_scndry_email(resultSet.getString("usr_usr_scndry_email"));
			details.setAd_usr_clsfctn(resultSet.getInt("ad_usr_clsfctn"));
			details.setAd_cmpny(resultSet.getInt("ad_cmpny"));
			details.setUsr_email(resultSet.getString("ad_email"));
			details.setUsr_email_psswrd(resultSet.getString("ad_email_psswrd"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return details;
	}

	

}
