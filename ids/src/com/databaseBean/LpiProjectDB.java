package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiLpiBean;
import com.bean.LpiProjectBean;
import com.bean.LpiProjectManagerBean;

public class LpiProjectDB extends JdbcDao{

	public LpiProjectDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	
	private final String CREATE = "INSERT INTO lpi_prjct "
			+ "(prj_id, prj_nm, prj_trgt, prj_drtn_frm, prj_drtn_to, "
			+ "prj_bdgt, prj_src, prj_objctv, prj_dscptn, prj_rgn, "
			+ "prj_prvnc, prj_mncplty, prj_pm_cd, prj_addrss, prj_cmp_cd) "
			+ "VALUES (?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?)";
	private final String UPDATE = "UPDATE lpi_prjct	SET	"
			+ "prj_nm=?, prj_trgt=?, prj_drtn_frm=?, prj_drtn_to=?, prj_bdgt=?, "
			+ "prj_src=?, prj_objctv=?, prj_dscptn=?, prj_rgn=?, prj_prvnc=?, "
			+ "prj_mncplty=?, prj_pm_cd=?, prj_addrss=? WHERE prj_cd=?";
	
	private final String GET_PM_BY_PMCODE = "SELECT * FROM lpi_prjct WHERE prj_cd=?";
	private final String GET_PRJ_BY_PRJ_ID = "SELECT * FROM lpi_prjct WHERE prj_id=?";
	private final String GET_PM_BY_CMPCD = "SELECT * FROM lpi_prjct_mngr WHERE pm_ad_cmpny=?";
	
	
	public int UPDATE(LpiProjectBean prj) throws SQLException, Exception {
		System.out.println("LpiProjectDB UPDATE");
		int insert = 0;
		try {
			String key[] = {"prj_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setString(1, prj.getPrjOrgName());
			statement.setString(2, prj.getPrjTargetBenefits());
			statement.setString(3, prj.getPrjDurationFrom());
			statement.setString(4, prj.getPrjDurationTo());
			statement.setString(5, prj.getPrjBudgetSource());
			statement.setString(6, prj.getPrjSource());
			statement.setString(7, prj.getPrjProjectObjective());
			statement.setString(8, prj.getPrjProjectDesc());
			statement.setInt(9, prj.getPrjRgn());
			statement.setInt(10, prj.getPrjPrvnc());
			statement.setInt(11, prj.getPrjMncplty());
			statement.setInt(12, prj.getPrjProjectManager());
			statement.setString(13, prj.getPrjAddress());
			statement.setInt(14, prj.getPrjCd());
			statement.executeUpdate();
			System.out.println("UPDAT STATEMENT >> "+ statement.toString());
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = prj.getPrjCd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	
	public int CREATE(LpiProjectBean prj) throws SQLException, Exception {
		System.out.println("LpiProjectDB CREATE");
		int insert = 0;
		try {
			String key[] = {"prj_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, prj.getPrjId());
			statement.setString(2, prj.getPrjOrgName());
			statement.setString(3, prj.getPrjTargetBenefits());
			statement.setString(4, prj.getPrjDurationFrom());
			statement.setString(5, prj.getPrjDurationTo());
			statement.setString(6, prj.getPrjBudgetSource());
			statement.setString(7, prj.getPrjSource());
			statement.setString(8, prj.getPrjProjectObjective());
			statement.setString(9, prj.getPrjProjectDesc());
			statement.setInt(10, prj.getPrjRgn());
			statement.setInt(11, prj.getPrjPrvnc());
			statement.setInt(12, prj.getPrjMncplty());
			statement.setInt(13, prj.getPrjProjectManager());
			statement.setString(14, prj.getPrjAddress());
			statement.setInt(15, prj.getPrjCmpCode());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	
	
	
	
	
	
	public LpiProjectBean getPrjByPrjCode(int prj_cd)throws Exception{
		System.out.println("LpiProjectDB getPrjByPrjCode");
		LpiProjectBean details = new LpiProjectBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_PMCODE);
			statement.setInt(1, prj_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectDB getPrjByPrjCode SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public LpiProjectBean getPrjByPrjId(String prj_id)throws Exception{
		System.out.println("LpiProjectDB getPrjByPrjId");
		LpiProjectBean details = new LpiProjectBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PRJ_BY_PRJ_ID);
			statement.setString(1, prj_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectDB getPmByPmCode SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	/*
	
	public LpiProjectManagerBean getPmByPmId(String pm_id)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByPmId");
		LpiProjectManagerBean details = new LpiProjectManagerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_PMID);
			statement.setString(1, pm_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByPmId SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
	}
	
	
	public ArrayList getPmByLpiCd(int lpi_cd)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByLpiCd");
		ArrayList pmList = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_CMPCD);
			statement.setInt(1, lpi_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiProjectManagerBean details = new LpiProjectManagerBean();
				details = toEntity(resultSet);
				pmList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByLpiCd SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}
		return pmList;
	}
	
	
	public ArrayList getByCriteria(String criteria)throws Exception{
		System.out.println("LpiProjectManagerDB getByCriteria");
		ArrayList pmList = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(criteria);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiProjectManagerBean details = new LpiProjectManagerBean();
				details = toEntity(resultSet);
				pmList.add(details);
				
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getByCriteria SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}
		return pmList;
	}
	
	*/
	
	
	
	public ArrayList getByCriteria(String criteria)throws Exception{
		System.out.println("LpiProjectDB getByCriteria");
		ArrayList prjList = new ArrayList();
		try {
			System.out.println("criteria >> "+criteria);
			statement = getDatabaseService().getConnection().prepareStatement(criteria);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiProjectBean details = new LpiProjectBean();
				details = toEntity(resultSet);
				prjList.add(details);
				
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getByCriteria SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return prjList;
	}
	
	private LpiProjectBean toEntity(ResultSet resultSet)throws Exception{
		LpiProjectBean details = new LpiProjectBean();
		try {
			details.setPrjCd(resultSet.getInt("prj_cd"));
			details.setPrjId(resultSet.getString("prj_id"));
			details.setPrjOrgName(resultSet.getString("prj_nm"));
			details.setPrjTargetBenefits(resultSet.getString("prj_trgt"));
			details.setPrjDurationFrom(resultSet.getString("prj_drtn_frm"));
			details.setPrjDurationTo(resultSet.getString("prj_drtn_to"));
			details.setPrjBudgetSource(resultSet.getString("prj_bdgt"));
			details.setPrjSource(resultSet.getString("prj_src"));
			details.setPrjProjectObjective(resultSet.getString("prj_objctv"));
			details.setPrjProjectDesc(resultSet.getString("prj_dscptn"));
			details.setPrjRgn(resultSet.getInt("prj_rgn"));
			details.setPrjPrvnc(resultSet.getInt("prj_prvnc"));
			details.setPrjMncplty(resultSet.getInt("prj_mncplty"));
			details.setPrjProjectManager(resultSet.getInt("prj_pm_cd"));
			details.setPrjAddress(resultSet.getString("prj_addrss"));
			details.setPrjCmpCode(resultSet.getInt("prj_cmp_cd"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	
	
	
	
	

}
