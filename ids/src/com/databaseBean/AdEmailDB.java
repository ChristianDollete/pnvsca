package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.AdEmailBean;

public class AdEmailDB  extends JdbcDao  {

	public AdEmailDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	ResultSet generatedKeys = null;
    int generatedKey = -1;
    
	
	
	
	
	private static final String UPDATE = "UPDATE ad_email SET "
			+ "eml_nm=?, eml_dflt_sbjct=?, eml_dscptn=?, eml_applcnt=?, eml_enble=?, eml_ad_cmpny=?, "
			+ "eml_ad_email_addrss=?, eml_ad_email_psswrd=? "
			+ "	WHERE eml_cd=?";
	
	private static final String GET_ALL_EMAIL = "SELECT * FROM ad_email ORDER BY eml_cd";
	private static final String GET_EMAIL_BY_EML_CD = "SELECT * FROM ad_email WHERE eml_cd=?";
	
	/*
	public int CREATE(AdUserBean usr) throws SQLException {
		System.out.println("AdUserDB  CREATE");
		boolean insert = false;
		int usr_id = 0;
		try {
			///getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"usr_id"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, usr.getUsr_usr_nm());
			statement.setString(2, usr.getUsr_usr_psswrd());
			statement.setString(3, usr.getUsr_usr_dscrptn());
			statement.setString(4, usr.getUsr_usr_prmry_email());
			statement.setString(5, usr.getUsr_usr_scndry_email());
			statement.setInt(6, usr.getAd_usr_clsfctn());
			statement.setInt(7, usr.getAd_cmpny());
			statement.executeUpdate();
			
			
			//getDatabaseService().getConnection().commit();
			
			insert = true;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			usr_id = generatedKey;
			
		} catch (SQLException e) {
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		return usr_id;
	}*/
	
	
	public int UPDATE(AdEmailBean eml) throws SQLException {
		System.out.println("AdEmailDB  UPDATE");
		boolean insert = false;
		int eml_cd = 0;
		try {
			///getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"usr_id"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setString(1, eml.getEmlName());
			statement.setString(2, eml.getEmlDefaultSubject());
			statement.setString(3, eml.getEmlDescription());
			int app = eml.getEmlCd();
			/*if(eml.getEmlName().equalsIgnoreCase("")){
				app = 1
			}*/
			statement.setInt(4, app);
			statement.setInt(5, eml.getEmlEnabled());
			statement.setInt(6, 1);
			statement.setString(7, eml.getEmlEmailAddress());
			statement.setString(8, eml.getEmlEmailPassword());
			statement.setInt(9, eml.getEmlCd());
			
			System.out.println("UPDATE USR >>"+ statement);
			statement.executeUpdate();
			
			
			//getDatabaseService().getConnection().commit();
			
			insert = true;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			eml_cd = eml.getEmlCd();
			
		} catch (SQLException e) {
			//getDatabaseService().getConnection().rollback();
			
			e.printStackTrace();
			System.out.println("UPDATE USR >>"+ statement);
			throw new SQLException(e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
			System.out.println("UPDATE USR >>"+ statement);
		}
		statement.close();
		return eml_cd;
	}
	
	/*
	public AdUserBean getUserByUserNameAndPassword(int Cmp_Code, String UsrName, String UsrPsswrd)throws Exception{
		System.out.println("AdUserDB getUserByUserNameAndPassword"); 
		int usr_cd = 0;
		AdUserBean details = new AdUserBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_USR_BY_USRNM_AND_PSSWRD_AND_CMP_ID);
			statement.setString(1, UsrName);
			statement.setString(2, UsrPsswrd);
			statement.setInt(3, Cmp_Code);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("AdUserDB getUserByUserNameAndPassword SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
		
	}
	*/
	
	public ArrayList getAllEmail()throws Exception{
		System.out.println("AdEmailDB getAllEmail"); 
		int usr_cd = 0;
		ArrayList emailList = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_EMAIL);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdEmailBean details = toEntity(resultSet);
				emailList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdEmailDB getAllEmail SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return emailList;
		
	}
	
	public AdEmailBean getEmailbyEMLCode(int eml_cd)throws Exception{
		System.out.println("AdEmailDB getEmailbyEMLCode"); 
		int usr_cd = 0;
		AdEmailBean details = new AdEmailBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_EMAIL_BY_EML_CD);
			statement.setInt(1, eml_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
				
			}
		} catch (SQLException e) {
			System.out.println("AdEmailDB getEmailbyEMLCode SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
		
	}
	
	
	
	private AdEmailBean toEntity(ResultSet resultSet) throws Exception{
		AdEmailBean details = new AdEmailBean();
		try {
			
			details.setEmlCd(resultSet.getInt("eml_cd"));
			details.setEmlName(resultSet.getString("eml_nm"));
			details.setEmlDefaultSubject(resultSet.getString("eml_dflt_sbjct"));
			details.setEmlDescription(resultSet.getString("eml_dscptn"));
			details.setEmlApplication(resultSet.getString("eml_nm"));
			details.setEmlEnabled(resultSet.getInt("eml_enble"));
			details.setEmlCmpCode(resultSet.getInt("eml_ad_cmpny"));
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return details;
	}

	

}
