package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.AdAddMunicipalityBean;

public class AdMunicipalityDB extends JdbcDao{
	public AdMunicipalityDB(DatabaseService databaseService) {
		super(databaseService);
	}
	

	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private static final String GET_ALL_MNCPLTY ="SELECT * FROM AD_MNCPLTY";
	private static final String GET_ALL_MNCPLTY_BY_PRVNC_ID ="SELECT * FROM AD_MNCPLTY WHERE PRVNC_ID=?";
	private static final String GET_ALL_MNCPLTY_BY_MUN_NM ="SELECT * FROM AD_MNCPLTY WHERE MUN_NM=?";
	
	public ArrayList getAllMunicipality() throws Exception{
		System.out.println("AdMunicipalityDB getAllMunicipality");
		ArrayList municipalityList = new ArrayList();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_MNCPLTY);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddMunicipalityBean details = new AdAddMunicipalityBean();
				details.setMun_id(resultSet.getInt("mun_id"));
				details.setMun_nm(resultSet.getString("mun_nm"));
				details.setMun_dscrptn(resultSet.getString("mun_dscrptn"));
				details.setMun_prvnc_id(resultSet.getInt("prvnc_id"));
				municipalityList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdMunicipalityDB getAllMunicipality SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return municipalityList;
	}
	
	public ArrayList getAllMunicipalityByProvinceId(int prvnc_id) throws Exception{
		System.out.println("AdMunicipalityDB getAllMunicipalityByProvinceId"); 
		ArrayList municipalityList = new ArrayList();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_MNCPLTY_BY_PRVNC_ID);
			statement.setInt(1, prvnc_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddMunicipalityBean details = new AdAddMunicipalityBean();
				details.setMun_id(resultSet.getInt("mun_id"));
				details.setMun_nm(resultSet.getString("mun_nm"));
				details.setMun_dscrptn(resultSet.getString("mun_dscrptn"));
				details.setMun_prvnc_id(resultSet.getInt("prvnc_id"));
				municipalityList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdMunicipalityDB getAllMunicipalityByProvinceId SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return municipalityList;
	}
	
	
	
	public AdAddMunicipalityBean getAllMunicipalityByMunName(String mun_nm) throws Exception{
		System.out.println("AdProvinceDB getProvinceByName"); 
		AdAddMunicipalityBean details = new AdAddMunicipalityBean();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_MNCPLTY_BY_MUN_NM);
			statement.setString(1, mun_nm);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details.setMun_id(resultSet.getInt("mun_id"));
				details.setMun_nm(resultSet.getString("mun_nm"));
				details.setMun_dscrptn(resultSet.getString("mun_dscrptn"));
				details.setMun_prvnc_id(resultSet.getInt("prvnc_id"));
			}
		} catch (SQLException e) {
			System.out.println("AdMunicipalityDB getAllMunicipalityByProvinceId SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
		
	}

}
