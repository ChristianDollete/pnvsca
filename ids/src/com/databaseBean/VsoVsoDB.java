package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiLpiBean;
import com.bean.VsoVolunteerBean;
import com.bean.VsoVsoBean;
import com.web.util.VsoVsoModDetails;

public class VsoVsoDB extends JdbcDao {

	public VsoVsoDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private final String CREATE = "INSERT INTO vso_vso	("
			+ "vso_id, vso_nm, vso_org_typ, vso_org_hd_nm, vso_org_hd_pstn, "
			+ "vso_org_rgn, vso_org_prvnc, vso_org_mncplty, vso_org_addrss, vso_org_tlph_nmbr, "
			+ "vso_org_fx_nmbr, vso_org_email, vso_org_wbst, vso_sec_rgstrtn_nmbr, vso_hd_offc_addrss, "
			+ "vso_cntry, vso_dt_estblishd, vso_vsn, vso_mssn, vso_goal, vso_annl_bdgt, "
			+ "vso_bdgt_scrc, vso_rcrtment, vso_enggmnt, vso_cpblty_bldng, vso_prmtn, "
			+ "vso_ntwrkng_of_vlntrs, vso_othrs1, vso_lvng, vso_meal, vso_insrnc, "
			+ "vso_rcgntn_or_awrd, vso_trvl_allwnc, vso_hsng, vso_trnng_and_ornttn, vso_othrs2, "
			+ "vso_usr_nm, vso_psswrd, vso_prmry_email, vso_scndry_email, "
			+ "vso_crdntr_nm, vso_crdntr_pstn, vso_crdntr_dprtmnt, vso_crdntr_tlphn_nmbr, vso_crdntr_mbl_nmbr, vso_cmp_cd, vso_othrs_nt1, vso_othrs_nt2 )	VALUES ("
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, ?, ?, ? )";
	
	
	
	private final String UPDATE = "	UPDATE vso_vso	SET "
		+" vso_id=?, "
		+" vso_nm=?, "
		+" vso_org_typ=?, "
		+" vso_org_hd_nm=?, "
		+" vso_org_hd_pstn=?, "
		+" vso_org_rgn=?, "
		+" vso_org_prvnc=?, "
		+" vso_org_mncplty=?, "
		+" vso_org_addrss=?, "
		+" vso_org_tlph_nmbr=?, "
		+" vso_org_fx_nmbr=?, "
		+" vso_org_email=?, "
		+" vso_org_wbst=?, "
		+" vso_sec_rgstrtn_nmbr=?, "
		+" vso_hd_offc_addrss=?, "
		+" vso_cntry=?, "
		+" vso_dt_estblishd=?, "
		+" vso_vsn=?, "
		+" vso_mssn=?, "
		+" vso_goal=?, "
		+" vso_annl_bdgt=?, "
		+" vso_bdgt_scrc=?, "
		+" vso_rcrtment=?, "
		+" vso_enggmnt=?, "
		+" vso_cpblty_bldng=?, "
		+" vso_prmtn=?, "
		+" vso_ntwrkng_of_vlntrs=?, "
		+" vso_othrs1=?, "
		+" vso_lvng=?, "
		+" vso_meal=?, "
		+" vso_insrnc=?, "
		+" vso_rcgntn_or_awrd=?, "
		+" vso_trvl_allwnc=?, "
		+" vso_hsng=?, "
		+" vso_trnng_and_ornttn=?, "
		+" vso_othrs2=?, "
		+" vso_usr_nm=?, "
		+" vso_psswrd=?, "
		+" vso_prmry_email=?, "
		+" vso_scndry_email=?, "
		+" vso_crdntr_nm=?, "
		+" vso_crdntr_pstn=?, "
		+" vso_crdntr_dprtmnt=?, "
		+" vso_crdntr_tlphn_nmbr=?, "
		+" vso_crdntr_mbl_nmbr=?, "
		+" vso_cmp_cd=?, "
		+" vso_othrs_nt1=? ,"
		+" vso_othrs_nt2=? "
		+" WHERE vso_cd=? ";
	
	
	private final String GET_ALL_VSO = "SELECT * FROM vso_vso";
	private final String GET_VSO_BY_VSO_ID = "SELECT * FROM vso_vso WHERE vso_id=?";
	private final String GET_VSO_BY_VSO_CD = "SELECT * FROM vso_vso WHERE vso_cmp_cd=?";
	
	public ArrayList getAllVso()throws Exception {
		System.out.println("VsoVsoDB getAllVso");
		ArrayList list = new ArrayList();
		
		try{
			String key[] = {"vso_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_VSO,key);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VsoVsoModDetails details = new VsoVsoModDetails();
				details = this.toEntity(resultSet);
				list.add(details);
			}
		}catch (Exception e){
			list = new ArrayList();
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}
	
	
	public VsoVsoModDetails getVsoByVsoId(String vso_id)throws Exception {
		System.out.println("VsoVsoDB getVsoByVsoId");
		
		VsoVsoModDetails details = new VsoVsoModDetails();
		try{
			String key[] = {"vso_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_VSO_BY_VSO_ID,key);
			statement.setString(1, vso_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				
				details = toEntity(resultSet);
				
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public VsoVsoModDetails getVsoByVsoCmpCode(int vso_cd)throws Exception {
		System.out.println("VsoVsoDB getVsoByVsoCmpCode");
		
		VsoVsoModDetails details = new VsoVsoModDetails();
		try{
			String key[] = {"vso_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_VSO_BY_VSO_CD,key);
			statement.setInt(1, vso_cd);
			System.out.println("STATEMENT >> "+ statement.toString());
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				
				details = toEntity(resultSet);
				
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public int CREATE(VsoVsoModDetails vso) throws SQLException {
		System.out.println("VsoVsoDB CREATE");
		int insert = 0;
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"vso_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE, key);
			statement.setString(1, vso.getVso_id());
			statement.setString(2, vso.getVso_nm());
			statement.setInt(3, vso.getVso_org_typ());
			statement.setString(4, vso.getVso_org_hd_nm());
			statement.setString(5, vso.getVso_org_hd_pstn());
			statement.setInt(6, vso.getVso_org_rgn());
			statement.setInt(7, vso.getVso_org_prvnc());
			statement.setInt(8, vso.getVso_org_mncplty());
			statement.setString(9, vso.getVso_org_addrss());
			statement.setString(10, vso.getVso_org_tlph_nmbr());
			statement.setString(11, vso.getVso_org_fx_nmbr());
			statement.setString(12, vso.getVso_org_email());
			statement.setString(13, vso.getVso_org_wbst());
			statement.setString(14, vso.getVso_sec_rgstrtn_nmbr());
			statement.setString(15, vso.getVso_hd_offc_addrss());
			statement.setInt(16, vso.getVso_cntry());
			statement.setString(17, vso.getVso_dt_estblishd());
			statement.setString(18, vso.getVso_vsn());
			statement.setString(19, vso.getVso_mssn());
			statement.setString(20, vso.getVso_goal());
			statement.setString(21, vso.getVso_annl_bdgt());
			statement.setString(22, vso.getVso_bdgt_scrc());
			statement.setInt(23, vso.getVso_rcrtment());
			statement.setInt(24, vso.getVso_enggmnt());
			statement.setInt(25, vso.getVso_cpblty_bldng());
			statement.setInt(26, vso.getVso_prmtn());
			statement.setInt(27, vso.getVso_ntwrkng_of_vlntrs());
			statement.setInt(28, vso.getVso_othrs1());
			statement.setInt(29, vso.getVso_lvng());
			statement.setInt(30, vso.getVso_meal());
			statement.setInt(31, vso.getVso_insrnc());
			statement.setInt(32, vso.getVso_rcgntn_or_awrd());
			statement.setInt(33, vso.getVso_trvl_allwnc());
			statement.setInt(34, vso.getVso_hsng());
			statement.setInt(35, vso.getVso_trnng_and_ornttn());
			statement.setInt(36, vso.getVso_othrs2());
			statement.setString(37, vso.getVso_usr_nm());
			statement.setString(38, vso.getVso_psswrd());
			statement.setString(39, vso.getVso_prmry_email());
			statement.setString(40, vso.getVso_scndry_email());
			statement.setString(41, vso.getVso_crdntr_nm());
			statement.setString(42, vso.getVso_crdntr_pstn());
			statement.setString(43, vso.getVso_crdntr_dprtmnt());
			statement.setString(44, vso.getVso_crdntr_tlphn_nmbr());
			statement.setString(45, vso.getVso_crdntr_mbl_nmbr());
			statement.setInt(46, vso.getVso_cmp_cd());
			statement.setString(47, vso.getVso_othrs_nt1());
			statement.setString(48, vso.getVso_othrs_nt2());
			statement.executeUpdate();
			//getDatabaseService().getConnection().commit();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
		
	}
	
	
	public int UPDATE(VsoVsoModDetails vso) throws SQLException {
		System.out.println("VsoVsoDB UPDATE");
		int insert = 0;
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			String key[] = {"vso_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE, key);
			statement.setString(1, vso.getVso_id());
			statement.setString(2, vso.getVso_nm());
			statement.setInt(3, vso.getVso_org_typ());
			statement.setString(4, vso.getVso_org_hd_nm());
			statement.setString(5, vso.getVso_org_hd_pstn());
			statement.setInt(6, vso.getVso_org_rgn());
			statement.setInt(7, vso.getVso_org_prvnc());
			statement.setInt(8, vso.getVso_org_mncplty());
			statement.setString(9, vso.getVso_org_addrss());
			statement.setString(10, vso.getVso_org_tlph_nmbr());
			statement.setString(11, vso.getVso_org_fx_nmbr());
			statement.setString(12, vso.getVso_org_email());
			statement.setString(13, vso.getVso_org_wbst());
			statement.setString(14, vso.getVso_sec_rgstrtn_nmbr());
			statement.setString(15, vso.getVso_hd_offc_addrss());
			statement.setInt(16, vso.getVso_cntry());
			statement.setString(17, vso.getVso_dt_estblishd());
			statement.setString(18, vso.getVso_vsn());
			statement.setString(19, vso.getVso_mssn());
			statement.setString(20, vso.getVso_goal());
			statement.setString(21, vso.getVso_annl_bdgt());
			statement.setString(22, vso.getVso_bdgt_scrc());
			statement.setInt(23, vso.getVso_rcrtment());
			statement.setInt(24, vso.getVso_enggmnt());
			statement.setInt(25, vso.getVso_cpblty_bldng());
			statement.setInt(26, vso.getVso_prmtn());
			statement.setInt(27, vso.getVso_ntwrkng_of_vlntrs());
			statement.setInt(28, vso.getVso_othrs1());
			statement.setInt(29, vso.getVso_lvng());
			statement.setInt(30, vso.getVso_meal());
			statement.setInt(31, vso.getVso_insrnc());
			statement.setInt(32, vso.getVso_rcgntn_or_awrd());
			statement.setInt(33, vso.getVso_trvl_allwnc());
			statement.setInt(34, vso.getVso_hsng());
			statement.setInt(35, vso.getVso_trnng_and_ornttn());
			statement.setInt(36, vso.getVso_othrs2());
			statement.setString(37, vso.getVso_usr_nm());
			statement.setString(38, vso.getVso_psswrd());
			statement.setString(39, vso.getVso_prmry_email());
			statement.setString(40, vso.getVso_scndry_email());
			statement.setString(41, vso.getVso_crdntr_nm());
			statement.setString(42, vso.getVso_crdntr_pstn());
			statement.setString(43, vso.getVso_crdntr_dprtmnt());
			statement.setString(44, vso.getVso_crdntr_tlphn_nmbr());
			statement.setString(45, vso.getVso_crdntr_mbl_nmbr());
			statement.setInt(46, vso.getVso_cmp_cd());
			statement.setString(47, vso.getVso_othrs_nt1());
			statement.setString(48, vso.getVso_othrs_nt2());
			statement.setInt(49, vso.getVso_cd());
			statement.executeUpdate();
			//getDatabaseService().getConnection().commit();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = vso.getVso_cd();
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
		
	}
	
	
	public ArrayList getAllVsoByCriteria(String CRITERIA)throws Exception {
		System.out.println("VsoVsoDB getAllVsoByCriteria");
		ArrayList list = new ArrayList();
		
		try{
			String key[] = {"vso_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(CRITERIA);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VsoVsoModDetails details = new VsoVsoModDetails();
				details = this.toEntity(resultSet);
				list.add(details);
			}
		}catch (Exception e){
			list = new ArrayList();
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		
		return list;
	}
	
	
	
	private VsoVsoModDetails toEntity(ResultSet resultSet)throws Exception{
		VsoVsoModDetails vso = new VsoVsoModDetails();
		try {


			vso.setVso_cd(resultSet.getInt("vso_cd"));
			vso.setVso_id(resultSet.getString("vso_id"));
			vso.setVso_nm(resultSet.getString("vso_nm"));
			vso.setVso_org_typ(resultSet.getInt("vso_org_typ"));
			vso.setVso_org_hd_nm(resultSet.getString("vso_org_hd_nm"));
			vso.setVso_org_hd_pstn(resultSet.getString("vso_org_hd_pstn"));
			vso.setVso_org_rgn(resultSet.getInt("vso_org_rgn"));
			vso.setVso_org_prvnc(resultSet.getInt("vso_org_prvnc"));
			vso.setVso_org_mncplty(resultSet.getInt("vso_org_mncplty"));
			vso.setVso_org_addrss(resultSet.getString("vso_org_addrss"));
			vso.setVso_org_tlph_nmbr(resultSet.getString("vso_org_tlph_nmbr"));
			vso.setVso_org_fx_nmbr(resultSet.getString("vso_org_fx_nmbr"));
			vso.setVso_org_email(resultSet.getString("vso_org_email"));
			vso.setVso_org_wbst(resultSet.getString("vso_org_wbst"));
			vso.setVso_sec_rgstrtn_nmbr(resultSet.getString("vso_sec_rgstrtn_nmbr"));
			vso.setVso_hd_offc_addrss(resultSet.getString("vso_hd_offc_addrss"));
			vso.setVso_cntry(resultSet.getInt("vso_cntry"));
			vso.setVso_dt_estblishd(resultSet.getString("vso_dt_estblishd"));
			vso.setVso_vsn(resultSet.getString("vso_vsn"));
			vso.setVso_mssn(resultSet.getString("vso_mssn"));
			vso.setVso_goal(resultSet.getString("vso_goal"));
			vso.setVso_annl_bdgt(resultSet.getString("vso_annl_bdgt"));
			vso.setVso_bdgt_scrc(resultSet.getString("vso_bdgt_scrc"));
			vso.setVso_rcrtment(resultSet.getInt("vso_rcrtment"));
			vso.setVso_enggmnt(resultSet.getInt("vso_enggmnt"));
			vso.setVso_cpblty_bldng(resultSet.getInt("vso_cpblty_bldng"));
			vso.setVso_prmtn(resultSet.getInt("vso_prmtn"));
			vso.setVso_ntwrkng_of_vlntrs(resultSet.getInt("vso_ntwrkng_of_vlntrs"));
			vso.setVso_othrs1(resultSet.getInt("vso_othrs1"));
			vso.setVso_lvng(resultSet.getInt("vso_lvng"));
			vso.setVso_meal(resultSet.getInt("vso_meal"));
			vso.setVso_insrnc(resultSet.getInt("vso_insrnc"));
			vso.setVso_rcgntn_or_awrd(resultSet.getInt("vso_rcgntn_or_awrd"));
			vso.setVso_trvl_allwnc(resultSet.getInt("vso_trvl_allwnc"));
			vso.setVso_hsng(resultSet.getInt("vso_hsng"));
			vso.setVso_trnng_and_ornttn(resultSet.getInt("vso_trnng_and_ornttn"));
			vso.setVso_othrs2(resultSet.getInt("vso_othrs2"));
			vso.setVso_usr_nm(resultSet.getString("vso_usr_nm"));
			vso.setVso_psswrd(resultSet.getString("vso_psswrd"));
			vso.setVso_prmry_email(resultSet.getString("vso_prmry_email"));
			vso.setVso_scndry_email(resultSet.getString("vso_prmry_email"));
			vso.setVso_crdntr_nm(resultSet.getString("vso_crdntr_nm"));
			vso.setVso_crdntr_pstn(resultSet.getString("vso_crdntr_pstn"));
			vso.setVso_crdntr_dprtmnt(resultSet.getString("vso_crdntr_dprtmnt"));
			vso.setVso_crdntr_tlphn_nmbr(resultSet.getString("vso_crdntr_tlphn_nmbr"));
			vso.setVso_crdntr_mbl_nmbr(resultSet.getString("vso_crdntr_mbl_nmbr"));
			vso.setVso_cmp_cd(resultSet.getInt("vso_cmp_cd"));
			vso.setVso_othrs_nt1(resultSet.getString("vso_othrs_nt1"));
			vso.setVso_othrs_nt2(resultSet.getString("vso_othrs_nt2"));
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return vso;
		
	}
	
	
	

}
