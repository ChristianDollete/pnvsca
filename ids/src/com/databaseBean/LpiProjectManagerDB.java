package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiLpiBean;
import com.bean.LpiProjectManagerBean;

public class LpiProjectManagerDB extends JdbcDao{

	public LpiProjectManagerDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	
	private final String CREATE = "INSERT INTO lpi_prjct_mngr "
			+ "(pm_id, pm_nm, pm_sex, pm_pstn, pm_eml, pm_tel_no, pm_mob_no, pm_fx_no, pm_usr_nm, pm_usr_psswrd, pm_ad_cmpny, pm_usr_cd) "
			+ "VALUES "
			+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
	private final String GET_PM_BY_PMCODE = "SELECT * FROM lpi_prjct_mngr WHERE pm_cd=?";
	private final String GET_PM_BY_PMID = "SELECT * FROM lpi_prjct_mngr WHERE pm_id=?";

	private final String GET_PM_BY_PM_USR_CD = "SELECT * FROM lpi_prjct_mngr a, ad_usr b WHERE a.pm_usr_cd = b.usr_id AND b.usr_id=?";
	private final String GET_PM_BY_CMPCD = "SELECT * FROM lpi_prjct_mngr WHERE pm_ad_cmpny=?";
	private final String UPDATE = "UPDATE lpi_prjct_mngr SET "
			+ "pm_nm=?, pm_sex=?, pm_pstn=?, pm_eml=?, pm_tel_no=?,	"
			+ "pm_mob_no=?, pm_fx_no=?,	pm_usr_nm=?, pm_usr_psswrd=?"
			+ " WHERE pm_id=?";
	
	public int UPDATE(com.bean.LpiProjectManagerBean pm) throws SQLException, Exception {
		System.out.println("LpiProjectManagerDB UPDATE");
		int insert = 0;
		try {
			String key[] = {"pm_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setString(1, pm.getPmName());
			statement.setString(2, pm.getSex());
			statement.setString(3, pm.getPmPosition());
			statement.setString(4, pm.getPmEmail());
			statement.setString(5, pm.getPmTelNo());
			statement.setString(6, pm.getPmMobNo());
			statement.setString(7, pm.getPmFaxNo());
			statement.setString(8, pm.getPmUserName());
			statement.setString(9, pm.getPmUserPassword());
			statement.setString(10, pm.getPmId());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = pm.getPmCd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	
	
	
	public int CREATE(com.bean.LpiProjectManagerBean pm) throws SQLException, Exception {
		System.out.println("LpiProjectManagerDB CREATE");
		int insert = 0;
		try {
			String key[] = {"pm_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, pm.getPmId());
			statement.setString(2, pm.getPmName());
			statement.setString(3, pm.getSex());
			statement.setString(4, pm.getPmPosition());
			statement.setString(5, pm.getPmEmail());
			statement.setString(6, pm.getPmTelNo());
			statement.setString(7, pm.getPmMobNo());
			statement.setString(8, pm.getPmFaxNo());
			statement.setString(9, pm.getPmUserName());
			statement.setString(10, pm.getPmUserPassword());
			statement.setInt(11, pm.getPm_cmpny_id());
			statement.setInt(12, pm.getPm_usr_cd());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public LpiProjectManagerBean getPmByPmCode(int pm_cd)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByPmCode");
		LpiProjectManagerBean details = new LpiProjectManagerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_PMCODE);
			statement.setInt(1, pm_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByPmCode SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
		
	
	public LpiProjectManagerBean getPmByPmId(String pm_id)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByPmId");
		LpiProjectManagerBean details = new LpiProjectManagerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_PMID);
			statement.setString(1, pm_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByPmId SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public LpiProjectManagerBean getPmByUsrCode(int  pm_usr_cd)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByUsrCode");
		LpiProjectManagerBean details = new LpiProjectManagerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_PM_USR_CD);
			statement.setInt(1, pm_usr_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByPmId SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	
	public ArrayList getPmByLpiCd(int lpi_cd)throws Exception{
		System.out.println("LpiProjectManagerDB getPmByLpiCd");
		ArrayList pmList = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_PM_BY_CMPCD);
			statement.setInt(1, lpi_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiProjectManagerBean details = new LpiProjectManagerBean();
				details = toEntity(resultSet);
				pmList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getPmByLpiCd SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return pmList;
	}
	
	
	public ArrayList getByCriteria(String criteria)throws Exception{
		System.out.println("LpiProjectManagerDB getByCriteria");
		ArrayList pmList = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(criteria);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiProjectManagerBean details = new LpiProjectManagerBean();
				details = toEntity(resultSet);
				pmList.add(details);
				
			}
		} catch (SQLException e) {
			System.out.println("LpiProjectManagerDB getByCriteria SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return pmList;
	}
	
	
	private LpiProjectManagerBean toEntity(ResultSet resultSet)throws Exception{
		LpiProjectManagerBean details = new LpiProjectManagerBean();
		try {
		
			details.setPm_cmpny_id(resultSet.getInt("pm_ad_cmpny"));
			details.setPmCd(resultSet.getInt("pm_cd"));
			details.setPmId(resultSet.getString("pm_id"));
			details.setPmName(resultSet.getString("pm_nm"));
			details.setSex(resultSet.getString("pm_sex"));
			details.setPmPosition(resultSet.getString("pm_pstn"));
			details.setPmEmail(resultSet.getString("pm_eml"));
			details.setPmTelNo(resultSet.getString("pm_tel_no"));
			details.setPmMobNo(resultSet.getString("pm_mob_no"));
			details.setPmFaxNo(resultSet.getString("pm_fx_no"));
			details.setPmUserName(resultSet.getString("pm_usr_nm"));
			details.setPmUserPassword(resultSet.getString("pm_usr_psswrd"));
			details.setPm_usr_cd(resultSet.getInt("pm_usr_cd"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		
		return details;
	}
	
	
	
	
	
	

}
