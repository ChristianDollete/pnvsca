package com.databaseBean;



public abstract class JdbcDao {
	
	private DatabaseService databaseService;

	public JdbcDao(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	public DatabaseService getDatabaseService() {
		return databaseService;
	}

	
}
