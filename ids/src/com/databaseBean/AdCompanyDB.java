package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.AdCompanyBean;

public class AdCompanyDB extends JdbcDao {

	public AdCompanyDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;

	private static final String CREATE = "INSERT INTO AD_CMPNY (CMP_NM, CMP_DSCRPTN, cmp_typ) VALUES (?,?,?)";
	private static final String GET_ALL_CMP_BY_CMP_NM = "SELECT * FROM AD_CMPNY WHERE CMP_NM=?";
	private static final String GET_ALL_CMP_BY_CMP_CD = "SELECT * FROM AD_CMPNY WHERE cmp_id=?";
	public boolean CREATE(AdCompanyBean cmp) throws SQLException {
		System.out.println("AdCompanyDB CREATE");
		boolean insert = false;
		try {
			/*getDatabaseService().getConnection().setAutoCommit(false);*/
			statement = getDatabaseService().getConnection().prepareStatement(CREATE);
			statement.setString(1, cmp.getCmp_nm());
			statement.setString(2, cmp.getCmp_dscrptn());
			statement.setInt(3, cmp.getCmp_typ());
			statement.executeUpdate();
			/*getDatabaseService().getConnection().commit();*/
			System.out.println("KEY >> "+statement.RETURN_GENERATED_KEYS);
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			/*getDatabaseService().getConnection().setAutoCommit(false);*/
			/*getDatabaseService().getConnection().rollback();*/
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public AdCompanyBean getCompanyByCmpName(String cmp_nm) throws Exception{
		System.out.println("AdCompanyDB getCompanyByCmpName");
		AdCompanyBean cmpBean = new AdCompanyBean();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_CMP_BY_CMP_NM);
			statement.setString(1, cmp_nm);
			resultSet = statement.executeQuery();
			
			if (resultSet.next()) {
				
				cmpBean = toBean(resultSet);
			}
			if(cmpBean == null){
				throw new Exception();
			}
			
		} catch (SQLException e) {
			System.out.println("AdCompanyDB getCompanyByCmpName SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return cmpBean;
	}
	
	public AdCompanyBean getCompanyByCmpId(int cmp_cd) throws Exception{
		System.out.println("AdCompanyDB getCompanyByCmpId");
		AdCompanyBean cmpBean = new AdCompanyBean();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_CMP_BY_CMP_CD);
			statement.setInt(1, cmp_cd);
			resultSet = statement.executeQuery();
			
			if (resultSet.next()) {
				
				cmpBean = toBean(resultSet);
			}
			if(cmpBean == null){
				throw new Exception();
			}
			
		} catch (SQLException e) {
			System.out.println("AdCompanyDB getCompanyByCmpId SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return cmpBean;
	}
	
	
	
	
	private AdCompanyBean toBean(ResultSet resultSet) throws SQLException{
		AdCompanyBean cmpBean = new AdCompanyBean();
		try {
			cmpBean.setCmp_id(resultSet.getInt("cmp_id"));
			cmpBean.setCmp_nm(resultSet.getString("cmp_nm"));
			cmpBean.setCmp_dscrptn(resultSet.getString("cmp_dscrptn"));
			cmpBean.setCmp_typ(resultSet.getInt("cmp_typ"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new SQLException();
		}
		
		return cmpBean;
		
	}
	
	
	
	

}
