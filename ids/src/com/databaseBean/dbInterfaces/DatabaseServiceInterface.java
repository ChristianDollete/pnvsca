package com.databaseBean.dbInterfaces;

import java.sql.Connection;

public interface DatabaseServiceInterface {
	public Connection getConnection();

}
