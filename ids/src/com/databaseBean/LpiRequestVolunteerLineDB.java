package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiRequestVolunteerBean;
import com.bean.LpiRequestVolunteerLineBean;
import com.web.util.LpiRequestVolunteerModDetails;

public class LpiRequestVolunteerLineDB extends JdbcDao {
	public LpiRequestVolunteerLineDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	
	private final String CREATE ="INSERT INTO lpi_rqst_vlntr_ln	("
			+ "rvl_ln_no, rvl_rgn, rvl_prnvc, rvl_mncplty, "
			+ "rvl_addrss, rvl_dt_frm, rvl_dt_to, rvl_objctv, rvl_output, "
			+ "rvl_actvts, rvl_implmttn_prd, rvl_fndng_src, rvl_fld_spcfctn, rvl_exprnc_trnnng, "
			+ "rvl_edctn, rvl_vso_cd, rvl_vlntr_cd, rvl_dt_arrvl, rvl_emrgncy, "
			+ "rvl_trnsprt, rvl_fclty, rvl_housing, rvl_othrs, rvl_othrs_spcfy, "
			+ "rvl_rv_cd, rvl_prfrd_dt_arrvl)	VALUES ("
			+ "?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, ?, "
			+ "?, ?)";

	
	private final String UPDATE = "UPDATE lpi_rqst_vlntr_ln	SET "
		+"rvl_ln_no=?, "
		+"rvl_rgn=?, "
		+"rvl_prnvc=?, "
		+"rvl_mncplty=?, "
		+"rvl_addrss=?, "
		+"rvl_dt_frm=?, "
		+"rvl_dt_to=?, "
		+"rvl_objctv=?, "
		+"rvl_output=?, "
		+"rvl_actvts=?, "
		+"rvl_implmttn_prd=?, "
		+"rvl_fndng_src=?, "
		+"rvl_fld_spcfctn=?, "
		+"rvl_exprnc_trnnng=?, "
		+"rvl_edctn=?, "
		+"rvl_vso_cd=?, "
		+"rvl_vlntr_cd=?, "
		+"rvl_dt_arrvl=?, "
		+"rvl_emrgncy=?, "
		+"rvl_trnsprt=?, "
		+"rvl_fclty=?, "
		+"rvl_housing=?, "
		+"rvl_othrs=?, "
		+"rvl_othrs_spcfy=?, "
		+"rvl_rv_cd=?, "
		+"rvl_prfrd_dt_arrvl=? "
	+"WHERE rvl_cd=? ";
	
	
	private final String GET_BY_RVL_CD = "SELECT * FROM lpi_rqst_vlntr_ln WHERE rvl_cd=?";
	private final String GET_BY_RVL_VLNTR_CD = "SELECT * FROM lpi_rqst_vlntr_ln a, vso_vlntr b, lpi_rqst_vlntr c "
			+ "WHERE a.rvl_vlntr_cd = b.vlntr_cd AND a.rvl_rv_cd = c.rv_cd AND b.vlntr_cd=? ORDER BY c.rv_cd DESC LIMIT 1";
	private final String GET_BY_RVL_RV_CD = "SELECT * FROM lpi_rqst_vlntr_ln WHERE rvl_rv_cd=?";
	
	
	public int CREATE(LpiRequestVolunteerLineBean rvl) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerLineDB CREATE");
		int insert = 0;
		try {
			String key[] = {"rvl_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setInt(1,rvl.getRvl_ln_no());
			statement.setInt(2,rvl.getRvl_rgn());
			statement.setInt(3,rvl.getRvl_prnvc());
			statement.setInt(4,rvl.getRvl_mncplty());
			statement.setString(5,rvl.getRvl_addrss());
			statement.setString(6,rvl.getRvl_dt_frm());
			statement.setString(7,rvl.getRvl_dt_to());
			statement.setString(8,rvl.getRvl_objctv());
			statement.setString(9,rvl.getRvl_output());
			statement.setString(10,rvl.getRvl_actvts());
			statement.setString(11,rvl.getRvl_implmttn_prd());
			statement.setString(12,rvl.getRvl_fndng_src());
			statement.setString(13,	rvl.getRvl_fld_spcfctn());
			statement.setString(14,	rvl.getRvl_exprnc_trnnng());
			statement.setString(15,	rvl.getRvl_edctn());
			statement.setInt(16,rvl.getRvl_vso_cd());
			statement.setInt(17,rvl.getRvl_vlntr_cd());
			statement.setString(18,rvl.getRvl_dt_arrvl());
			statement.setInt(19,rvl.getRvl_emrgncy());
			statement.setInt(20,rvl.getRvl_trnsprt());
			statement.setInt(21,rvl.getRvl_fclty());
			statement.setInt(22,rvl.getRvl_housing());
			statement.setInt(23,rvl.getRvl_othrs());
			statement.setString(24,rvl.getRvl_othrs_spcfy());
			statement.setInt(25,rvl.getRvl_rv_cd());
			statement.setString(26, rvl.getRvl_prfrd_dt_arrvl());
			System.out.println("QUERY >> "+ statement.toString());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	public int UPDATE(LpiRequestVolunteerLineBean rvl) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerLineDB UPDATE");
		int insert = 0;
		try {
			String key[] = {"rvl_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setInt(1,rvl.getRvl_ln_no());
			statement.setInt(2,rvl.getRvl_rgn());
			statement.setInt(3,rvl.getRvl_prnvc());
			statement.setInt(4,rvl.getRvl_mncplty());
			statement.setString(5,rvl.getRvl_addrss());
			statement.setString(6,rvl.getRvl_dt_frm());
			statement.setString(7,rvl.getRvl_dt_to());
			statement.setString(8,rvl.getRvl_objctv());
			statement.setString(9,rvl.getRvl_output());
			statement.setString(10,rvl.getRvl_actvts());
			statement.setString(11,rvl.getRvl_implmttn_prd());
			statement.setString(12,rvl.getRvl_fndng_src());
			statement.setString(13,	rvl.getRvl_fld_spcfctn());
			statement.setString(14,	rvl.getRvl_exprnc_trnnng());
			statement.setString(15,	rvl.getRvl_edctn());
			statement.setInt(16,rvl.getRvl_vso_cd());
			statement.setInt(17,rvl.getRvl_vlntr_cd());
			statement.setString(18,rvl.getRvl_dt_arrvl());
			statement.setInt(19,rvl.getRvl_emrgncy());
			statement.setInt(20,rvl.getRvl_trnsprt());
			statement.setInt(21,rvl.getRvl_fclty());
			statement.setInt(22,rvl.getRvl_housing());
			statement.setInt(23,rvl.getRvl_othrs());
			statement.setString(24,rvl.getRvl_othrs_spcfy());
			statement.setInt(25,rvl.getRvl_rv_cd());
			statement.setString(26, rvl.getRvl_prfrd_dt_arrvl());
			statement.setInt(27, rvl.getRvl_cd());

			System.out.println("QUERY >> "+ statement.toString());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = rvl.getRvl_cd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	public LpiRequestVolunteerLineBean getRvlByRVL_CD(int rvl_cd)throws Exception{
		System.out.println("LpiRequestVolunteerLineDB getRvlByRVL_CD");
		LpiRequestVolunteerLineBean details = new LpiRequestVolunteerLineBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_RVL_CD);
			statement.setInt(1, rvl_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerLineDB getRvByRV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public LpiRequestVolunteerLineBean getRvlByVLNTR_CD(int rvl_vlntr_cd)throws Exception{
		System.out.println("LpiRequestVolunteerLineDB getRvlByVLNTR_CD");
		LpiRequestVolunteerLineBean details = new LpiRequestVolunteerLineBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_RVL_VLNTR_CD);
			statement.setInt(1, rvl_vlntr_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerLineDB getRvByRV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public ArrayList getRvlByRVL_RV_CD(int rvl_rv_cd)throws Exception{
		System.out.println("LpiRequestVolunteerLineDB getRvlByRVL_RV_CD");
		ArrayList list = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_RVL_RV_CD);
			statement.setInt(1, rvl_rv_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiRequestVolunteerLineBean details = new LpiRequestVolunteerLineBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerLineDB getRvlByRVL_RV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}
	
	
	
	
	private LpiRequestVolunteerLineBean toEntity(ResultSet resultset)throws Exception{
		LpiRequestVolunteerLineBean details = new LpiRequestVolunteerLineBean();
		try {
			details.setRvl_cd(resultset.getInt("rvl_cd"));
			details.setRvl_ln_no(resultset.getInt("rvl_ln_no"));
			details.setRvl_rgn(resultset.getInt("rvl_rgn"));
			details.setRvl_prnvc(resultset.getInt("rvl_prnvc"));
			details.setRvl_mncplty(resultset.getInt("rvl_mncplty"));
			details.setRvl_addrss(resultset.getString("rvl_addrss"));
			details.setRvl_dt_frm(resultset.getString("rvl_dt_frm"));
			details.setRvl_dt_to(resultset.getString("rvl_dt_to"));
			details.setRvl_objctv(resultset.getString("rvl_objctv"));
			details.setRvl_output(resultset.getString("rvl_output"));
			details.setRvl_actvts(resultset.getString("rvl_actvts"));
			details.setRvl_implmttn_prd(resultset.getString("rvl_implmttn_prd"));
			details.setRvl_fndng_src(resultset.getString("rvl_fndng_src"));
			details.setRvl_fld_spcfctn(resultset.getString("rvl_fld_spcfctn"));
			details.setRvl_exprnc_trnnng(resultset.getString("rvl_exprnc_trnnng"));
			details.setRvl_edctn(resultset.getString("rvl_edctn"));
			details.setRvl_vso_cd(resultset.getInt("rvl_vso_cd"));
			details.setRvl_vlntr_cd(resultset.getInt("rvl_vlntr_cd"));
			details.setRvl_dt_arrvl(resultset.getString("rvl_dt_arrvl"));
			details.setRvl_emrgncy(resultset.getInt("rvl_emrgncy"));
			details.setRvl_trnsprt(resultset.getInt("rvl_trnsprt"));
			details.setRvl_fclty(resultset.getInt("rvl_fclty"));
			details.setRvl_housing(resultset.getInt("rvl_housing"));
			details.setRvl_othrs(resultset.getInt("rvl_othrs"));
			details.setRvl_othrs_spcfy(resultset.getString("rvl_othrs_spcfy"));
			details.setRvl_rv_cd(resultset.getInt("rvl_rv_cd"));
			details.setRvl_prfrd_dt_arrvl(resultSet.getString("rvl_prfrd_dt_arrvl"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
	}
	
	
	
	
}
