package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.AdAddRegionBean;

public class AdRegionDB extends JdbcDao{
	public AdRegionDB(DatabaseService databaseService) {
		super(databaseService);
	}
	

	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private static final String GET_ALL_RGN ="SELECT * FROM AD_RGN";
	private static final String GET_RGN_BY_ID ="SELECT * FROM AD_RGN WHERE RGN_ID=?";

	private static final String GET_RGN_BY_RGN_NM ="SELECT * FROM AD_RGN WHERE RGN_NM=?";
	
	public ArrayList getAllRegion() throws Exception{
		System.out.println("AdRegionDB getAllRegion");
		ArrayList regionList = new ArrayList();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_RGN);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddRegionBean details = new AdAddRegionBean();
				details.setRgn_id(resultSet.getInt("rgn_id"));
				details.setRgn_nm(resultSet.getString("rgn_nm"));
				details.setRgn_dscrptn(resultSet.getString("rgn_dscrptn"));
				regionList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdRegionDB getAllRegion SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}


		finally{
			if(statement != null) statement.close(); 
			if(!getDatabaseService().getConnection().isClosed())  getDatabaseService().getConnection().close(); 
		}
		
		
		return regionList;
	}
	
	public AdAddRegionBean getRegionById(int rgn_id) throws Exception{
		System.out.println("AdRegionDB getRegionById");
		AdAddRegionBean rgn = new AdAddRegionBean();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_RGN_BY_ID);
			statement.setInt(1, rgn_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				rgn.setRgn_id(resultSet.getInt("rgn_id"));
				rgn.setRgn_nm(resultSet.getString("rgn_nm"));
				rgn.setRgn_dscrptn(resultSet.getString("rgn_dscrptn"));
				
			}
		} catch (SQLException e) {
			System.out.println("AdRegionDB getRegionById SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return rgn;
		
	}
	
	public AdAddRegionBean getRegionByRgnNm(String rgn_nm) throws Exception{
		System.out.println("AdRegionDB getRegionByRgnNm");
		AdAddRegionBean rgn = new AdAddRegionBean();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_RGN_BY_RGN_NM);
			statement.setString(1, rgn_nm);
			resultSet = statement.executeQuery();
			
			while (resultSet.next()) {
				rgn.setRgn_id(resultSet.getInt("rgn_id"));
				rgn.setRgn_nm(resultSet.getString("rgn_nm"));
				rgn.setRgn_dscrptn(resultSet.getString("rgn_dscrptn"));
				
			}
		} catch (SQLException e) {
			System.out.println("AdRegionDB getRegionById SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return rgn;
		
	}

}
