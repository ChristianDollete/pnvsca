package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.web.common;

public class AdSequence extends JdbcDao {

	public AdSequence(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private static final String GET_LPI_ID = "SELECT sqnc_lpi FROM ad_id_sqnc";
	private static final String UPDATE_LPI_ID = "UPDATE ad_id_sqnc SET sqnc_lpi=?";
	
	private static final String GET_LPI_PM_ID = "SELECT sqnc_prj_mngr FROM ad_id_sqnc";
	private static final String UPDATE_LPI_PM_ID = "UPDATE ad_id_sqnc SET sqnc_prj_mngr=?";
	
	private static final String GET_LPI_PRJ_ID = "SELECT sqnc_prj FROM ad_id_sqnc";
	private static final String UPDATE_LPI_PRJ_ID = "UPDATE ad_id_sqnc SET sqnc_prj=?";
	
	
	private static final String GET_VSO_ID = "SELECT sqnc_vso FROM ad_id_sqnc";
	private static final String UPDATE_VSO_ID = "UPDATE ad_id_sqnc SET sqnc_vso=?";
	
	private static final String GET_VSO_VLNTR_ID = "SELECT sqnc_vlntr FROM ad_id_sqnc";
	private static final String UPDATE_VSO_VLNTR_ID = "UPDATE ad_id_sqnc SET sqnc_vlntr=?";
	
	
	private static final String GET_LPI_RQST_VLNTR_ID = "SELECT sqnc_rqst_vlntr FROM ad_id_sqnc";
	private static final String UPDATE_LPI_RQST_VLNTR_ID = "UPDATE ad_id_sqnc SET sqnc_rqst_vlntr=?";
	
	
	private static final String GET_LPI_RQST_VLNTR_EXTNSN_ASSGNMNT_ID = "SELECT sqnc_vlntr_extnsn_assgnmnt FROM ad_id_sqnc";
	private static final String UPDATE_LPI_RQST_VLNTR_EXTNSN_ASSGNMNT_ID = "UPDATE ad_id_sqnc SET sqnc_vlntr_extnsn_assgnmnt=?";
	
	
	
	
	
	public String getLpiId() throws SQLException {
		System.out.println("AdSequence getLpiId");
		boolean insert = false;
		String LPI_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_LPI_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LPI_ID = common.incrementStringNumber(resultSet.getString("sqnc_lpi").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_LPI_ID);
			statement.setString(1, LPI_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return LPI_ID;
	}
	
	public String getLpiRvId() throws SQLException {
		System.out.println("AdSequence getLpiRvId");
		boolean insert = false;
		String RV_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_LPI_RQST_VLNTR_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				RV_ID = common.incrementStringNumber(resultSet.getString("sqnc_rqst_vlntr").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_LPI_RQST_VLNTR_ID);
			statement.setString(1, RV_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return RV_ID;
	}
	
	public String getLpiRvaId() throws SQLException {
		System.out.println("AdSequence getLpiRvaId");
		boolean insert = false;
		String RV_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_LPI_RQST_VLNTR_EXTNSN_ASSGNMNT_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				RV_ID = common.incrementStringNumber(resultSet.getString("sqnc_vlntr_extnsn_assgnmnt").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_LPI_RQST_VLNTR_EXTNSN_ASSGNMNT_ID);
			statement.setString(1, RV_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return RV_ID;
	}
	
	
	
	
	public String getLpiPmId() throws SQLException {
		System.out.println("AdSequence getLpiPmId");
		boolean insert = false;
		String LPI_PM_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_LPI_PM_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LPI_PM_ID = common.incrementStringNumber(resultSet.getString("sqnc_prj_mngr").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_LPI_PM_ID);
			statement.setString(1, LPI_PM_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return LPI_PM_ID;
	}
	
	public String getLpiPrjId() throws SQLException {
		System.out.println("AdSequence getLpiPrjId");
		boolean insert = false;
		String LPI_PM_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_LPI_PRJ_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LPI_PM_ID = common.incrementStringNumber(resultSet.getString("sqnc_prj").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_LPI_PRJ_ID);
			statement.setString(1, LPI_PM_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return LPI_PM_ID;
	}
	
	
	
	
	public String getVsoId() throws SQLException {
		System.out.println("AdSequence getVsoId");
		boolean insert = false;
		String VSO_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_VSO_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VSO_ID = common.incrementStringNumber(resultSet.getString("sqnc_vso").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_VSO_ID);
			statement.setString(1, VSO_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return VSO_ID;
	}
	
	
	public String getVsoVlntrId() throws SQLException {
		System.out.println("AdSequence getVsoVlntrId");
		boolean insert = false;
		String VSO_VLNT_ID = "";
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_VSO_VLNTR_ID);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VSO_VLNT_ID = common.incrementStringNumber(resultSet.getString("sqnc_vlntr").toString());
			}
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE_VSO_VLNTR_ID);
			statement.setString(1, VSO_VLNT_ID);
			statement.executeUpdate();
			insert = true;
		} catch (SQLException e) {
			if (statement != null) {
				statement.close();
			}
			e.printStackTrace();
			throw new SQLException();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return VSO_VLNT_ID;
	}
	

}
