package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiProjectBean;
import com.bean.LpiRequestVolunteerBean;

public class LpiRequestVolunteerDB extends JdbcDao {
	public LpiRequestVolunteerDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private final String CREATE = "INSERT INTO lpi_rqst_vlntr (rv_id, rv_status, rv_prj_cd, rv_cmp_cd,rv_drtn_mnths,rv_no_vlntr)	VALUES "
			+ "( ?, ?, ?, ?, ?,?)";
	
	private final String UPDATE = "	UPDATE lpi_rqst_vlntr SET "
			+ "rv_id=?,	rv_status=?, rv_prj_cd=?, rv_cmp_cd=? , rv_drtn_mnths=?, rv_no_vlntr=? WHERE rv_cd=?";
	
	private final String GET_BY_RV_CD = "SELECT * FROM lpi_rqst_vlntr WHERE rv_cd=?";
	private final String GET_BY_RV_ID = "SELECT * FROM lpi_rqst_vlntr WHERE rv_id=?";
	
	
	public int CREATE(LpiRequestVolunteerBean rv) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerDB CREATE");
		int insert = 0;
		try {
			String key[] = {"rv_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, rv.getRv_id());
			statement.setString(2, rv.getRv_status());
			statement.setInt(3, rv.getRv_prj_cd());
			statement.setInt(4, rv.getRv_cmp_cd());
			statement.setString(5, rv.getRv_drtn_mnths());
			statement.setInt(6, rv.getRv_no_vlntr());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public int UPDATE(LpiRequestVolunteerBean rv) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerDB UPDATE");
		int insert = 0;
		try {
			String key[] = {"rv_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setString(1, rv.getRv_id());
			statement.setString(2, rv.getRv_status());
			statement.setInt(3, rv.getRv_prj_cd());
			statement.setInt(4, rv.getRv_cmp_cd());
			statement.setString(5, rv.getRv_drtn_mnths());
			statement.setInt(6, rv.getRv_no_vlntr());
			statement.setInt(7, rv.getRv_cd());
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = rv.getRv_cd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public LpiRequestVolunteerBean getRvByRV_CD(int rv_cd)throws Exception{
		System.out.println("LpiRequestVolunteerDB getRvByRV_CD");
		LpiRequestVolunteerBean details = new LpiRequestVolunteerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_RV_CD);
			statement.setInt(1, rv_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerDB getRvByRV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public LpiRequestVolunteerBean getRvByRV_ID(String rv_id)throws Exception{
		System.out.println("LpiRequestVolunteerDB getRvByRV_ID");
		LpiRequestVolunteerBean details = new LpiRequestVolunteerBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_RV_ID);
			statement.setString(1, rv_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerDB getRvByRV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	
	
	public ArrayList getByCriteria(String criteria)throws Exception{
		System.out.println("LpiRequestVolunteerDB getByCriteria");
		ArrayList list = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(criteria);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiRequestVolunteerBean details = new LpiRequestVolunteerBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerDB getByCriteria SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}
	
	
	
	private LpiRequestVolunteerBean toEntity(ResultSet resultSet)throws Exception{
		LpiRequestVolunteerBean details = new LpiRequestVolunteerBean();
		try {
			details.setRv_cd(resultSet.getInt("rv_cd"));
			details.setRv_id(resultSet.getString("rv_id"));
			details.setRv_status(resultSet.getString("rv_status"));
			details.setRv_prj_cd(resultSet.getInt("rv_prj_cd"));
			details.setRv_cmp_cd(resultSet.getInt("rv_cmp_cd"));
			details.setRv_drtn_mnths(resultSet.getString("rv_drtn_mnths"));
			details.setRv_no_vlntr(resultSet.getInt("rv_no_vlntr"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		
		return details;
	}
	
	
	
	
	
}
