package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiProjectManagerBean;
import com.bean.VsoVolunteerBean;

public class VsoVolunteerDB extends JdbcDao {

	public VsoVolunteerDB(DatabaseService databaseService) {super(databaseService);	}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	
	private final String CREATE = "INSERT INTO vso_vlntr ("
			+ "vlntr_id, vlntr_fname, vlntr_mname, vlntr_lname, vlntr_ntnly, "
			+ "vlntr_cvl_stts, vlntr_brth_dt, vltnr_vs_exprtn, vlntr_prjct_cd, vlntr_prjct_nm, "
			+ "vlntr_prjct_pm_cd, vlntr_prjct_pm_nm, vlntr_usr_nm, vlntr_usr_psswrd, vlntr_usr_email, "
			+ "vlntr_usr_scnd_email, vlntr_cmp_cd, vlntr_sex, vlntr_usr_cd) "
			+ "VALUES (?, ?, ?, ?, ?, "
			+         "?, ?, ?, ?, ?, "
			+         "?, ?, ?, ?, ?, "
			+         "?, ?, ?, ?)";
	
	private final String GET_BY_VLNTR_CD = "SELECT * FROM vso_vlntr WHERE vlntr_cd =?";
	private final String GET_BY_VLNTR_ID = "SELECT * FROM vso_vlntr WHERE vlntr_id =?";
	private final String GET_BY_VLNTR_CMP_CD = "SELECT * FROM vso_vlntr WHERE vlntr_cmp_cd =?";
	private final String GET_BY_VLNTR_USR_CD = "SELECT * FROM vso_vlntr a, ad_usr b WHERE a.vlntr_usr_cd = b.usr_id AND b.usr_id=?";
	
	
	private final String UPDATE = "UPDATE vso_vlntr SET "
			+ "vlntr_id=?, vlntr_fname=?, vlntr_mname=?, vlntr_lname=?, vlntr_ntnly=?, "
			+ "vlntr_cvl_stts=?, vlntr_brth_dt=?, vltnr_vs_exprtn=?, vlntr_prjct_cd=?, vlntr_prjct_nm=?, "
			+ "vlntr_prjct_pm_cd=?, vlntr_prjct_pm_nm=?, vlntr_usr_nm=?, vlntr_usr_psswrd=?, vlntr_usr_email=?, "
			+ "vlntr_usr_scnd_email=?, vlntr_cmp_cd=?, vlntr_sex=?, vlntr_usr_cd=? WHERE vlntr_cd=?";
	
	
	public int Create(VsoVolunteerBean details)throws SQLException, Exception {
		System.out.println("VsoVolunteerDB Create");
		int insert = 0;
		try {
			String key[] = {"vlntr_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			statement.setString(1, details.getVlntr_id());
			statement.setString(2, details.getVlntr_fname());
			statement.setString(3, details.getVlntr_mname());
			statement.setString(4, details.getVlntr_lname());
			statement.setString(5, details.getVlntr_ntnly());
			statement.setString(6, details.getVlntr_cvl_stts());
			statement.setString(7, details.getVlntr_brth_dt());
			statement.setString(8, details.getVlntr_vs_exprtn());
			statement.setInt   (9, details.getVlntr_prjct_cd());
			statement.setString(10, details.getVlntr_prjct_nm());
			statement.setInt   (11, details.getVlntr_prjct_pm_cd());
			statement.setString(12, details.getVlntr_prjct_pm_nm());
			
			statement.setString(13, details.getVlntr_usr_nm());
			statement.setString(14, details.getVlntr_usr_psswrd());
			statement.setString(15, details.getVlntr_usr_email());
			statement.setString(16, details.getVlntr_usr_scnd_email());
			statement.setInt(17, details.getVlntr_cmp_cd());
			statement.setString(18, details.getVlntr_sex());
			statement.setInt(19, details.getVlntr_usr_cd());
			
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public int Update(VsoVolunteerBean details)throws SQLException, Exception {
		System.out.println("VsoVolunteerDB Update");
		int insert = 0;
		try {
			String key[] = {"vlntr_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			statement.setString(1, details.getVlntr_id());
			statement.setString(2, details.getVlntr_fname());
			statement.setString(3, details.getVlntr_mname());
			statement.setString(4, details.getVlntr_lname());
			statement.setString(5, details.getVlntr_ntnly());
			statement.setString(6, details.getVlntr_cvl_stts());
			statement.setString(7, details.getVlntr_brth_dt());
			statement.setString(8, details.getVlntr_vs_exprtn());
			statement.setInt   (9, details.getVlntr_prjct_cd());
			statement.setString(10, details.getVlntr_prjct_nm());
			statement.setInt   (11, details.getVlntr_prjct_pm_cd());
			statement.setString(12, details.getVlntr_prjct_pm_nm());
			
			statement.setString(13, details.getVlntr_usr_nm());
			statement.setString(14, details.getVlntr_usr_psswrd());
			statement.setString(15, details.getVlntr_usr_email());
			statement.setString(16, details.getVlntr_usr_scnd_email());
			statement.setInt(17, details.getVlntr_cmp_cd());
			statement.setString(18, details.getVlntr_sex());
			statement.setInt(19, details.getVlntr_usr_cd());
			statement.setInt(20, details.getVlntr_cd());
			
			
			
			System.out.println("STATEMENT >> "+statement.toString());
			statement.executeUpdate();
		/*	int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);*/
			System.out.println("VLNTR CD >> "+details.getVlntr_cd());
			insert = details.getVlntr_cd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public ArrayList getVlntrByCmpCd(int vlntr_cmp_cd)throws Exception {
		System.out.println("VsoVolunteerDB getVlntrByCmpCd");
		
		ArrayList list = new ArrayList();
		try{
			String key[] = {"vlntr_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_VLNTR_CMP_CD,key);
			statement.setInt(1, vlntr_cmp_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VsoVolunteerBean details = new VsoVolunteerBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}

	
	public VsoVolunteerBean getByVlntrCd(int vlntr_cd)throws Exception {
		System.out.println("VsoVolunteerDB getByVlntrCd");
		VsoVolunteerBean details = new VsoVolunteerBean();
		
		try{
			String key[] = {"vlntr_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_VLNTR_CD,key);
			statement.setInt(1, vlntr_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public VsoVolunteerBean getByVlntrId(String vlntr_id)throws Exception {
		System.out.println("VsoVolunteerDB getByVlntrCd");
		VsoVolunteerBean details = new VsoVolunteerBean();
		
		try{
			String key[] = {"vlntr_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_VLNTR_ID,key);
			statement.setString(1, vlntr_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public VsoVolunteerBean getVlntrByUsrCode(int vlntr_usr_cd)throws Exception {
		System.out.println("VsoVolunteerDB getVlntrByUsrCode");
		VsoVolunteerBean details = new VsoVolunteerBean();
		
		try{
			String key[] = {"vlntr_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_VLNTR_USR_CD,key);
			statement.setInt(1, vlntr_usr_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public ArrayList getByCriteria(String criteria)throws Exception {
		System.out.println("VsoVolunteerDB getByCriteria");
		ArrayList list = new ArrayList();
		
		try{
			String key[] = {"vlntr_cd"};
			statement = getDatabaseService().getConnection().prepareStatement(criteria,key);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				VsoVolunteerBean details = new VsoVolunteerBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		}catch (Exception e){
			list = new ArrayList();
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}
	
	
	private VsoVolunteerBean toEntity(ResultSet resultSet)throws Exception{
		VsoVolunteerBean details = new VsoVolunteerBean();
		try {
			details.setVlntr_cd(resultSet.getInt("vlntr_cd"));
			details.setVlntr_id(resultSet.getString("vlntr_id"));
			details.setVlntr_fname(resultSet.getString("vlntr_fname"));
			details.setVlntr_mname(resultSet.getString("vlntr_mname"));
			details.setVlntr_lname(resultSet.getString("vlntr_lname"));
			details.setVlntr_sex(resultSet.getString("vlntr_sex"));
			details.setVlntr_ntnly(resultSet.getString("vlntr_ntnly"));
			details.setVlntr_cvl_stts(resultSet.getString("vlntr_cvl_stts"));
			details.setVlntr_brth_dt(resultSet.getString("vlntr_brth_dt"));
			details.setVlntr_vs_exprtn(resultSet.getString("vltnr_vs_exprtn"));
			
			details.setVlntr_prjct_cd(resultSet.getInt("vlntr_prjct_cd"));
			details.setVlntr_prjct_nm(resultSet.getString("vlntr_prjct_nm"));
			details.setVlntr_prjct_pm_cd(resultSet.getInt("vlntr_prjct_pm_cd"));
			details.setVlntr_prjct_pm_nm(resultSet.getString("vlntr_prjct_pm_nm"));
			
			details.setVlntr_usr_nm(resultSet.getString("vlntr_usr_nm"));
			details.setVlntr_usr_psswrd(resultSet.getString("vlntr_usr_psswrd"));
			details.setVlntr_usr_email(resultSet.getString("vlntr_usr_email"));
			details.setVlntr_usr_scnd_email(resultSet.getString("vlntr_usr_scnd_email"));
			details.setVlntr_cmp_cd(resultSet.getInt("vlntr_cmp_cd"));
			details.setVlntr_usr_cd(resultSet.getInt("vlntr_usr_cd"));
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return details;
	}
	

}
