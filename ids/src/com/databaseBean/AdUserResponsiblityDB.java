package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.AdUserResponsibilityBean;

public class AdUserResponsiblityDB extends JdbcDao {

	public AdUserResponsiblityDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	
	private static final String CREATE ="INSERT INTO ad_usr_rspnsblty ("
			+ "usr_id, res_lpi_prjct_mngr, res_lpi_prjct, res_lpi_rqst_vlntr, res_vso_vlntr) "
			+ "VALUES (?, ?, ?, ?, ?)";
	
	
	private static final String GET_RES_BY_USR_CD = "SELECT * FROM ad_usr_rspnsblty WHERE usr_id=?";
	
	
	public int CREATE(AdUserResponsibilityBean res) 
			throws SQLException {
		System.out.println("AdUserResponsiblityDB CREATE");
		int insert = 0;
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			statement = getDatabaseService().getConnection().prepareStatement(CREATE);
			statement.setInt(1, res.getRes_usr_id());
			statement.setInt(2, res.getRes_lpi_prjct_mngr());
			statement.setInt(3, res.getRes_lpi_prjct());
			statement.setInt(4, res.getRes_lpi_rqst_vlntr());
			statement.setInt(5, res.getRes_vso_vlntr());
			
			statement.executeUpdate();
			//getDatabaseService().getConnection().commit();
			System.out.println("KEY >> "+statement.RETURN_GENERATED_KEYS);
			
			insert = statement.RETURN_GENERATED_KEYS;
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public AdUserResponsibilityBean getResByUsrCd(int usr_cd) throws SQLException {
		System.out.println("AdUserResponsiblityDB getResByUsrCd");
		int insert = 0;
		AdUserResponsibilityBean details = new AdUserResponsibilityBean();
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			statement = getDatabaseService().getConnection().prepareStatement(GET_RES_BY_USR_CD);
			statement.setInt(1, usr_cd);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details.setRes_id(resultSet.getInt("res_id"));
				details.setRes_lpi_prjct(resultSet.getInt("res_lpi_prjct"));
				details.setRes_lpi_prjct_mngr(resultSet.getInt("res_lpi_prjct_mngr"));
				details.setRes_lpi_rqst_vlntr(resultSet.getInt("res_lpi_rqst_vlntr"));
				details.setRes_vso_vlntr(resultSet.getInt("res_vso_vlntr"));
				details.setRes_usr_id(resultSet.getInt("usr_id"));
				
			}
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	
	
	
	

}
