package com.databaseBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.databaseBean.dbInterfaces.DatabaseServiceInterface;


public class DatabaseService implements DatabaseServiceInterface {

	private Connection connection;
	private ResourceBundle props;

	
	public DatabaseService() {
		try {
			props = ResourceBundle.getBundle("com.application");
			Class.forName(props.getString("jdbc.driver"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}


	@Override
	public Connection getConnection() {
		try {
			connection = DriverManager.getConnection(
					props.getString("jdbc.url"),
					props.getString("jdbc.username"),
					props.getString("jdbc.password"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	
		return connection;
	}

}
