package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.LpiProjectBean;
import com.bean.LpiRequestVolunteerBean;
import com.bean.LpiRequestVolunteerExtensionAssignmentBean;

public class LpiRequestVolunteerExtensionAssignmentDB extends JdbcDao {
	public LpiRequestVolunteerExtensionAssignmentDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private final String CREATE = "INSERT INTO lpi_rqst_vlntr_assgnmnt ("
			+ "rva_id, rva_lpi_cd, rva_lpi_prjct_cd, rva_vlntr_cd, rva_drtn_vlntr_assgnmnt, "
			+ "rva_prd_extnsn_rqst, rva_rsn_rqst_extnsn, rva_dfrd_due, rva_dfrd_due_rsn, rva_ds_apprvd, "
			+ "rva_ds_apprvd_rsn, rva_apprvd_to, rva_apprvd_prd, rva_dt, rva_cmp_cd) VALUES"
			+ " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private final String UPDATE = "	UPDATE lpi_rqst_vlntr SET "
			+ "rv_id=?,	rv_status=?, rv_prj_cd=?, rv_cmp_cd=? , rv_drtn_mnths=?, rv_no_vlntr=? WHERE rv_cd=?";
	

	public int CREATE(LpiRequestVolunteerExtensionAssignmentBean rva) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerExtensionAssignmentDB CREATE");
		int insert = 0;
		try {
			String key[] = {"rva_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			
			statement.setString(1, rva.getRva_id());
			statement.setInt(2, rva.getRva_lpi_cd());
			statement.setInt(3, rva.getRva_lpi_prjct_cd());
			statement.setInt(4, rva.getRva_vlntr_cd());
			statement.setString(5, rva.getRva_drtn_vlntr_assgnmnt());
			statement.setString(6, rva.getRva_prd_extnsn_rqst());
			statement.setString(7, rva.getRva_rsn_rqst_extnsn());
			statement.setInt(8, rva.getRva_dfrd_due());
			statement.setInt(9, rva.getRva_dfrd_due_rsn());
			statement.setInt(10, rva.getRva_ds_apprvd());
			statement.setInt(11, rva.getRva_ds_apprvd_rsn());
			statement.setString(12, rva.getRva_apprvd_to());
			statement.setString(13, rva.getRva_apprvd_prd());
			statement.setString(14, rva.getRva_dt());
			statement.setInt(15, rva.getRva_cmp_cd());
			

			System.out.println(statement.toString());
			statement.executeUpdate();
			
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public int UPDATE(LpiRequestVolunteerExtensionAssignmentBean rv) throws SQLException, Exception {
		System.out.println("LpiRequestVolunteerExtensionAssignmentDB UPDATE");
		int insert = 0;
		try {
			String key[] = {"rv_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			
			statement.executeUpdate();
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = rv.getRva_cd();
		} catch (SQLException e){
			e.printStackTrace();
			throw new SQLException();
		}catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	
	
	public LpiRequestVolunteerExtensionAssignmentBean getByRVA_ID(String rva_id)throws Exception{
		System.out.println("LpiRequestVolunteerExtensionAssignmentDB getByRVA_ID");
		LpiRequestVolunteerExtensionAssignmentBean details = new LpiRequestVolunteerExtensionAssignmentBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement("");
			
			
			statement.setString(1, rva_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerDB getRvByRV_CD SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	
	
	public ArrayList getByCriteria(String criteria)throws Exception{
		System.out.println("LpiRequestVolunteerExtensionAssignmentDB getByCriteria");
		ArrayList list = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(criteria);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiRequestVolunteerExtensionAssignmentBean details = new LpiRequestVolunteerExtensionAssignmentBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiRequestVolunteerExtensionAssignmentDB getByCriteria SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
	}
	
	
	
	private LpiRequestVolunteerExtensionAssignmentBean toEntity(ResultSet resultSet)throws Exception{
		LpiRequestVolunteerExtensionAssignmentBean details = new LpiRequestVolunteerExtensionAssignmentBean();
		try {
			details.setRva_cd(resultSet.getInt("rva_cd"));
			details.setRva_id(resultSet.getString("rva_id"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	
	
	
	
}
