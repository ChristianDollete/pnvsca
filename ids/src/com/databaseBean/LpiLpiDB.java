package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.bean.LpiLpiBean;
import com.bean.LpiProjectBean;

public class LpiLpiDB extends JdbcDao {
	public LpiLpiDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private final String CREATE = "INSERT INTO lpi_lpi ( "
			+ "lpi_id, lpi_nm, lpi_typ, lpi_mndt, lpi_addrss, "
			+ "lpi_rgn, lpi_prvnc, lpi_mncplty, lpi_tlphn_nmbr, lpi_email, "
			+ "lpi_fx_nmbr, lpi_wbst, lpi_usr_nm, lpi_usr_psswrd, lpi_prmry_email, "
			+ "lpi_scndry_email, lpi_crdntr_nm, lpi_crdntr_pstn, lpi_crdntr_tlphn_nmbr, lpi_crdntr_dprtmnt, "
			+ "lpi_crdntr_mble_nmbr,lpi_cmp_id) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?)";

	private final String GET_BY_LPI_CMP_CD = "SELECT * FROM lpi_lpi WHERE  lpi_cmp_id=?";
	private final String GET_BY_LPI_ID = "SELECT * FROM lpi_lpi WHERE  lpi_id=?";
	private final String GET_ALL_LPI = "SELECT * FROM lpi_lpi";
	
	private final String UPDATE ="UPDATE lpi_lpi SET " 
			+ "lpi_id=?, "
			+ "lpi_nm=?, "
			+ "lpi_typ=?, "
			+ "lpi_mndt=?, "
			+ "lpi_addrss=?, "
			+ "lpi_rgn=?, "
			+ "lpi_prvnc=?, "
			+ "lpi_mncplty=?, "
			+ "lpi_tlphn_nmbr=?, "
			+ "lpi_email=?, "
			+ "lpi_fx_nmbr=?, "
			+ "lpi_wbst=?, "
			+ "lpi_usr_nm=?, "
			+ "lpi_usr_psswrd=?, "
			+ "lpi_prmry_email=?, "
			+ "lpi_scndry_email=?, "
			+ "lpi_crdntr_nm=?, "
			+ "lpi_crdntr_pstn=?, "
			+ "lpi_crdntr_tlphn_nmbr=?, "
			+ "lpi_crdntr_dprtmnt=?, "
			+ "lpi_crdntr_mble_nmbr=? "	
		    + "WHERE lpi_cmp_id=? ";
	
	public int CREATE(LpiLpiBean lpi) throws SQLException {
		System.out.println("LpiLpiDB CREATE");
		int insert = 0;
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			
			String key[] = {"lpi_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(CREATE,key);
			Date date = new Date();
			statement.setString(1, lpi.getLpi_id());
			statement.setString(2, lpi.getLpi_nm());
			statement.setInt(3, lpi.getLpi_typ());
			statement.setString(4, lpi.getLpi_mndt());
			statement.setString(5, lpi.getLpi_addrss());
			statement.setInt(6, lpi.getLpi_rgn());
			statement.setInt(7, lpi.getLpi_prvnc());
			statement.setInt(8, lpi.getLpi_mncplty());
			statement.setString(9, lpi.getLpi_tlphn_nmbr());
			statement.setString(10, lpi.getLpi_email());
			statement.setString(11, lpi.getLpi_fx_nmbr());
			statement.setString(12, lpi.getLpi_wbst());
			statement.setString(13, lpi.getLpi_usr_nm());
			statement.setString(14, lpi.getLpi_usr_psswrd());
			statement.setString(15, lpi.getLpi_prmry_email());
			statement.setString(16, lpi.getLpi_scndry_email());
			statement.setString(17, lpi.getLpi_crdntr_nm());
			statement.setString(18, lpi.getLpi_crdntr_pstn());
			statement.setString(19, lpi.getLpi_crdntr_tlphn_nmbr());
			statement.setString(20, lpi.getLpi_crdntr_dprtmnt());
			statement.setString(21, lpi.getLpi_crdntr_mble_nmbr());
			statement.setInt(22, lpi.getLpi_cmp_id());
			statement.executeUpdate();
			
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ generatedKey);
			insert = generatedKey;
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public int UPDATE(LpiLpiBean lpi) throws SQLException {
		System.out.println("LpiLpiDB UPDATE");
		int insert = 0;
		try {
			//getDatabaseService().getConnection().setAutoCommit(false);
			
			String key[] = {"lpi_cd"}; 
			statement = getDatabaseService().getConnection().prepareStatement(UPDATE,key);
			Date date = new Date();
			statement.setString(1, lpi.getLpi_id());
			statement.setString(2, lpi.getLpi_nm());
			statement.setInt(3, lpi.getLpi_typ());
			statement.setString(4, lpi.getLpi_mndt());
			statement.setString(5, lpi.getLpi_addrss());
			statement.setInt(6, lpi.getLpi_rgn());
			statement.setInt(7, lpi.getLpi_prvnc());
			statement.setInt(8, lpi.getLpi_mncplty());
			statement.setString(9, lpi.getLpi_tlphn_nmbr());
			statement.setString(10, lpi.getLpi_email());
			statement.setString(11, lpi.getLpi_fx_nmbr());
			statement.setString(12, lpi.getLpi_wbst());
			statement.setString(13, lpi.getLpi_usr_nm());
			statement.setString(14, lpi.getLpi_usr_psswrd());
			statement.setString(15, lpi.getLpi_prmry_email());
			statement.setString(16, lpi.getLpi_scndry_email());
			statement.setString(17, lpi.getLpi_crdntr_nm());
			statement.setString(18, lpi.getLpi_crdntr_pstn());
			statement.setString(19, lpi.getLpi_crdntr_tlphn_nmbr());
			statement.setString(20, lpi.getLpi_crdntr_dprtmnt());
			statement.setString(21, lpi.getLpi_crdntr_mble_nmbr());
			statement.setInt(22, lpi.getLpi_cmp_id());
			statement.executeUpdate();
			
			int generatedKey = 0;
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
			    generatedKey = rs.getInt(1);
			}
			System.out.println("GENERATED KEY >> "+ lpi.getLpi_cd());
			insert = lpi.getLpi_cd();
		} catch (SQLException e) {
			
			//getDatabaseService().getConnection().rollback();
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return insert;
	}
	
	public LpiLpiBean getLpiByLpiCmpCd(int lpi_cmp_id)throws Exception{
		System.out.println("LpiLpiDB getLpiByLpiCmpCd");
		LpiLpiBean details = new LpiLpiBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_LPI_CMP_CD);
			statement.setInt(1, lpi_cmp_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiLpiDB getLpiByLpiCmpCd SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	public LpiLpiBean getLpiByLpiId(String lpi_id)throws Exception{
		System.out.println("LpiLpiDB getLpiByLpiId");
		LpiLpiBean details = new LpiLpiBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_BY_LPI_ID);
			statement.setString(1, lpi_id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details = toEntity(resultSet);
			}
		} catch (SQLException e) {
			System.out.println("LpiLpiDB getLpiByLpiCmpCd SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	
	public ArrayList getAllLpi()throws Exception{
		System.out.println("LpiLpiDB getAllLpi");
		ArrayList list = new ArrayList();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_LPI);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				LpiLpiBean details = new LpiLpiBean();
				details = toEntity(resultSet);
				list.add(details);
			}
		} catch (SQLException e) {
			System.out.println("LpiLpiDB getAllLpi SQLException"); 
			e.printStackTrace();
			throw new Exception(e);
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return list;
		
		
		
	}
	
	private LpiLpiBean toEntity(ResultSet resultSet) throws SQLException, Exception{
		System.out.println("LpiLpiDB toEntity");
		LpiLpiBean details = new LpiLpiBean();
		try {
			details.setLpi_cd(resultSet.getInt("lpi_cd"));
			details.setLpi_id(resultSet.getString("lpi_id"));
			details.setLpi_nm(resultSet.getString("lpi_nm"));
			details.setLpi_typ(resultSet.getInt("lpi_typ"));
			details.setLpi_mndt(resultSet.getString("lpi_mndt"));
			details.setLpi_addrss(resultSet.getString("lpi_addrss"));
			details.setLpi_rgn(resultSet.getInt("lpi_rgn"));
			details.setLpi_prvnc(resultSet.getInt("lpi_prvnc"));
			details.setLpi_mncplty(resultSet.getInt("lpi_mncplty"));
			details.setLpi_tlphn_nmbr(resultSet.getString("lpi_tlphn_nmbr"));
			details.setLpi_email(resultSet.getString("lpi_email"));
			details.setLpi_fx_nmbr(resultSet.getString("lpi_fx_nmbr"));
			details.setLpi_wbst(resultSet.getString("lpi_wbst"));
			details.setLpi_usr_nm(resultSet.getString("lpi_usr_nm"));
			details.setLpi_usr_psswrd(resultSet.getString("lpi_usr_psswrd"));
			details.setLpi_prmry_email(resultSet.getString("lpi_prmry_email"));
			details.setLpi_scndry_email(resultSet.getString("lpi_scndry_email"));
			details.setLpi_crdntr_nm(resultSet.getString("lpi_crdntr_nm"));
			details.setLpi_crdntr_pstn(resultSet.getString("lpi_crdntr_pstn"));
			details.setLpi_crdntr_tlphn_nmbr(resultSet.getString("lpi_crdntr_tlphn_nmbr"));
			details.setLpi_crdntr_dprtmnt(resultSet.getString("lpi_crdntr_dprtmnt"));
			details.setLpi_crdntr_mble_nmbr(resultSet.getString("lpi_crdntr_mble_nmbr"));
			details.setLpi_cmp_id(resultSet.getInt("lpi_cmp_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
		return details;
		
	}

}
