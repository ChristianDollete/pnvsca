package com.databaseBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bean.AdAddCountryBean;

public class AdCountryDB extends JdbcDao {

	public AdCountryDB(DatabaseService databaseService) {super(databaseService);}
	private PreparedStatement statement;
	private ResultSet resultSet;
	
	private static final String GET_ALL_CNTRY ="SELECT * FROM AD_CNTRY";
	private static final String GET_ALL_CNTRY_BY_CNNTRY_NM ="SELECT * FROM AD_CNTRY WHERE cntry_nm=?";
	
	public ArrayList getAllCountry() throws Exception{
		System.out.println("AdCountryDB getAllCountry");
		ArrayList countryList = new ArrayList();
		
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_CNTRY);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AdAddCountryBean details = new AdAddCountryBean();
				details.setCntry_cd(resultSet.getInt("cntry_cd"));
				details.setCntry_nm(resultSet.getString("cntry_nm"));
				details.setCntry_dscrptn(resultSet.getString("cntry_dscrptn"));
				countryList.add(details);
			}
		} catch (SQLException e) {
			System.out.println("AdCountryDB getAllCountry SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return countryList;
	}
	
	public AdAddCountryBean getAllCountryByCntryName(String cntry_nm) throws Exception{
		System.out.println("AdCountryDB getAllCountryByCntryName");
		AdAddCountryBean details = new AdAddCountryBean();
		try {
			statement = getDatabaseService().getConnection().prepareStatement(GET_ALL_CNTRY_BY_CNNTRY_NM);
			statement.setString(1, cntry_nm);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				details.setCntry_cd(resultSet.getInt("cntry_cd"));
				details.setCntry_nm(resultSet.getString("cntry_nm"));
				details.setCntry_dscrptn(resultSet.getString("cntry_dscrptn"));
			}
		} catch (SQLException e) {
			System.out.println("AdCountryDB getAllCountryByCntryName SQLException"); 
			e.printStackTrace();
			throw new Exception();
		}finally {
			if (statement != null) {
				statement.close();
			}
			if (getDatabaseService().getConnection() != null) {
				getDatabaseService().getConnection().close();
			}
		}
		statement.close();
		return details;
	}
	

}
