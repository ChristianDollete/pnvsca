<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>
<%@ include file="cssLink.jsp" %>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
		<div class="content-wrapper">
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>VOLUNTEER ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
							<s:form action="AdVsoVolunteerProfile" theme="simple" method="post" enctype="multipart/form-data">
								<s:hidden name="frm.vlntrCd" />
								<s:hidden name="frm.vlntr_usr_cd" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Volunteer Id</label></td>
										<td colspan="3"><s:textfield name="frm.vlntrId"
												style="width: 100%;" cssClass='form-control'
												placeholder='Volunteer ID' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Name</label></td>
										<td>
											<s:textfield name="frm.vlntrFname"
												style="width: 100%;" cssClass='form-control'
												placeholder='First Name' />
										</td>
										<td>
											<s:textfield name="frm.vlntrMname"
												style="width: 100%;" cssClass='form-control'
												placeholder='Middle Name' />
										</td>
										<td>
											<s:textfield name="frm.vlntrLname"
												style="width: 100%;" cssClass='form-control'
												placeholder='Last Name' />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Sex
												</label></td>
										<td colspan="3">
											<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
											<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
											<s:select style="width: 33%;" cssClass='form-control'
												headerKey="0" headerValue="MALE / FEMALE"
												list="frm.getGenderLists()" name="frm.genderList"
												value="frm.gender" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Nationality
												</label></td>
										<td colspan="1">
											<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
											<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="NATIONALITY"
												list="frm.getVlntrNtnlyLsts()" name="frm.vlntrNtnlyLst"
												value="frm.vlntrNtnly" />
										</td>
										<td style="vertical-align: middle;" align="center"><label 
											class='control-label-required'>Civil Status</label></td>
										<td>
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="CIVIL STATUS"
												list="frm.getVlntrCstatusLsts()" name="frm.vlntrCstatusLst"
												value="frm.vlntrCstatus" />
	          							</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Birth Date</label></td>
										<td>
										    <s:textfield name="frm.vlntrDateOfBirth"	style="width: 100%;" cssClass='form-control pull-right' id="datepicker"	placeholder='MM/DD/YYYY' />
	          							</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Visa Expiration</label></td>
										<td>
											<s:textfield name="frm.vlntrVisaExpiration"	style="width: 100%;" cssClass='form-control pull-right' id="datepicker2"	placeholder='MM/DD/YYYY' />
	          							</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label 
											class='control-label'>Project</label></td>
										<td>
											<s:hidden name="frm.prjCd" />
										    <s:textfield name="frm.vlntrPrjct" style="width: 100%;" cssClass='form-control' disabled="true"/>
	          							</td>
										<td style="vertical-align: middle;" align="center"><label 
											class='control-label'>Project Manager</label></td>
										<td>
											<s:hidden name="frm.pmCd" />
										    <s:textfield name="frm.vlntrPrjPm" style="width: 100%;" cssClass='form-control'  disabled="true"	/>
	          							</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Project Date</label></td>
										<td><s:textfield name="frm.vlntrPrjDateFrom"
												style="width: 100%;" cssClass='form-control'
												placeholder='Date From'  disabled="true"/></td>
										<td colspan="2"><s:textfield name="frm.vlntrPrjDateTo"
												style="width: 50%;" cssClass='form-control'
												placeholder='Date To'    disabled="true"/></td>
									</tr>
									<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        							<td colspan="4">&nbsp;</td>
	        						</tr>
	        						<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>User Name</label></td>
										<td colspan="3"><s:textfield name="frm.vlntrUserName"
												style="width: 100%;" cssClass='form-control'
												placeholder='Volunteer User Name' /></td>
									</tr>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>User Password</label></td>
										<td>
											<s:password name="frm.vlntrUserPass"
												style="width: 100%;" cssClass='form-control'
												/>
										</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Confirm Password</label></td>
										<td>
											<s:password name="frm.vlntrUserPassConfirm"
												style="width: 100%;" cssClass='form-control'
												/>
										</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label 
											class='control-label-required'>Email</label></td>
										<td>
											<s:textfield name="frm.vlntrUserEmail" style="width: 100%;" cssClass='form-control'	/>
	          							</td>
										<td style="vertical-align: middle;" align="center"><label 
											class='control-label'>Secondary Email</label></td>
										<td>
											<s:textfield name="frm.vlntrUserSecondEmail" style="width: 100%;" cssClass='form-control'	/>
	          							</td>
									</tr>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>

									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Update" name="frm.btnName" cssClass="btn btn-success"	style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"	name="frm.btnName" cssClass="btn btn-danger"	style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"	name="frm.btnName" cssClass="btn btn-primary"	style="width: 100%; font-size:11px; " /></td>
									</tr>

								</table>
							</s:form>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<!-- bootstrap datepicker -->
	
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<script src="BS/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
	<script>
		$(function () {
			 //Date picker
		    $('#datepicker').datepicker({
		      autoclose: true
		    });
		 });
		$(function () {
			 //Date picker
		    $('#datepicker2').datepicker({
		      autoclose: true
		    });
		 });
	</script>
</body>
</html>