<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>


<script type="text/javascript">
	function handleChangeR(value){
		
		   document.getElementById("AnnexA_frm_provinceList").innerHTML = "";
		   select = document.getElementById("AnnexA_frm_provinceList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Province';
	       select.add( option );
		   var name = value;   
		   
		   $.ajax({
               url: "LpiSignUpRegion", data: {'frm.region':name}, type: 'POST', async: true,
               success: function (res) {
                   for (var i = 0; i < res.provinces.length; i++) {
                	   
                  //     $('#LpiSignUp_frm_provinceList').append('<option value=' + res.provinces[i].toString() + '>' + res.provinces[i].toString() + '</option>');
                       var option2 = document.createElement('option');
            		   option2 = document.createElement( 'option' );
            	       option2.value = res.provinces[i].toString();
            	       option2.text = res.provinces[i].toString();
            	       select.add( option2 );
                   
                   }
               }
           });
		   
		  
	}
	
	function handleChangeP(value){
		
		   document.getElementById("AnnexA_frm_municipalityList").innerHTML = "";
		   select = document.getElementById("AnnexA_frm_municipalityList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Municipality';
	       select.add( option );
		   var name = value;   
		   $.ajax({
            url: "LpiSignUpMun", data: {'frm.province':name}, type: 'POST', async: true,
            success: function (res) {
                for (var i = 0; i < res.municipalities.length; i++) {
                	
                    //$('#LpiSignUp_frm_municipalityList').append('<option value=' + res.municipalities[i].toString() + '>' + res.municipalities[i].toString() + '</option>');
                	var option2 = document.createElement('option');
          		    option2 = document.createElement( 'option' );
          	        option2.value = res.municipalities[i].toString();
          	        option2.text = res.municipalities[i].toString();
          	        select.add( option2 );
                    
                }
            }
        });
		   
		  
	}
	
	
	function AnnexAPrint(value){
		var url="/ids/AnnexA.action?frm.link=1313&frm.vsoId="+value;
		var x = screen.width/2 - 700/2;
	    var y = screen.height/2 - 350/2;
	    window.open(url,"_blank","directories=no, status=no,top=y;left=x;width=700, height=350,resizable,menubar,scrollbars");
	  
    }
	</script>



</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="AnnexA" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>Annex A Find Entry</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
								<s:hidden name="frm.pmId" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Name of Organization</label></td>
										<td colspan="3"><s:textfield name="frm.vsoName"
												style="width: 100%;" cssClass='form-control'
												placeholder='Vso Name' /></td>
									</tr>
									
									<tr>
					        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Type of Organization </label></td>
					        			<td colspan="3" >
					        				<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
					        				<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
					        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Search Organization Type"
															list="frm.getVsoTypeLists()"
															name="frm.vsoTypeList"
															value="frm.vsoType"  />
					        			</td>
					        		</tr>
									
									<tr>
	        			
					        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Local Address </label></td>
					        			<td >
					        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Region"
															list="frm.getRegionLists()"
															name="frm.regionList"
															value="frm.region"   onchange="handleChangeR(this.value)" />
										</td>
										<td>				
											<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Province"
															list="frm.getProvinceLists()"
															name="frm.provinceList"
															value="frm.province" onchange="handleChangeP(this.value)"   />
										</td>
										<td>				
											<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Municipality"
															list="frm.getMunicipalityLists()"
															name="frm.municipalityList"
															value="frm.municipality"  />
															
					        			</td>
					        		</tr>
					        		<tr>
					        			<td colspan="1"></td>
					        			<td colspan="3"><s:textfield name="frm.address" style="width: 100%;" cssClass='form-control' placeholder="Address" /></td>
					        		</tr>
									<tr>
					        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Date Established </label></td>
					        			<td colspan="3">
					        				<s:textfield name="frm.vsoDateEstablished"	style="width: 100%;" cssClass='form-control pull-right' id="datepicker"	placeholder='Date Established (MM/DD/YYYY)' />
					          			</td>
					        		</tr>
									
									
									
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									
									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Search"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									</table>
									
						</form>
					</div>
				</div>
				
				
				<div class="box">
		            <div class="box-header"  style="background-color: #337ab7; color: white;">
		              <h3 class="box-title" >Volunteer Service Organization List</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example2" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>Vso Id</th>
		                  <th>Name of Organization</th>
		                  <th>Type of Organization</th>
		                  <th>Date Established</th>
		                  <th>Vso Email</th>
		                  <th>PNVSCA Coordinator</th>
		                  <th>View</th>
		                </tr>
		                </thead>
		                	<tbody>
		                		<s:iterator value="frm.getVsoLists()" var="vsoL">
		                		<tr>
		                		<td>
									<s:hidden name="#vsoL.vsoId"/>
									<s:property value="#vsoL.vsoId"/>
								</td>
								<td>
									<s:property value="#vsoL.vsoName"/>
								</td>
		                  		<td>
		                  			<s:property value="#vsoL.vsoType"/>
		                  		</td>
		                  		<td><s:property value="#vsoL.vsoDateEstablished"/></td>
		                  		<td><s:property value="#vsoL.vsoEmail"/></td>
		                  		<td><s:property value="#vsoL.vsoCoordinatorName"/></td>
		                  		<td>
		                  			<a style="text-decoration: none;" href="#" onclick="AnnexAPrint('<s:property value="#vsoL.vsoId"/>');" >
										PRINT
									</a>  
		                  		</td>
		                  		</tr>
		                  		</s:iterator>
		                  		
							</tbody>
		                <tfoot>
		                <tr>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                </tr>
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
	         	</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	
	
	
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>

<%@ include file="datePickerFooter.jsp" %>
</body>
</html>