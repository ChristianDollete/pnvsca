<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>

<script type="text/javascript">
	function handleChangePrj(value){
		
		   document.getElementById("LpiRequestVolunteerFindEntry_frm_rv_prj_mngr_nm").innerHTML = "";
		   var name = value;   
		   $.ajax({
               url: "LpiRequestVolunteerProject", data: {'frm.rv_prj_nm':name}, type: 'POST', async: true,
               success: function (res) {
            	   document.getElementById("LpiRequestVolunteerFindEntry_frm_rv_prj_mngr_nm").value=res.prjMngrName.toString();
               }
           });
		   
		  
	}
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="LpiRequestVolunteerFindEntry" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>LPI REQUEST FOR VOLUNTEER FIND ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
								<s:hidden name="frm.rv_cd" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Request No.</label></td>
										<td colspan="3"><s:textfield name="frm.rv_id"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Project / Activity</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Project"
												list="frm.getRv_prjLists()" name="frm.rv_prjList"
												value="frm.rv_prj_nm" onchange="handleChangePrj(this.value)" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Project Manager.</label></td>
										<td colspan="3"><s:textfield name="frm.rv_prj_mngr_nm"
												style="width: 100%;" cssClass='form-control'
												disabled="true"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Status</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Status"
												list="frm.getRv_statusLists()" name="frm.rv_statusList"
												value="frm.rv_status" />
										</td>
									</tr>
									<tr>
										<td colspan="1" style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Requested No. of Volunteers</label></td>
										<td colspan="2"><s:textfield name="frm.rv_no_vlntr"
												style="width: 50%;" cssClass='form-control'
												disabled="false"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Project Duration in Months</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Duration in Months"
												list="frm.getRv_prj_drtn_mnthsLsts()" name="frm.rv_prj_drtn_mnthsLst"
												value="frm.rv_prj_drtn_mnths" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td style="width: 25%;">&nbsp;</td>
										<td style="width: 25%;" align="center"><s:submit value="Search"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td style="width: 25%;" align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td style="width: 25%;" align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									</table>
									
						</form>
					</div>
				</div>
				
				
				<div class="box">
		            <div class="box-header" style="background-color: #337ab7; color: white;" >
		              <h3 class="box-title">Request for Volunteer List</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example2" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>Request No.</th>
		                  <th>Project Duration in Months</th>
		                  <th>Request No. of Volunteer</th>
		                  <th>Request Status</th>
		                  <th>View</th>
		                </tr>
		                </thead>
		                	<tbody>
		                		<s:iterator value="frm.getRv_ln()" var="rvl">
		                		<tr>
		                		<td>
									<s:hidden name="#rvl.rv_cd"/>
									<s:property value="#rvl.rv_id"/>
								</td>
								<td>
									<s:property value="#rvl.rv_prj_drtn_mnths"/>
								</td>
		                  		<td>
		                  			<s:property value="#rvl.rv_no_vlntr"/>
		                  		</td>
		                  		<td>
		                  			<s:property value="#rvl.rv_statusList"/>
		                  		</td>
		                  		<td>
		                  			<a style="text-decoration: none;" 
		                  				href="<s:url action="LpiRequestVolunteerEntry" escapeAmp="false">
											    <s:param name="frm.link" value="77"/>
										        <s:param name="frm.rv_cd" value="#rvl.rv_cd"/>
										      </s:url>"
									>
										OPEN
									</a> 
		                  		</td>
		                  		</tr>
		                  		</s:iterator>
		                  		
							</tbody>
		                <tfoot>
		                <tr>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                </tr>
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
	         	</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>

<script src="BS/dist/js/demo.js"></script>
<script src="BS/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="BS/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="BS/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="BS/plugins/fastclick/fastclick.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
	
	
</body>
</html>