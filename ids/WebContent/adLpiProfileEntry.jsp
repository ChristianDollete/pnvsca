<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>
<%@ include file="cssLink.jsp" %>
<script type="text/javascript">
	function handleChangeR(value){
		   document.getElementById("AdProfile_frm_provinceList").innerHTML = "";
		   select = document.getElementById("AdProfile_frm_provinceList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Province';
	       select.add( option );
		   var name = value;   
		   
		   $.ajax({
               url: "LpiSignUpRegion", data: {'frm.region':name}, type: 'POST', async: true,
               success: function (res) {
                   for (var i = 0; i < res.provinces.length; i++) {
                	   
                  //     $('#LpiSignUp_frm_provinceList').append('<option value=' + res.provinces[i].toString() + '>' + res.provinces[i].toString() + '</option>');
                       var option2 = document.createElement('option');
            		   option2 = document.createElement( 'option' );
            	       option2.value = res.provinces[i].toString();
            	       option2.text = res.provinces[i].toString();
            	       select.add( option2 );
                   
                   }
               }
           });
		   
		  
	}
	
	function handleChangeP(value){
		
		   document.getElementById("AdProfile_frm_municipalityList").innerHTML = "";
		   select = document.getElementById("AdProfile_frm_municipalityList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Municipality';
	       select.add( option );
		   var name = value;   
		   $.ajax({
            url: "LpiSignUpMun", data: {'frm.province':name}, type: 'POST', async: true,
            success: function (res) {
                for (var i = 0; i < res.municipalities.length; i++) {
                	
                    //$('#LpiSignUp_frm_municipalityList').append('<option value=' + res.municipalities[i].toString() + '>' + res.municipalities[i].toString() + '</option>');
                	var option2 = document.createElement('option');
          		    option2 = document.createElement( 'option' );
          	        option2.value = res.municipalities[i].toString();
          	        option2.text = res.municipalities[i].toString();
          	        select.add( option2 );
                    
                }
            }
        });
		   
		  
	}
	</script>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
		<div class="content-wrapper">
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>LOCAL PARTNER INSTITUTION (LPI) ACCOUNT</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
							<s:form action="AdProfile" theme="simple" method="post" enctype="multipart/form-data">
							<s:hidden name="frm.lpiCode"/>
							
							<table style="width: 100%;" border="0">
							<s:if test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
			        		<tr>
			        			<td colspan="4" style="width: 100%; vertical-align: middle;">
			        				<label class='control-label-required'>Status :</label>
			        				<label class='control-label'><s:label name="frm.appMessage" /></label>
			        			</td>
			        		</tr>
			        		</s:if>
			        	
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Name of Organization</label></td>
			        			<td colspan="3"><s:textfield name="frm.lpiName" style="width: 100%;" cssClass='form-control' placeholder='Organization' disabled="true"/>
			        			<s:hidden name="frm.lpiName"/>
			        			</td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Type of Organization</label></td>
			        			<td colspan="3" >
			        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Search Organization Type"
													list="frm.getLpiTypeLists()" 
													name="frm.lpiTypeList"
													value="frm.lpiType"  />
			        			</td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: top;"><label class='control-label'>Mandate</label></td>
			        			<td colspan="3"><s:textarea name="frm.lpiMandate" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Address</label></td>
			        			<td >
			        				<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
			        				<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
			        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Region"
													list="frm.getRegionLists()"
													name="frm.regionList"
													value="frm.region"   onchange="handleChangeR(this.value)" />
								</td>
								<td>				
									<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Province"
													list="frm.getProvinceLists()"
													name="frm.provinceList"
													value="frm.province" onchange="handleChangeP(this.value)"   />
								</td>
								<td>				
									<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Municipality"
													list="frm.getMunicipalityLists()"
													name="frm.municipalityList"
													value="frm.municipality"  />
													
			        			</td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'></label></td>
			        			<td colspan="3"><s:textfield name="frm.address" style="width: 100%;" cssClass='form-control' placeholder="Address" /></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Telephone No.</label></td>
			        			<td ><s:textfield name="frm.lpiTelephoneNum" style="width: 100%;" cssClass='form-control' /></td>
			        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email</label></td>
			        			<td ><s:textfield name="frm.lpiEmail" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Fax No.</label></td>
			        			<td ><s:textfield name="frm.lpiFaxNum" style="width: 100%;" cssClass='form-control' /></td>
			        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website</label></td>
			        			<td ><s:textfield name="frm.lpiWebsite" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
			        			<td colspan="4">&nbsp;</td>
			        		</tr>
			        		<tr>
			        			<td colspan="4">&nbsp;</td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Institute Code</label></td>
			        			<td colspan="3"><s:textfield name="frm.lpiId" style="width: 100%;" cssClass='form-control' placeholder="(Use UACS code for government offices; for NGOs used SEC Registration Number)"/></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>&nbsp;</label></td>
			        			<td colspan="3"><a href="BS/attachment/Government Organization Codes_UACS.pdf" download="UACS_CODE"><small>Download Government Organization Codes_UACS</small></a>
			        			</td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Username</label></td>
			        			<td colspan="3"><s:textfield name="frm.lpiUserName" style="width: 50%;" cssClass='form-control' placeholder="(Please Enter Username)"/></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Password</label></td>
			        			<td colspan="3"><s:password name="frm.lpiUserPass" style="width: 50%;" cssClass='form-control' placeholder="(Enter Password)"/></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Confirm Password</label></td>
			        			<td colspan="3"><s:password name="frm.lpiUserPassConfirm" style="width: 50%;" cssClass='form-control' placeholder="(Confirm Password)"/></td>
			        		</tr>
			        		
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Primary Email</label></td>
			        			<td ><s:textfield name="frm.lpiUserPrimaryEmail" style="width: 100%;" cssClass='form-control' /></td>
			        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secondary Email</label></td>
			        			<td ><s:textfield name="frm.lpiUserSecondaryEmail" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Name of Coordinator</label></td>
			        			<td colspan="3"><s:textfield name="frm.lpiCoordinatorName" style="width: 100%;" cssClass='form-control'/></td>
			        		</tr>
			        		
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Position</label></td>
			        			<td ><s:textfield name="frm.lpiCoordinatorPosition" style="width: 100%;" cssClass='form-control' /></td>
			        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Department</label></td>
			        			<td ><s:textfield name="frm.lpiCoordinatorDepartment" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		
			        		<tr>
			        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Telephone No. :</label></td>
			        			<td ><s:textfield name="frm.lpiCoordinatorTelNum" style="width: 100%;" cssClass='form-control' /></td>
			        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mobile No. :</label></td>
			        			<td ><s:textfield name="frm.lpiCoordinatorMobileNum" style="width: 100%;" cssClass='form-control' /></td>
			        		</tr>
			        		<tr>
								<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>&nbsp;</label></td>
								<td colspan="3">&nbsp;</td>
							</tr>
			        		<tr>
			        		<td>&nbsp;</td>
							<td align="center">
								<s:submit value="Update" name="frm.lpiBtnName" cssClass="btn btn-success" style="width: 100%; font-size:11px; " />
							</td>
							<td align="center">
								<s:submit value="Reset"  name="frm.lpiBtnName" cssClass="btn btn-danger" style="width: 100%; font-size:11px;"/>
							</td>
							<td align="center">
								<s:submit value="Close"  name="frm.lpiBtnName" cssClass="btn btn-primary" style="width: 100%; font-size:11px; "  />
							</td>
						</tr>
								</table>
							</s:form>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
	
<%@ include file="datePickerFooter.jsp" %>
</body>
</html>