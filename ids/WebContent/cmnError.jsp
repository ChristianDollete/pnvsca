<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>
<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<title>VMID SYSTEM</title>
	<%@ include file="cssLink.jsp" %>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<%@ include file="adMainHeaderBar.jsp"%>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h3 style="color: red;">
        Error Handler: Please Contact your VMIDS Administrator.
      </h3>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->
  </div>



<%@ include file="adMainFooter.jsp"%>
</div>
<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="BS/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="BS/dist/js/app.min.js"></script>
</body>
</html>