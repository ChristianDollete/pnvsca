<%
	com.web.UserSession user2 = (com.web.UserSession)session.getAttribute(session.getId());
	Integer CmpId2 = user2.getUserCmpCode();
	String UserName2 = user2.getUserFName();
	Integer usrProfile = user2.getUserProfile();
	Integer lpiNo = user2.getLpiNo();
	Integer vsoNo = user2.getVsoNo();
	
%>
<header class="main-header">
    <!-- Logo -->
    <a href="<s:url action="AdMain"><s:param name="frm.link" value="2"/></s:url>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="BS/dist/img/logo.png" style="height: 50px;" ></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="BS/dist/img/logo.png" style="height: 50px;"></span>	
      
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
  	  <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
       
      </a>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <%if(usrProfile==1){ %>
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope"></i>
              <span class="label label-success"><%=lpiNo%></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <%=lpiNo%> LPI Registered</li>
              <li class="footer"><a href="#">View All</a></li>
            </ul>
          </li>
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-warning"><%=vsoNo %></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <%=vsoNo %> VSO Registered</li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <%} %>
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="BS/dist/img/avatar6.png" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><%=UserName2%></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="BS/dist/img/avatar6.png" class="img-circle" alt="User Image"><p>
                  <%=UserName2%>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                	<% if(usrProfile == 2){	%>
             		<a href="<s:url action="AdProfile"><s:param name="frm.link" value="1" /></s:url>" class="btn btn-default btn-flat">LPI Profile</a>
             		<% }else if(usrProfile == 21){	%>
             		<a href="<s:url action="AdLpiPmProfile"><s:param name="frm.link" value="1" /></s:url>" class="btn btn-default btn-flat">PM Profile</a>
             		<% }else if(usrProfile == 3){	%>
             		<a href="<s:url action="AdVsoProfile"><s:param name="frm.link" value="1" /></s:url>" class="btn btn-default btn-flat">VSO Profile</a>
             		<% }else if(usrProfile == 31){	%>
             		<a href="<s:url action="AdVsoVolunteerProfile"><s:param name="frm.link" value="1" /></s:url>" class="btn btn-default btn-flat">VOLUNTEER Profile</a>
                	<%}else{%>
                	<a href="#>" class="btn btn-default btn-flat">No Profile</a>
                	<%}%>
                </div>
                <div class="pull-right">
                	<a href="<s:url action="AdMain"><s:param name="frm.link" value="1"/></s:url>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>
    </nav>
  </header>
  <%@ include file="adMainSideBar.jsp"%>