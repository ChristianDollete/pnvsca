<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>
<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>
<%@ include file="cssLink.jsp" %>

<script type="text/javascript">
	function handleChangePrj(value){
		
		   document.getElementById("LpiRequestVolunteerExtensionAssignment_frm_rva_prjct_lctn").innerHTML = "";
		   var name = value;   
		   $.ajax({
               url: "LpiRequestVolunteerExtensionAssignmentProject", data: {'frm.rva_prjct':name}, type: 'POST', async: true,
               success: function (res) {
            	   document.getElementById("LpiRequestVolunteerExtensionAssignment_frm_rva_prjct_lctn").value=res.prjLocation.toString();   
               }
           });
		 
		   this.handleChangeV(value);
	}
	
	
	function handleChangeV(value){
		document.getElementById("LpiRequestVolunteerExtensionAssignment_frm_rva_vlntr_nm_list").innerHTML = "";
		select = document.getElementById("LpiRequestVolunteerExtensionAssignment_frm_rva_vlntr_nm_list");
		var option = document.createElement('option');
		option = document.createElement( 'option' );
		option.value = '0';
		option.text = 'Select Volunteer';
		select.add( option );
		var name = value;   
		$.ajax({
			url: "LpiRequestVolunteerExtensionAssignmentVolunteer", data: {'frm.rva_prjct':name}, type: 'POST', async: true,
			success: function (res) {
				for (var i = 0; i < res.volunteers.length; i++) {
					var option2 = document.createElement('option');
					option2 = document.createElement( 'option' );
					option2.value = res.volunteers[i].toString();
					option2.text = res.volunteers[i].toString();
					select.add( option2 );
				}
			}
		});
		
		
	
		
	}
</script>


</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
		<div class="content-wrapper">
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>LPI REQUEST FOR VOLUNTEER EXTENSION ASSIGNMENT ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
							<s:form action="LpiRequestVolunteerExtensionAssignment" theme="simple" method="post" enctype="multipart/form-data">
								<s:hidden name="frm.pmId" />
								<s:hidden name="frm.rva_lpi_cd" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Request No.</label></td>
										<td colspan="3"><s:textfield name="frm.rva_id"
												style="width: 100%;" cssClass='form-control'
												placeholder='System Generated'  disabled="true"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>LPI Name</label></td>
										<td colspan="3"><s:textfield name="frm.lpi_nm"
												style="width: 100%;" cssClass='form-control'
												placeholder='' disabled="true"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>LPI Address</label></td>
										<td colspan="3"><s:textfield name="frm.lpi_addrss"
												style="width: 100%;" cssClass='form-control'
												placeholder='' disabled="true"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label 
											class='control-label'>LPI Email</label></td>
										<td>
											<s:textfield name="frm.lpi_email" style="width: 100%;" cssClass='form-control'	placeholder='' disabled="true"/>
	          							</td>
										<td style="width: 20%; vertical-align: middle;" align="center"><label 
											class='control-label'>LPI Contact No</label></td>
										<td>
											<s:textfield name="frm.lpi_cntct_no"	style="width: 100%;" cssClass='form-control' placeholder='' disabled="true"/>
	          							</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Project / Activity</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Project"
												list="frm.getRva_prjct_lists()" name="frm.rva_prjct_list"
												value="frm.rva_prjct" onchange="handleChangePrj(this.value)" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Project Location</label></td>
										<td colspan="3"><s:textfield name="frm.rva_prjct_lctn"
												style="width: 100%;" cssClass='form-control'
												placeholder='' disabled="true"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Volunteer Name</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Volunteer"
												list="frm.getRva_vlntr_nm_lists()" name="frm.rva_vlntr_nm_list"
												value="frm.rva_vlntr_nm" />
										</td>
									</tr>
									
									
									<tr>	
										<!-- <td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td> -->
										<td colspan="4">&nbsp;</td>
									</tr>
									
									
									<tr>
										<td style="width: 20%;">&nbsp;</td>
										<td style="width: 26.66%;" align="center"><s:submit value="Save"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td style="width: 26.67%;" align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td style="width: 26.67%;" align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>
								</table>
							</s:form>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
</body>
</html>