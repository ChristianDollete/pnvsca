<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>






<script type="text/javascript">
	function handleChangeR(value){
		
		   document.getElementById("LpiRequestVolunteerLineEntry_frm_provinceList").innerHTML = "";
		   select = document.getElementById("LpiRequestVolunteerLineEntry_frm_provinceList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Province';
	       select.add( option );
		   var name = value;   
		   
		   $.ajax({
               url: "LpiSignUpRegion", data: {'frm.region':name}, type: 'POST', async: true,
               success: function (res) {
                   for (var i = 0; i < res.provinces.length; i++) {
                	   
                  //     $('#LpiSignUp_frm_provinceList').append('<option value=' + res.provinces[i].toString() + '>' + res.provinces[i].toString() + '</option>');
                       var option2 = document.createElement('option');
            		   option2 = document.createElement( 'option' );
            	       option2.value = res.provinces[i].toString();
            	       option2.text = res.provinces[i].toString();
            	       select.add( option2 );
                   
                   }
               }
           });
		   
		  
	}
	
	function handleChangeP(value){
		
		   document.getElementById("LpiRequestVolunteerLineEntry_frm_municipalityList").innerHTML = "";
		   select = document.getElementById("LpiRequestVolunteerLineEntry_frm_municipalityList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Municipality';
	       select.add( option );
		   var name = value;   
		   $.ajax({
            url: "LpiSignUpMun", data: {'frm.province':name}, type: 'POST', async: true,
            success: function (res) {
                for (var i = 0; i < res.municipalities.length; i++) {
                	
                    //$('#LpiSignUp_frm_municipalityList').append('<option value=' + res.municipalities[i].toString() + '>' + res.municipalities[i].toString() + '</option>');
                	var option2 = document.createElement('option');
          		    option2 = document.createElement( 'option' );
          	        option2.value = res.municipalities[i].toString();
          	        option2.text = res.municipalities[i].toString();
          	        select.add( option2 );
                    
                }
            }
        });
		   
		  
	}
	
	
	
	function handleSelectV(value){
		
		   document.getElementById("LpiRequestVolunteerLineEntry_frm_rvl_vlntr_nmLst").innerHTML = "";
		   select = document.getElementById("LpiRequestVolunteerLineEntry_frm_rvl_vlntr_nmLst");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Volunteer';
	       select.add( option );
		   var name = value;   
		   $.ajax({
         url: "LpiRVLgetVolunteer", data: {'frm.rvl_vso_nm':name}, type: 'POST', async: true,
         success: function (res) {
             for (var i = 0; i < res.volunteers.length; i++) {
             	
                 //$('#LpiSignUp_frm_municipalityList').append('<option value=' + res.municipalities[i].toString() + '>' + res.municipalities[i].toString() + '</option>');
             	var option2 = document.createElement('option');
       		    option2 = document.createElement( 'option' );
       	        option2.value = res.volunteers[i].toString();
       	        option2.text = res.volunteers[i].toString();
       	        select.add( option2 );
                 
             }
         }
     });
		   
		  
	}
	
	
	</script>





</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="LpiRequestVolunteerLineEntry" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>LPI REQUEST FOR VOLUNTEER LINE ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
								<s:hidden name="frm.rvl_cd" />
								<s:hidden name="frm.rvl_rv_cd" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td colspan="4" style="width: 100%; vertical-align: left;">
										<s:hidden name="frm.rvl_rv_rqst_id" />
										<label class='control-label-required'>Volunteer Request No. : </label><s:property value="frm.rvl_rv_rqst_id"/></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3">
											<label><small>&nbsp;</small>
										</label>
										</td>
									</tr>
									<tr>
										<td colspan="4" style="width: 100%; vertical-align: left;">
										<s:hidden name="frm.rvl_ln_no" />
										<label class='control-label-required'>Local Assignment of Volunteer No. : </label><s:property value="frm.rvl_ln_no"/></td>
									</tr>
									
									<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        							<td colspan="4">&nbsp;</td>
	        						</tr>
	        						<tr>
	        						<td colspan="4">&nbsp;</td>
	        						</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Location</label></td>
										<td><s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Region"
												list="frm.getRegionLists()" 
												name="frm.regionList"
												value="frm.region" onchange="handleChangeR(this.value)" />
										</td>
										<td><s:select style="width: 100%;"
												cssClass='form-control' headerKey="0"
												headerValue="Select Province" 
												list="frm.getProvinceLists()"
												name="frm.provinceList" 
												value="frm.province"
												onchange="handleChangeP(this.value)" /></td>
										<td><s:select style="width: 100%;"
												cssClass='form-control' headerKey="0"
												headerValue="Select Municipality"
												list="frm.getMunicipalityLists()"
												name="frm.municipalityList" value="frm.municipality" /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'></label></td>
										<td colspan="3"><s:textfield name="frm.address"
												style="width: 100%;" cssClass='form-control'
												placeholder="Address" /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Preferred Date of Arrival</label></td>
										<td colspan="3">
											<s:textfield name="frm.rvl_prfrd_dt_arrvl"	style="width: 100%;" cssClass='form-control pull-right' id="datepicker2"	placeholder='Date Arrival (MM/DD/YYYY)' />
	          							</td>
									</tr>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Objective of Volunteer Assignment.</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_objctv"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Expected Output</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_output"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Volunteer Activities</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_actvts"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Implementation Period</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_implmttn_prd"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Funding Source</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_fndng_src"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Specialization</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_fld_spcfctn"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Experience and Training</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_exprnc_trnnng"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Education</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Education"
												list="frm.getRvl_edctnLsts()" name="frm.rvl_edctnLst"
												value="frm.rvl_edctn" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>VSO</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select VSO"
												list="frm.getRvl_vso_nmLsts()" 
												name="frm.rvl_vso_nmLst"
												value="frm.rvl_vso_nm" 
												onchange="handleSelectV(this.value)"
												/>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Date of Arrival</label></td>
										<td colspan="3">
											<s:textfield name="frm.rvl_dt_arrvl"	style="width: 100%;" cssClass='form-control pull-right' id="datepicker"	placeholder='Date Arrival (MM/DD/YYYY)' />
	          							</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3">
											<label><small>&nbsp;</small>
										</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3" style="width: 100%; vertical-align: middle;">
											<s:checkbox name="frm.rvl_emrgncy" fieldValue="true" />&nbsp;&nbsp;&nbsp;<label class='control-label'>Emergency Medical Assistance</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3" style="width: 100%; vertical-align: middle;">
											<s:checkbox name="frm.rvl_trnsprt" fieldValue="true" />&nbsp;&nbsp;&nbsp;<label class='control-label'>Transport for Project Travel</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3" style="width: 100%; vertical-align: middle;">
											<s:checkbox name="frm.rvl_fclty" fieldValue="true" />&nbsp;&nbsp;&nbsp;<label class='control-label'>Facilities, Equipment Supplies And Materials</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3" style="width: 100%; vertical-align: middle;">
											<s:checkbox name="frm.rvl_housing" fieldValue="true" />&nbsp;&nbsp;&nbsp;<label class='control-label'>Housing</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;">
											<label><small>&nbsp;</small></label>
										</td>
										<td colspan="3" style="width: 100%; vertical-align: middle;">
											<s:checkbox name="frm.rvl_othrs" fieldValue="true" />&nbsp;&nbsp;&nbsp;<label class='control-label'>Others, Please Specify</label>
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3"><s:textfield name="frm.rvl_othrs_spcfy"
												style="width: 100%;" cssClass='form-control'
												placeholder='Please Specify' /></td>
									</tr>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									
									<% if(usrProfile == 3){ %>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Volunteer</label></td>
										<td colspan="3">
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Volunteer"
												list="frm.getRvl_vlntr_nmLsts()" name="frm.rvl_vlntr_nmLst"
												value="frm.rvl_vlntr_nm" />
										</td>
									</tr>
									<%} %>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td style="width: 25%;">&nbsp;</td>
										<td style="width: 25%;" align="center"><s:submit value="Save"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td style="width: 25%;" align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td style="width: 25%;" align="center">
																			
											<a href="<s:url action="LpiRequestVolunteerEntry" escapeAmp="false">
														<s:param name="frm.btnName" value="Return"/>
														<s:param name="frm.rv_id" value="frm.rvl_rv_rqst_id"/>
													</s:url>" class="btn btn btn-primary" style="width: 100%; font-size:11px; role="button">
													Return
											</a>
											
											<%-- 
											<s:submit value="Return"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " 
												action="LpiRequestVolunteerEntry"
												<s:param name="frm.rv_id" value="frm.rvl_rv_rqst_id"/>
												/> --%>
										</td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									</table>
									
						</form>
					</div>
				</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
	
<%@ include file="datePickerFooter.jsp" %>
	
</body>
</html>