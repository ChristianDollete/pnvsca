<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
		<div class="content-wrapper">
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>PNVSCA ACCESS</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
							<s:form action="AdAdmin" theme="simple" method="post" enctype="multipart/form-data">
								
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>UserName</label></td>
										<td colspan="3"><s:textfield name="frm.adUserName"
												style="width: 100%;" cssClass='form-control'
												placeholder='Log-in User Name' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>User Description</label></td>
										<td colspan="3"><s:textfield name="frm.adUserDesc"
												style="width: 100%;" cssClass='form-control'
												placeholder='Description' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>User Password</label></td>
										<td colspan="3"><s:password name="frm.adUserPassword"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Confirm Password</label></td>
										<td colspan="3"><s:password name="frm.adUserConfirmPassword"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        							<td colspan="4" align="center">
	        								&nbsp;
	        							</td>
	        						</tr>
									<tr style="border-bottom-color: black; border-bottom: 1px;  border-bottom-style: solid;">
	        							<td colspan="4" align="center" valign="middle">
	        								<label class='control-label-required'>Email Account</label>
	        							</td>
	        						</tr>
	        						<tr>
	        							<td colspan="4" align="center">
	        								&nbsp;
	        							</td>
	        						</tr>
	        						<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Email Address</label></td>
										<td colspan="3"><s:textfield name="frm.adEmailAddress"
												style="width: 100%;" cssClass='form-control'
												placeholder='Description' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Email Password</label></td>
										<td colspan="3"><s:password name="frm.adEmailPassword"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Email Confirm Password</label></td>
										<td colspan="3"><s:password name="frm.adEmailConfirmPassword"
												style="width: 100%;" cssClass='form-control'
												placeholder='' /></td>
									</tr>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>

									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Save"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>

								</table>
							</s:form>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
	
<%@ include file="datePickerFooter.jsp" %>
</body>
</html>