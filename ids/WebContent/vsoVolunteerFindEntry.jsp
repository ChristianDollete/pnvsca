<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="VsoVolunteerFindEntry" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>VOLUNTEER FIND ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
								<s:hidden name="frm.pmId" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Volunteer Id</label></td>
										<td colspan="3"><s:textfield name="frm.vlntrId"
												style="width: 100%;" cssClass='form-control'
												placeholder='Volunteer ID' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Name</label></td>
										<td>
											<s:textfield name="frm.vlntrFname"
												style="width: 100%;" cssClass='form-control'
												placeholder='First Name' />
										</td>
										<td>
											<s:textfield name="frm.vlntrMname"
												style="width: 100%;" cssClass='form-control'
												placeholder='Middle Name' />
										</td>
										<td>
											<s:textfield name="frm.vlntrLname"
												style="width: 100%;" cssClass='form-control'
												placeholder='Last Name' />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Visa Expiration</label></td>
										<td>
										    <s:textfield name="frm.vlntrVisaExpiration" style="width: 100%;" cssClass='form-control'	/>
	          							</td>
										<td>
											&nbsp;
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Project Assigned
												</label></td>
										<td colspan="3">
											<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
											<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
											<s:select style="width: 100%;" cssClass='form-control'
												headerKey="0" headerValue="Select Project"
												list="frm.getVlntrPrjLsts()" name="frm.vlntrPrjLst"
												value="frm.vlntrPrj" />
										</td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									
									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Search"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									</table>
									
						</form>
					</div>
				</div>
				
				
				<div class="box">
		            <div class="box-header"  style="background-color: #337ab7; color: white;">
		              <h3 class="box-title" >Volunteer List</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example2" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>Volunteer Id</th>
		                  <th>Volunteer Name</th>
		                  <th>Visa Expiration</th>
		                  <th>Project Assigned</th>
		                  <th>Duration From</th>
		                  <th>Duration To</th>
		                  <th>View</th>
		                </tr>
		                </thead>
		                	<tbody>
		                		<s:iterator value="frm.getVlntrList()" var="vl">
		                		<tr>
		                		<td>
									<s:hidden name="#vl.vlntrCd"/>
									<s:property value="#vl.vlntrId"/>
								</td>
								<td>
									<s:property value="#vl.vlntrName"/>
								</td>
		                  		<td>
		                  			<s:property value="#vl.vlntrVisaExpiration"/>
		                  		</td>
		                  		<td><s:property value="#vl.vlntrPrjct"/></td>
		                  		<td><s:property value="#vl.vlntrPrjDateFrom"/></td>
		                  		<td><s:property value="#vl.vlntrPrjDateTo"/></td>
		                  		<td>
		                  			<a style="text-decoration: none;" href="<s:url action="VsoVolunteerEntry" escapeAmp="false">
									<s:param name="frm.link" value="66"/>
									<s:param name="frm.vlntrCd" value="#vl.vlntrCd"/>
									</s:url>">
										OPEN
									</a> 
		                  		</td>
		                  		</tr>
		                  		</s:iterator>
		                  		
							</tbody>
		                <tfoot>
		                <tr>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                </tr>
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
	         	</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>

<%@ include file="datePickerFooter.jsp" %>
</body>
</html>