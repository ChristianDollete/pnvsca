<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="LpiProjectManagerFindEntry" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>PROJECT MANAGER FIND ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
								<s:hidden name="frm.pmId" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr>
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Full Name</label></td>
										<td colspan="3"><s:textfield name="frm.pmName"
												style="width: 100%;" cssClass='form-control'
												placeholder='Project Manager Name' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Position</label></td>
										<td colspan="3"><s:textfield name="frm.pmPosition"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Email</label></td>
										<td colspan="3"><s:textfield name="frm.pmEmail"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Search"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
									</table>
									
						</form>
					</div>
				</div>
				
				
				<div class="box">
		            <div class="box-header" style="background-color: #337ab7; color: white;" >
		              <h3 class="box-title">Project Manager List</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example2" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>Project Manager Id</th>
		                  <th>Project Manager Name</th>
		                  <th>Position</th>
		                  <th>Email</th>
		                  <th>View</th>
		                </tr>
		                </thead>
		                	<tbody>
		                		<s:iterator value="frm.getPmList()" var="pmL">
		                		<tr>
		                		<td>
									<s:hidden name="#pmL.pmlId"/>
									<s:property value="#pmL.pmlId"/>
								</td>
								<td>
									<s:property value="#pmL.pmlName"/>
								</td>
		                  		<td>
		                  			<s:hidden name="#pmL.pmlCd"/>
									<s:property value="#pmL.pmlPosition"/>
		                  		</td>
		                  		<td><s:property value="#pmL.pmlEmail"/></td>
		                  		<td>
		                  			<a style="text-decoration: none;" href="<s:url action="LpiProjectManagerEntry" escapeAmp="false">
									<s:param name="frm.link" value="22"/>
									<s:param name="frm.pmId" value="#pmL.pmlId"/>
									</s:url>">
										OPEN
									</a> 
		                  		</td>
		                  		</tr>
		                  		</s:iterator>
		                  		
							</tbody>
		                <tfoot>
		                <tr>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                </tr>
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
	         	</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>

<script src="BS/dist/js/demo.js"></script>
<script src="BS/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="BS/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="BS/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="BS/plugins/fastclick/fastclick.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
	
	
</body>
</html>