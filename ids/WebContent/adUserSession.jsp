<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>



</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
			
		<s:form action="AdUserSession" theme="simple" method="post" enctype="multipart/form-data">
						
		<div class="content-wrapper">
			
			<div class='container' style="width: 100%;">
			
				<div class="box">
		            <div class="box-header"  style="background-color: #337ab7; color: white;">
		              <h3 class="box-title">User Session List</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example2" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>User Name</th>
		                  <th>User Type</th>
		                  <th>Organization Name</th>
		                  <th>Session Id</th>
		                  <th>Force Log-out</th>
		                </tr>
		                </thead>
		                	<tbody>
		                		<s:iterator value="frm.getUserSessions()" var="session">
		                		<tr>
		                		<td>
									<s:property value="#session.userName"/>
								</td>
								<td>
									<s:property value="#session.userType"/>
								</td>
		                  		<td>
		                  			<s:property value="#session.userOrgName"/>
		                  		</td>
		                  		<td><s:property value="#session.userSessionId"/></td>
		                  		
		                  		<td>
		                  			<a href="<s:url action="AdUserSession" escapeAmp="false">
		                  			<s:param name="frm.link" value="111"/>
									<s:param name="frm.btnName" value="Logout"/>
									<s:param name="frm.userSessionId" value="#session.userSessionId"/>
									</s:url>" class="btn btn btn-primary" style="width: 100%; font-size:11px; role="button">
										Log-out
									</a> 
		                  		</td>
		                  		</tr>
		                  		</s:iterator>
		                  		
							</tbody>
		                <tfoot>
		                <tr>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                  <th>&nbsp;</th>
		                </tr>
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
	         	</div>
			</div>
		</div>
		</s:form>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>

<%@ include file="datePickerFooter.jsp" %>
</body>
</html>