<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>LPI Registration Form</title>
	<link rel="stylesheet" href="BS/cmp/css/style.css">
	<link href='BS/cmp/bootstrap/bootstrap.css' rel='stylesheet' type='text/css'>
	<link href='BS/cmp/bootstrap/datePickerBootstrap.css' rel='stylesheet' type='text/css'>
	<link href='BS/cmp/bootstrap/switchBootstrap.css' rel='stylesheet' type='text/css'>
	<script src='BS/cmp/bootstrap/Jquery.js' type='text/javascript'></script>
	<script src='BS/cmp/bootstrap/bootstrapMinJs.js' type='text/javascript'></script>
	<script src='BS/cmp/bootstrap/datePickerBootstrapJs.js' type='text/javascript'></script>
	<script type="text/javascript">
	function handleChangeR(value){
		
		   document.getElementById("VsoSignUp_frm_provinceList").innerHTML = "";
		   select = document.getElementById("VsoSignUp_frm_provinceList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Province';
	       select.add( option );
		   var name = value;   
		   
		   $.ajax({
               url: "VsoSignUpRegion", data: {'frm.region':name}, type: 'POST', async: true,
               success: function (res) {
                   for (var i = 0; i < res.provinces.length; i++) {
                	   
                  //     $('#LpiSignUp_frm_provinceList').append('<option value=' + res.provinces[i].toString() + '>' + res.provinces[i].toString() + '</option>');
                       var option2 = document.createElement('option');
            		   option2 = document.createElement( 'option' );
            	       option2.value = res.provinces[i].toString();
            	       option2.text = res.provinces[i].toString();
            	       select.add( option2 );
                   
                   }
               }
           });
		   
		  
	}
	
	function handleChangeP(value){
		
		   document.getElementById("VsoSignUp_frm_municipalityList").innerHTML = "";
		   select = document.getElementById("VsoSignUp_frm_municipalityList");
		   var option = document.createElement('option');
		   option = document.createElement( 'option' );
	       option.value = '0';
	       option.text = 'Select Municipality';
	       select.add( option );
		   var name = value;   
		   $.ajax({
            url: "VsoSignUpMun", data: {'frm.province':name}, type: 'POST', async: true,
            success: function (res) {
                for (var i = 0; i < res.municipalities.length; i++) {
                	
                    //$('#LpiSignUp_frm_municipalityList').append('<option value=' + res.municipalities[i].toString() + '>' + res.municipalities[i].toString() + '</option>');
                	var option2 = document.createElement('option');
          		    option2 = document.createElement( 'option' );
          	        option2.value = res.municipalities[i].toString();
          	        option2.text = res.municipalities[i].toString();
          	        select.add( option2 );
                    
                }
            }
        });
		   
		  
	}
	</script>
	
</head>		


<body>
<s:form action="VsoSignUp" theme="simple" method="post" enctype="multipart/form-data">
<s:hidden name="frm.vsoId"/>
	<div class='container'>
	    <div class='panel panel-primary dialog-panel'>
	      <div class='panel-heading'>
	        <h5>LOCAL PARTNER INSTITUTION (LPI) REGISTRATION</h5>
	      </div>
	      <div class='panel-body'>
	        <form class='form-horizontal' role='form'>
	        	<table style="width: 100%;" border="0">
		        	<s:if test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
		        	<tr>
		        		<td colspan="4" style="width: 100%; vertical-align: middle;">
		        			<label class='control-label-required'>Status :</label>
		        			<label class='control-label'><s:label name="frm.appMessage" /></label>
		        		</td>
		        	</tr>
		        	</s:if>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Organization :</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoName" style="width: 100%;" cssClass='form-control' placeholder='Organization'/></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Type of Organization :</label></td>
	        			<td colspan="3" >
	        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Search Organization Type"
											list="frm.getVsoOrgTypeLists()"
											name="frm.vsoOrgTypeLists"
											value="frm.vsoOrgType"  />
	        			</td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Head of Organization :</label></td>
	        			<td colspan="1"><s:textfield name="frm.vsoOrgHead" style="width: 100%;" cssClass='form-control' placeholder='Head'/></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Position :</label></td>
	        			<td colspan="1"><s:textfield name="frm.vsoOrgHeadPosition" style="width: 100%;" cssClass='form-control' placeholder='Position'/></td>
	        		</tr>
	        		
	        		<tr>
	        			
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Local Address :</label></td>
	        			<td >
	        				<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Region"
											list="frm.getRegionLists()"
											name="frm.regionList"
											value="frm.region"   onchange="handleChangeR(this.value)" />
						</td>
						<td>				
							<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Province"
											list="frm.getProvinceLists()"
											name="frm.provinceList"
											value="frm.province" onchange="handleChangeP(this.value)"   />
						</td>
						<td>				
							<s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Municipality"
											list="frm.getMunicipalityLists()"
											name="frm.municipalityList"
											value="frm.municipality"  />
											
	        			</td>
	        		
	        			
	        		</tr>
	        		<tr>
	        			<td colspan="1"></td>
	        			<td colspan="3"><s:textfield name="frm.address" style="width: 100%;" cssClass='form-control' placeholder="Address" /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Telephone No. :</label></td>
	        			<td ><s:textfield name="frm.vsoOrgTelephoneNumber" style="width: 100%;" cssClass='form-control' /></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email :</label></td>
	        			<td ><s:textfield name="frm.vsoOrgEmail" style="width: 100%;" cssClass='form-control' /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Fax No. :</label></td>
	        			<td ><s:textfield name="frm.vsoOrgFaxNumber" style="width: 100%;" cssClass='form-control' /></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Website :</label></td>
	        			<td ><s:textfield name="frm.vsoWebsite" style="width: 100%;" cssClass='form-control' /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>SEC Registration No.  :</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoSecRegistrationNumber" style="width: 100%;" cssClass='form-control' placeholder='SEC Registration No. '/></td>
	        		</tr>
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Head Office Address :</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoHeadOfficeAddress" style="width: 100%;" cssClass='form-control' placeholder="(For international organizations only)" /></td>
	        		</tr>
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class=''></label></td>
	        			<td ><label class=''>&nbsp;</label></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Country</label></td>
	        			<td ><s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Country"
											list="frm.getCountryLists()"
											name="frm.countryList"
											value="frm.country"  /></td>
	        		
					</tr>
					<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Date Established</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoDateEstablished" style="width: 100%;" cssClass='form-control' placeholder="Date Established" /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Vision</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoVision" style="width: 100%;" cssClass='form-control' placeholder="Vision " /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Mission</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoMission" style="width: 100%;" cssClass='form-control' placeholder="Mission  " /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Goals</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoGoals" style="width: 100%;" cssClass='form-control' placeholder="Goals   " /></td>
	        		</tr>	
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Annual Budget</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoAnnualBudget" style="width: 100%;" cssClass='form-control' placeholder="Annual Budget   " /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Budget Source   </label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoBudgetSource" style="width: 100%;" cssClass='form-control' placeholder="Budget Source  " /></td>
	        		</tr>		
	        		
	        		<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        			<td colspan="4">&nbsp;</td>
	        		</tr>
	        		<tr>
	        			<td colspan="4">&nbsp;</td>
	        		</tr>
	        		<tr>
	        			
	        			<td colspan="2" style="width: 50%; vertical-align: middle;" ><label class='control-label'>Recruitment, Training and Deployment of volunteers</label></td>
	        			<td colspan="2"><s:select  style="width: 100%;" cssClass='form-control' headerKey="0" headerValue="Select Deployment of volunteers"
											list="frm.getVsoRecruitmentLists()"
											name="frm.vsoRecruitmentList"
											value="frm.vsoRecruitment"  /></td>
	        		
					</tr>
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoEngagement" fieldValue="false" /></td>
	        			<td colspan="3"> <label class='control-label'>Engagement of volunteers in implementing development programs or projects   </label> </td >
	        		</tr>		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoCapabilityBuilding" fieldValue="false" /></td>
	        			<td colspan="3"> <label class='control-label'>Capability building/ training for volunteers and volunteer managers   </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoPromotion" fieldValue="false" /></td>
	        			<td colspan="3"> <label class='control-label'>Promotion and advocacy for volunteerism   </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoNetworking" fieldValue="false" /></td>
	        			<td colspan="3"> <label class='control-label'>Networking of volunteers   </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoOthers1" fieldValue="false" /></td>
	        			<td colspan="3"> <label class='control-label'>Others. Please Specify   </label> </td >
	        		</tr>
	        		
	        		
	        		<tr>
	        			<td colspan="4">&nbsp;</td>
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="4"> <label class='control-label'>The organization provides the following to its volunteers: </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoLivingAllowance" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Living allowance  </label> </td >
	        			<td colspan="1" style="text-align:center;width: 25%; vertical-align: middle;"><s:checkbox name="frm.vsoTravel" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Travel allowance  </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoMeal" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Meal allowance  </label> </td >
	        			<td colspan="1" style="text-align:center;width: 25%; vertical-align: middle;"><s:checkbox name="frm.vsoHousing" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Housing or Accomodation   </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoInsurance" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Insurance   </label> </td >
	        			<td colspan="1" style="text-align:center;width: 25%; vertical-align: middle;"><s:checkbox name="frm.vsoTraining" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Training and orientation    </label> </td >
	        		</tr>
	        		
	        		<tr>
	        			<td colspan="1" style="text-align:center;width: 20%; vertical-align: middle;"><s:checkbox name="frm.vsoRecognition" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Recognition or award   </label> </td >
	        			<td colspan="1" style="text-align:center;width: 25%; vertical-align: middle;"><s:checkbox name="frm.vsoOthers2" fieldValue="false" /></td>
	        			<td colspan="1"> <label class='control-label'>Others     </label> </td >
	        		</tr>
	        			
	        		<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        			<td colspan="4">&nbsp;</td>
	        		</tr>
	        		<tr>
	        			<td colspan="4">&nbsp;</td>
	        		</tr>
	        		
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Username :</label></td>
	        			<td colspan="3"><s:textfield name="frm.vsoUserName" style="width: 50%;" cssClass='form-control' placeholder="(Please Enter Username)"/></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Password :</label></td>
	        			<td colspan="3"><s:password name="frm.vsoUserPassword" style="width: 50%;" cssClass='form-control' placeholder="(Enter Password)"/></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Confirm Password :</label></td>
	        			<td colspan="3"><s:password name="frm.vsouserPasswordConfirm" style="width: 50%;" cssClass='form-control' placeholder="(Confirm Password)"/></td>
	        		</tr>
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Primary Email :</label></td>
	        			<td ><s:textfield name="frm.vsoUserPrimaryEmail" style="width: 100%;" cssClass='form-control' /></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label-required'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secondary Email :</label></td>
	        			<td ><s:textfield name="frm.vsoUserSecondaryEmail" style="width: 100%;" cssClass='form-control' /></td>
	        		</tr>
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label-required'>Name of Coordinator :</label></td>
	        			<td colspan="3"><s:password name="frm.vsoCoordinatorName" style="width: 100%;" cssClass='form-control'/></td>
	        		</tr>
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Position :</label></td>
	        			<td ><s:textfield name="frm.vsoCoordinatorPosition" style="width: 100%;" cssClass='form-control' /></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Department :</label></td>
	        			<td ><s:textfield name="frm.vsoCoordinatorDepartment" style="width: 100%;" cssClass='form-control' /></td>
	        		</tr>
	        		
	        		<tr>
	        			<td style="width: 20%; vertical-align: middle;"><label class='control-label'>Telephone No. :</label></td>
	        			<td ><s:textfield name="frm.vsoCoordinatorTelephone" style="width: 100%;" cssClass='form-control' /></td>
	        			<td style="width: 25%; vertical-align: middle;" ><label class='control-label'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mobile No. :</label></td>
	        			<td ><s:textfield name="frm.vsoCoordinatorMobileNumber" style="width: 100%;" cssClass='form-control' /></td>
	        		</tr>
	        		<tr>
	        		<td>&nbsp;</td>
	        	
					<td align="center">
						<s:submit name="frm.vsoBtnName"  value="Sign-Up" cssClass="btn btn-success" style="width: 100%; font-size:11px; " />
					</td>
					<td align="center">
						<s:submit name="frm.vsoBtnName"  value="Reset"   cssClass="btn btn-danger" style="width: 100%; font-size:11px;"  />
					</td>
					<td align="center">
						<s:submit name="frm.vsoBtnName"  value="LogIn"   cssClass="btn btn-primary" style="width: 100%; font-size:11px; "  />
					</td>
					
				</tr>
	        	</table>
	        </form>
	      </div>
	     </div>
	</div>

</s:form>

  
</body>
  
      <script src="BS/cmp/js/index.js"></script>


</body>
</html>