<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "0");
%>
<%@ page import="com.web.UserSession"%>

<%
	UserSession user = (UserSession)session.getAttribute(session.getId());
	
	if(user==null){
		%>
		<jsp:forward page="adLogon.jsp"/>
		<% 
	}
	Integer CmpId = user.getUserCmpCode();
	String UserName = user.getUserFName();
	
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>VMID SYSTEM</title>

<%@ include file="cssLink.jsp" %>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="adMainHeaderBar.jsp"%>
		<div class="content-wrapper">
			<div class='container' style="width: 100%;">
				<div class='panel panel-primary dialog-panel'>
					<div class='panel-heading'>
						<h5>PROJECT MANAGER ENTRY</h5>
					</div>
					<div class='panel-body'>
						<form class='form-horizontal' role='form'>
							<s:form action="LpiProjectManagerEntry" theme="simple" method="post" enctype="multipart/form-data">
								<s:hidden name="frm.pmId" />
								<table style="width: 100%;" border="0">
									<s:if
										test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
										<tr align="left">
											<td colspan="4" style="width: 100%; vertical-align: middle;">
												<label class='control-label-required'>Status :</label> <label
												class='control-label'><s:label name="frm.appMessage" /></label>
											</td>
										</tr>
									</s:if>
									
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Full Name</label></td>
										<td colspan="3"><s:textfield name="frm.pmName"
												style="width: 100%;" cssClass='form-control'
												placeholder='Project Manager Name' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Sex
												</label></td>
										<td colspan="3">
											<%-- <s:select headerKey="-1" list="#{'1':'Jan', '2':'Feb', '3':'Mar', '4':'Apr'}" name="yourMonth" value="2" /> --%>
											<%-- <s:select list="frm.lpiTypeLists" name="frm.lpiTypeList" value="frm.lpiType" />  --%>
											<s:select style="width: 25%;" cssClass='form-control'
												headerKey="0" headerValue="MALE / FEMALE"
												list="frm.getGenderLists()" name="frm.genderList"
												value="frm.gender" />
										</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label'>Position</label></td>
										<td colspan="3"><s:textfield name="frm.pmPosition"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Email</label></td>
										<td colspan="3"><s:textfield name="frm.pmEmail"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Telephone No.</label></td>
										<td colspan="3"><s:textfield name="frm.pmTelNo"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Mobile No.</label></td>
										<td colspan="3"><s:textfield name="frm.pmMobNo"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Fax No.</label></td>
										<td colspan="3"><s:textfield name="frm.pmFaxNo"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr style="border-bottom-color: black; border-bottom: 1px; border-bottom-style: solid;">
	        							<td colspan="4">&nbsp;</td>
	        						</tr>
	        						<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>User Name</label></td>
										<td colspan="3"><s:textfield name="frm.pmUserName"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Password</label></td>
										<td colspan="3"><s:password name="frm.pmUserPass"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>Confirm Password</label></td>
										<td colspan="3"><s:password name="frm.pmUserConfirmPass"
												style="width: 50%;" cssClass='form-control' /></td>
									</tr>
									<tr>
										<td style="width: 20%; vertical-align: middle;"><label
											class='control-label-required'>&nbsp;</label></td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td align="center"><s:submit value="Save"
												name="frm.btnName" cssClass="btn btn-success"
												style="width: 100%; font-size:11px; " /></td>
										<td align="center"><s:submit value="Reset"
												name="frm.btnName" cssClass="btn btn-danger"
												style="width: 100%; font-size:11px;" /></td>
										<td align="center"><s:submit value="Close"
												name="frm.btnName" cssClass="btn btn-primary"
												style="width: 100%; font-size:11px; " /></td>
									</tr>


								</table>
							</s:form>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="adMainFooter.jsp"%>
	</div>
	<script src="BS/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="BS/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="BS/dist/js/app.min.js"></script>
</body>
</html>