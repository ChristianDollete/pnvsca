<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
   response.setHeader("Cache-Control", "no-cache");
   response.setHeader("Pragma", "no-cache");
   response.setHeader("Expires", "0");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>VMID SYSTEM</title>
  
	<link rel="stylesheet" type="text/css" href="./BS/login/css/css.css">
	<link rel="stylesheet" type="text/css" href="./BS/login/css/css2.css">
 	<link rel="stylesheet" type="text/css" href="./BS/login/css/style.css">
	<script src="./BS/login/css/javaScript.js"></script>



    
</head>
<body>

  <div class="body"></div>
		<div class="grad"></div>
		<div class="header" align="center">
			<div align="center">VOLUNTEER MANAGEMENT INFORMATION AND DEPLOYMENT SYSTEM</div>
		</div>
		<br>
		<div class="login">
				<s:form action="AdLogon" theme="simple" method="post" enctype="multipart/form-data">
				
					<table style="width: 100%;" border="0">
						<tr align="center" valign="middle">
							<td colspan="2" style="background-color: #3c8dbc; color: white;  background-clip: padding-box; border-radius: 5px; border: 1px solid #3c8dbc;">
								<label><br>Sign in with your PNVSCA Account<br><br></label>
							</td>
						</tr>
						<tr>
							<td colspan="2"><label>&nbsp;</label></td>
							
						<tr>
						<s:if test="{frm.appMessage != null & !frm.appMessage.equals(\"\")}">
		        		<tr>
		        			<td valign="top"><label style="color: red;" >Status :</label></td>
		        			<td  valign="top" style="width: 100%; vertical-align: middle;">
		        				<label class='control-label'><s:label name="frm.appMessage" /></label>
		        			</td>
		        		</tr>
		        		</s:if>
					
						<tr>
							<td valign="middle"><label>Institute ID</label></td>
							<td valign="middle"><s:textfield name="frm.cmpShortName"/></td>
						</tr>
						<tr>
							<td valign="middle"><label>User ID</label></td>
							<td valign="middle"><s:textfield name="frm.userName"/></td>
						</tr>
						<tr>
							<td valign="middle"><label>Password</label></td>
							<td valign="middle"><s:password name="frm.userPassword"/></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><s:submit value="Login" style="width: 100%;"  /></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr height="50%">
							<td colspan="2" style="border-bottom:2px; border-bottom-style: solid; border-bottom-color: white;"></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><label style="font-size: medium;">No Account Yet?</label></td>
						</tr>
						<tr>
							<td colspan="2" style="border-bottom:2px; border-bottom-style: solid; border-bottom-color: white;"></td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<a style="font-size: 16px; font-family: sans-serif; color: white; text-decoration: none;" href="<s:url action="LpiSignUp"><s:param name="frm.link" value="1"/></s:url>" onclick="update();">
									<small>Sign up for Local Partner Institution (LPI) account</small>
								</a> 
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<a style="font-size: 16px; font-family: sans-serif; color: white; text-decoration: none;" href="<s:url action="VsoSignUp"><s:param name="frm.link" value="3"/></s:url>">
									<small>Sign up for Volunteer Service Organization (VSO) account</small>
								</a> 
							</td>
						</tr>
						<%-- <tr>
							<td colspan="2" align="center">
								<a style="font-size: 16px; font-family: sans-serif; color: white; text-decoration: none;" href="<s:url action="AnnexA"></s:url>">
									<small>PRINT</small>
								</a> 
							</td>
						<tr> --%>
						
					</table>
				</s:form>
		</div>
		
	<script src="./BS/login/css/jquery.js"></script>
</body>
</html>