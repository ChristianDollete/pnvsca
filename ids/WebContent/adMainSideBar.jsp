
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><h5>MAIN NAVIGATION</h5></li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>LPI DASHBOARD</a></li>
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>VSO DASHBOARD</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>PROJECT MANAGER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="LpiProjectManagerFindEntry"><s:param name="frm.link" value="1"/></s:url>">
            				<i class="fa fa-circle-o"></i>FIND / VIEW PM</a></li>
            <li><a href="<s:url action="LpiProjectManagerEntry"><s:param name="frm.link" value="2"/></s:url>">
            				<i class="fa fa-circle-o"></i>ADD NEW PM</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>PROJECT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="LpiProjectFindEntry"><s:param name="frm.link" value="3"/></s:url>">
            				<i class="fa fa-circle-o"></i>FIND / VIEW PROJECT</a></li>
            <li><a href="<s:url action="LpiProjectEntry"><s:param name="frm.link" value="4"/></s:url>">
            				<i class="fa fa-circle-o"></i>ADD NEW PROJECT</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa  fa-user-plus"></i> <span>VOLUNTEER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="VsoVolunteerFindEntry"><s:param name="frm.link" value="5"/></s:url>">
            	<i class="fa fa-circle-o"></i>FIND / VIEW VOLUNTEER</a></li>
            <li><a href="<s:url action="VsoVolunteerEntry"><s:param name="frm.link" value="6"/></s:url>">
            	<i class="fa fa-circle-o"></i>ADD NEW VOLUNTEER</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>VOLUNTEER REQUEST</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="LpiRequestVolunteerEntry"><s:param name="frm.link" value="7"/></s:url>">
            	<i class="fa fa-circle-o"></i>ADD NEW REQUEST</a></li>
            <li><a href="<s:url action="LpiRequestVolunteerFindEntry"><s:param name="frm.link" value="8"/></s:url>">
            	<i class="fa fa-circle-o"></i>VIEW/EDIT REQUEST</a></li>
            <%-- <li><a href="<s:url action="LpiRequestVolunteerFindEntry"><s:param name="frm.link" value="20"/></s:url>">
            	<i class="fa fa-circle-o"></i>APPROVE REQUEST</a></li> --%>
          </ul>

 		<li class="treeview">
          <a href="#">
            <i class="fa fa-th-large"></i> <span>VOLUNTEER EXTENSION</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="LpiRequestVolunteerExtensionAssignment"><s:param name="frm.link" value="9"/></s:url>">
            	<i class="fa fa-circle-o"></i>ADD NEW EXT. ASSIGNMENT</a></li>
           	 <li><a href="<s:url action="LpiRequestVolunteerExtensionAssignment"><s:param name="frm.link" value="21"/></s:url>">
            	<i class="fa fa-circle-o"></i>FIND EXT. ASSIGNMENT</a></li>
             <li><a href="<s:url action="LpiRequestVolunteerExtensionAssignment"><s:param name="frm.link" value="22"/></s:url>">
            	<i class="fa fa-circle-o"></i>APPROVE REQUESTT</a></li>
            
          </ul>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>VOLUNTEER EXT. ASSISTANCE</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<li><a href="<s:url action="LpiRequestVolunteerExtensionAssignment"><s:param name="frm.link" value="23"/></s:url>">
            	<i class="fa fa-circle-o"></i>VOLUNTEER EXT. ASSISTANCE</a></li>
		  </ul>

        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear (alias)"></i> <span>ADMIN PANEL</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<s:url action="AdAdmin"><s:param name="frm.link" value="10"/></s:url>">
            	<i class="fa fa-key"></i>AUTHENTICATION</a></li>
            <li><a href="<s:url action="AdUserSession"><s:param name="frm.link" value="11"/></s:url>">
            	<i class="fa fa-users"></i>USER SESSION</a></li>
            <li><a href="<s:url action="AdEmail"><s:param name="frm.link" value="12"/></s:url>">
            	<i class="fa fa-envelope-o"></i>EMAIL</a></li>
            
            
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa   fa-files-o"></i> <span>DOCUMENT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="<s:url action="AnnexA"><s:param name="frm.link" value="13"/></s:url>">
            	<i class="fa fa-file-text-o"></i>ANNEX A Report</a></li> 
           
            <li><a href="<s:url action="Annex31"><s:param name="frm.link" value="14"/></s:url>">
            	<i class="fa fa-file-text-o"></i>ANNEX 3.1 Report</a></li> 
            <li><a href="<s:url action="Annex447"><s:param name="frm.link" value="15"/></s:url>">
            	<i class="fa fa-file-text-o"></i>ANNEX 4.4.7 / FORM 2-A</a></li> 
          </ul>
        </li>
       
       </ul>
    </section>
    <!-- /.sidebar -->
  </aside>